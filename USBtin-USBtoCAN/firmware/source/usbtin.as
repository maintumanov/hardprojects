opt subtitle "Microchip Technology Omniscient Code Generator (PRO mode) build 59893"

opt pagewidth 120

	opt pm

	processor	18F2550
porta	equ	0F80h
portb	equ	0F81h
portc	equ	0F82h
portd	equ	0F83h
porte	equ	0F84h
lata	equ	0F89h
latb	equ	0F8Ah
latc	equ	0F8Bh
latd	equ	0F8Ch
late	equ	0F8Dh
trisa	equ	0F92h
trisb	equ	0F93h
trisc	equ	0F94h
trisd	equ	0F95h
trise	equ	0F96h
pie1	equ	0F9Dh
pir1	equ	0F9Eh
ipr1	equ	0F9Fh
pie2	equ	0FA0h
pir2	equ	0FA1h
ipr2	equ	0FA2h
t3con	equ	0FB1h
tmr3l	equ	0FB2h
tmr3h	equ	0FB3h
ccp1con	equ	0FBDh
ccpr1l	equ	0FBEh
ccpr1h	equ	0FBFh
adcon1	equ	0FC1h
adcon0	equ	0FC2h
adresl	equ	0FC3h
adresh	equ	0FC4h
sspcon2	equ	0FC5h
sspcon1	equ	0FC6h
sspstat	equ	0FC7h
sspadd	equ	0FC8h
sspbuf	equ	0FC9h
t2con	equ	0FCAh
pr2	equ	0FCBh
tmr2	equ	0FCCh
t1con	equ	0FCDh
tmr1l	equ	0FCEh
tmr1h	equ	0FCFh
rcon	equ	0FD0h
wdtcon	equ	0FD1h
lvdcon	equ	0FD2h
osccon	equ	0FD3h
t0con	equ	0FD5h
tmr0l	equ	0FD6h
tmr0h	equ	0FD7h
status	equ	0FD8h
fsr2	equ	0FD9h
fsr2l	equ	0FD9h
fsr2h	equ	0FDAh
plusw2	equ	0FDBh
preinc2	equ	0FDCh
postdec2	equ	0FDDh
postinc2	equ	0FDEh
indf2	equ	0FDFh
bsr	equ	0FE0h
fsr1	equ	0FE1h
fsr1l	equ	0FE1h
fsr1h	equ	0FE2h
plusw1	equ	0FE3h
preinc1	equ	0FE4h
postdec1	equ	0FE5h
postinc1	equ	0FE6h
indf1	equ	0FE7h
wreg	equ	0FE8h
fsr0	equ	0FE9h
fsr0l	equ	0FE9h
fsr0h	equ	0FEAh
plusw0	equ	0FEBh
preinc0	equ	0FECh
postdec0	equ	0FEDh
postinc0	equ	0FEEh
indf0	equ	0FEFh
intcon3	equ	0FF0h
intcon2	equ	0FF1h
intcon	equ	0FF2h
prod	equ	0FF3h
prodl	equ	0FF3h
prodh	equ	0FF4h
tablat	equ	0FF5h
tblptr	equ	0FF6h
tblptrl	equ	0FF6h
tblptrh	equ	0FF7h
tblptru	equ	0FF8h
pcl	equ	0FF9h
pclat	equ	0FFAh
pclath	equ	0FFAh
pclatu	equ	0FFBh
stkptr	equ	0FFCh
tosl	equ	0FFDh
tosh	equ	0FFEh
tosu	equ	0FFFh
clrc   macro
	bcf	status,0
endm
setc   macro
	bsf	status,0
endm
clrz   macro
	bcf	status,2
endm
setz   macro
	bsf	status,2
endm
skipnz macro
	btfsc	status,2
endm
skipz  macro
	btfss	status,2
endm
skipnc macro
	btfsc	status,0
endm
skipc  macro
	btfss	status,0
endm
pushw macro
	movwf postinc1
endm
pushf macro arg1
	movff arg1, postinc1
endm
popw macro
	movf postdec1,w
	movf indf1,w
endm
popf macro arg1
	movf postdec1,w
	movff indf1,arg1
endm
popfc macro arg1
	movff plusw1,arg1
	decfsz fsr1,f
endm
	global	__ramtop
	global	__accesstop
# 49 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UFRM equ 0F66h ;# 
# 55 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UFRML equ 0F66h ;# 
# 132 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UFRMH equ 0F67h ;# 
# 171 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UIR equ 0F68h ;# 
# 226 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UIE equ 0F69h ;# 
# 281 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEIR equ 0F6Ah ;# 
# 331 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEIE equ 0F6Bh ;# 
# 381 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
USTAT equ 0F6Ch ;# 
# 440 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UCON equ 0F6Dh ;# 
# 490 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UADDR equ 0F6Eh ;# 
# 553 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UCFG equ 0F6Fh ;# 
# 634 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP0 equ 0F70h ;# 
# 765 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP1 equ 0F71h ;# 
# 896 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP2 equ 0F72h ;# 
# 1027 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP3 equ 0F73h ;# 
# 1158 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP4 equ 0F74h ;# 
# 1289 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP5 equ 0F75h ;# 
# 1420 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP6 equ 0F76h ;# 
# 1551 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP7 equ 0F77h ;# 
# 1682 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP8 equ 0F78h ;# 
# 1769 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP9 equ 0F79h ;# 
# 1856 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP10 equ 0F7Ah ;# 
# 1943 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP11 equ 0F7Bh ;# 
# 2030 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP12 equ 0F7Ch ;# 
# 2117 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP13 equ 0F7Dh ;# 
# 2204 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP14 equ 0F7Eh ;# 
# 2291 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP15 equ 0F7Fh ;# 
# 2378 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PORTA equ 0F80h ;# 
# 2534 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PORTB equ 0F81h ;# 
# 2643 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PORTC equ 0F82h ;# 
# 2796 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PORTE equ 0F84h ;# 
# 3029 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
LATA equ 0F89h ;# 
# 3164 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
LATB equ 0F8Ah ;# 
# 3296 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
LATC equ 0F8Bh ;# 
# 3411 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TRISA equ 0F92h ;# 
# 3416 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
DDRA equ 0F92h ;# 
# 3608 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TRISB equ 0F93h ;# 
# 3613 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
DDRB equ 0F93h ;# 
# 3829 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TRISC equ 0F94h ;# 
# 3834 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
DDRC equ 0F94h ;# 
# 4000 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
OSCTUNE equ 0F9Bh ;# 
# 4058 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PIE1 equ 0F9Dh ;# 
# 4131 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PIR1 equ 0F9Eh ;# 
# 4204 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
IPR1 equ 0F9Fh ;# 
# 4277 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PIE2 equ 0FA0h ;# 
# 4347 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PIR2 equ 0FA1h ;# 
# 4417 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
IPR2 equ 0FA2h ;# 
# 4487 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
EECON1 equ 0FA6h ;# 
# 4552 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
EECON2 equ 0FA7h ;# 
# 4558 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
EEDATA equ 0FA8h ;# 
# 4564 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
EEADR equ 0FA9h ;# 
# 4570 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
RCSTA equ 0FABh ;# 
# 4575 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
RCSTA1 equ 0FABh ;# 
# 4779 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TXSTA equ 0FACh ;# 
# 4784 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TXSTA1 equ 0FACh ;# 
# 5076 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TXREG equ 0FADh ;# 
# 5081 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TXREG1 equ 0FADh ;# 
# 5087 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
RCREG equ 0FAEh ;# 
# 5092 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
RCREG1 equ 0FAEh ;# 
# 5098 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SPBRG equ 0FAFh ;# 
# 5103 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SPBRG1 equ 0FAFh ;# 
# 5109 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SPBRGH equ 0FB0h ;# 
# 5115 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
T3CON equ 0FB1h ;# 
# 5237 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR3 equ 0FB2h ;# 
# 5243 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR3L equ 0FB2h ;# 
# 5249 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR3H equ 0FB3h ;# 
# 5255 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CMCON equ 0FB4h ;# 
# 5350 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CVRCON equ 0FB5h ;# 
# 5434 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ECCP1AS equ 0FB6h ;# 
# 5439 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCP1AS equ 0FB6h ;# 
# 5563 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ECCP1DEL equ 0FB7h ;# 
# 5568 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCP1DEL equ 0FB7h ;# 
# 5602 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
BAUDCON equ 0FB8h ;# 
# 5607 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
BAUDCTL equ 0FB8h ;# 
# 5781 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCP2CON equ 0FBAh ;# 
# 5844 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCPR2 equ 0FBBh ;# 
# 5850 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCPR2L equ 0FBBh ;# 
# 5856 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCPR2H equ 0FBCh ;# 
# 5862 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCP1CON equ 0FBDh ;# 
# 5925 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCPR1 equ 0FBEh ;# 
# 5931 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCPR1L equ 0FBEh ;# 
# 5937 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCPR1H equ 0FBFh ;# 
# 5943 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ADCON2 equ 0FC0h ;# 
# 6013 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ADCON1 equ 0FC1h ;# 
# 6103 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ADCON0 equ 0FC2h ;# 
# 6225 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ADRES equ 0FC3h ;# 
# 6231 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ADRESL equ 0FC3h ;# 
# 6237 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ADRESH equ 0FC4h ;# 
# 6243 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SSPCON2 equ 0FC5h ;# 
# 6304 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SSPCON1 equ 0FC6h ;# 
# 6373 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SSPSTAT equ 0FC7h ;# 
# 6639 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SSPADD equ 0FC8h ;# 
# 6645 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SSPBUF equ 0FC9h ;# 
# 6651 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
T2CON equ 0FCAh ;# 
# 6748 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PR2 equ 0FCBh ;# 
# 6753 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
MEMCON equ 0FCBh ;# 
# 6857 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR2 equ 0FCCh ;# 
# 6863 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
T1CON equ 0FCDh ;# 
# 6967 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR1 equ 0FCEh ;# 
# 6973 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR1L equ 0FCEh ;# 
# 6979 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR1H equ 0FCFh ;# 
# 6985 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
RCON equ 0FD0h ;# 
# 7133 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
WDTCON equ 0FD1h ;# 
# 7160 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
HLVDCON equ 0FD2h ;# 
# 7165 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
LVDCON equ 0FD2h ;# 
# 7429 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
OSCCON equ 0FD3h ;# 
# 7511 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
T0CON equ 0FD5h ;# 
# 7580 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR0 equ 0FD6h ;# 
# 7586 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR0L equ 0FD6h ;# 
# 7592 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR0H equ 0FD7h ;# 
# 7598 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
STATUS equ 0FD8h ;# 
# 7676 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR2 equ 0FD9h ;# 
# 7682 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR2L equ 0FD9h ;# 
# 7688 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR2H equ 0FDAh ;# 
# 7694 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PLUSW2 equ 0FDBh ;# 
# 7700 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PREINC2 equ 0FDCh ;# 
# 7706 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
POSTDEC2 equ 0FDDh ;# 
# 7712 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
POSTINC2 equ 0FDEh ;# 
# 7718 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
INDF2 equ 0FDFh ;# 
# 7724 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
BSR equ 0FE0h ;# 
# 7730 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR1 equ 0FE1h ;# 
# 7736 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR1L equ 0FE1h ;# 
# 7742 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR1H equ 0FE2h ;# 
# 7748 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PLUSW1 equ 0FE3h ;# 
# 7754 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PREINC1 equ 0FE4h ;# 
# 7760 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
POSTDEC1 equ 0FE5h ;# 
# 7766 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
POSTINC1 equ 0FE6h ;# 
# 7772 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
INDF1 equ 0FE7h ;# 
# 7778 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
WREG equ 0FE8h ;# 
# 7784 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR0 equ 0FE9h ;# 
# 7790 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR0L equ 0FE9h ;# 
# 7796 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR0H equ 0FEAh ;# 
# 7802 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PLUSW0 equ 0FEBh ;# 
# 7808 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PREINC0 equ 0FECh ;# 
# 7814 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
POSTDEC0 equ 0FEDh ;# 
# 7820 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
POSTINC0 equ 0FEEh ;# 
# 7826 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
INDF0 equ 0FEFh ;# 
# 7832 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
INTCON3 equ 0FF0h ;# 
# 7923 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
INTCON2 equ 0FF1h ;# 
# 7999 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
INTCON equ 0FF2h ;# 
# 8135 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PROD equ 0FF3h ;# 
# 8141 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PRODL equ 0FF3h ;# 
# 8147 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PRODH equ 0FF4h ;# 
# 8153 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TABLAT equ 0FF5h ;# 
# 8161 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TBLPTR equ 0FF6h ;# 
# 8167 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TBLPTRL equ 0FF6h ;# 
# 8173 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TBLPTRH equ 0FF7h ;# 
# 8179 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TBLPTRU equ 0FF8h ;# 
# 8187 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PCLAT equ 0FF9h ;# 
# 8194 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PC equ 0FF9h ;# 
# 8200 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PCL equ 0FF9h ;# 
# 8206 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PCLATH equ 0FFAh ;# 
# 8212 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PCLATU equ 0FFBh ;# 
# 8218 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
STKPTR equ 0FFCh ;# 
# 8293 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TOS equ 0FFDh ;# 
# 8299 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TOSL equ 0FFDh ;# 
# 8305 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TOSH equ 0FFEh ;# 
# 8311 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TOSU equ 0FFFh ;# 
# 49 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UFRM equ 0F66h ;# 
# 55 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UFRML equ 0F66h ;# 
# 132 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UFRMH equ 0F67h ;# 
# 171 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UIR equ 0F68h ;# 
# 226 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UIE equ 0F69h ;# 
# 281 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEIR equ 0F6Ah ;# 
# 331 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEIE equ 0F6Bh ;# 
# 381 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
USTAT equ 0F6Ch ;# 
# 440 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UCON equ 0F6Dh ;# 
# 490 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UADDR equ 0F6Eh ;# 
# 553 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UCFG equ 0F6Fh ;# 
# 634 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP0 equ 0F70h ;# 
# 765 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP1 equ 0F71h ;# 
# 896 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP2 equ 0F72h ;# 
# 1027 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP3 equ 0F73h ;# 
# 1158 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP4 equ 0F74h ;# 
# 1289 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP5 equ 0F75h ;# 
# 1420 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP6 equ 0F76h ;# 
# 1551 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP7 equ 0F77h ;# 
# 1682 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP8 equ 0F78h ;# 
# 1769 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP9 equ 0F79h ;# 
# 1856 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP10 equ 0F7Ah ;# 
# 1943 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP11 equ 0F7Bh ;# 
# 2030 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP12 equ 0F7Ch ;# 
# 2117 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP13 equ 0F7Dh ;# 
# 2204 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP14 equ 0F7Eh ;# 
# 2291 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP15 equ 0F7Fh ;# 
# 2378 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PORTA equ 0F80h ;# 
# 2534 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PORTB equ 0F81h ;# 
# 2643 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PORTC equ 0F82h ;# 
# 2796 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PORTE equ 0F84h ;# 
# 3029 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
LATA equ 0F89h ;# 
# 3164 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
LATB equ 0F8Ah ;# 
# 3296 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
LATC equ 0F8Bh ;# 
# 3411 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TRISA equ 0F92h ;# 
# 3416 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
DDRA equ 0F92h ;# 
# 3608 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TRISB equ 0F93h ;# 
# 3613 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
DDRB equ 0F93h ;# 
# 3829 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TRISC equ 0F94h ;# 
# 3834 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
DDRC equ 0F94h ;# 
# 4000 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
OSCTUNE equ 0F9Bh ;# 
# 4058 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PIE1 equ 0F9Dh ;# 
# 4131 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PIR1 equ 0F9Eh ;# 
# 4204 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
IPR1 equ 0F9Fh ;# 
# 4277 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PIE2 equ 0FA0h ;# 
# 4347 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PIR2 equ 0FA1h ;# 
# 4417 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
IPR2 equ 0FA2h ;# 
# 4487 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
EECON1 equ 0FA6h ;# 
# 4552 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
EECON2 equ 0FA7h ;# 
# 4558 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
EEDATA equ 0FA8h ;# 
# 4564 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
EEADR equ 0FA9h ;# 
# 4570 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
RCSTA equ 0FABh ;# 
# 4575 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
RCSTA1 equ 0FABh ;# 
# 4779 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TXSTA equ 0FACh ;# 
# 4784 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TXSTA1 equ 0FACh ;# 
# 5076 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TXREG equ 0FADh ;# 
# 5081 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TXREG1 equ 0FADh ;# 
# 5087 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
RCREG equ 0FAEh ;# 
# 5092 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
RCREG1 equ 0FAEh ;# 
# 5098 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SPBRG equ 0FAFh ;# 
# 5103 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SPBRG1 equ 0FAFh ;# 
# 5109 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SPBRGH equ 0FB0h ;# 
# 5115 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
T3CON equ 0FB1h ;# 
# 5237 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR3 equ 0FB2h ;# 
# 5243 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR3L equ 0FB2h ;# 
# 5249 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR3H equ 0FB3h ;# 
# 5255 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CMCON equ 0FB4h ;# 
# 5350 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CVRCON equ 0FB5h ;# 
# 5434 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ECCP1AS equ 0FB6h ;# 
# 5439 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCP1AS equ 0FB6h ;# 
# 5563 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ECCP1DEL equ 0FB7h ;# 
# 5568 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCP1DEL equ 0FB7h ;# 
# 5602 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
BAUDCON equ 0FB8h ;# 
# 5607 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
BAUDCTL equ 0FB8h ;# 
# 5781 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCP2CON equ 0FBAh ;# 
# 5844 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCPR2 equ 0FBBh ;# 
# 5850 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCPR2L equ 0FBBh ;# 
# 5856 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCPR2H equ 0FBCh ;# 
# 5862 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCP1CON equ 0FBDh ;# 
# 5925 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCPR1 equ 0FBEh ;# 
# 5931 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCPR1L equ 0FBEh ;# 
# 5937 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCPR1H equ 0FBFh ;# 
# 5943 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ADCON2 equ 0FC0h ;# 
# 6013 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ADCON1 equ 0FC1h ;# 
# 6103 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ADCON0 equ 0FC2h ;# 
# 6225 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ADRES equ 0FC3h ;# 
# 6231 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ADRESL equ 0FC3h ;# 
# 6237 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ADRESH equ 0FC4h ;# 
# 6243 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SSPCON2 equ 0FC5h ;# 
# 6304 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SSPCON1 equ 0FC6h ;# 
# 6373 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SSPSTAT equ 0FC7h ;# 
# 6639 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SSPADD equ 0FC8h ;# 
# 6645 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SSPBUF equ 0FC9h ;# 
# 6651 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
T2CON equ 0FCAh ;# 
# 6748 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PR2 equ 0FCBh ;# 
# 6753 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
MEMCON equ 0FCBh ;# 
# 6857 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR2 equ 0FCCh ;# 
# 6863 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
T1CON equ 0FCDh ;# 
# 6967 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR1 equ 0FCEh ;# 
# 6973 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR1L equ 0FCEh ;# 
# 6979 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR1H equ 0FCFh ;# 
# 6985 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
RCON equ 0FD0h ;# 
# 7133 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
WDTCON equ 0FD1h ;# 
# 7160 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
HLVDCON equ 0FD2h ;# 
# 7165 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
LVDCON equ 0FD2h ;# 
# 7429 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
OSCCON equ 0FD3h ;# 
# 7511 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
T0CON equ 0FD5h ;# 
# 7580 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR0 equ 0FD6h ;# 
# 7586 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR0L equ 0FD6h ;# 
# 7592 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR0H equ 0FD7h ;# 
# 7598 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
STATUS equ 0FD8h ;# 
# 7676 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR2 equ 0FD9h ;# 
# 7682 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR2L equ 0FD9h ;# 
# 7688 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR2H equ 0FDAh ;# 
# 7694 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PLUSW2 equ 0FDBh ;# 
# 7700 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PREINC2 equ 0FDCh ;# 
# 7706 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
POSTDEC2 equ 0FDDh ;# 
# 7712 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
POSTINC2 equ 0FDEh ;# 
# 7718 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
INDF2 equ 0FDFh ;# 
# 7724 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
BSR equ 0FE0h ;# 
# 7730 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR1 equ 0FE1h ;# 
# 7736 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR1L equ 0FE1h ;# 
# 7742 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR1H equ 0FE2h ;# 
# 7748 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PLUSW1 equ 0FE3h ;# 
# 7754 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PREINC1 equ 0FE4h ;# 
# 7760 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
POSTDEC1 equ 0FE5h ;# 
# 7766 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
POSTINC1 equ 0FE6h ;# 
# 7772 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
INDF1 equ 0FE7h ;# 
# 7778 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
WREG equ 0FE8h ;# 
# 7784 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR0 equ 0FE9h ;# 
# 7790 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR0L equ 0FE9h ;# 
# 7796 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR0H equ 0FEAh ;# 
# 7802 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PLUSW0 equ 0FEBh ;# 
# 7808 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PREINC0 equ 0FECh ;# 
# 7814 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
POSTDEC0 equ 0FEDh ;# 
# 7820 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
POSTINC0 equ 0FEEh ;# 
# 7826 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
INDF0 equ 0FEFh ;# 
# 7832 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
INTCON3 equ 0FF0h ;# 
# 7923 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
INTCON2 equ 0FF1h ;# 
# 7999 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
INTCON equ 0FF2h ;# 
# 8135 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PROD equ 0FF3h ;# 
# 8141 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PRODL equ 0FF3h ;# 
# 8147 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PRODH equ 0FF4h ;# 
# 8153 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TABLAT equ 0FF5h ;# 
# 8161 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TBLPTR equ 0FF6h ;# 
# 8167 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TBLPTRL equ 0FF6h ;# 
# 8173 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TBLPTRH equ 0FF7h ;# 
# 8179 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TBLPTRU equ 0FF8h ;# 
# 8187 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PCLAT equ 0FF9h ;# 
# 8194 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PC equ 0FF9h ;# 
# 8200 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PCL equ 0FF9h ;# 
# 8206 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PCLATH equ 0FFAh ;# 
# 8212 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PCLATU equ 0FFBh ;# 
# 8218 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
STKPTR equ 0FFCh ;# 
# 8293 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TOS equ 0FFDh ;# 
# 8299 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TOSL equ 0FFDh ;# 
# 8305 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TOSH equ 0FFEh ;# 
# 8311 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TOSU equ 0FFFh ;# 
# 49 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UFRM equ 0F66h ;# 
# 55 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UFRML equ 0F66h ;# 
# 132 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UFRMH equ 0F67h ;# 
# 171 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UIR equ 0F68h ;# 
# 226 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UIE equ 0F69h ;# 
# 281 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEIR equ 0F6Ah ;# 
# 331 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEIE equ 0F6Bh ;# 
# 381 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
USTAT equ 0F6Ch ;# 
# 440 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UCON equ 0F6Dh ;# 
# 490 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UADDR equ 0F6Eh ;# 
# 553 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UCFG equ 0F6Fh ;# 
# 634 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP0 equ 0F70h ;# 
# 765 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP1 equ 0F71h ;# 
# 896 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP2 equ 0F72h ;# 
# 1027 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP3 equ 0F73h ;# 
# 1158 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP4 equ 0F74h ;# 
# 1289 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP5 equ 0F75h ;# 
# 1420 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP6 equ 0F76h ;# 
# 1551 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP7 equ 0F77h ;# 
# 1682 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP8 equ 0F78h ;# 
# 1769 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP9 equ 0F79h ;# 
# 1856 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP10 equ 0F7Ah ;# 
# 1943 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP11 equ 0F7Bh ;# 
# 2030 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP12 equ 0F7Ch ;# 
# 2117 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP13 equ 0F7Dh ;# 
# 2204 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP14 equ 0F7Eh ;# 
# 2291 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP15 equ 0F7Fh ;# 
# 2378 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PORTA equ 0F80h ;# 
# 2534 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PORTB equ 0F81h ;# 
# 2643 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PORTC equ 0F82h ;# 
# 2796 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PORTE equ 0F84h ;# 
# 3029 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
LATA equ 0F89h ;# 
# 3164 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
LATB equ 0F8Ah ;# 
# 3296 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
LATC equ 0F8Bh ;# 
# 3411 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TRISA equ 0F92h ;# 
# 3416 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
DDRA equ 0F92h ;# 
# 3608 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TRISB equ 0F93h ;# 
# 3613 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
DDRB equ 0F93h ;# 
# 3829 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TRISC equ 0F94h ;# 
# 3834 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
DDRC equ 0F94h ;# 
# 4000 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
OSCTUNE equ 0F9Bh ;# 
# 4058 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PIE1 equ 0F9Dh ;# 
# 4131 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PIR1 equ 0F9Eh ;# 
# 4204 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
IPR1 equ 0F9Fh ;# 
# 4277 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PIE2 equ 0FA0h ;# 
# 4347 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PIR2 equ 0FA1h ;# 
# 4417 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
IPR2 equ 0FA2h ;# 
# 4487 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
EECON1 equ 0FA6h ;# 
# 4552 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
EECON2 equ 0FA7h ;# 
# 4558 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
EEDATA equ 0FA8h ;# 
# 4564 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
EEADR equ 0FA9h ;# 
# 4570 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
RCSTA equ 0FABh ;# 
# 4575 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
RCSTA1 equ 0FABh ;# 
# 4779 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TXSTA equ 0FACh ;# 
# 4784 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TXSTA1 equ 0FACh ;# 
# 5076 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TXREG equ 0FADh ;# 
# 5081 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TXREG1 equ 0FADh ;# 
# 5087 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
RCREG equ 0FAEh ;# 
# 5092 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
RCREG1 equ 0FAEh ;# 
# 5098 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SPBRG equ 0FAFh ;# 
# 5103 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SPBRG1 equ 0FAFh ;# 
# 5109 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SPBRGH equ 0FB0h ;# 
# 5115 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
T3CON equ 0FB1h ;# 
# 5237 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR3 equ 0FB2h ;# 
# 5243 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR3L equ 0FB2h ;# 
# 5249 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR3H equ 0FB3h ;# 
# 5255 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CMCON equ 0FB4h ;# 
# 5350 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CVRCON equ 0FB5h ;# 
# 5434 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ECCP1AS equ 0FB6h ;# 
# 5439 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCP1AS equ 0FB6h ;# 
# 5563 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ECCP1DEL equ 0FB7h ;# 
# 5568 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCP1DEL equ 0FB7h ;# 
# 5602 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
BAUDCON equ 0FB8h ;# 
# 5607 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
BAUDCTL equ 0FB8h ;# 
# 5781 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCP2CON equ 0FBAh ;# 
# 5844 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCPR2 equ 0FBBh ;# 
# 5850 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCPR2L equ 0FBBh ;# 
# 5856 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCPR2H equ 0FBCh ;# 
# 5862 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCP1CON equ 0FBDh ;# 
# 5925 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCPR1 equ 0FBEh ;# 
# 5931 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCPR1L equ 0FBEh ;# 
# 5937 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCPR1H equ 0FBFh ;# 
# 5943 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ADCON2 equ 0FC0h ;# 
# 6013 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ADCON1 equ 0FC1h ;# 
# 6103 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ADCON0 equ 0FC2h ;# 
# 6225 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ADRES equ 0FC3h ;# 
# 6231 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ADRESL equ 0FC3h ;# 
# 6237 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ADRESH equ 0FC4h ;# 
# 6243 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SSPCON2 equ 0FC5h ;# 
# 6304 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SSPCON1 equ 0FC6h ;# 
# 6373 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SSPSTAT equ 0FC7h ;# 
# 6639 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SSPADD equ 0FC8h ;# 
# 6645 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SSPBUF equ 0FC9h ;# 
# 6651 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
T2CON equ 0FCAh ;# 
# 6748 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PR2 equ 0FCBh ;# 
# 6753 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
MEMCON equ 0FCBh ;# 
# 6857 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR2 equ 0FCCh ;# 
# 6863 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
T1CON equ 0FCDh ;# 
# 6967 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR1 equ 0FCEh ;# 
# 6973 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR1L equ 0FCEh ;# 
# 6979 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR1H equ 0FCFh ;# 
# 6985 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
RCON equ 0FD0h ;# 
# 7133 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
WDTCON equ 0FD1h ;# 
# 7160 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
HLVDCON equ 0FD2h ;# 
# 7165 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
LVDCON equ 0FD2h ;# 
# 7429 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
OSCCON equ 0FD3h ;# 
# 7511 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
T0CON equ 0FD5h ;# 
# 7580 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR0 equ 0FD6h ;# 
# 7586 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR0L equ 0FD6h ;# 
# 7592 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR0H equ 0FD7h ;# 
# 7598 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
STATUS equ 0FD8h ;# 
# 7676 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR2 equ 0FD9h ;# 
# 7682 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR2L equ 0FD9h ;# 
# 7688 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR2H equ 0FDAh ;# 
# 7694 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PLUSW2 equ 0FDBh ;# 
# 7700 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PREINC2 equ 0FDCh ;# 
# 7706 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
POSTDEC2 equ 0FDDh ;# 
# 7712 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
POSTINC2 equ 0FDEh ;# 
# 7718 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
INDF2 equ 0FDFh ;# 
# 7724 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
BSR equ 0FE0h ;# 
# 7730 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR1 equ 0FE1h ;# 
# 7736 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR1L equ 0FE1h ;# 
# 7742 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR1H equ 0FE2h ;# 
# 7748 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PLUSW1 equ 0FE3h ;# 
# 7754 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PREINC1 equ 0FE4h ;# 
# 7760 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
POSTDEC1 equ 0FE5h ;# 
# 7766 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
POSTINC1 equ 0FE6h ;# 
# 7772 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
INDF1 equ 0FE7h ;# 
# 7778 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
WREG equ 0FE8h ;# 
# 7784 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR0 equ 0FE9h ;# 
# 7790 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR0L equ 0FE9h ;# 
# 7796 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR0H equ 0FEAh ;# 
# 7802 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PLUSW0 equ 0FEBh ;# 
# 7808 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PREINC0 equ 0FECh ;# 
# 7814 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
POSTDEC0 equ 0FEDh ;# 
# 7820 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
POSTINC0 equ 0FEEh ;# 
# 7826 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
INDF0 equ 0FEFh ;# 
# 7832 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
INTCON3 equ 0FF0h ;# 
# 7923 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
INTCON2 equ 0FF1h ;# 
# 7999 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
INTCON equ 0FF2h ;# 
# 8135 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PROD equ 0FF3h ;# 
# 8141 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PRODL equ 0FF3h ;# 
# 8147 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PRODH equ 0FF4h ;# 
# 8153 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TABLAT equ 0FF5h ;# 
# 8161 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TBLPTR equ 0FF6h ;# 
# 8167 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TBLPTRL equ 0FF6h ;# 
# 8173 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TBLPTRH equ 0FF7h ;# 
# 8179 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TBLPTRU equ 0FF8h ;# 
# 8187 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PCLAT equ 0FF9h ;# 
# 8194 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PC equ 0FF9h ;# 
# 8200 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PCL equ 0FF9h ;# 
# 8206 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PCLATH equ 0FFAh ;# 
# 8212 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PCLATU equ 0FFBh ;# 
# 8218 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
STKPTR equ 0FFCh ;# 
# 8293 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TOS equ 0FFDh ;# 
# 8299 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TOSL equ 0FFDh ;# 
# 8305 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TOSH equ 0FFEh ;# 
# 8311 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TOSU equ 0FFFh ;# 
# 49 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UFRM equ 0F66h ;# 
# 55 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UFRML equ 0F66h ;# 
# 132 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UFRMH equ 0F67h ;# 
# 171 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UIR equ 0F68h ;# 
# 226 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UIE equ 0F69h ;# 
# 281 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEIR equ 0F6Ah ;# 
# 331 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEIE equ 0F6Bh ;# 
# 381 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
USTAT equ 0F6Ch ;# 
# 440 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UCON equ 0F6Dh ;# 
# 490 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UADDR equ 0F6Eh ;# 
# 553 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UCFG equ 0F6Fh ;# 
# 634 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP0 equ 0F70h ;# 
# 765 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP1 equ 0F71h ;# 
# 896 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP2 equ 0F72h ;# 
# 1027 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP3 equ 0F73h ;# 
# 1158 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP4 equ 0F74h ;# 
# 1289 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP5 equ 0F75h ;# 
# 1420 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP6 equ 0F76h ;# 
# 1551 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP7 equ 0F77h ;# 
# 1682 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP8 equ 0F78h ;# 
# 1769 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP9 equ 0F79h ;# 
# 1856 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP10 equ 0F7Ah ;# 
# 1943 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP11 equ 0F7Bh ;# 
# 2030 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP12 equ 0F7Ch ;# 
# 2117 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP13 equ 0F7Dh ;# 
# 2204 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP14 equ 0F7Eh ;# 
# 2291 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
UEP15 equ 0F7Fh ;# 
# 2378 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PORTA equ 0F80h ;# 
# 2534 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PORTB equ 0F81h ;# 
# 2643 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PORTC equ 0F82h ;# 
# 2796 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PORTE equ 0F84h ;# 
# 3029 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
LATA equ 0F89h ;# 
# 3164 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
LATB equ 0F8Ah ;# 
# 3296 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
LATC equ 0F8Bh ;# 
# 3411 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TRISA equ 0F92h ;# 
# 3416 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
DDRA equ 0F92h ;# 
# 3608 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TRISB equ 0F93h ;# 
# 3613 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
DDRB equ 0F93h ;# 
# 3829 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TRISC equ 0F94h ;# 
# 3834 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
DDRC equ 0F94h ;# 
# 4000 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
OSCTUNE equ 0F9Bh ;# 
# 4058 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PIE1 equ 0F9Dh ;# 
# 4131 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PIR1 equ 0F9Eh ;# 
# 4204 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
IPR1 equ 0F9Fh ;# 
# 4277 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PIE2 equ 0FA0h ;# 
# 4347 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PIR2 equ 0FA1h ;# 
# 4417 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
IPR2 equ 0FA2h ;# 
# 4487 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
EECON1 equ 0FA6h ;# 
# 4552 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
EECON2 equ 0FA7h ;# 
# 4558 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
EEDATA equ 0FA8h ;# 
# 4564 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
EEADR equ 0FA9h ;# 
# 4570 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
RCSTA equ 0FABh ;# 
# 4575 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
RCSTA1 equ 0FABh ;# 
# 4779 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TXSTA equ 0FACh ;# 
# 4784 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TXSTA1 equ 0FACh ;# 
# 5076 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TXREG equ 0FADh ;# 
# 5081 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TXREG1 equ 0FADh ;# 
# 5087 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
RCREG equ 0FAEh ;# 
# 5092 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
RCREG1 equ 0FAEh ;# 
# 5098 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SPBRG equ 0FAFh ;# 
# 5103 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SPBRG1 equ 0FAFh ;# 
# 5109 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SPBRGH equ 0FB0h ;# 
# 5115 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
T3CON equ 0FB1h ;# 
# 5237 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR3 equ 0FB2h ;# 
# 5243 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR3L equ 0FB2h ;# 
# 5249 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR3H equ 0FB3h ;# 
# 5255 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CMCON equ 0FB4h ;# 
# 5350 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CVRCON equ 0FB5h ;# 
# 5434 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ECCP1AS equ 0FB6h ;# 
# 5439 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCP1AS equ 0FB6h ;# 
# 5563 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ECCP1DEL equ 0FB7h ;# 
# 5568 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCP1DEL equ 0FB7h ;# 
# 5602 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
BAUDCON equ 0FB8h ;# 
# 5607 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
BAUDCTL equ 0FB8h ;# 
# 5781 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCP2CON equ 0FBAh ;# 
# 5844 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCPR2 equ 0FBBh ;# 
# 5850 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCPR2L equ 0FBBh ;# 
# 5856 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCPR2H equ 0FBCh ;# 
# 5862 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCP1CON equ 0FBDh ;# 
# 5925 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCPR1 equ 0FBEh ;# 
# 5931 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCPR1L equ 0FBEh ;# 
# 5937 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
CCPR1H equ 0FBFh ;# 
# 5943 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ADCON2 equ 0FC0h ;# 
# 6013 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ADCON1 equ 0FC1h ;# 
# 6103 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ADCON0 equ 0FC2h ;# 
# 6225 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ADRES equ 0FC3h ;# 
# 6231 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ADRESL equ 0FC3h ;# 
# 6237 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
ADRESH equ 0FC4h ;# 
# 6243 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SSPCON2 equ 0FC5h ;# 
# 6304 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SSPCON1 equ 0FC6h ;# 
# 6373 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SSPSTAT equ 0FC7h ;# 
# 6639 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SSPADD equ 0FC8h ;# 
# 6645 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
SSPBUF equ 0FC9h ;# 
# 6651 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
T2CON equ 0FCAh ;# 
# 6748 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PR2 equ 0FCBh ;# 
# 6753 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
MEMCON equ 0FCBh ;# 
# 6857 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR2 equ 0FCCh ;# 
# 6863 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
T1CON equ 0FCDh ;# 
# 6967 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR1 equ 0FCEh ;# 
# 6973 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR1L equ 0FCEh ;# 
# 6979 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR1H equ 0FCFh ;# 
# 6985 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
RCON equ 0FD0h ;# 
# 7133 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
WDTCON equ 0FD1h ;# 
# 7160 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
HLVDCON equ 0FD2h ;# 
# 7165 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
LVDCON equ 0FD2h ;# 
# 7429 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
OSCCON equ 0FD3h ;# 
# 7511 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
T0CON equ 0FD5h ;# 
# 7580 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR0 equ 0FD6h ;# 
# 7586 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR0L equ 0FD6h ;# 
# 7592 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TMR0H equ 0FD7h ;# 
# 7598 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
STATUS equ 0FD8h ;# 
# 7676 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR2 equ 0FD9h ;# 
# 7682 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR2L equ 0FD9h ;# 
# 7688 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR2H equ 0FDAh ;# 
# 7694 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PLUSW2 equ 0FDBh ;# 
# 7700 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PREINC2 equ 0FDCh ;# 
# 7706 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
POSTDEC2 equ 0FDDh ;# 
# 7712 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
POSTINC2 equ 0FDEh ;# 
# 7718 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
INDF2 equ 0FDFh ;# 
# 7724 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
BSR equ 0FE0h ;# 
# 7730 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR1 equ 0FE1h ;# 
# 7736 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR1L equ 0FE1h ;# 
# 7742 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR1H equ 0FE2h ;# 
# 7748 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PLUSW1 equ 0FE3h ;# 
# 7754 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PREINC1 equ 0FE4h ;# 
# 7760 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
POSTDEC1 equ 0FE5h ;# 
# 7766 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
POSTINC1 equ 0FE6h ;# 
# 7772 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
INDF1 equ 0FE7h ;# 
# 7778 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
WREG equ 0FE8h ;# 
# 7784 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR0 equ 0FE9h ;# 
# 7790 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR0L equ 0FE9h ;# 
# 7796 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
FSR0H equ 0FEAh ;# 
# 7802 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PLUSW0 equ 0FEBh ;# 
# 7808 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PREINC0 equ 0FECh ;# 
# 7814 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
POSTDEC0 equ 0FEDh ;# 
# 7820 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
POSTINC0 equ 0FEEh ;# 
# 7826 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
INDF0 equ 0FEFh ;# 
# 7832 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
INTCON3 equ 0FF0h ;# 
# 7923 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
INTCON2 equ 0FF1h ;# 
# 7999 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
INTCON equ 0FF2h ;# 
# 8135 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PROD equ 0FF3h ;# 
# 8141 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PRODL equ 0FF3h ;# 
# 8147 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PRODH equ 0FF4h ;# 
# 8153 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TABLAT equ 0FF5h ;# 
# 8161 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TBLPTR equ 0FF6h ;# 
# 8167 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TBLPTRL equ 0FF6h ;# 
# 8173 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TBLPTRH equ 0FF7h ;# 
# 8179 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TBLPTRU equ 0FF8h ;# 
# 8187 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PCLAT equ 0FF9h ;# 
# 8194 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PC equ 0FF9h ;# 
# 8200 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PCL equ 0FF9h ;# 
# 8206 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PCLATH equ 0FFAh ;# 
# 8212 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
PCLATU equ 0FFBh ;# 
# 8218 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
STKPTR equ 0FFCh ;# 
# 8293 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TOS equ 0FFDh ;# 
# 8299 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TOSL equ 0FFDh ;# 
# 8305 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TOSH equ 0FFEh ;# 
# 8311 "C:\Program Files (x86)\Microchip\xc8\v1.33\include\pic18f2550.h"
TOSU equ 0FFFh ;# 
	FNCALL	_main,_clock_getMS
	FNCALL	_main,_clock_init
	FNCALL	_main,_clock_process
	FNCALL	_main,_mcp2515_init
	FNCALL	_main,_mcp2515_receive_message
	FNCALL	_main,_parseLine
	FNCALL	_main,_sendHex
	FNCALL	_main,_usb_chReceived
	FNCALL	_main,_usb_getch
	FNCALL	_main,_usb_init
	FNCALL	_main,_usb_process
	FNCALL	_main,_usb_putch
	FNCALL	_parseLine,_clock_reset
	FNCALL	_parseLine,_mcp2515_bit_modify
	FNCALL	_parseLine,_mcp2515_read_register
	FNCALL	_parseLine,_mcp2515_set_SJA1000_filter_code
	FNCALL	_parseLine,_mcp2515_set_SJA1000_filter_mask
	FNCALL	_parseLine,_mcp2515_set_bittiming
	FNCALL	_parseLine,_mcp2515_write_register
	FNCALL	_parseLine,_parseHex
	FNCALL	_parseLine,_sendByteHex
	FNCALL	_parseLine,_sendHex
	FNCALL	_parseLine,_transmitStd
	FNCALL	_parseLine,_usb_putch
	FNCALL	_transmitStd,_mcp2515_send_message
	FNCALL	_transmitStd,_parseHex
	FNCALL	_mcp2515_send_message,_mcp2515_read_status
	FNCALL	_mcp2515_send_message,_spi_transmit
	FNCALL	_mcp2515_read_status,_spi_transmit
	FNCALL	_sendByteHex,_sendHex
	FNCALL	_sendHex,_usb_putstr
	FNCALL	_usb_putstr,_usb_putch
	FNCALL	_usb_putch,_usb_process
	FNCALL	_usb_process,_usb_handleDescriptorRequest
	FNCALL	_usb_process,_usb_sendProcess
	FNCALL	_usb_process,_usb_txprocess
	FNCALL	_usb_handleDescriptorRequest,_usb_loadDescriptor
	FNCALL	_usb_loadDescriptor,_usb_sendProcess
	FNCALL	_mcp2515_set_bittiming,_mcp2515_write_register
	FNCALL	_mcp2515_set_SJA1000_filter_mask,_mcp2515_write_register
	FNCALL	_mcp2515_set_SJA1000_filter_code,_mcp2515_write_register
	FNCALL	_mcp2515_read_register,_spi_transmit
	FNCALL	_mcp2515_receive_message,_mcp2515_bit_modify
	FNCALL	_mcp2515_receive_message,_mcp2515_rx_status
	FNCALL	_mcp2515_receive_message,_spi_transmit
	FNCALL	_mcp2515_rx_status,_spi_transmit
	FNCALL	_mcp2515_bit_modify,_spi_transmit
	FNCALL	_mcp2515_init,_mcp2515_write_register
	FNCALL	_mcp2515_init,_spi_transmit
	FNCALL	_mcp2515_write_register,_spi_transmit
	FNCALL	_clock_init,_clock_reset
	FNROOT	_main
	global	_usb_config_desc
psect	smallconst,class=SMALLCONST,space=0,reloc=2,noexec
global __psmallconst
__psmallconst:
	db	0
	file	"D:\Projects\USBtin\firmware\source\usb_cdc.c"
	line	94
_usb_config_desc:
	db	low(09h)
	db	low(02h)
	db	low(043h)
	db	low(0)
	db	low(02h)
	db	low(01h)
	db	low(0)
	db	low(080h)
	db	low(032h)
	db	low(09h)
	db	low(04h)
	db	low(0)
	db	low(0)
	db	low(01h)
	db	low(02h)
	db	low(02h)
	db	low(01h)
	db	low(0)
	db	low(05h)
	db	low(024h)
	db	low(0)
	db	low(010h)
	db	low(01h)
	db	low(04h)
	db	low(024h)
	db	low(02h)
	db	low(02h)
	db	low(05h)
	db	low(024h)
	db	low(06h)
	db	low(0)
	db	low(01h)
	db	low(05h)
	db	low(024h)
	db	low(01h)
	db	low(0)
	db	low(01h)
	db	low(07h)
	db	low(05h)
	db	low(082h)
	db	low(03h)
	db	low(08h)
	db	low(0)
	db	low(02h)
	db	low(09h)
	db	low(04h)
	db	low(01h)
	db	low(0)
	db	low(02h)
	db	low(0Ah)
	db	low(0)
	db	low(0)
	db	low(0)
	db	low(07h)
	db	low(05h)
	db	low(03h)
	db	low(02h)
	db	low(08h)
	db	low(0)
	db	low(0)
	db	low(07h)
	db	low(05h)
	db	low(081h)
	db	low(02h)
	db	low(040h)
	db	low(0)
	db	low(0)
	global __end_of_usb_config_desc
__end_of_usb_config_desc:
	global	_usb_string_manuf
psect	smallconst
	file	"D:\Projects\USBtin\firmware\source\usb_cdc.c"
	line	186
_usb_string_manuf:
	db	low(036h)
	db	low(03h)
	db	low(04Dh)
	db	low(0)
	db	low(069h)
	db	low(0)
	db	low(063h)
	db	low(0)
	db	low(072h)
	db	low(0)
	db	low(06Fh)
	db	low(0)
	db	low(063h)
	db	low(0)
	db	low(068h)
	db	low(0)
	db	low(069h)
	db	low(0)
	db	low(070h)
	db	low(0)
	db	low(020h)
	db	low(0)
	db	low(054h)
	db	low(0)
	db	low(065h)
	db	low(0)
	db	low(063h)
	db	low(0)
	db	low(068h)
	db	low(0)
	db	low(06Eh)
	db	low(0)
	db	low(06Fh)
	db	low(0)
	db	low(06Ch)
	db	low(0)
	db	low(06Fh)
	db	low(0)
	db	low(067h)
	db	low(0)
	db	low(079h)
	db	low(0)
	db	low(02Ch)
	db	low(0)
	db	low(020h)
	db	low(0)
	db	low(049h)
	db	low(0)
	db	low(06Eh)
	db	low(0)
	db	low(063h)
	db	low(0)
	db	low(02Eh)
	db	low(0)
	global __end_of_usb_string_manuf
__end_of_usb_string_manuf:
	global	_usb_dev_desc
psect	smallconst
	file	"D:\Projects\USBtin\firmware\source\usb_cdc.c"
	line	77
_usb_dev_desc:
	db	low(012h)
	db	low(01h)
	db	low(0)
	db	low(02h)
	db	low(02h)
	db	low(0)
	db	low(0)
	db	low(08h)
	db	low(0D8h)
	db	low(04h)
	db	low(0Ah)
	db	low(0)
	db	low(0)
	db	low(01h)
	db	low(01h)
	db	low(02h)
	db	low(0)
	db	low(01h)
	global __end_of_usb_dev_desc
__end_of_usb_dev_desc:
	global	_usb_string_product
psect	smallconst
	file	"D:\Projects\USBtin\firmware\source\usb_cdc.c"
	line	217
_usb_string_product:
	db	low(0Eh)
	db	low(03h)
	db	low(055h)
	db	low(0)
	db	low(053h)
	db	low(0)
	db	low(042h)
	db	low(0)
	db	low(074h)
	db	low(0)
	db	low(069h)
	db	low(0)
	db	low(06Eh)
	db	low(0)
	global __end_of_usb_string_product
__end_of_usb_string_product:
	global	_usb_string_0
psect	smallconst
	file	"D:\Projects\USBtin\firmware\source\usb_cdc.c"
	line	180
_usb_string_0:
	db	low(04h)
	db	low(03h)
	db	low(09h)
	db	low(04h)
	global __end_of_usb_string_0
__end_of_usb_string_0:
	global	_usb_config_desc
	global	_usb_string_manuf
	global	_usb_dev_desc
	global	_usb_string_product
	global	_usb_string_0
	global	_linecoding
	global	_usb_sendbuffer
	global	_clock_lastclock
	global	_clock_msticker
	global	_configured
	global	_usb_sendleft
	global	_dolinecoding
	global	_state
	global	_txbuffer_bytesleft
	global	_txbuffer_writepos
	global	_usb_config
	global	_usb_getchpos
	global	_usb_setaddress
	global	_txbuffer
	global	_timestamping
	global	_TMR0
_TMR0	set	0xFD6
	global	_LATBbits
_LATBbits	set	0xF8A
	global	_LATCbits
_LATCbits	set	0xF8B
	global	_OSCCON
_OSCCON	set	0xFD3
	global	_PORTAbits
_PORTAbits	set	0xF80
	global	_PORTCbits
_PORTCbits	set	0xF82
	global	_SSPBUF
_SSPBUF	set	0xFC9
	global	_SSPCON1
_SSPCON1	set	0xFC6
	global	_SSPSTAT
_SSPSTAT	set	0xFC7
	global	_SSPSTATbits
_SSPSTATbits	set	0xFC7
	global	_T0CON
_T0CON	set	0xFD5
	global	_TRISBbits
_TRISBbits	set	0xF93
	global	_TRISCbits
_TRISCbits	set	0xF94
	global	_UADDR
_UADDR	set	0xF6E
	global	_UCFG
_UCFG	set	0xF6F
	global	_UCON
_UCON	set	0xF6D
	global	_UCONbits
_UCONbits	set	0xF6D
	global	_UEP0
_UEP0	set	0xF70
	global	_UEP1
_UEP1	set	0xF71
	global	_UEP2
_UEP2	set	0xF72
	global	_UEP3
_UEP3	set	0xF73
	global	_UIRbits
_UIRbits	set	0xF68
	global	_USTAT
_USTAT	set	0xF6C
	global	_ep

	DABS	1,512,32,_ep
	global	_ep0out_buffer

	DABS	1,640,8,_ep0out_buffer
	global	_ep0in_buffer

	DABS	1,648,8,_ep0in_buffer
	global	_ep2in_buffer

	DABS	1,664,8,_ep2in_buffer
	global	_ep1in_buffer

	DABS	1,680,64,_ep1in_buffer
	global	_ep3out_buffer

	DABS	1,672,8,_ep3out_buffer
; #config settings
	file	"usbtin.as"
	line	#
psect	cinit,class=CODE,delta=1,reloc=2
global __pcinit
__pcinit:
global start_initialization
start_initialization:

global __initialization
__initialization:
psect	bssCOMRAM,class=COMRAM,space=1,noexec
global __pbssCOMRAM
__pbssCOMRAM:
	global	_linecoding
_linecoding:
       ds      7
	global	_usb_sendbuffer
_usb_sendbuffer:
       ds      3
	global	_clock_lastclock
_clock_lastclock:
       ds      2
	global	_clock_msticker
_clock_msticker:
       ds      2
	global	_configured
_configured:
       ds      2
	global	_usb_sendleft
_usb_sendleft:
       ds      2
	global	_dolinecoding
_dolinecoding:
       ds      1
	global	_state
_state:
       ds      1
	global	_txbuffer_bytesleft
_txbuffer_bytesleft:
       ds      1
	global	_txbuffer_writepos
_txbuffer_writepos:
       ds      1
	global	_usb_config
_usb_config:
       ds      1
	global	_usb_getchpos
_usb_getchpos:
       ds      1
	global	_usb_setaddress
_usb_setaddress:
       ds      1
psect	bssBANK1,class=BANK1,space=1,noexec
global __pbssBANK1
__pbssBANK1:
	global	_txbuffer
_txbuffer:
       ds      128
	global	_timestamping
_timestamping:
       ds      1
	line	#
psect	cinit
; Clear objects allocated to BANK1 (129 bytes)
	global __pbssBANK1
lfsr	0,__pbssBANK1
movlw	129
clear_0:
clrf	postinc0,c
decf	wreg
bnz	clear_0
; Clear objects allocated to COMRAM (25 bytes)
	global __pbssCOMRAM
lfsr	0,__pbssCOMRAM
movlw	25
clear_1:
clrf	postinc0,c
decf	wreg
bnz	clear_1
psect cinit,class=CODE,delta=1
global end_of_initialization,__end_of__initialization

;End of C runtime variable initialization code

end_of_initialization:
__end_of__initialization:	GLOBAL	__Lmediumconst
	movlw	low highword(__Lmediumconst)
	movwf	tblptru
movlb 0
goto _main	;jump to C main() function
psect	cstackBANK1,class=BANK1,space=1,noexec
global __pcstackBANK1
__pcstackBANK1:
	global	main@line
main@line:	; 100 bytes @ 0x0
	ds   100
	global	main@timestamp
main@timestamp:	; 2 bytes @ 0x64
	ds   2
	global	main@idlen
main@idlen:	; 1 bytes @ 0x66
	ds   1
	global	main@type
main@type:	; 1 bytes @ 0x67
	ds   1
	global	main@i
main@i:	; 1 bytes @ 0x68
	ds   1
	global	main@ch
main@ch:	; 1 bytes @ 0x69
	ds   1
	global	main@linepos
main@linepos:	; 1 bytes @ 0x6A
	ds   1
	global	main@canmsg
main@canmsg:	; 14 bytes @ 0x6B
	ds   14
psect	cstackBANK0,class=BANK0,space=1,noexec
global __pcstackBANK0
__pcstackBANK0:
	global	parseLine@value
parseLine@value:	; 1 bytes @ 0x0
	ds   1
	global	parseLine@status
parseLine@status:	; 1 bytes @ 0x1
	ds   1
	global	parseLine@cnf1
parseLine@cnf1:	; 4 bytes @ 0x2
	ds   4
	global	parseLine@cnf2
parseLine@cnf2:	; 4 bytes @ 0x6
	ds   4
	global	parseLine@cnf3
parseLine@cnf3:	; 4 bytes @ 0xA
	ds   4
	global	parseLine@address
parseLine@address:	; 4 bytes @ 0xE
	ds   4
	global	parseLine@address_440
parseLine@address_440:	; 4 bytes @ 0x12
	ds   4
	global	parseLine@data
parseLine@data:	; 4 bytes @ 0x16
	ds   4
	global	parseLine@stamping
parseLine@stamping:	; 4 bytes @ 0x1A
	ds   4
	global	parseLine@am0
parseLine@am0:	; 4 bytes @ 0x1E
	ds   4
	global	parseLine@am1
parseLine@am1:	; 4 bytes @ 0x22
	ds   4
	global	parseLine@am2
parseLine@am2:	; 4 bytes @ 0x26
	ds   4
	global	parseLine@am3
parseLine@am3:	; 4 bytes @ 0x2A
	ds   4
	global	parseLine@ac0
parseLine@ac0:	; 4 bytes @ 0x2E
	ds   4
	global	parseLine@ac1
parseLine@ac1:	; 4 bytes @ 0x32
	ds   4
	global	parseLine@ac2
parseLine@ac2:	; 4 bytes @ 0x36
	ds   4
	global	parseLine@ac3
parseLine@ac3:	; 4 bytes @ 0x3A
	ds   4
	global	parseLine@flags
parseLine@flags:	; 1 bytes @ 0x3E
	ds   1
	global	parseLine@result
parseLine@result:	; 1 bytes @ 0x3F
	ds   1
psect	cstackCOMRAM,class=COMRAM,space=1,noexec
global __pcstackCOMRAM
__pcstackCOMRAM:
?_clock_reset:	; 0 bytes @ 0x0
??_clock_reset:	; 0 bytes @ 0x0
?_usb_putstr:	; 0 bytes @ 0x0
?_usb_putch:	; 0 bytes @ 0x0
?_mcp2515_init:	; 0 bytes @ 0x0
?_usb_init:	; 0 bytes @ 0x0
??_usb_init:	; 0 bytes @ 0x0
?_usb_process:	; 0 bytes @ 0x0
??_usb_getch:	; 0 bytes @ 0x0
??_usb_chReceived:	; 0 bytes @ 0x0
?_clock_init:	; 0 bytes @ 0x0
??_clock_init:	; 0 bytes @ 0x0
?_clock_process:	; 0 bytes @ 0x0
??_clock_process:	; 0 bytes @ 0x0
?_sendByteHex:	; 0 bytes @ 0x0
?_main:	; 0 bytes @ 0x0
??_spi_transmit:	; 0 bytes @ 0x0
?_usb_sendProcess:	; 0 bytes @ 0x0
??_usb_sendProcess:	; 0 bytes @ 0x0
?_usb_txprocess:	; 0 bytes @ 0x0
??_usb_txprocess:	; 0 bytes @ 0x0
?_mcp2515_send_message:	; 1 bytes @ 0x0
?_mcp2515_read_register:	; 1 bytes @ 0x0
?_usb_getch:	; 1 bytes @ 0x0
?_usb_chReceived:	; 1 bytes @ 0x0
?_parseHex:	; 1 bytes @ 0x0
?_spi_transmit:	; 1 bytes @ 0x0
?_mcp2515_read_status:	; 1 bytes @ 0x0
?_mcp2515_rx_status:	; 1 bytes @ 0x0
	global	?_clock_getMS
?_clock_getMS:	; 2 bytes @ 0x0
	global	spi_transmit@c
spi_transmit@c:	; 1 bytes @ 0x0
	global	usb_txprocess@i
usb_txprocess@i:	; 1 bytes @ 0x0
	global	usb_getch@ch
usb_getch@ch:	; 1 bytes @ 0x0
	global	parseHex@line
parseHex@line:	; 2 bytes @ 0x0
	ds   1
??_mcp2515_read_register:	; 0 bytes @ 0x1
?_mcp2515_write_register:	; 0 bytes @ 0x1
?_mcp2515_bit_modify:	; 0 bytes @ 0x1
??_mcp2515_read_status:	; 0 bytes @ 0x1
??_mcp2515_rx_status:	; 0 bytes @ 0x1
	global	mcp2515_write_register@data
mcp2515_write_register@data:	; 1 bytes @ 0x1
	global	mcp2515_read_register@address
mcp2515_read_register@address:	; 1 bytes @ 0x1
	global	mcp2515_bit_modify@mask
mcp2515_bit_modify@mask:	; 1 bytes @ 0x1
	global	mcp2515_read_status@status
mcp2515_read_status@status:	; 1 bytes @ 0x1
	global	mcp2515_rx_status@status
mcp2515_rx_status@status:	; 1 bytes @ 0x1
	global	usb_txprocess@readpos
usb_txprocess@readpos:	; 1 bytes @ 0x1
	ds   1
??_mcp2515_send_message:	; 0 bytes @ 0x2
??_mcp2515_write_register:	; 0 bytes @ 0x2
??_clock_getMS:	; 0 bytes @ 0x2
	global	parseHex@len
parseHex@len:	; 1 bytes @ 0x2
	global	mcp2515_write_register@address
mcp2515_write_register@address:	; 1 bytes @ 0x2
	global	mcp2515_read_register@data
mcp2515_read_register@data:	; 1 bytes @ 0x2
	global	mcp2515_bit_modify@data
mcp2515_bit_modify@data:	; 1 bytes @ 0x2
	global	usb_sendProcess@i
usb_sendProcess@i:	; 1 bytes @ 0x2
	global	usb_txprocess@count
usb_txprocess@count:	; 1 bytes @ 0x2
	ds   1
?_mcp2515_set_bittiming:	; 0 bytes @ 0x3
??_mcp2515_bit_modify:	; 0 bytes @ 0x3
?_mcp2515_set_SJA1000_filter_mask:	; 0 bytes @ 0x3
?_mcp2515_set_SJA1000_filter_code:	; 0 bytes @ 0x3
??_mcp2515_init:	; 0 bytes @ 0x3
	global	parseHex@value
parseHex@value:	; 1 bytes @ 0x3
	global	mcp2515_bit_modify@address
mcp2515_bit_modify@address:	; 1 bytes @ 0x3
	global	mcp2515_init@dummy
mcp2515_init@dummy:	; 1 bytes @ 0x3
	global	mcp2515_set_SJA1000_filter_mask@amr1
mcp2515_set_SJA1000_filter_mask@amr1:	; 1 bytes @ 0x3
	global	mcp2515_set_SJA1000_filter_code@acr1
mcp2515_set_SJA1000_filter_code@acr1:	; 1 bytes @ 0x3
	global	mcp2515_set_bittiming@cnf2
mcp2515_set_bittiming@cnf2:	; 1 bytes @ 0x3
	global	usb_sendProcess@length
usb_sendProcess@length:	; 2 bytes @ 0x3
	ds   1
??_parseHex:	; 0 bytes @ 0x4
?_mcp2515_receive_message:	; 1 bytes @ 0x4
	global	mcp2515_set_SJA1000_filter_mask@amr2
mcp2515_set_SJA1000_filter_mask@amr2:	; 1 bytes @ 0x4
	global	mcp2515_set_SJA1000_filter_code@acr2
mcp2515_set_SJA1000_filter_code@acr2:	; 1 bytes @ 0x4
	global	mcp2515_set_bittiming@cnf3
mcp2515_set_bittiming@cnf3:	; 1 bytes @ 0x4
	global	mcp2515_receive_message@p_canmsg
mcp2515_receive_message@p_canmsg:	; 2 bytes @ 0x4
	ds   1
??_mcp2515_set_bittiming:	; 0 bytes @ 0x5
?_usb_loadDescriptor:	; 1 bytes @ 0x5
	global	mcp2515_set_SJA1000_filter_mask@amr3
mcp2515_set_SJA1000_filter_mask@amr3:	; 1 bytes @ 0x5
	global	mcp2515_set_SJA1000_filter_code@acr3
mcp2515_set_SJA1000_filter_code@acr3:	; 1 bytes @ 0x5
	global	mcp2515_set_bittiming@cnf1
mcp2515_set_bittiming@cnf1:	; 1 bytes @ 0x5
	global	usb_loadDescriptor@size
usb_loadDescriptor@size:	; 2 bytes @ 0x5
	ds   1
??_mcp2515_set_SJA1000_filter_mask:	; 0 bytes @ 0x6
??_mcp2515_set_SJA1000_filter_code:	; 0 bytes @ 0x6
??_mcp2515_receive_message:	; 0 bytes @ 0x6
	global	mcp2515_set_SJA1000_filter_mask@amr0
mcp2515_set_SJA1000_filter_mask@amr0:	; 1 bytes @ 0x6
	global	mcp2515_set_SJA1000_filter_code@acr0
mcp2515_set_SJA1000_filter_code@acr0:	; 1 bytes @ 0x6
	ds   1
	global	usb_loadDescriptor@length
usb_loadDescriptor@length:	; 2 bytes @ 0x7
	ds   2
??_usb_loadDescriptor:	; 0 bytes @ 0x9
	global	usb_loadDescriptor@descbuffer
usb_loadDescriptor@descbuffer:	; 1 bytes @ 0x9
	ds   1
?_usb_handleDescriptorRequest:	; 1 bytes @ 0xA
	global	mcp2515_send_message@i
mcp2515_send_message@i:	; 1 bytes @ 0xA
	global	usb_handleDescriptorRequest@index
usb_handleDescriptorRequest@index:	; 1 bytes @ 0xA
	ds   1
	global	mcp2515_send_message@status
mcp2515_send_message@status:	; 1 bytes @ 0xB
	global	usb_handleDescriptorRequest@length
usb_handleDescriptorRequest@length:	; 2 bytes @ 0xB
	global	mcp2515_receive_message@temp
mcp2515_receive_message@temp:	; 4 bytes @ 0xB
	ds   1
	global	mcp2515_send_message@address
mcp2515_send_message@address:	; 1 bytes @ 0xC
	ds   1
??_usb_handleDescriptorRequest:	; 0 bytes @ 0xD
	global	mcp2515_send_message@p_canmsg
mcp2515_send_message@p_canmsg:	; 1 bytes @ 0xD
	global	usb_handleDescriptorRequest@type
usb_handleDescriptorRequest@type:	; 1 bytes @ 0xD
	ds   1
??_usb_process:	; 0 bytes @ 0xE
?_transmitStd:	; 1 bytes @ 0xE
	global	transmitStd@line
transmitStd@line:	; 2 bytes @ 0xE
	ds   1
	global	mcp2515_receive_message@i
mcp2515_receive_message@i:	; 1 bytes @ 0xF
	global	usb_process@i
usb_process@i:	; 1 bytes @ 0xF
	ds   1
??_transmitStd:	; 0 bytes @ 0x10
	global	mcp2515_receive_message@address
mcp2515_receive_message@address:	; 1 bytes @ 0x10
	global	usb_process@i_1029
usb_process@i_1029:	; 1 bytes @ 0x10
	ds   1
	global	mcp2515_receive_message@status
mcp2515_receive_message@status:	; 1 bytes @ 0x11
	global	usb_process@i_1030
usb_process@i_1030:	; 1 bytes @ 0x11
	ds   1
??_usb_putch:	; 0 bytes @ 0x12
	global	_transmitStd$428
_transmitStd$428:	; 1 bytes @ 0x12
	global	usb_putch@ch
usb_putch@ch:	; 1 bytes @ 0x12
	ds   1
??_usb_putstr:	; 0 bytes @ 0x13
	global	transmitStd@idlen
transmitStd@idlen:	; 1 bytes @ 0x13
	global	usb_putstr@s
usb_putstr@s:	; 1 bytes @ 0x13
	ds   1
?_sendHex:	; 0 bytes @ 0x14
	global	transmitStd@i
transmitStd@i:	; 1 bytes @ 0x14
	global	sendHex@value
sendHex@value:	; 4 bytes @ 0x14
	ds   1
	global	transmitStd@temp
transmitStd@temp:	; 4 bytes @ 0x15
	ds   3
	global	sendHex@len
sendHex@len:	; 1 bytes @ 0x18
	ds   1
??_sendHex:	; 0 bytes @ 0x19
	global	transmitStd@canmsg
transmitStd@canmsg:	; 14 bytes @ 0x19
	ds   1
	global	sendHex@s
sendHex@s:	; 20 bytes @ 0x1A
	ds   20
	global	sendHex@hex
sendHex@hex:	; 1 bytes @ 0x2E
	ds   1
??_sendByteHex:	; 0 bytes @ 0x2F
	global	sendByteHex@value
sendByteHex@value:	; 1 bytes @ 0x2F
	ds   1
?_parseLine:	; 0 bytes @ 0x30
	global	parseLine@line
parseLine@line:	; 2 bytes @ 0x30
	ds   2
??_parseLine:	; 0 bytes @ 0x32
	ds   1
??_main:	; 0 bytes @ 0x33
	ds   2
;!
;!Data Sizes:
;!    Strings     0
;!    Constant    157
;!    Data        0
;!    BSS         154
;!    Persistent  0
;!    Stack       0
;!
;!Auto Spaces:
;!    Space          Size  Autos    Used
;!    COMRAM           95     53      78
;!    BANK0           160     64      64
;!    BANK1           256    121     250
;!    BANK2hh          24      0       0
;!    BANK2hl           8      0       0
;!    BANK2l           96      0       0
;!    BANK3           256      0       0
;!    BANK4           256      0       0
;!    BANK5           256      0       0
;!    BANK6           256      0       0
;!    BANK7           256      0       0

;!
;!Pointer List with Targets:
;!
;!    usb_putstr@s	PTR unsigned char  size(1) Largest target is 20
;!		 -> sendHex@s(COMRAM[20]), 
;!
;!    usb_loadDescriptor@descbuffer	PTR const unsigned char  size(1) Largest target is 67
;!		 -> usb_string_product(CODE[14]), usb_string_manuf(CODE[54]), usb_string_0(CODE[4]), usb_config_desc(CODE[67]), 
;!		 -> usb_dev_desc(CODE[18]), 
;!
;!    usb_sendbuffer	PTR const unsigned char  size(3) Largest target is 32767
;!		 -> usb_string_product(CODE[14]), usb_string_manuf(CODE[54]), usb_string_0(CODE[4]), usb_config_desc(CODE[67]), 
;!		 -> usb_dev_desc(CODE[18]), ROM(CODE[32767]), 
;!
;!    mcp2515_receive_message@p_canmsg	PTR struct . size(2) Largest target is 14
;!		 -> main@canmsg(BANK1[14]), 
;!
;!    mcp2515_send_message@p_canmsg	PTR struct . size(1) Largest target is 14
;!		 -> transmitStd@canmsg(COMRAM[14]), 
;!
;!    parseLine@line	PTR unsigned char  size(2) Largest target is 100
;!		 -> main@line(BANK1[100]), 
;!
;!    transmitStd@line	PTR unsigned char  size(2) Largest target is 100
;!		 -> main@line(BANK1[100]), 
;!
;!    parseHex@line	PTR unsigned char  size(2) Largest target is 100
;!		 -> main@line(BANK1[100]), 
;!
;!    parseHex@value	PTR unsigned long  size(1) Largest target is 4
;!		 -> parseLine@ac3(BANK0[4]), parseLine@ac2(BANK0[4]), parseLine@ac1(BANK0[4]), parseLine@ac0(BANK0[4]), 
;!		 -> parseLine@am3(BANK0[4]), parseLine@am2(BANK0[4]), parseLine@am1(BANK0[4]), parseLine@am0(BANK0[4]), 
;!		 -> parseLine@stamping(BANK0[4]), parseLine@data(BANK0[4]), parseLine@address_440(BANK0[4]), parseLine@address(BANK0[4]), 
;!		 -> parseLine@cnf3(BANK0[4]), parseLine@cnf2(BANK0[4]), parseLine@cnf1(BANK0[4]), transmitStd@temp(COMRAM[4]), 
;!


;!
;!Critical Paths under _main in COMRAM
;!
;!    _main->_parseLine
;!    _parseLine->_sendByteHex
;!    _transmitStd->_mcp2515_send_message
;!    _mcp2515_send_message->_mcp2515_read_status
;!    _mcp2515_read_status->_spi_transmit
;!    _sendByteHex->_sendHex
;!    _sendHex->_usb_putstr
;!    _usb_putstr->_usb_putch
;!    _usb_putch->_usb_process
;!    _usb_process->_usb_handleDescriptorRequest
;!    _usb_handleDescriptorRequest->_usb_loadDescriptor
;!    _usb_loadDescriptor->_usb_sendProcess
;!    _mcp2515_set_bittiming->_mcp2515_write_register
;!    _mcp2515_set_SJA1000_filter_mask->_mcp2515_write_register
;!    _mcp2515_set_SJA1000_filter_code->_mcp2515_write_register
;!    _mcp2515_read_register->_spi_transmit
;!    _mcp2515_receive_message->_mcp2515_bit_modify
;!    _mcp2515_rx_status->_spi_transmit
;!    _mcp2515_bit_modify->_spi_transmit
;!    _mcp2515_init->_mcp2515_write_register
;!    _mcp2515_write_register->_spi_transmit
;!
;!Critical Paths under _main in BANK0
;!
;!    _main->_parseLine
;!
;!Critical Paths under _main in BANK1
;!
;!    None.
;!
;!Critical Paths under _main in BANK2hh
;!
;!    None.
;!
;!Critical Paths under _main in BANK2hl
;!
;!    None.
;!
;!Critical Paths under _main in BANK2l
;!
;!    None.
;!
;!Critical Paths under _main in BANK3
;!
;!    None.
;!
;!Critical Paths under _main in BANK4
;!
;!    None.
;!
;!Critical Paths under _main in BANK5
;!
;!    None.
;!
;!Critical Paths under _main in BANK6
;!
;!    None.
;!
;!Critical Paths under _main in BANK7
;!
;!    None.

;;
;;Main: autosize = 0, tempsize = 2, incstack = 0, save=0
;;

;!
;!Call Graph Tables:
;!
;! ---------------------------------------------------------------------------------
;! (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;! ---------------------------------------------------------------------------------
;! (0) _main                                               123   123      0   40375
;!                                             51 COMRAM     2     2      0
;!                                              0 BANK1    121   121      0
;!                        _clock_getMS
;!                         _clock_init
;!                      _clock_process
;!                       _mcp2515_init
;!            _mcp2515_receive_message
;!                          _parseLine
;!                            _sendHex
;!                     _usb_chReceived
;!                          _usb_getch
;!                           _usb_init
;!                        _usb_process
;!                          _usb_putch
;! ---------------------------------------------------------------------------------
;! (1) _usb_init                                             0     0      0       0
;! ---------------------------------------------------------------------------------
;! (1) _usb_getch                                            1     1      0      23
;!                                              0 COMRAM     1     1      0
;! ---------------------------------------------------------------------------------
;! (1) _usb_chReceived                                       0     0      0       0
;! ---------------------------------------------------------------------------------
;! (1) _parseLine                                           67    65      2   29293
;!                                             48 COMRAM     3     1      2
;!                                              0 BANK0     64    64      0
;!                        _clock_reset
;!                 _mcp2515_bit_modify
;!              _mcp2515_read_register
;!    _mcp2515_set_SJA1000_filter_code
;!    _mcp2515_set_SJA1000_filter_mask
;!              _mcp2515_set_bittiming
;!             _mcp2515_write_register
;!                           _parseHex
;!                        _sendByteHex
;!                            _sendHex
;!                        _transmitStd
;!                          _usb_putch
;! ---------------------------------------------------------------------------------
;! (2) _transmitStd                                         25    23      2    4525
;!                                             14 COMRAM    25    23      2
;!               _mcp2515_send_message
;!                           _parseHex
;! ---------------------------------------------------------------------------------
;! (3) _parseHex                                             8     4      4    3269
;!                                              0 COMRAM     8     4      4
;! ---------------------------------------------------------------------------------
;! (3) _mcp2515_send_message                                12    12      0     588
;!                                              2 COMRAM    12    12      0
;!                _mcp2515_read_status
;!                       _spi_transmit
;! ---------------------------------------------------------------------------------
;! (4) _mcp2515_read_status                                  1     1      0      45
;!                                              1 COMRAM     1     1      0
;!                       _spi_transmit
;! ---------------------------------------------------------------------------------
;! (2) _sendByteHex                                          1     1      0    2959
;!                                             47 COMRAM     1     1      0
;!                            _sendHex
;! ---------------------------------------------------------------------------------
;! (1) _sendHex                                             27    22      5    2937
;!                                             20 COMRAM    27    22      5
;!                         _usb_putstr
;! ---------------------------------------------------------------------------------
;! (2) _usb_putstr                                           1     1      0    1961
;!                                             19 COMRAM     1     1      0
;!                          _usb_putch
;! ---------------------------------------------------------------------------------
;! (1) _usb_putch                                            1     1      0    1886
;!                                             18 COMRAM     1     1      0
;!                        _usb_process
;! ---------------------------------------------------------------------------------
;! (2) _usb_process                                          4     4      0    1864
;!                                             14 COMRAM     4     4      0
;!        _usb_handleDescriptorRequest
;!                    _usb_sendProcess
;!                      _usb_txprocess
;! ---------------------------------------------------------------------------------
;! (3) _usb_txprocess                                        3     3      0     183
;!                                              0 COMRAM     3     3      0
;! ---------------------------------------------------------------------------------
;! (3) _usb_handleDescriptorRequest                          4     1      3    1319
;!                                             10 COMRAM     4     1      3
;!                 _usb_loadDescriptor
;! ---------------------------------------------------------------------------------
;! (4) _usb_loadDescriptor                                   5     1      4    1037
;!                                              5 COMRAM     5     1      4
;!                    _usb_sendProcess
;! ---------------------------------------------------------------------------------
;! (3) _usb_sendProcess                                      5     5      0     114
;!                                              0 COMRAM     5     5      0
;! ---------------------------------------------------------------------------------
;! (2) _mcp2515_set_bittiming                                3     1      2    3716
;!                                              3 COMRAM     3     1      2
;!             _mcp2515_write_register
;! ---------------------------------------------------------------------------------
;! (2) _mcp2515_set_SJA1000_filter_mask                      4     1      3    2650
;!                                              3 COMRAM     4     1      3
;!             _mcp2515_write_register
;! ---------------------------------------------------------------------------------
;! (2) _mcp2515_set_SJA1000_filter_code                      4     1      3    2738
;!                                              3 COMRAM     4     1      3
;!             _mcp2515_write_register
;! ---------------------------------------------------------------------------------
;! (2) _mcp2515_read_register                                2     2      0      67
;!                                              1 COMRAM     2     2      0
;!                       _spi_transmit
;! ---------------------------------------------------------------------------------
;! (1) _mcp2515_receive_message                             14    12      2    1387
;!                                              4 COMRAM    14    12      2
;!                 _mcp2515_bit_modify
;!                  _mcp2515_rx_status
;!                       _spi_transmit
;! ---------------------------------------------------------------------------------
;! (2) _mcp2515_rx_status                                    1     1      0      45
;!                                              1 COMRAM     1     1      0
;!                       _spi_transmit
;! ---------------------------------------------------------------------------------
;! (2) _mcp2515_bit_modify                                   3     1      2     728
;!                                              1 COMRAM     3     1      2
;!                       _spi_transmit
;! ---------------------------------------------------------------------------------
;! (1) _mcp2515_init                                         1     1      0    2438
;!                                              3 COMRAM     1     1      0
;!             _mcp2515_write_register
;!                       _spi_transmit
;! ---------------------------------------------------------------------------------
;! (2) _mcp2515_write_register                               2     1      1    2370
;!                                              1 COMRAM     2     1      1
;!                       _spi_transmit
;! ---------------------------------------------------------------------------------
;! (4) _spi_transmit                                         1     1      0      22
;!                                              0 COMRAM     1     1      0
;! ---------------------------------------------------------------------------------
;! (1) _clock_process                                        2     2      0       0
;!                                              0 COMRAM     2     2      0
;! ---------------------------------------------------------------------------------
;! (1) _clock_init                                           0     0      0       0
;!                        _clock_reset
;! ---------------------------------------------------------------------------------
;! (2) _clock_reset                                          0     0      0       0
;! ---------------------------------------------------------------------------------
;! (1) _clock_getMS                                          2     0      2       0
;!                                              0 COMRAM     2     0      2
;! ---------------------------------------------------------------------------------
;! Estimated maximum stack depth 4
;! ---------------------------------------------------------------------------------
;!
;! Call Graph Graphs:
;!
;! _main (ROOT)
;!   _clock_getMS
;!   _clock_init
;!     _clock_reset
;!   _clock_process
;!   _mcp2515_init
;!     _mcp2515_write_register
;!       _spi_transmit
;!     _spi_transmit
;!   _mcp2515_receive_message
;!     _mcp2515_bit_modify
;!       _spi_transmit
;!     _mcp2515_rx_status
;!       _spi_transmit
;!     _spi_transmit
;!   _parseLine
;!     _clock_reset
;!     _mcp2515_bit_modify
;!       _spi_transmit
;!     _mcp2515_read_register
;!       _spi_transmit
;!     _mcp2515_set_SJA1000_filter_code
;!       _mcp2515_write_register
;!         _spi_transmit
;!     _mcp2515_set_SJA1000_filter_mask
;!       _mcp2515_write_register
;!         _spi_transmit
;!     _mcp2515_set_bittiming
;!       _mcp2515_write_register
;!         _spi_transmit
;!     _mcp2515_write_register
;!       _spi_transmit
;!     _parseHex
;!     _sendByteHex
;!       _sendHex
;!         _usb_putstr
;!           _usb_putch
;!             _usb_process
;!               _usb_handleDescriptorRequest
;!                 _usb_loadDescriptor
;!                   _usb_sendProcess
;!               _usb_sendProcess
;!               _usb_txprocess
;!     _sendHex
;!       _usb_putstr
;!         _usb_putch
;!           _usb_process
;!             _usb_handleDescriptorRequest
;!               _usb_loadDescriptor
;!                 _usb_sendProcess
;!             _usb_sendProcess
;!             _usb_txprocess
;!     _transmitStd
;!       _mcp2515_send_message
;!         _mcp2515_read_status
;!           _spi_transmit
;!         _spi_transmit
;!       _parseHex
;!     _usb_putch
;!       _usb_process
;!         _usb_handleDescriptorRequest
;!           _usb_loadDescriptor
;!             _usb_sendProcess
;!         _usb_sendProcess
;!         _usb_txprocess
;!   _sendHex
;!     _usb_putstr
;!       _usb_putch
;!         _usb_process
;!           _usb_handleDescriptorRequest
;!             _usb_loadDescriptor
;!               _usb_sendProcess
;!           _usb_sendProcess
;!           _usb_txprocess
;!   _usb_chReceived
;!   _usb_getch
;!   _usb_init
;!   _usb_process
;!     _usb_handleDescriptorRequest
;!       _usb_loadDescriptor
;!         _usb_sendProcess
;!     _usb_sendProcess
;!     _usb_txprocess
;!   _usb_putch
;!     _usb_process
;!       _usb_handleDescriptorRequest
;!         _usb_loadDescriptor
;!           _usb_sendProcess
;!       _usb_sendProcess
;!       _usb_txprocess
;!

;! Address spaces:

;!Name               Size   Autos  Total    Cost      Usage
;!BIGRAMhhh          518      0       0      25        0.0%
;!BIGRAMl            1FF      0       0      28        0.0%
;!EEDATA             100      0       0       0        0.0%
;!BITBANK7           100      0       0      23        0.0%
;!BANK7              100      0       0      24        0.0%
;!BITBANK6           100      0       0      21        0.0%
;!BANK6              100      0       0      22        0.0%
;!BITBANK5           100      0       0      18        0.0%
;!BANK5              100      0       0      19        0.0%
;!BITBANK4           100      0       0      16        0.0%
;!BANK4              100      0       0      17        0.0%
;!BITBANK3           100      0       0      14        0.0%
;!BANK3              100      0       0      15        0.0%
;!BITBANK1           100      0       0       6        0.0%
;!BANK1              100     79      FA       7       97.7%
;!BITBANK0            A0      0       0       4        0.0%
;!BANK0               A0     40      40       5       40.0%
;!BIGRAMhl            60      0       0      27        0.0%
;!BITBANK2l           60      0       0      10        0.0%
;!BANK2l              60      0       0      13        0.0%
;!BITCOMRAM           5F      0       0       0        0.0%
;!COMRAM              5F     35      4E       1       82.1%
;!BANK2hh             18      0       0      11        0.0%
;!BITBANK2hh          18      0       0       8        0.0%
;!BIGRAMhhl            8      0       0      26        0.0%
;!BANK2hl              8      0       0      12        0.0%
;!BITBANK2hl           8      0       0       9        0.0%
;!BITSFR               0      0       0      40        0.0%
;!SFR                  0      0       0      40        0.0%
;!STACK                0      0       0       2        0.0%
;!NULL                 0      0       0       0        0.0%
;!ABS                  0      0     188      20        0.0%
;!DATA                 0      0     188       3        0.0%
;!CODE                 0      0       0       0        0.0%

	global	_main

;; *************** function _main *****************
;; Defined at:
;;		line 354 in file "D:\Projects\USBtin\firmware\source\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  i               1  104[BANK1 ] unsigned char 
;;  timestamp       2  100[BANK1 ] unsigned short 
;;  type            1  103[BANK1 ] unsigned char 
;;  idlen           1  102[BANK1 ] unsigned char 
;;  canmsg         14  107[BANK1 ] struct .
;;  ch              1  105[BANK1 ] unsigned char 
;;  line          100    0[BANK1 ] unsigned char [100]
;;  linepos         1  106[BANK1 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr1l, fsr1h, fsr2l, fsr2h, status,2, status,0, tblptrl, tblptrh, tblptru, prodl, prodh, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0     121       0       0       0       0       0       0       0       0
;;      Temps:          2       0       0       0       0       0       0       0       0       0       0
;;      Totals:         2       0     121       0       0       0       0       0       0       0       0
;;Total ram usage:      123 bytes
;; Hardware stack levels required when called:    9
;; This function calls:
;;		_clock_getMS
;;		_clock_init
;;		_clock_process
;;		_mcp2515_init
;;		_mcp2515_receive_message
;;		_parseLine
;;		_sendHex
;;		_usb_chReceived
;;		_usb_getch
;;		_usb_init
;;		_usb_process
;;		_usb_putch
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	text0,class=CODE,space=0,reloc=2
	file	"D:\Projects\USBtin\firmware\source\main.c"
	line	354
global __ptext0
__ptext0:
psect	text0
	file	"D:\Projects\USBtin\firmware\source\main.c"
	line	354
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:
;incstack = 0
	opt	stack 22
	line	357
	
l2704:
;main.c: 357: mcp2515_init();
	call	_mcp2515_init	;wreg free
	line	360
	
l2706:
;main.c: 360: OSCCON = 0x30;
	movlw	low(030h)
	movwf	((c:4051)),c	;volatile
	line	365
	
l2708:
;main.c: 365: TRISBbits.TRISB5 = 0;
	bcf	((c:3987)),c,5	;volatile
	line	366
	
l2710:
;main.c: 366: LATBbits.LATB5 = 0;
	bcf	((c:3978)),c,5	;volatile
	line	369
	
l2712:
;main.c: 369: clock_init();
	call	_clock_init	;wreg free
	line	370
	
l2714:
;main.c: 370: usb_init();
	call	_usb_init	;wreg free
	line	373
	
l2716:; BSR set to: 2

;main.c: 372: char line[100];
;main.c: 373: unsigned char linepos = 0;
	movlb	1	; () banked
	clrf	((main@linepos))&0ffh
	line	378
	
l2718:
;main.c: 378: usb_process();
	call	_usb_process	;wreg free
	line	379
	
l2720:
;main.c: 379: clock_process();
	call	_clock_process	;wreg free
	line	382
;main.c: 382: while (usb_chReceived()) {
	goto	l2740
	line	383
	
l2722:; BSR set to: 2

;main.c: 383: unsigned char ch = usb_getch();
	call	_usb_getch	;wreg free
	movlb	1	; () banked
	movwf	((main@ch))&0ffh
	line	385
	
l2724:; BSR set to: 1

;main.c: 385: if (ch == 13) {
	movf	((main@ch))&0ffh,w
	xorlw	13

	btfss	status,2
	goto	u2281
	goto	u2280
u2281:
	goto	l2732
u2280:
	line	386
	
l2726:; BSR set to: 1

;main.c: 386: line[linepos] = 0;
	movlw	low(main@line)
	addwf	((main@linepos))&0ffh,w
	movwf	c:fsr2l
	clrf	1+c:fsr2l
	movlw	high(main@line)
	addwfc	1+c:fsr2l
	movlw	low(0)
	movwf	indf2
	line	387
	
l2728:; BSR set to: 1

;main.c: 387: parseLine(line);
		movlw	high(main@line)
	movwf	((c:parseLine@line+1)),c
	movlw	low(main@line)
	movwf	((c:parseLine@line)),c

	call	_parseLine	;wreg free
	line	388
	
l2730:
;main.c: 388: linepos = 0;
	movlb	1	; () banked
	clrf	((main@linepos))&0ffh
	line	389
;main.c: 389: } else if (ch != 10) {
	goto	l2740
	
l2732:; BSR set to: 1

	movf	((main@ch))&0ffh,w
	xorlw	10

	btfsc	status,2
	goto	u2291
	goto	u2290
u2291:
	goto	l2740
u2290:
	line	390
	
l2734:; BSR set to: 1

;main.c: 390: line[linepos] = ch;
	movlw	low(main@line)
	addwf	((main@linepos))&0ffh,w
	movwf	c:fsr2l
	clrf	1+c:fsr2l
	movlw	high(main@line)
	addwfc	1+c:fsr2l
	movff	(main@ch),indf2

	line	391
	
l2736:; BSR set to: 1

;main.c: 391: if (linepos < 100 - 1) linepos++;
	movlw	(063h)&0ffh
	subwf	((main@linepos))&0ffh,w
	btfsc	status,0
	goto	u2301
	goto	u2300
u2301:
	goto	l182
u2300:
	
l2738:; BSR set to: 1

	incf	((main@linepos))&0ffh
	goto	l2740
	line	394
	
l182:; BSR set to: 1

	line	382
	
l2740:
	call	_usb_chReceived	;wreg free
	iorlw	0
	btfss	status,2
	goto	u2311
	goto	u2310
u2311:
	goto	l2722
u2310:
	line	397
	
l2742:; BSR set to: 2

;main.c: 392: }
;main.c: 394: }
;main.c: 397: if ((state != 0) && (!PORTCbits.RC2)) {
	movf	((c:_state)),c,w	;volatile
	btfsc	status,2
	goto	u2321
	goto	u2320
u2321:
	goto	l2786
u2320:
	
l2744:; BSR set to: 2

	btfsc	((c:3970)),c,2	;volatile
	goto	u2331
	goto	u2330
u2331:
	goto	l2786
u2330:
	goto	l2784
	line	404
	
l2748:
;main.c: 402: char type;
;main.c: 403: unsigned char idlen;
;main.c: 404: unsigned short timestamp = clock_getMS();
	call	_clock_getMS	;wreg free
	movff	0+?_clock_getMS,(main@timestamp)
	movff	1+?_clock_getMS,(main@timestamp+1)
	line	406
	
l2750:
;main.c: 406: if (canmsg.flags.rtr) type = 'r';
	movlb	1	; () banked
	btfss	(0+(main@canmsg+04h))&0ffh,0
	goto	u2341
	goto	u2340
u2341:
	goto	l2754
u2340:
	
l2752:; BSR set to: 1

	movlw	low(072h)
	movwf	((main@type))&0ffh
	goto	l2756
	line	407
	
l2754:; BSR set to: 1

;main.c: 407: else type = 't';
	movlw	low(074h)
	movwf	((main@type))&0ffh
	line	409
	
l2756:; BSR set to: 1

;main.c: 409: if (canmsg.flags.extended) {
	btfss	(0+(main@canmsg+04h))&0ffh,1
	goto	u2351
	goto	u2350
u2351:
	goto	l2762
u2350:
	line	410
	
l2758:; BSR set to: 1

;main.c: 410: type -= 'a' - 'A';
	movlw	(020h)&0ffh
	subwf	((main@type))&0ffh
	line	411
	
l2760:; BSR set to: 1

;main.c: 411: idlen = 8;
	movlw	low(08h)
	movwf	((main@idlen))&0ffh
	line	412
;main.c: 412: } else {
	goto	l2764
	line	413
	
l2762:; BSR set to: 1

;main.c: 413: idlen = 3;
	movlw	low(03h)
	movwf	((main@idlen))&0ffh
	line	416
	
l2764:; BSR set to: 1

;main.c: 414: }
;main.c: 416: usb_putch(type);
	movf	((main@type))&0ffh,w
	
	call	_usb_putch
	line	417
	
l2766:
;main.c: 417: sendHex(canmsg.id, idlen);
	movff	(main@canmsg),(c:sendHex@value)
	movff	(main@canmsg+1),(c:sendHex@value+1)
	movff	(main@canmsg+2),(c:sendHex@value+2)
	movff	(main@canmsg+3),(c:sendHex@value+3)
	movff	(main@idlen),(c:sendHex@len)
	call	_sendHex	;wreg free
	line	419
	
l2768:
;main.c: 419: sendHex(canmsg.length, 1);
	movlb	1	; () banked
	movf	(0+(main@canmsg+05h))&0ffh,w
	movwf	((c:sendHex@value)),c
	clrf	((c:sendHex@value+1)),c
	clrf	((c:sendHex@value+2)),c
	clrf	((c:sendHex@value+3)),c

	movlw	low(01h)
	movwf	((c:sendHex@len)),c
	call	_sendHex	;wreg free
	line	421
	
l2770:
;main.c: 421: if (!canmsg.flags.rtr) {
	movlb	1	; () banked
	btfsc	(0+(main@canmsg+04h))&0ffh,0
	goto	u2361
	goto	u2360
u2361:
	goto	l2780
u2360:
	line	423
	
l2772:; BSR set to: 1

;main.c: 422: unsigned char i;
;main.c: 423: for (i = 0; i < canmsg.length; i++) {
	clrf	((main@i))&0ffh
	goto	l2778
	line	424
	
l2774:; BSR set to: 1

;main.c: 424: sendHex(canmsg.data[i], 2);
	movf	((main@i))&0ffh,w
	movwf	(??_main+0+0)&0ffh,c
	clrf	(??_main+0+0+1)&0ffh,c

	movlw	06h
	addwf	(??_main+0+0),c
	movlw	0
	addwfc	(??_main+0+1),c
	movlw	low(main@canmsg)
	addwf	(??_main+0+0),c,w
	movwf	c:fsr2l
	movlw	high(main@canmsg)
	addwfc	(??_main+0+1),c,w
	movwf	1+c:fsr2l
	movf	indf2,w
	movwf	((c:sendHex@value)),c
	clrf	((c:sendHex@value+1)),c
	clrf	((c:sendHex@value+2)),c
	clrf	((c:sendHex@value+3)),c

	movlw	low(02h)
	movwf	((c:sendHex@len)),c
	call	_sendHex	;wreg free
	line	423
	
l2776:
	movlb	1	; () banked
	incf	((main@i))&0ffh
	
l2778:; BSR set to: 1

	movf	(0+(main@canmsg+05h))&0ffh,w
	subwf	((main@i))&0ffh,w
	btfss	status,0
	goto	u2371
	goto	u2370
u2371:
	goto	l2774
u2370:
	line	428
	
l2780:; BSR set to: 1

;main.c: 425: }
;main.c: 426: }
;main.c: 428: if (timestamping) {
	movf	((_timestamping))&0ffh,w
	btfsc	status,2
	goto	u2381
	goto	u2380
u2381:
	goto	l197
u2380:
	line	429
	
l2782:; BSR set to: 1

;main.c: 429: sendHex(timestamp, 4);
	movff	(main@timestamp),(c:sendHex@value)
	movff	(main@timestamp+1),(c:sendHex@value+1)
	clrf	((c:sendHex@value+2)),c
	clrf	((c:sendHex@value+3)),c

	movlw	low(04h)
	movwf	((c:sendHex@len)),c
	call	_sendHex	;wreg free
	line	430
	
l197:
	line	432
;main.c: 430: }
;main.c: 432: usb_putch(13);
	movlw	(0Dh)&0ffh
	
	call	_usb_putch
	line	401
	
l2784:
	movlb	1	; () banked
		movlw	high(main@canmsg)
	movwf	((c:mcp2515_receive_message@p_canmsg+1)),c
	movlw	low(main@canmsg)
	movwf	((c:mcp2515_receive_message@p_canmsg)),c

	call	_mcp2515_receive_message	;wreg free
	iorlw	0
	btfss	status,2
	goto	u2391
	goto	u2390
u2391:
	goto	l2748
u2390:
	line	437
	
l2786:
;main.c: 433: }
;main.c: 434: }
;main.c: 437: LATBbits.LATB5 = state != 0;
	movlw	(0)&0ffh
	clrf	prodl
	cpfseq	((c:_state)),c	;volatile
	incf	prodl
	movff	prodl,??_main+0+0
	swapf	(??_main+0+0),c
	rlncf	(??_main+0+0),c
	movf	((c:3978)),c,w	;volatile
	xorwf	(??_main+0+0),c,w
	andlw	not (((1<<1)-1)<<5)
	xorwf	(??_main+0+0),c,w
	movwf	((c:3978)),c	;volatile
	line	440
	
l2788:
;main.c: 440: if (!PORTAbits.RA3) {
	btfsc	((c:3968)),c,3	;volatile
	goto	u2401
	goto	u2400
u2401:
	goto	l2718
u2400:
	line	441
	
l2790:
;main.c: 441: UCON = 0;
	clrf	((c:3949)),c	;volatile
	line	442
	
l2792:
;main.c: 442: _delay(1000);
	movlw	250
u2417:
	nop
decfsz	wreg,f
	goto	u2417

	line	443
# 443 "D:\Projects\USBtin\firmware\source\main.c"
reset ;# 
psect	text0
	goto	l2718
	global	start
	goto	start
	opt stack 0
	line	446
GLOBAL	__end_of_main
	__end_of_main:
	signat	_main,88
	global	_usb_init

;; *************** function _usb_init *****************
;; Defined at:
;;		line 419 in file "D:\Projects\USBtin\firmware\source\usb_cdc.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : F/2
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text1,class=CODE,space=0,reloc=2
	file	"D:\Projects\USBtin\firmware\source\usb_cdc.c"
	line	419
global __ptext1
__ptext1:
psect	text1
	file	"D:\Projects\USBtin\firmware\source\usb_cdc.c"
	line	419
	global	__size_of_usb_init
	__size_of_usb_init	equ	__end_of_usb_init-_usb_init
	
_usb_init:
;incstack = 0
	opt	stack 30
	line	420
	
l2622:
;usb_cdc.c: 420: ep[0].out.stat = 0x80;
	movlw	low(080h)
	movlb	2	; () banked
	movwf	((512))&0ffh	;volatile
	line	421
;usb_cdc.c: 421: ep[0].out.cnt = 0x08;
	movlw	low(08h)
	movwf	(0+(512+01h))&0ffh	;volatile
	line	422
;usb_cdc.c: 422: ep[0].out.adrl = 0x80;
	movlw	low(080h)
	movwf	(0+(512+02h))&0ffh	;volatile
	line	423
;usb_cdc.c: 423: ep[0].out.adrh = 0x02;
	movlw	low(02h)
	movwf	(0+(512+03h))&0ffh	;volatile
	line	425
	
l2624:; BSR set to: 2

;usb_cdc.c: 425: ep[0].in.stat = 0;
	clrf	(0+(512+04h))&0ffh	;volatile
	line	426
;usb_cdc.c: 426: ep[0].in.cnt = 0x08;
	movlw	low(08h)
	movwf	(0+(512+05h))&0ffh	;volatile
	line	427
;usb_cdc.c: 427: ep[0].in.adrl = 0x88;
	movlw	low(088h)
	movwf	(0+(512+06h))&0ffh	;volatile
	line	428
;usb_cdc.c: 428: ep[0].in.adrh = 0x02;
	movlw	low(02h)
	movwf	(0+(512+07h))&0ffh	;volatile
	line	430
;usb_cdc.c: 430: ep[1].in.stat = 0x40;
	movlw	low(040h)
	movwf	(0+(512+0Ch))&0ffh	;volatile
	line	431
;usb_cdc.c: 431: ep[1].in.cnt = 0x40;
	movlw	low(040h)
	movwf	(0+(512+0Dh))&0ffh	;volatile
	line	432
;usb_cdc.c: 432: ep[1].in.adrl = 0xA8;
	movlw	low(0A8h)
	movwf	(0+(512+0Eh))&0ffh	;volatile
	line	433
;usb_cdc.c: 433: ep[1].in.adrh = 0x02;
	movlw	low(02h)
	movwf	(0+(512+0Fh))&0ffh	;volatile
	line	435
	
l2626:; BSR set to: 2

;usb_cdc.c: 435: ep[2].in.stat = 0;
	clrf	(0+(512+014h))&0ffh	;volatile
	line	436
;usb_cdc.c: 436: ep[2].in.cnt = 0x08;
	movlw	low(08h)
	movwf	(0+(512+015h))&0ffh	;volatile
	line	437
;usb_cdc.c: 437: ep[2].in.adrl = 0x98;
	movlw	low(098h)
	movwf	(0+(512+016h))&0ffh	;volatile
	line	438
;usb_cdc.c: 438: ep[2].in.adrh = 0x02;
	movlw	low(02h)
	movwf	(0+(512+017h))&0ffh	;volatile
	line	440
;usb_cdc.c: 440: ep[3].out.stat = 0x80;
	movlw	low(080h)
	movwf	(0+(512+018h))&0ffh	;volatile
	line	441
;usb_cdc.c: 441: ep[3].out.cnt = 0x08;
	movlw	low(08h)
	movwf	(0+(512+019h))&0ffh	;volatile
	line	442
;usb_cdc.c: 442: ep[3].out.adrl = 0xA0;
	movlw	low(0A0h)
	movwf	(0+(512+01Ah))&0ffh	;volatile
	line	443
;usb_cdc.c: 443: ep[3].out.adrh = 0x02;
	movlw	low(02h)
	movwf	(0+(512+01Bh))&0ffh	;volatile
	line	445
;usb_cdc.c: 445: UEP0 = 0x16;
	movlw	low(016h)
	movwf	((c:3952)),c	;volatile
	line	446
;usb_cdc.c: 446: UEP1 = 0x1A;
	movlw	low(01Ah)
	movwf	((c:3953)),c	;volatile
	line	447
;usb_cdc.c: 447: UEP2 = 0x1A;
	movlw	low(01Ah)
	movwf	((c:3954)),c	;volatile
	line	448
;usb_cdc.c: 448: UEP3 = 0x1C;
	movlw	low(01Ch)
	movwf	((c:3955)),c	;volatile
	line	450
;usb_cdc.c: 450: UCFG = 0x14;
	movlw	low(014h)
	movwf	((c:3951)),c	;volatile
	line	451
;usb_cdc.c: 451: UCON = 0x08;
	movlw	low(08h)
	movwf	((c:3949)),c	;volatile
	line	452
	
l420:; BSR set to: 2

	return
	opt stack 0
GLOBAL	__end_of_usb_init
	__end_of_usb_init:
	signat	_usb_init,88
	global	_usb_getch

;; *************** function _usb_getch *****************
;; Defined at:
;;		line 403 in file "D:\Projects\USBtin\firmware\source\usb_cdc.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  ch              1    0[COMRAM] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, fsr2l, fsr2h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/2
;;		On exit  : F/2
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         1       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         1       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text2,class=CODE,space=0,reloc=2
	line	403
global __ptext2
__ptext2:
psect	text2
	file	"D:\Projects\USBtin\firmware\source\usb_cdc.c"
	line	403
	global	__size_of_usb_getch
	__size_of_usb_getch	equ	__end_of_usb_getch-_usb_getch
	
_usb_getch:; BSR set to: 2

;incstack = 0
	opt	stack 30
	line	404
	
l2628:
	line	406
;usb_cdc.c: 404: while (!usb_chReceived) {}
	
l2630:
;usb_cdc.c: 406: unsigned char ch = ep3out_buffer[usb_getchpos];
	movlb	2	; () banked
	movlw	low(672)	;volatile
	addwf	((c:_usb_getchpos)),c,w
	movwf	c:fsr2l
	clrf	1+c:fsr2l
	movlw	high(672)	;volatile
	addwfc	1+c:fsr2l
	movf	indf2,w
	movwf	((c:usb_getch@ch)),c
	line	407
	
l2632:; BSR set to: 2

;usb_cdc.c: 407: usb_getchpos++;
	incf	((c:_usb_getchpos)),c
	line	408
	
l2634:; BSR set to: 2

;usb_cdc.c: 408: if (usb_getchpos == ep[3].out.cnt) {
	movf	((c:_usb_getchpos)),c,w
	lfsr	2,512+019h	;volatile
	cpfseq	indf2
	goto	u2161
	goto	u2160
u2161:
	goto	l416
u2160:
	line	409
	
l2636:; BSR set to: 2

;usb_cdc.c: 409: ep[3].out.cnt = 0x08;
	movlw	low(08h)
	movwf	(0+(512+019h))&0ffh	;volatile
	line	410
;usb_cdc.c: 410: ep[3].out.stat = 0x80;
	movlw	low(080h)
	movwf	(0+(512+018h))&0ffh	;volatile
	line	411
	
l2638:; BSR set to: 2

;usb_cdc.c: 411: usb_getchpos = 0;
	clrf	((c:_usb_getchpos)),c
	line	412
	
l416:; BSR set to: 2

	line	413
;usb_cdc.c: 412: }
;usb_cdc.c: 413: return ch;
	movf	((c:usb_getch@ch)),c,w
	line	414
	
l417:; BSR set to: 2

	return
	opt stack 0
GLOBAL	__end_of_usb_getch
	__end_of_usb_getch:
	signat	_usb_getch,89
	global	_usb_chReceived

;; *************** function _usb_chReceived *****************
;; Defined at:
;;		line 394 in file "D:\Projects\USBtin\firmware\source\usb_cdc.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/1
;;		On exit  : F/2
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text3,class=CODE,space=0,reloc=2
	line	394
global __ptext3
__ptext3:
psect	text3
	file	"D:\Projects\USBtin\firmware\source\usb_cdc.c"
	line	394
	global	__size_of_usb_chReceived
	__size_of_usb_chReceived	equ	__end_of_usb_chReceived-_usb_chReceived
	
_usb_chReceived:; BSR set to: 2

;incstack = 0
	opt	stack 30
	line	395
	
l2642:
;usb_cdc.c: 395: return (ep[3].out.stat & 0x80) == 0;
	movlb	2	; () banked
	
	btfss	(0+(512+018h))&0ffh,(7)&7	;volatile
	goto	u2171
	goto	u2170
u2171:
	movlw	1
	goto	u2176
u2170:
	movlw	0
u2176:
	line	396
	
l410:; BSR set to: 2

	return
	opt stack 0
GLOBAL	__end_of_usb_chReceived
	__end_of_usb_chReceived:
	signat	_usb_chReceived,89
	global	_parseLine

;; *************** function _parseLine *****************
;; Defined at:
;;		line 173 in file "D:\Projects\USBtin\firmware\source\main.c"
;; Parameters:    Size  Location     Type
;;  line            2   48[COMRAM] PTR unsigned char 
;;		 -> main@line(100), 
;; Auto vars:     Size  Location     Type
;;  value           1    0[BANK0 ] unsigned char 
;;  ac3             4   58[BANK0 ] unsigned long 
;;  ac2             4   54[BANK0 ] unsigned long 
;;  ac1             4   50[BANK0 ] unsigned long 
;;  ac0             4   46[BANK0 ] unsigned long 
;;  am3             4   42[BANK0 ] unsigned long 
;;  am2             4   38[BANK0 ] unsigned long 
;;  am1             4   34[BANK0 ] unsigned long 
;;  am0             4   30[BANK0 ] unsigned long 
;;  stamping        4   26[BANK0 ] unsigned long 
;;  flags           1   62[BANK0 ] unsigned char 
;;  status          1    1[BANK0 ] unsigned char 
;;  data            4   22[BANK0 ] unsigned long 
;;  address         4   18[BANK0 ] unsigned long 
;;  address         4   14[BANK0 ] unsigned long 
;;  cnf3            4   10[BANK0 ] unsigned long 
;;  cnf2            4    6[BANK0 ] unsigned long 
;;  cnf1            4    2[BANK0 ] unsigned long 
;;  result          1   63[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr1l, fsr1h, fsr2l, fsr2h, status,2, status,0, tblptrl, tblptrh, tblptru, prodl, prodh, cstack
;; Tracked objects:
;;		On entry : F/1
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         2       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0      64       0       0       0       0       0       0       0       0       0
;;      Temps:          1       0       0       0       0       0       0       0       0       0       0
;;      Totals:         3      64       0       0       0       0       0       0       0       0       0
;;Total ram usage:       67 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    8
;; This function calls:
;;		_clock_reset
;;		_mcp2515_bit_modify
;;		_mcp2515_read_register
;;		_mcp2515_set_SJA1000_filter_code
;;		_mcp2515_set_SJA1000_filter_mask
;;		_mcp2515_set_bittiming
;;		_mcp2515_write_register
;;		_parseHex
;;		_sendByteHex
;;		_sendHex
;;		_transmitStd
;;		_usb_putch
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text4,class=CODE,space=0,reloc=2
	file	"D:\Projects\USBtin\firmware\source\main.c"
	line	173
global __ptext4
__ptext4:
psect	text4
	file	"D:\Projects\USBtin\firmware\source\main.c"
	line	173
	global	__size_of_parseLine
	__size_of_parseLine	equ	__end_of_parseLine-_parseLine
	
_parseLine:; BSR set to: 2

;incstack = 0
	opt	stack 22
	line	175
	
l2420:; BSR set to: 1

;main.c: 175: unsigned char result = 7;
	movlw	low(07h)
	movlb	0	; () banked
	movwf	((parseLine@result))&0ffh
	line	177
;main.c: 177: switch (line[0]) {
	goto	l2596
	line	178
;main.c: 178: case 'S':
	
l122:; BSR set to: 0

	line	179
;main.c: 179: if (state == 0)
	tstfsz	((c:_state)),c	;volatile
	goto	u1851
	goto	u1850
u1851:
	goto	l2598
u1850:
	goto	l2462
	line	182
	
l2424:; BSR set to: 0

	movlw	low(0ADh)
	movwf	((c:mcp2515_set_bittiming@cnf2)),c
	movlw	low(06h)
	movwf	((c:mcp2515_set_bittiming@cnf3)),c
	movlw	(0FBh)&0ffh
	
	call	_mcp2515_set_bittiming
	
l2426:
	movlw	low(0Dh)
	movlb	0	; () banked
	movwf	((parseLine@result))&0ffh
	goto	l2598
	line	183
	
l2428:; BSR set to: 0

	movlw	low(0ADh)
	movwf	((c:mcp2515_set_bittiming@cnf2)),c
	movlw	low(06h)
	movwf	((c:mcp2515_set_bittiming@cnf3)),c
	movlw	(0DDh)&0ffh
	
	call	_mcp2515_set_bittiming
	goto	l2426
	line	184
	
l2432:; BSR set to: 0

	movlw	low(0ADh)
	movwf	((c:mcp2515_set_bittiming@cnf2)),c
	movlw	low(06h)
	movwf	((c:mcp2515_set_bittiming@cnf3)),c
	movlw	(0CBh)&0ffh
	
	call	_mcp2515_set_bittiming
	goto	l2426
	line	185
	
l2436:; BSR set to: 0

	movlw	low(0ADh)
	movwf	((c:mcp2515_set_bittiming@cnf2)),c
	movlw	low(06h)
	movwf	((c:mcp2515_set_bittiming@cnf3)),c
	movlw	(0C5h)&0ffh
	
	call	_mcp2515_set_bittiming
	goto	l2426
	line	186
	
l2440:; BSR set to: 0

	movlw	low(0A4h)
	movwf	((c:mcp2515_set_bittiming@cnf2)),c
	movlw	low(04h)
	movwf	((c:mcp2515_set_bittiming@cnf3)),c
	movlw	(0C5h)&0ffh
	
	call	_mcp2515_set_bittiming
	goto	l2426
	line	187
	
l2444:; BSR set to: 0

	movlw	low(0A4h)
	movwf	((c:mcp2515_set_bittiming@cnf2)),c
	movlw	low(04h)
	movwf	((c:mcp2515_set_bittiming@cnf3)),c
	movlw	(0C2h)&0ffh
	
	call	_mcp2515_set_bittiming
	goto	l2426
	line	188
	
l2448:; BSR set to: 0

	movlw	low(09Ah)
	movwf	((c:mcp2515_set_bittiming@cnf2)),c
	movlw	low(03h)
	movwf	((c:mcp2515_set_bittiming@cnf3)),c
	movlw	(0C1h)&0ffh
	
	call	_mcp2515_set_bittiming
	goto	l2426
	line	189
	
l2452:; BSR set to: 0

	movlw	low(0A3h)
	movwf	((c:mcp2515_set_bittiming@cnf2)),c
	movlw	low(04h)
	movwf	((c:mcp2515_set_bittiming@cnf3)),c
	movlw	(0C0h)&0ffh
	
	call	_mcp2515_set_bittiming
	goto	l2426
	line	190
	
l2456:; BSR set to: 0

	movlw	low(09Ah)
	movwf	((c:mcp2515_set_bittiming@cnf2)),c
	movlw	low(03h)
	movwf	((c:mcp2515_set_bittiming@cnf3)),c
	movlw	(0C0h)&0ffh
	
	call	_mcp2515_set_bittiming
	goto	l2426
	line	181
	
l2462:; BSR set to: 0

	lfsr	2,01h
	movf	((c:parseLine@line)),c,w
	addwf	fsr2l
	movf	((c:parseLine@line+1)),c,w
	addwfc	fsr2h
	movf	indf2,w
	; Switch size 1, requested type "space"
; Number of cases is 9, Range of values is 48 to 56
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           28    15 (average)
;	Chosen strategy is simple_byte

	xorlw	48^0	; case 48
	skipnz
	goto	l2424
	xorlw	49^48	; case 49
	skipnz
	goto	l2428
	xorlw	50^49	; case 50
	skipnz
	goto	l2432
	xorlw	51^50	; case 51
	skipnz
	goto	l2436
	xorlw	52^51	; case 52
	skipnz
	goto	l2440
	xorlw	53^52	; case 53
	skipnz
	goto	l2444
	xorlw	54^53	; case 54
	skipnz
	goto	l2448
	xorlw	55^54	; case 55
	skipnz
	goto	l2452
	xorlw	56^55	; case 56
	skipnz
	goto	l2456
	goto	l2598

	line	195
;main.c: 195: case 's':
	
l136:; BSR set to: 0

	line	196
;main.c: 196: if (state == 0)
	tstfsz	((c:_state)),c	;volatile
	goto	u1861
	goto	u1860
u1861:
	goto	l2598
u1860:
	line	199
	
l2464:; BSR set to: 0

;main.c: 197: {
;main.c: 198: unsigned long cnf1, cnf2, cnf3;
;main.c: 199: if (parseHex(&line[1], 2, &cnf1) && parseHex(&line[3], 2, &cnf2) && parseHex(&line[5], 2, &cnf3)) {
	movlw	low(01h)
	addwf	((c:parseLine@line)),c,w
	
	movwf	((c:parseHex@line)),c
	movlw	high(01h)
	addwfc	((c:parseLine@line+1)),c,w
	movwf	1+((c:parseHex@line)),c
	movlw	low(02h)
	movwf	((c:parseHex@len)),c
		movlw	low(parseLine@cnf1)
	movwf	((c:parseHex@value)),c

	call	_parseHex	;wreg free
	iorlw	0
	btfsc	status,2
	goto	u1871
	goto	u1870
u1871:
	goto	l2598
u1870:
	
l2466:
	movlw	low(03h)
	addwf	((c:parseLine@line)),c,w
	
	movwf	((c:parseHex@line)),c
	movlw	high(03h)
	addwfc	((c:parseLine@line+1)),c,w
	movwf	1+((c:parseHex@line)),c
	movlw	low(02h)
	movwf	((c:parseHex@len)),c
	movlb	0	; () banked
		movlw	low(parseLine@cnf2)
	movwf	((c:parseHex@value)),c

	call	_parseHex	;wreg free
	iorlw	0
	btfsc	status,2
	goto	u1881
	goto	u1880
u1881:
	goto	l2598
u1880:
	
l2468:
	movlw	low(05h)
	addwf	((c:parseLine@line)),c,w
	
	movwf	((c:parseHex@line)),c
	movlw	high(05h)
	addwfc	((c:parseLine@line+1)),c,w
	movwf	1+((c:parseHex@line)),c
	movlw	low(02h)
	movwf	((c:parseHex@len)),c
	movlb	0	; () banked
		movlw	low(parseLine@cnf3)
	movwf	((c:parseHex@value)),c

	call	_parseHex	;wreg free
	iorlw	0
	btfsc	status,2
	goto	u1891
	goto	u1890
u1891:
	goto	l2598
u1890:
	line	200
	
l2470:
;main.c: 200: mcp2515_set_bittiming(cnf1, cnf2, cnf3);
	movff	(parseLine@cnf2),(c:mcp2515_set_bittiming@cnf2)
	movff	(parseLine@cnf3),(c:mcp2515_set_bittiming@cnf3)
	movlb	0	; () banked
	movf	((parseLine@cnf1))&0ffh,w
	
	call	_mcp2515_set_bittiming
	goto	l2426
	line	208
	
l2474:; BSR set to: 0

;main.c: 206: {
;main.c: 207: unsigned long address;
;main.c: 208: if (parseHex(&line[1], 2, &address)) {
	movlw	low(01h)
	addwf	((c:parseLine@line)),c,w
	
	movwf	((c:parseHex@line)),c
	movlw	high(01h)
	addwfc	((c:parseLine@line+1)),c,w
	movwf	1+((c:parseHex@line)),c
	movlw	low(02h)
	movwf	((c:parseHex@len)),c
		movlw	low(parseLine@address)
	movwf	((c:parseHex@value)),c

	call	_parseHex	;wreg free
	iorlw	0
	btfsc	status,2
	goto	u1901
	goto	u1900
u1901:
	goto	l2598
u1900:
	line	209
	
l2476:
;main.c: 209: unsigned char value = mcp2515_read_register(address);
	movlb	0	; () banked
	movf	((parseLine@address))&0ffh,w
	
	call	_mcp2515_read_register
	movlb	0	; () banked
	movwf	((parseLine@value))&0ffh
	line	210
	
l2478:; BSR set to: 0

;main.c: 210: sendByteHex(value);
	movf	((parseLine@value))&0ffh,w
	
	call	_sendByteHex
	goto	l2426
	line	218
	
l2482:; BSR set to: 0

;main.c: 216: {
;main.c: 217: unsigned long address, data;
;main.c: 218: if (parseHex(&line[1], 2, &address) && parseHex(&line[3], 2, &data)) {
	movlw	low(01h)
	addwf	((c:parseLine@line)),c,w
	
	movwf	((c:parseHex@line)),c
	movlw	high(01h)
	addwfc	((c:parseLine@line+1)),c,w
	movwf	1+((c:parseHex@line)),c
	movlw	low(02h)
	movwf	((c:parseHex@len)),c
		movlw	low(parseLine@address_440)
	movwf	((c:parseHex@value)),c

	call	_parseHex	;wreg free
	iorlw	0
	btfsc	status,2
	goto	u1911
	goto	u1910
u1911:
	goto	l2598
u1910:
	
l2484:
	movlw	low(03h)
	addwf	((c:parseLine@line)),c,w
	
	movwf	((c:parseHex@line)),c
	movlw	high(03h)
	addwfc	((c:parseLine@line+1)),c,w
	movwf	1+((c:parseHex@line)),c
	movlw	low(02h)
	movwf	((c:parseHex@len)),c
	movlb	0	; () banked
		movlw	low(parseLine@data)
	movwf	((c:parseHex@value)),c

	call	_parseHex	;wreg free
	iorlw	0
	btfsc	status,2
	goto	u1921
	goto	u1920
u1921:
	goto	l2598
u1920:
	line	219
	
l2486:
;main.c: 219: mcp2515_write_register(address, data);
	movff	(parseLine@data),(c:mcp2515_write_register@data)
	movlb	0	; () banked
	movf	((parseLine@address_440))&0ffh,w
	
	call	_mcp2515_write_register
	goto	l2426
	line	227
	
l2490:; BSR set to: 0

;main.c: 226: {
;main.c: 227: usb_putch('V');
	movlw	(056h)&0ffh
	
	call	_usb_putch
	line	228
;main.c: 228: sendByteHex(1);
	movlw	(01h)&0ffh
	
	call	_sendByteHex
	line	229
;main.c: 229: sendByteHex(0);
	movlw	(0)&0ffh
	
	call	_sendByteHex
	goto	l2426
	line	235
	
l2494:; BSR set to: 0

;main.c: 234: {
;main.c: 235: usb_putch('v');
	movlw	(076h)&0ffh
	
	call	_usb_putch
	line	236
;main.c: 236: sendByteHex(1);
	movlw	(01h)&0ffh
	
	call	_sendByteHex
	line	237
;main.c: 237: sendByteHex(4);
	movlw	(04h)&0ffh
	
	call	_sendByteHex
	goto	l2426
	line	243
	
l2498:; BSR set to: 0

;main.c: 242: {
;main.c: 243: usb_putch('N');
	movlw	(04Eh)&0ffh
	
	call	_usb_putch
	line	244
;main.c: 244: sendHex(0xFFFF, 4);
	movlw	low(0FFFFh)
	movwf	((c:sendHex@value)),c
	movlw	high(0FFFFh)
	movwf	((c:sendHex@value+1)),c
	movlw	low highword(0FFFFh)
	movwf	((c:sendHex@value+2)),c
	movlw	high highword(0FFFFh)
	movwf	((c:sendHex@value+3)),c
	movlw	low(04h)
	movwf	((c:sendHex@len)),c
	call	_sendHex	;wreg free
	goto	l2426
	line	248
;main.c: 248: case 'O':
	
l146:; BSR set to: 0

	line	249
;main.c: 249: if (state == 0)
	tstfsz	((c:_state)),c	;volatile
	goto	u1931
	goto	u1930
u1931:
	goto	l2598
u1930:
	line	251
	
l2502:; BSR set to: 0

;main.c: 250: {
;main.c: 251: mcp2515_bit_modify(0x0F, 0xE0, 0x00);
	movlw	low(0E0h)
	movwf	((c:mcp2515_bit_modify@mask)),c
	movlw	low(0)
	movwf	((c:mcp2515_bit_modify@data)),c
	movlw	(0Fh)&0ffh
	
	call	_mcp2515_bit_modify
	line	253
	
l2504:
;main.c: 253: clock_reset();
	call	_clock_reset	;wreg free
	line	255
;main.c: 255: state = 1;
	movlw	low(01h)
	movwf	((c:_state)),c	;volatile
	line	256
;main.c: 256: result = 13;
	movlw	low(0Dh)
	movlb	0	; () banked
	movwf	((parseLine@result))&0ffh
	goto	l2598
	line	259
;main.c: 259: case 'l':
	
l148:; BSR set to: 0

	line	260
;main.c: 260: if (state == 0)
	tstfsz	((c:_state)),c	;volatile
	goto	u1941
	goto	u1940
u1941:
	goto	l2598
u1940:
	line	262
	
l2506:; BSR set to: 0

;main.c: 261: {
;main.c: 262: mcp2515_bit_modify(0x0F, 0xE0, 0x40);
	movlw	low(0E0h)
	movwf	((c:mcp2515_bit_modify@mask)),c
	movlw	low(040h)
	movwf	((c:mcp2515_bit_modify@data)),c
	movlw	(0Fh)&0ffh
	
	call	_mcp2515_bit_modify
	line	264
	
l2508:
;main.c: 264: state = 1;
	movlw	low(01h)
	movwf	((c:_state)),c	;volatile
	goto	l2426
	line	268
;main.c: 268: case 'L':
	
l150:; BSR set to: 0

	line	269
;main.c: 269: if (state == 0)
	tstfsz	((c:_state)),c	;volatile
	goto	u1951
	goto	u1950
u1951:
	goto	l2598
u1950:
	line	271
	
l2512:; BSR set to: 0

;main.c: 270: {
;main.c: 271: mcp2515_bit_modify(0x0F, 0xE0, 0x60);
	movlw	low(0E0h)
	movwf	((c:mcp2515_bit_modify@mask)),c
	movlw	low(060h)
	movwf	((c:mcp2515_bit_modify@data)),c
	movlw	(0Fh)&0ffh
	
	call	_mcp2515_bit_modify
	line	273
	
l2514:
;main.c: 273: state = 2;
	movlw	low(02h)
	movwf	((c:_state)),c	;volatile
	goto	l2426
	line	278
	
l2518:; BSR set to: 0

;main.c: 278: if (state != 0)
	movf	((c:_state)),c,w	;volatile
	btfsc	status,2
	goto	u1961
	goto	u1960
u1961:
	goto	l2598
u1960:
	line	280
	
l2520:; BSR set to: 0

;main.c: 279: {
;main.c: 280: mcp2515_bit_modify(0x0F, 0xE0, 0x80);
	movlw	low(0E0h)
	movwf	((c:mcp2515_bit_modify@mask)),c
	movlw	low(080h)
	movwf	((c:mcp2515_bit_modify@data)),c
	movlw	(0Fh)&0ffh
	
	call	_mcp2515_bit_modify
	line	282
	
l2522:
;main.c: 282: state = 0;
	clrf	((c:_state)),c	;volatile
	goto	l2426
	line	288
	
l156:; BSR set to: 0

	line	290
	
l2526:; BSR set to: 0

;main.c: 287: case 'R':
;main.c: 288: case 't':
;main.c: 289: case 'T':
;main.c: 290: if (state == 1)
	decf	((c:_state)),c,w	;volatile

	btfss	status,2
	goto	u1971
	goto	u1970
u1971:
	goto	l2598
u1970:
	line	292
	
l2528:; BSR set to: 0

;main.c: 291: {
;main.c: 292: if (transmitStd(line)) {
		movff	(c:parseLine@line+1),(c:transmitStd@line+1)
	movff	(c:parseLine@line),(c:transmitStd@line)

	call	_transmitStd	;wreg free
	iorlw	0
	btfsc	status,2
	goto	u1981
	goto	u1980
u1981:
	goto	l2598
u1980:
	line	293
	
l2530:
;main.c: 293: if (line[0] < 'Z') usb_putch('Z');
	movff	(c:parseLine@line),fsr2l
	movff	(c:parseLine@line+1),fsr2h
	movf	indf2,w
	movwf	(??_parseLine+0+0)&0ffh,c
	movlw	(05Ah)&0ffh
	subwf	((??_parseLine+0+0)),c,w
	btfsc	status,0
	goto	u1991
	goto	u1990
u1991:
	goto	l2534
u1990:
	
l2532:
	movlw	(05Ah)&0ffh
	
	call	_usb_putch
	goto	l2426
	line	294
	
l2534:
;main.c: 294: else usb_putch('z');
	movlw	(07Ah)&0ffh
	
	call	_usb_putch
	goto	l2426
	line	302
	
l2538:; BSR set to: 0

;main.c: 301: {
;main.c: 302: unsigned char flags = mcp2515_read_register(0x2d);
	movlw	(02Dh)&0ffh
	
	call	_mcp2515_read_register
	movlb	0	; () banked
	movwf	((parseLine@flags))&0ffh
	line	303
	
l2540:; BSR set to: 0

;main.c: 303: unsigned char status = 0;
	clrf	((parseLine@status))&0ffh
	line	305
	
l2542:; BSR set to: 0

;main.c: 305: if (flags & 0x01) status |= 0x04;
	
	btfss	((parseLine@flags))&0ffh,(0)&7
	goto	u2001
	goto	u2000
u2001:
	goto	l2546
u2000:
	
l2544:; BSR set to: 0

	bsf	(0+(2/8)+(parseLine@status))&0ffh,(2)&7
	line	306
	
l2546:; BSR set to: 0

;main.c: 306: if (flags & 0xC0) status |= 0x08;
	movff	(parseLine@flags),??_parseLine+0+0
	movlw	0C0h
	andwf	(??_parseLine+0+0),c
	btfsc	status,2
	goto	u2011
	goto	u2010
u2011:
	goto	l2550
u2010:
	
l2548:; BSR set to: 0

	bsf	(0+(3/8)+(parseLine@status))&0ffh,(3)&7
	line	307
	
l2550:; BSR set to: 0

;main.c: 307: if (flags & 0x18) status |= 0x20;
	movff	(parseLine@flags),??_parseLine+0+0
	movlw	018h
	andwf	(??_parseLine+0+0),c
	btfsc	status,2
	goto	u2021
	goto	u2020
u2021:
	goto	l2554
u2020:
	
l2552:; BSR set to: 0

	bsf	(0+(5/8)+(parseLine@status))&0ffh,(5)&7
	line	308
	
l2554:; BSR set to: 0

;main.c: 308: if (flags & 0x20) status |= 0x80;
	
	btfss	((parseLine@flags))&0ffh,(5)&7
	goto	u2031
	goto	u2030
u2031:
	goto	l2558
u2030:
	
l2556:; BSR set to: 0

	bsf	(0+(7/8)+(parseLine@status))&0ffh,(7)&7
	line	310
	
l2558:; BSR set to: 0

;main.c: 310: usb_putch('F');
	movlw	(046h)&0ffh
	
	call	_usb_putch
	line	311
	
l2560:
;main.c: 311: sendByteHex(status);
	movlb	0	; () banked
	movf	((parseLine@status))&0ffh,w
	
	call	_sendByteHex
	goto	l2426
	line	318
	
l2564:; BSR set to: 0

;main.c: 316: {
;main.c: 317: unsigned long stamping;
;main.c: 318: if (parseHex(&line[1], 1, &stamping)) {
	movlw	low(01h)
	addwf	((c:parseLine@line)),c,w
	
	movwf	((c:parseHex@line)),c
	movlw	high(01h)
	addwfc	((c:parseLine@line+1)),c,w
	movwf	1+((c:parseHex@line)),c
	movlw	low(01h)
	movwf	((c:parseHex@len)),c
		movlw	low(parseLine@stamping)
	movwf	((c:parseHex@value)),c

	call	_parseHex	;wreg free
	iorlw	0
	btfsc	status,2
	goto	u2041
	goto	u2040
u2041:
	goto	l2598
u2040:
	line	319
	
l2566:
;main.c: 319: timestamping = (stamping != 0);
	movlb	0	; () banked
	movf	((parseLine@stamping+3))&0ffh,w
	iorwf ((parseLine@stamping))&0ffh,w
	iorwf ((parseLine@stamping+1))&0ffh,w
	iorwf ((parseLine@stamping+2))&0ffh,w

	btfss	status,2
	goto	u2051
	goto	u2050
u2051:
	movlw	1
	goto	u2056
u2050:
	movlw	0
u2056:
	movlb	1	; () banked
	movwf	((_timestamping))&0ffh
	goto	l2426
	line	324
;main.c: 324: case 'm':
	
l169:; BSR set to: 0

	line	325
;main.c: 325: if (state == 0)
	tstfsz	((c:_state)),c	;volatile
	goto	u2061
	goto	u2060
u2061:
	goto	l2598
u2060:
	line	328
	
l2570:; BSR set to: 0

;main.c: 326: {
;main.c: 327: unsigned long am0, am1, am2, am3;
;main.c: 328: if (parseHex(&line[1], 2, &am0) && parseHex(&line[3], 2, &am1) && parseHex(&line[5], 2, &am2) && parseHex(&line[7], 2, &am3)) {
	movlw	low(01h)
	addwf	((c:parseLine@line)),c,w
	
	movwf	((c:parseHex@line)),c
	movlw	high(01h)
	addwfc	((c:parseLine@line+1)),c,w
	movwf	1+((c:parseHex@line)),c
	movlw	low(02h)
	movwf	((c:parseHex@len)),c
		movlw	low(parseLine@am0)
	movwf	((c:parseHex@value)),c

	call	_parseHex	;wreg free
	iorlw	0
	btfsc	status,2
	goto	u2071
	goto	u2070
u2071:
	goto	l2598
u2070:
	
l2572:
	movlw	low(03h)
	addwf	((c:parseLine@line)),c,w
	
	movwf	((c:parseHex@line)),c
	movlw	high(03h)
	addwfc	((c:parseLine@line+1)),c,w
	movwf	1+((c:parseHex@line)),c
	movlw	low(02h)
	movwf	((c:parseHex@len)),c
	movlb	0	; () banked
		movlw	low(parseLine@am1)
	movwf	((c:parseHex@value)),c

	call	_parseHex	;wreg free
	iorlw	0
	btfsc	status,2
	goto	u2081
	goto	u2080
u2081:
	goto	l2598
u2080:
	
l2574:
	movlw	low(05h)
	addwf	((c:parseLine@line)),c,w
	
	movwf	((c:parseHex@line)),c
	movlw	high(05h)
	addwfc	((c:parseLine@line+1)),c,w
	movwf	1+((c:parseHex@line)),c
	movlw	low(02h)
	movwf	((c:parseHex@len)),c
	movlb	0	; () banked
		movlw	low(parseLine@am2)
	movwf	((c:parseHex@value)),c

	call	_parseHex	;wreg free
	iorlw	0
	btfsc	status,2
	goto	u2091
	goto	u2090
u2091:
	goto	l2598
u2090:
	
l2576:
	movlw	low(07h)
	addwf	((c:parseLine@line)),c,w
	
	movwf	((c:parseHex@line)),c
	movlw	high(07h)
	addwfc	((c:parseLine@line+1)),c,w
	movwf	1+((c:parseHex@line)),c
	movlw	low(02h)
	movwf	((c:parseHex@len)),c
	movlb	0	; () banked
		movlw	low(parseLine@am3)
	movwf	((c:parseHex@value)),c

	call	_parseHex	;wreg free
	iorlw	0
	btfsc	status,2
	goto	u2101
	goto	u2100
u2101:
	goto	l2598
u2100:
	line	329
	
l2578:
;main.c: 329: mcp2515_set_SJA1000_filter_mask(am0, am1, am2, am3);
	movff	(parseLine@am1),(c:mcp2515_set_SJA1000_filter_mask@amr1)
	movff	(parseLine@am2),(c:mcp2515_set_SJA1000_filter_mask@amr2)
	movff	(parseLine@am3),(c:mcp2515_set_SJA1000_filter_mask@amr3)
	movlb	0	; () banked
	movf	((parseLine@am0))&0ffh,w
	
	call	_mcp2515_set_SJA1000_filter_mask
	goto	l2426
	line	334
;main.c: 334: case 'M':
	
l172:; BSR set to: 0

	line	335
;main.c: 335: if (state == 0)
	tstfsz	((c:_state)),c	;volatile
	goto	u2111
	goto	u2110
u2111:
	goto	l2598
u2110:
	line	338
	
l2582:; BSR set to: 0

;main.c: 336: {
;main.c: 337: unsigned long ac0, ac1, ac2, ac3;
;main.c: 338: if (parseHex(&line[1], 2, &ac0) && parseHex(&line[3], 2, &ac1) && parseHex(&line[5], 2, &ac2) && parseHex(&line[7], 2, &ac3)) {
	movlw	low(01h)
	addwf	((c:parseLine@line)),c,w
	
	movwf	((c:parseHex@line)),c
	movlw	high(01h)
	addwfc	((c:parseLine@line+1)),c,w
	movwf	1+((c:parseHex@line)),c
	movlw	low(02h)
	movwf	((c:parseHex@len)),c
		movlw	low(parseLine@ac0)
	movwf	((c:parseHex@value)),c

	call	_parseHex	;wreg free
	iorlw	0
	btfsc	status,2
	goto	u2121
	goto	u2120
u2121:
	goto	l2598
u2120:
	
l2584:
	movlw	low(03h)
	addwf	((c:parseLine@line)),c,w
	
	movwf	((c:parseHex@line)),c
	movlw	high(03h)
	addwfc	((c:parseLine@line+1)),c,w
	movwf	1+((c:parseHex@line)),c
	movlw	low(02h)
	movwf	((c:parseHex@len)),c
	movlb	0	; () banked
		movlw	low(parseLine@ac1)
	movwf	((c:parseHex@value)),c

	call	_parseHex	;wreg free
	iorlw	0
	btfsc	status,2
	goto	u2131
	goto	u2130
u2131:
	goto	l2598
u2130:
	
l2586:
	movlw	low(05h)
	addwf	((c:parseLine@line)),c,w
	
	movwf	((c:parseHex@line)),c
	movlw	high(05h)
	addwfc	((c:parseLine@line+1)),c,w
	movwf	1+((c:parseHex@line)),c
	movlw	low(02h)
	movwf	((c:parseHex@len)),c
	movlb	0	; () banked
		movlw	low(parseLine@ac2)
	movwf	((c:parseHex@value)),c

	call	_parseHex	;wreg free
	iorlw	0
	btfsc	status,2
	goto	u2141
	goto	u2140
u2141:
	goto	l2598
u2140:
	
l2588:
	movlw	low(07h)
	addwf	((c:parseLine@line)),c,w
	
	movwf	((c:parseHex@line)),c
	movlw	high(07h)
	addwfc	((c:parseLine@line+1)),c,w
	movwf	1+((c:parseHex@line)),c
	movlw	low(02h)
	movwf	((c:parseHex@len)),c
	movlb	0	; () banked
		movlw	low(parseLine@ac3)
	movwf	((c:parseHex@value)),c

	call	_parseHex	;wreg free
	iorlw	0
	btfsc	status,2
	goto	u2151
	goto	u2150
u2151:
	goto	l2598
u2150:
	line	339
	
l2590:
;main.c: 339: mcp2515_set_SJA1000_filter_code(ac0, ac1, ac2, ac3);
	movff	(parseLine@ac1),(c:mcp2515_set_SJA1000_filter_code@acr1)
	movff	(parseLine@ac2),(c:mcp2515_set_SJA1000_filter_code@acr2)
	movff	(parseLine@ac3),(c:mcp2515_set_SJA1000_filter_code@acr3)
	movlb	0	; () banked
	movf	((parseLine@ac0))&0ffh,w
	
	call	_mcp2515_set_SJA1000_filter_code
	goto	l2426
	line	177
	
l2596:; BSR set to: 0

	movff	(c:parseLine@line),fsr2l
	movff	(c:parseLine@line+1),fsr2h
	movf	indf2,w
	; Switch size 1, requested type "space"
; Number of cases is 19, Range of values is 67 to 118
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           58    30 (average)
;	Chosen strategy is simple_byte

	xorlw	67^0	; case 67
	skipnz
	goto	l2518
	xorlw	70^67	; case 70
	skipnz
	goto	l2538
	xorlw	71^70	; case 71
	skipnz
	goto	l2474
	xorlw	76^71	; case 76
	skipnz
	goto	l150
	xorlw	77^76	; case 77
	skipnz
	goto	l172
	xorlw	78^77	; case 78
	skipnz
	goto	l2498
	xorlw	79^78	; case 79
	skipnz
	goto	l146
	xorlw	82^79	; case 82
	skipnz
	goto	l2526
	xorlw	83^82	; case 83
	skipnz
	goto	l122
	xorlw	84^83	; case 84
	skipnz
	goto	l2526
	xorlw	86^84	; case 86
	skipnz
	goto	l2490
	xorlw	87^86	; case 87
	skipnz
	goto	l2482
	xorlw	90^87	; case 90
	skipnz
	goto	l2564
	xorlw	108^90	; case 108
	skipnz
	goto	l148
	xorlw	109^108	; case 109
	skipnz
	goto	l169
	xorlw	114^109	; case 114
	skipnz
	goto	l156
	xorlw	115^114	; case 115
	skipnz
	goto	l136
	xorlw	116^115	; case 116
	skipnz
	goto	l2526
	xorlw	118^116	; case 118
	skipnz
	goto	l2494
	goto	l2598

	line	347
	
l2598:
;main.c: 347: usb_putch(result);
	movlb	0	; () banked
	movf	((parseLine@result))&0ffh,w
	
	call	_usb_putch
	line	348
	
l175:
	return
	opt stack 0
GLOBAL	__end_of_parseLine
	__end_of_parseLine:
	signat	_parseLine,4216
	global	_transmitStd

;; *************** function _transmitStd *****************
;; Defined at:
;;		line 135 in file "D:\Projects\USBtin\firmware\source\main.c"
;; Parameters:    Size  Location     Type
;;  line            2   14[COMRAM] PTR unsigned char 
;;		 -> main@line(100), 
;; Auto vars:     Size  Location     Type
;;  i               1   20[COMRAM] unsigned char 
;;  canmsg         14   25[COMRAM] struct .
;;  temp            4   21[COMRAM] unsigned long 
;;  idlen           1   19[COMRAM] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, fsr2l, fsr2h, status,2, status,0, prodl, prodh, cstack
;; Tracked objects:
;;		On entry : E/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         2       0       0       0       0       0       0       0       0       0       0
;;      Locals:        21       0       0       0       0       0       0       0       0       0       0
;;      Temps:          2       0       0       0       0       0       0       0       0       0       0
;;      Totals:        25       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:       25 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_mcp2515_send_message
;;		_parseHex
;; This function is called by:
;;		_parseLine
;; This function uses a non-reentrant model
;;
psect	text5,class=CODE,space=0,reloc=2
	line	135
global __ptext5
__ptext5:
psect	text5
	file	"D:\Projects\USBtin\firmware\source\main.c"
	line	135
	global	__size_of_transmitStd
	__size_of_transmitStd	equ	__end_of_transmitStd-_transmitStd
	
_transmitStd:
;incstack = 0
	opt	stack 26
	line	140
	
l2300:
;main.c: 136: canmsg_t canmsg;
;main.c: 137: unsigned long temp;
;main.c: 138: unsigned char idlen;
;main.c: 140: canmsg.flags.rtr = ((line[0] == 'r') || (line[0] == 'R'));
	movlw	low(01h)
	movwf	((c:_transmitStd$428)),c
	
l2302:
	movff	(c:transmitStd@line),fsr2l
	movff	(c:transmitStd@line+1),fsr2h
	movf	indf2,w
	xorlw	072h

	btfsc	status,2
	goto	u1741
	goto	u1740
u1741:
	goto	l108
u1740:
	
l2304:
	movff	(c:transmitStd@line),fsr2l
	movff	(c:transmitStd@line+1),fsr2h
	movf	indf2,w
	xorlw	052h

	btfsc	status,2
	goto	u1751
	goto	u1750
u1751:
	goto	l108
u1750:
	
l2306:
	clrf	((c:_transmitStd$428)),c
	
l108:
	btfsc	(c:_transmitStd$428),c,0
	bra	u1765
	bcf	(0+((c:transmitStd@canmsg)+04h)),c,0
	bra	u1766
	u1765:
	bsf	(0+((c:transmitStd@canmsg)+04h)),c,0
	u1766:

	line	143
	
l2308:
;main.c: 143: if (line[0] < 'Z') {
	movff	(c:transmitStd@line),fsr2l
	movff	(c:transmitStd@line+1),fsr2h
	movf	indf2,w
	movwf	(??_transmitStd+0+0)&0ffh,c
	movlw	(05Ah)&0ffh
	subwf	((??_transmitStd+0+0)),c,w
	btfsc	status,0
	goto	u1771
	goto	u1770
u1771:
	goto	l109
u1770:
	line	144
	
l2310:
;main.c: 144: canmsg.flags.extended = 1;
	bsf	(0+((c:transmitStd@canmsg)+04h)),c,1
	line	145
	
l2312:
;main.c: 145: idlen = 8;
	movlw	low(08h)
	movwf	((c:transmitStd@idlen)),c
	line	146
;main.c: 146: } else {
	goto	l2316
	
l109:
	line	147
;main.c: 147: canmsg.flags.extended = 0;
	bcf	(0+((c:transmitStd@canmsg)+04h)),c,1
	line	148
	
l2314:
;main.c: 148: idlen = 3;
	movlw	low(03h)
	movwf	((c:transmitStd@idlen)),c
	line	151
	
l2316:
;main.c: 149: }
;main.c: 151: if (!parseHex(&line[1], idlen, &temp)) return 0;
	movlw	low(01h)
	addwf	((c:transmitStd@line)),c,w
	
	movwf	((c:parseHex@line)),c
	movlw	high(01h)
	addwfc	((c:transmitStd@line+1)),c,w
	movwf	1+((c:parseHex@line)),c
	movff	(c:transmitStd@idlen),(c:parseHex@len)
		movlw	low((c:transmitStd@temp))
	movwf	((c:parseHex@value)),c

	call	_parseHex	;wreg free
	iorlw	0
	btfss	status,2
	goto	u1781
	goto	u1780
u1781:
	goto	l111
u1780:
	
l2318:
	movlw	(0)&0ffh
	goto	l112
	
l111:
	line	152
;main.c: 152: canmsg.id = temp;
	movff	(c:transmitStd@temp),(c:transmitStd@canmsg)
	movff	(c:transmitStd@temp+1),(c:transmitStd@canmsg+1)
	movff	(c:transmitStd@temp+2),(c:transmitStd@canmsg+2)
	movff	(c:transmitStd@temp+3),(c:transmitStd@canmsg+3)
	line	154
	
l2322:
;main.c: 154: if (!parseHex(&line[1 + idlen], 1, &temp)) return 0;
	movf	((c:transmitStd@idlen)),c,w
	addwf	((c:transmitStd@line)),c,w
	movwf	(??_transmitStd+0+0)&0ffh,c
	movlw	0
	addwfc	((c:transmitStd@line+1)),c,w
	movwf	(??_transmitStd+0+0+1)&0ffh,c
	movlw	low(01h)
	addwf	(??_transmitStd+0+0),c,w
	
	movwf	((c:parseHex@line)),c
	movlw	high(01h)
	addwfc	(??_transmitStd+0+1),c,w
	movwf	1+((c:parseHex@line)),c
	movlw	low(01h)
	movwf	((c:parseHex@len)),c
		movlw	low((c:transmitStd@temp))
	movwf	((c:parseHex@value)),c

	call	_parseHex	;wreg free
	iorlw	0
	btfss	status,2
	goto	u1791
	goto	u1790
u1791:
	goto	l113
u1790:
	goto	l2318
	
l113:
	line	155
;main.c: 155: canmsg.length = temp;
	movff	(c:transmitStd@temp),0+((c:transmitStd@canmsg)+05h)
	line	157
;main.c: 157: if (!canmsg.flags.rtr) {
	btfsc	(0+((c:transmitStd@canmsg)+04h)),c,0
	goto	u1801
	goto	u1800
u1801:
	goto	l2342
u1800:
	line	159
	
l2328:
;main.c: 158: unsigned char i;
;main.c: 159: for (i = 0; i < canmsg.length; i++) {
	clrf	((c:transmitStd@i)),c
	goto	l2340
	line	160
	
l2330:
;main.c: 160: if (!parseHex(&line[idlen + 2 + i*2], 2, &temp)) return 0;
	movf	((c:transmitStd@i)),c,w
	mullw	02h
	movff	(c:transmitStd@line),??_transmitStd+0+0
	movff	(c:transmitStd@line+1),??_transmitStd+0+0+1
	movf	(prodl),c,w
	addwf	(??_transmitStd+0+0),c
	movf	(prodh),c,w
	addwfc	(??_transmitStd+0+1),c
	movf	((c:transmitStd@idlen)),c,w
	addwf	(??_transmitStd+0+0),c
	movlw	0
	addwfc	(??_transmitStd+0+1),c
	movlw	low(02h)
	addwf	(??_transmitStd+0+0),c,w
	
	movwf	((c:parseHex@line)),c
	movlw	high(02h)
	addwfc	(??_transmitStd+0+1),c,w
	movwf	1+((c:parseHex@line)),c
	movlw	low(02h)
	movwf	((c:parseHex@len)),c
		movlw	low((c:transmitStd@temp))
	movwf	((c:parseHex@value)),c

	call	_parseHex	;wreg free
	iorlw	0
	btfss	status,2
	goto	u1811
	goto	u1810
u1811:
	goto	l2336
u1810:
	goto	l2318
	line	161
	
l2336:
;main.c: 161: canmsg.data[i] = temp;
	movff	(c:transmitStd@i),??_transmitStd+0+0
	movlw	06h
	addwf	(??_transmitStd+0+0),c
	movf	(??_transmitStd+0+0),c,w
	addlw	low((c:transmitStd@canmsg))
	movwf	fsr2l
	clrf	fsr2h
	movff	(c:transmitStd@temp),indf2

	line	159
	
l2338:
	incf	((c:transmitStd@i)),c
	
l2340:
	movf	(0+((c:transmitStd@canmsg)+05h)),c,w
	subwf	((c:transmitStd@i)),c,w
	btfss	status,0
	goto	u1821
	goto	u1820
u1821:
	goto	l2330
u1820:
	line	165
	
l2342:
;main.c: 162: }
;main.c: 163: }
;main.c: 165: return mcp2515_send_message(&canmsg);
	movlw	((c:transmitStd@canmsg))&0ffh
	
	call	_mcp2515_send_message
	line	166
	
l112:
	return
	opt stack 0
GLOBAL	__end_of_transmitStd
	__end_of_transmitStd:
	signat	_transmitStd,4217
	global	_parseHex

;; *************** function _parseHex *****************
;; Defined at:
;;		line 78 in file "D:\Projects\USBtin\firmware\source\main.c"
;; Parameters:    Size  Location     Type
;;  line            2    0[COMRAM] PTR unsigned char 
;;		 -> main@line(100), 
;;  len             1    2[COMRAM] unsigned char 
;;  value           1    3[COMRAM] PTR unsigned long 
;;		 -> parseLine@ac3(4), parseLine@ac2(4), parseLine@ac1(4), parseLine@ac0(4), 
;;		 -> parseLine@am3(4), parseLine@am2(4), parseLine@am1(4), parseLine@am0(4), 
;;		 -> parseLine@stamping(4), parseLine@data(4), parseLine@address_440(4), parseLine@address(4), 
;;		 -> parseLine@cnf3(4), parseLine@cnf2(4), parseLine@cnf1(4), transmitStd@temp(4), 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, fsr2l, fsr2h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         4       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          4       0       0       0       0       0       0       0       0       0       0
;;      Totals:         8       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        8 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_transmitStd
;;		_parseLine
;; This function uses a non-reentrant model
;;
psect	text6,class=CODE,space=0,reloc=2
	line	78
global __ptext6
__ptext6:
psect	text6
	file	"D:\Projects\USBtin\firmware\source\main.c"
	line	78
	global	__size_of_parseHex
	__size_of_parseHex	equ	__end_of_parseHex-_parseHex
	
_parseHex:
;incstack = 0
	opt	stack 28
	line	79
	
l2174:
;main.c: 79: *value = 0;
	movf	((c:parseHex@value)),c,w
	movwf	fsr2l
	clrf	fsr2h
	clrf	postinc2,c
	clrf	postinc2,c
	clrf	postinc2,c
	clrf	postdec2,c

	line	80
;main.c: 80: while (len--) {
	goto	l2208
	line	81
	
l2176:
;main.c: 81: if (*line == 0) return 0;
	movff	(c:parseHex@line),fsr2l
	movff	(c:parseHex@line+1),fsr2h
	movf	indf2,w
	btfss	status,2
	goto	u1491
	goto	u1490
u1491:
	goto	l2182
u1490:
	
l2178:
	movlw	(0)&0ffh
	goto	l86
	line	82
	
l2182:
;main.c: 82: *value <<= 4;
	movf	((c:parseHex@value)),c,w
	movwf	fsr2l
	clrf	fsr2h
	movlw	04h
u1505:
	bcf	status,0
	rlcf	postinc2
	rlcf	postinc2
	rlcf	postinc2
	rlcf	postdec2
	decf	fsr2
	decf	fsr2
	decfsz	wreg
	goto	u1505
	line	83
	
l2184:
;main.c: 83: if ((*line >= '0') && (*line <= '9')) {
	movff	(c:parseHex@line),fsr2l
	movff	(c:parseHex@line+1),fsr2h
	movlw	(030h-1)
	cpfsgt	indf2
	goto	u1511
	goto	u1510
u1511:
	goto	l2190
u1510:
	
l2186:
	movff	(c:parseHex@line),fsr2l
	movff	(c:parseHex@line+1),fsr2h
	movf	indf2,w
	movwf	(??_parseHex+0+0)&0ffh,c
	movlw	(03Ah)&0ffh
	subwf	((??_parseHex+0+0)),c,w
	btfsc	status,0
	goto	u1521
	goto	u1520
u1521:
	goto	l2190
u1520:
	line	84
	
l2188:
;main.c: 84: *value += *line - '0';
	movlw	low(0FFD0h)
	movwf	(??_parseHex+0+0)&0ffh,c
	movlw	high(0FFD0h)
	movwf	(??_parseHex+0+0+1)&0ffh,c
	movff	(c:parseHex@line),fsr2l
	movff	(c:parseHex@line+1),fsr2h
	movf	indf2,w
	movwf	(??_parseHex+2+0)&0ffh,c
	clrf	(??_parseHex+2+0+1)&0ffh,c

	movf	(??_parseHex+0+0),c,w
	addwf	(??_parseHex+2+0),c
	movf	(??_parseHex+0+1),c,w
	addwfc	(??_parseHex+2+1),c
	movf	((c:parseHex@value)),c,w
	movwf	fsr2l
	clrf	fsr2h
	movf	(??_parseHex+2+0),c,w
	addwf	postinc2
	movf	(??_parseHex+2+1),c,w
	addwfc	postinc2
	movlw	0
	addwfc	postinc2
	addwfc	postdec2
	movf	postdec2
	movf	postdec2
	line	85
;main.c: 85: } else if ((*line >= 'A') && (*line <= 'F')) {
	goto	l2206
	
l2190:
	movff	(c:parseHex@line),fsr2l
	movff	(c:parseHex@line+1),fsr2h
	movlw	(041h-1)
	cpfsgt	indf2
	goto	u1531
	goto	u1530
u1531:
	goto	l2196
u1530:
	
l2192:
	movff	(c:parseHex@line),fsr2l
	movff	(c:parseHex@line+1),fsr2h
	movf	indf2,w
	movwf	(??_parseHex+0+0)&0ffh,c
	movlw	(047h)&0ffh
	subwf	((??_parseHex+0+0)),c,w
	btfsc	status,0
	goto	u1541
	goto	u1540
u1541:
	goto	l2196
u1540:
	line	86
	
l2194:
;main.c: 86: *value += *line - 'A' + 10;
	movlw	low(0FFC9h)
	movwf	(??_parseHex+0+0)&0ffh,c
	movlw	high(0FFC9h)
	movwf	(??_parseHex+0+0+1)&0ffh,c
	movff	(c:parseHex@line),fsr2l
	movff	(c:parseHex@line+1),fsr2h
	movf	indf2,w
	movwf	(??_parseHex+2+0)&0ffh,c
	clrf	(??_parseHex+2+0+1)&0ffh,c

	movf	(??_parseHex+0+0),c,w
	addwf	(??_parseHex+2+0),c
	movf	(??_parseHex+0+1),c,w
	addwfc	(??_parseHex+2+1),c
	movf	((c:parseHex@value)),c,w
	movwf	fsr2l
	clrf	fsr2h
	movf	(??_parseHex+2+0),c,w
	addwf	postinc2
	movf	(??_parseHex+2+1),c,w
	addwfc	postinc2
	movlw	0
	addwfc	postinc2
	addwfc	postdec2
	movf	postdec2
	movf	postdec2
	line	87
;main.c: 87: } else if ((*line >= 'a') && (*line <= 'f')) {
	goto	l2206
	
l2196:
	movff	(c:parseHex@line),fsr2l
	movff	(c:parseHex@line+1),fsr2h
	movlw	(061h-1)
	cpfsgt	indf2
	goto	u1551
	goto	u1550
u1551:
	goto	l2178
u1550:
	
l2198:
	movff	(c:parseHex@line),fsr2l
	movff	(c:parseHex@line+1),fsr2h
	movf	indf2,w
	movwf	(??_parseHex+0+0)&0ffh,c
	movlw	(067h)&0ffh
	subwf	((??_parseHex+0+0)),c,w
	btfsc	status,0
	goto	u1561
	goto	u1560
u1561:
	goto	l2178
u1560:
	line	88
	
l2200:
;main.c: 88: *value += *line - 'a' + 10;
	movlw	low(0FFA9h)
	movwf	(??_parseHex+0+0)&0ffh,c
	movlw	high(0FFA9h)
	movwf	(??_parseHex+0+0+1)&0ffh,c
	movff	(c:parseHex@line),fsr2l
	movff	(c:parseHex@line+1),fsr2h
	movf	indf2,w
	movwf	(??_parseHex+2+0)&0ffh,c
	clrf	(??_parseHex+2+0+1)&0ffh,c

	movf	(??_parseHex+0+0),c,w
	addwf	(??_parseHex+2+0),c
	movf	(??_parseHex+0+1),c,w
	addwfc	(??_parseHex+2+1),c
	movf	((c:parseHex@value)),c,w
	movwf	fsr2l
	clrf	fsr2h
	movf	(??_parseHex+2+0),c,w
	addwf	postinc2
	movf	(??_parseHex+2+1),c,w
	addwfc	postinc2
	movlw	0
	addwfc	postinc2
	addwfc	postdec2
	movf	postdec2
	movf	postdec2
	line	90
;main.c: 89: } else return 0;
	
l2206:
;main.c: 90: line++;
	infsnz	((c:parseHex@line)),c
	incf	((c:parseHex@line+1)),c
	line	80
	
l2208:
	decf	((c:parseHex@len)),c
	incf	((c:parseHex@len))&0ffh,w

	btfss	status,2
	goto	u1571
	goto	u1570
u1571:
	goto	l2176
u1570:
	line	92
	
l2210:
;main.c: 91: }
;main.c: 92: return 1;
	movlw	(01h)&0ffh
	line	93
	
l86:
	return
	opt stack 0
GLOBAL	__end_of_parseHex
	__end_of_parseHex:
	signat	_parseHex,12409
	global	_mcp2515_send_message

;; *************** function _mcp2515_send_message *****************
;; Defined at:
;;		line 280 in file "D:\Projects\USBtin\firmware\source\mcp2515.c"
;; Parameters:    Size  Location     Type
;;  p_canmsg        1    wreg     PTR struct .
;;		 -> transmitStd@canmsg(14), 
;; Auto vars:     Size  Location     Type
;;  p_canmsg        1   13[COMRAM] PTR struct .
;;		 -> transmitStd@canmsg(14), 
;;  i               1   10[COMRAM] unsigned char 
;;  address         1   12[COMRAM] unsigned char 
;;  status          1   11[COMRAM] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, fsr2l, fsr2h, status,2, status,0, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         4       0       0       0       0       0       0       0       0       0       0
;;      Temps:          8       0       0       0       0       0       0       0       0       0       0
;;      Totals:        12       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:       12 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_mcp2515_read_status
;;		_spi_transmit
;; This function is called by:
;;		_transmitStd
;; This function uses a non-reentrant model
;;
psect	text7,class=CODE,space=0,reloc=2
	file	"D:\Projects\USBtin\firmware\source\mcp2515.c"
	line	280
global __ptext7
__ptext7:
psect	text7
	file	"D:\Projects\USBtin\firmware\source\mcp2515.c"
	line	280
	global	__size_of_mcp2515_send_message
	__size_of_mcp2515_send_message	equ	__end_of_mcp2515_send_message-_mcp2515_send_message
	
_mcp2515_send_message:
;incstack = 0
	opt	stack 26
;mcp2515_send_message@p_canmsg stored from wreg
	movwf	((c:mcp2515_send_message@p_canmsg)),c
	line	282
	
l2232:
;mcp2515.c: 282: unsigned char status = mcp2515_read_status();
	call	_mcp2515_read_status	;wreg free
	movwf	((c:mcp2515_send_message@status)),c
	line	286
	
l2234:
;mcp2515.c: 283: unsigned char address;
;mcp2515.c: 286: if (p_canmsg->length > 8) return 0;
	movf	((c:mcp2515_send_message@p_canmsg)),c,w
	movwf	fsr2l
	clrf	fsr2h
	movlw	low(05h)
	addwf	fsr2l

	movlw	(09h-1)
	cpfsgt	indf2
	goto	u1611
	goto	u1610
u1611:
	goto	l260
u1610:
	
l2236:
	movlw	(0)&0ffh
	goto	l261
	
l260:
	line	289
;mcp2515.c: 289: if ((status & 0x04) == 0) {
	
	btfsc	((c:mcp2515_send_message@status)),c,(2)&7
	goto	u1621
	goto	u1620
u1621:
	goto	l262
u1620:
	line	290
	
l2240:
;mcp2515.c: 290: address = 0x00;
	clrf	((c:mcp2515_send_message@address)),c
	line	291
;mcp2515.c: 291: } else if ((status & 0x10) == 0) {
	goto	l263
	
l262:
	
	btfsc	((c:mcp2515_send_message@status)),c,(4)&7
	goto	u1631
	goto	u1630
u1631:
	goto	l264
u1630:
	line	292
	
l2242:
;mcp2515.c: 292: address = 0x02;
	movlw	low(02h)
	movwf	((c:mcp2515_send_message@address)),c
	line	293
;mcp2515.c: 293: } else if ((status & 0x40) == 0) {
	goto	l263
	
l264:
	
	btfsc	((c:mcp2515_send_message@status)),c,(6)&7
	goto	u1641
	goto	u1640
u1641:
	goto	l2236
u1640:
	line	294
	
l2244:
;mcp2515.c: 294: address = 0x04;
	movlw	low(04h)
	movwf	((c:mcp2515_send_message@address)),c
	line	298
;mcp2515.c: 295: } else {
	
l263:
	line	301
;mcp2515.c: 298: }
;mcp2515.c: 301: LATCbits.LATC6 = 0;
	bcf	((c:3979)),c,6	;volatile
	line	303
	
l2250:
;mcp2515.c: 303: spi_transmit(0x40 | address);
	movf	((c:mcp2515_send_message@address)),c,w
	iorlw	low(040h)
	
	call	_spi_transmit
	line	305
	
l2252:
;mcp2515.c: 305: if (p_canmsg->flags.extended) {
	movf	((c:mcp2515_send_message@p_canmsg)),c,w
	movwf	fsr2l
	clrf	fsr2h
	movlw	low(04h)
	addwf	fsr2l

	btfss	indf2,1
	goto	u1651
	goto	u1650
u1651:
	goto	l2260
u1650:
	line	306
	
l2254:
;mcp2515.c: 306: spi_transmit(p_canmsg->id >> 21);
	movf	((c:mcp2515_send_message@p_canmsg)),c,w
	movwf	fsr2l
	clrf	fsr2h
	movff	postinc2,??_mcp2515_send_message+0+0
	movff	postinc2,??_mcp2515_send_message+0+0+1
	movff	postinc2,??_mcp2515_send_message+0+0+2
	movff	postinc2,??_mcp2515_send_message+0+0+3
	movlw	015h+1
	goto	u1660
u1665:
	bcf	status,0
	rrcf	(??_mcp2515_send_message+0+3),c
	rrcf	(??_mcp2515_send_message+0+2),c
	rrcf	(??_mcp2515_send_message+0+1),c
	rrcf	(??_mcp2515_send_message+0+0),c
u1660:
	decfsz	wreg
	goto	u1665
	movf	(??_mcp2515_send_message+0+0),c,w
	
	call	_spi_transmit
	line	307
;mcp2515.c: 307: spi_transmit(((p_canmsg->id >> 13) & 0xe0) | ((p_canmsg->id >> 16) & 0x03) | 0x08);
	movf	((c:mcp2515_send_message@p_canmsg)),c,w
	movwf	fsr2l
	clrf	fsr2h
	movff	postinc2,??_mcp2515_send_message+0+0
	movff	postinc2,??_mcp2515_send_message+0+0+1
	movff	postinc2,??_mcp2515_send_message+0+0+2
	movff	postinc2,??_mcp2515_send_message+0+0+3
	movlw	010h+1
	goto	u1670
u1675:
	bcf	status,0
	rrcf	(??_mcp2515_send_message+0+3),c
	rrcf	(??_mcp2515_send_message+0+2),c
	rrcf	(??_mcp2515_send_message+0+1),c
	rrcf	(??_mcp2515_send_message+0+0),c
u1670:
	decfsz	wreg
	goto	u1675
	movlw	03h
	andwf	(??_mcp2515_send_message+0+0),c
	movf	((c:mcp2515_send_message@p_canmsg)),c,w
	movwf	fsr2l
	clrf	fsr2h
	movff	postinc2,??_mcp2515_send_message+4+0
	movff	postinc2,??_mcp2515_send_message+4+0+1
	movff	postinc2,??_mcp2515_send_message+4+0+2
	movff	postinc2,??_mcp2515_send_message+4+0+3
	movlw	0Dh+1
	goto	u1680
u1685:
	bcf	status,0
	rrcf	(??_mcp2515_send_message+4+3),c
	rrcf	(??_mcp2515_send_message+4+2),c
	rrcf	(??_mcp2515_send_message+4+1),c
	rrcf	(??_mcp2515_send_message+4+0),c
u1680:
	decfsz	wreg
	goto	u1685
	movf	(??_mcp2515_send_message+4+0),c,w
	andlw	low(0E0h)
	iorwf	(??_mcp2515_send_message+0+0),c,w
	iorlw	low(08h)
	
	call	_spi_transmit
	line	308
	
l2256:
;mcp2515.c: 308: spi_transmit(p_canmsg->id >> 8);
	movf	((c:mcp2515_send_message@p_canmsg)),c,w
	movwf	fsr2l
	clrf	fsr2h
	movff	postinc2,??_mcp2515_send_message+0+0
	movff	postinc2,??_mcp2515_send_message+0+0+1
	movff	postinc2,??_mcp2515_send_message+0+0+2
	movff	postinc2,??_mcp2515_send_message+0+0+3
	movff	??_mcp2515_send_message+0+1,??_mcp2515_send_message+0+0
	movff	??_mcp2515_send_message+0+2,??_mcp2515_send_message+0+1
	movff	??_mcp2515_send_message+0+3,??_mcp2515_send_message+0+2
	clrf	(??_mcp2515_send_message+0+3),c
	movf	(??_mcp2515_send_message+0+0),c,w
	
	call	_spi_transmit
	line	309
	
l2258:
;mcp2515.c: 309: spi_transmit(p_canmsg->id);
	movf	((c:mcp2515_send_message@p_canmsg)),c,w
	movwf	fsr2l
	clrf	fsr2h
	movf	indf2,w
	
	call	_spi_transmit
	line	310
;mcp2515.c: 310: } else {
	goto	l2266
	line	311
	
l2260:
;mcp2515.c: 311: spi_transmit(p_canmsg->id >> 3);
	movf	((c:mcp2515_send_message@p_canmsg)),c,w
	movwf	fsr2l
	clrf	fsr2h
	movff	postinc2,??_mcp2515_send_message+0+0
	movff	postinc2,??_mcp2515_send_message+0+0+1
	movff	postinc2,??_mcp2515_send_message+0+0+2
	movff	postinc2,??_mcp2515_send_message+0+0+3
	movlw	03h+1
	goto	u1690
u1695:
	bcf	status,0
	rrcf	(??_mcp2515_send_message+0+3),c
	rrcf	(??_mcp2515_send_message+0+2),c
	rrcf	(??_mcp2515_send_message+0+1),c
	rrcf	(??_mcp2515_send_message+0+0),c
u1690:
	decfsz	wreg
	goto	u1695
	movf	(??_mcp2515_send_message+0+0),c,w
	
	call	_spi_transmit
	line	312
;mcp2515.c: 312: spi_transmit(p_canmsg->id << 5);
	movf	((c:mcp2515_send_message@p_canmsg)),c,w
	movwf	fsr2l
	clrf	fsr2h
	movf	indf2,w
	movwf	(??_mcp2515_send_message+0+0)&0ffh,c
	movlw	05h
	movwf	(??_mcp2515_send_message+1+0)&0ffh,c
u1705:
	bcf	status,0
	rlcf	((??_mcp2515_send_message+0+0)),c
	decfsz	(??_mcp2515_send_message+1+0)&0ffh,c
	goto	u1705
	movf	((??_mcp2515_send_message+0+0)),c,w
	
	call	_spi_transmit
	line	313
	
l2262:
;mcp2515.c: 313: spi_transmit(0);
	movlw	(0)&0ffh
	
	call	_spi_transmit
	line	314
	
l2264:
;mcp2515.c: 314: spi_transmit(0);
	movlw	(0)&0ffh
	
	call	_spi_transmit
	line	318
	
l2266:
;mcp2515.c: 315: }
;mcp2515.c: 318: if (p_canmsg->flags.rtr) {
	movf	((c:mcp2515_send_message@p_canmsg)),c,w
	movwf	fsr2l
	clrf	fsr2h
	movlw	low(04h)
	addwf	fsr2l

	btfss	indf2,0
	goto	u1711
	goto	u1710
u1711:
	goto	l2270
u1710:
	line	319
	
l2268:
;mcp2515.c: 319: spi_transmit(p_canmsg->length | 0x40);
	movf	((c:mcp2515_send_message@p_canmsg)),c,w
	movwf	fsr2l
	clrf	fsr2h
	movlw	low(05h)
	addwf	fsr2l

	movf	indf2,w
	iorlw	low(040h)
	
	call	_spi_transmit
	line	320
;mcp2515.c: 320: } else {
	goto	l271
	line	321
	
l2270:
;mcp2515.c: 321: spi_transmit(p_canmsg->length);
	movf	((c:mcp2515_send_message@p_canmsg)),c,w
	movwf	fsr2l
	clrf	fsr2h
	movlw	low(05h)
	addwf	fsr2l

	movf	indf2,w
	
	call	_spi_transmit
	line	323
	
l2272:
;mcp2515.c: 322: unsigned char i;
;mcp2515.c: 323: for (i = 0; i < p_canmsg->length; i++) {
	clrf	((c:mcp2515_send_message@i)),c
	goto	l2278
	line	324
	
l2274:
;mcp2515.c: 324: spi_transmit(p_canmsg->data[i]);
	movf	((c:mcp2515_send_message@p_canmsg)),c,w
	addwf	((c:mcp2515_send_message@i)),c,w
	movwf	fsr2l
	clrf	fsr2h
	movlw	low(06h)
	addwf	fsr2l

	movf	indf2,w
	
	call	_spi_transmit
	line	323
	
l2276:
	incf	((c:mcp2515_send_message@i)),c
	
l2278:
	movf	((c:mcp2515_send_message@p_canmsg)),c,w
	movwf	fsr2l
	clrf	fsr2h
	movlw	low(05h)
	addwf	fsr2l

	movf	indf2,w
	subwf	((c:mcp2515_send_message@i)),c,w
	btfss	status,0
	goto	u1721
	goto	u1720
u1721:
	goto	l2274
u1720:
	line	326
	
l271:
	line	329
;mcp2515.c: 325: }
;mcp2515.c: 326: }
;mcp2515.c: 329: LATCbits.LATC6 = 1;
	bsf	((c:3979)),c,6	;volatile
	line	331
;mcp2515.c: 331: _delay(1);
	nop
	line	334
;mcp2515.c: 334: LATCbits.LATC6 = 0;
	bcf	((c:3979)),c,6	;volatile
	line	335
;mcp2515.c: 335: if (address == 0) address = 1;
	tstfsz	((c:mcp2515_send_message@address)),c
	goto	u1731
	goto	u1730
u1731:
	goto	l2282
u1730:
	
l2280:
	movlw	low(01h)
	movwf	((c:mcp2515_send_message@address)),c
	line	336
	
l2282:
;mcp2515.c: 336: spi_transmit(0x80 | address);
	movf	((c:mcp2515_send_message@address)),c,w
	iorlw	low(080h)
	
	call	_spi_transmit
	line	338
	
l2284:
;mcp2515.c: 338: LATCbits.LATC6 = 1;
	bsf	((c:3979)),c,6	;volatile
	line	340
	
l2286:
;mcp2515.c: 340: return 1;
	movlw	(01h)&0ffh
	line	341
	
l261:
	return
	opt stack 0
GLOBAL	__end_of_mcp2515_send_message
	__end_of_mcp2515_send_message:
	signat	_mcp2515_send_message,4217
	global	_mcp2515_read_status

;; *************** function _mcp2515_read_status *****************
;; Defined at:
;;		line 239 in file "D:\Projects\USBtin\firmware\source\mcp2515.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  status          1    1[COMRAM] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         1       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         1       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_spi_transmit
;; This function is called by:
;;		_mcp2515_send_message
;; This function uses a non-reentrant model
;;
psect	text8,class=CODE,space=0,reloc=2
	line	239
global __ptext8
__ptext8:
psect	text8
	file	"D:\Projects\USBtin\firmware\source\mcp2515.c"
	line	239
	global	__size_of_mcp2515_read_status
	__size_of_mcp2515_read_status	equ	__end_of_mcp2515_read_status-_mcp2515_read_status
	
_mcp2515_read_status:
;incstack = 0
	opt	stack 26
	line	242
	
l2164:
;mcp2515.c: 242: LATCbits.LATC6 = 0;
	bcf	((c:3979)),c,6	;volatile
	line	244
	
l2166:
;mcp2515.c: 244: spi_transmit(0xA0);
	movlw	(0A0h)&0ffh
	
	call	_spi_transmit
	line	245
;mcp2515.c: 245: unsigned char status = spi_transmit(0xff);
	movlw	(0FFh)&0ffh
	
	call	_spi_transmit
	movwf	((c:mcp2515_read_status@status)),c
	line	248
	
l2168:
;mcp2515.c: 248: LATCbits.LATC6 = 1;
	bsf	((c:3979)),c,6	;volatile
	line	250
	
l2170:
;mcp2515.c: 250: return status;
	movf	((c:mcp2515_read_status@status)),c,w
	line	251
	
l254:
	return
	opt stack 0
GLOBAL	__end_of_mcp2515_read_status
	__end_of_mcp2515_read_status:
	signat	_mcp2515_read_status,89
	global	_sendByteHex

;; *************** function _sendByteHex *****************
;; Defined at:
;;		line 125 in file "D:\Projects\USBtin\firmware\source\main.c"
;; Parameters:    Size  Location     Type
;;  value           1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  value           1   47[COMRAM] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr1l, fsr1h, fsr2l, fsr2h, status,2, status,0, tblptrl, tblptrh, tblptru, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         1       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         1       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    7
;; This function calls:
;;		_sendHex
;; This function is called by:
;;		_parseLine
;; This function uses a non-reentrant model
;;
psect	text9,class=CODE,space=0,reloc=2
	file	"D:\Projects\USBtin\firmware\source\main.c"
	line	125
global __ptext9
__ptext9:
psect	text9
	file	"D:\Projects\USBtin\firmware\source\main.c"
	line	125
	global	__size_of_sendByteHex
	__size_of_sendByteHex	equ	__end_of_sendByteHex-_sendByteHex
	
_sendByteHex:
;incstack = 0
	opt	stack 22
;sendByteHex@value stored from wreg
	movwf	((c:sendByteHex@value)),c
	line	127
	
l2298:
;main.c: 127: sendHex(value, 2);
	movf	((c:sendByteHex@value)),c,w
	movwf	((c:sendHex@value)),c
	clrf	((c:sendHex@value+1)),c
	clrf	((c:sendHex@value+2)),c
	clrf	((c:sendHex@value+3)),c

	movlw	low(02h)
	movwf	((c:sendHex@len)),c
	call	_sendHex	;wreg free
	line	128
	
l104:
	return
	opt stack 0
GLOBAL	__end_of_sendByteHex
	__end_of_sendByteHex:
	signat	_sendByteHex,4216
	global	_sendHex

;; *************** function _sendHex *****************
;; Defined at:
;;		line 101 in file "D:\Projects\USBtin\firmware\source\main.c"
;; Parameters:    Size  Location     Type
;;  value           4   20[COMRAM] unsigned long 
;;  len             1   24[COMRAM] unsigned char 
;; Auto vars:     Size  Location     Type
;;  hex             1   46[COMRAM] unsigned char 
;;  s              20   26[COMRAM] unsigned char [20]
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr1l, fsr1h, fsr2l, fsr2h, status,2, status,0, tblptrl, tblptrh, tblptru, cstack
;; Tracked objects:
;;		On entry : 0/1
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         5       0       0       0       0       0       0       0       0       0       0
;;      Locals:        21       0       0       0       0       0       0       0       0       0       0
;;      Temps:          1       0       0       0       0       0       0       0       0       0       0
;;      Totals:        27       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:       27 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    6
;; This function calls:
;;		_usb_putstr
;; This function is called by:
;;		_sendByteHex
;;		_parseLine
;;		_main
;; This function uses a non-reentrant model
;;
psect	text10,class=CODE,space=0,reloc=2
	line	101
global __ptext10
__ptext10:
psect	text10
	file	"D:\Projects\USBtin\firmware\source\main.c"
	line	101
	global	__size_of_sendHex
	__size_of_sendHex	equ	__end_of_sendHex-_sendHex
	
_sendHex:
;incstack = 0
	opt	stack 24
	line	104
	
l2214:
;main.c: 103: char s[20];
;main.c: 104: s[len] = 0;
	movf	((c:sendHex@len)),c,w
	addlw	low((c:sendHex@s))
	movwf	fsr2l
	clrf	fsr2h
	movlw	low(0)
	movwf	indf2
	line	106
;main.c: 106: while (len--) {
	goto	l2228
	line	108
	
l2216:
;main.c: 108: unsigned char hex = value & 0x0f;
	movf	((c:sendHex@value)),c,w
	andlw	low(0Fh)
	movwf	((c:sendHex@hex)),c
	line	109
	
l2218:
;main.c: 109: if (hex > 9) hex = hex - 10 + 'A';
	movlw	(0Ah-1)
	cpfsgt	((c:sendHex@hex)),c
	goto	u1581
	goto	u1580
u1581:
	goto	l2222
u1580:
	
l2220:
	movlw	(037h)&0ffh
	addwf	((c:sendHex@hex)),c
	goto	l2224
	line	110
	
l2222:
;main.c: 110: else hex = hex + '0';
	movlw	(030h)&0ffh
	addwf	((c:sendHex@hex)),c
	line	111
	
l2224:
;main.c: 111: s[len] = hex;
	movf	((c:sendHex@len)),c,w
	addlw	low((c:sendHex@s))
	movwf	fsr2l
	clrf	fsr2h
	movff	(c:sendHex@hex),indf2

	line	113
	
l2226:
;main.c: 113: value = value >> 4;
	movlw	04h
	movwf	(??_sendHex+0+0)&0ffh,c
u1595:
	bcf	status,0
	rrcf	((c:sendHex@value+3)),c
	rrcf	((c:sendHex@value+2)),c
	rrcf	((c:sendHex@value+1)),c
	rrcf	((c:sendHex@value)),c
	decfsz	(??_sendHex+0+0)&0ffh,c
	goto	u1595

	line	106
	
l2228:
	decf	((c:sendHex@len)),c
	incf	((c:sendHex@len))&0ffh,w

	btfss	status,2
	goto	u1601
	goto	u1600
u1601:
	goto	l2216
u1600:
	line	116
	
l2230:
;main.c: 114: }
;main.c: 116: usb_putstr(s);
	movlw	((c:sendHex@s))&0ffh
	
	call	_usb_putstr
	line	118
	
l101:
	return
	opt stack 0
GLOBAL	__end_of_sendHex
	__end_of_sendHex:
	signat	_sendHex,8312
	global	_usb_putstr

;; *************** function _usb_putstr *****************
;; Defined at:
;;		line 352 in file "D:\Projects\USBtin\firmware\source\usb_cdc.c"
;; Parameters:    Size  Location     Type
;;  s               1    wreg     PTR unsigned char 
;;		 -> sendHex@s(20), 
;; Auto vars:     Size  Location     Type
;;  s               1   19[COMRAM] PTR unsigned char 
;;		 -> sendHex@s(20), 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr1l, fsr1h, fsr2l, fsr2h, status,2, status,0, tblptrl, tblptrh, tblptru, cstack
;; Tracked objects:
;;		On entry : 0/1
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         1       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         1       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    5
;; This function calls:
;;		_usb_putch
;; This function is called by:
;;		_sendHex
;; This function uses a non-reentrant model
;;
psect	text11,class=CODE,space=0,reloc=2
	file	"D:\Projects\USBtin\firmware\source\usb_cdc.c"
	line	352
global __ptext11
__ptext11:
psect	text11
	file	"D:\Projects\USBtin\firmware\source\usb_cdc.c"
	line	352
	global	__size_of_usb_putstr
	__size_of_usb_putstr	equ	__end_of_usb_putstr-_usb_putstr
	
_usb_putstr:
;incstack = 0
	opt	stack 24
;usb_putstr@s stored from wreg
	movwf	((c:usb_putstr@s)),c
	line	353
	
l2156:
;usb_cdc.c: 353: while (*s) {
	goto	l2162
	line	354
	
l2158:
;usb_cdc.c: 354: usb_putch((unsigned char) *s);
	movf	((c:usb_putstr@s)),c,w
	movwf	fsr2l
	clrf	fsr2h
	movf	indf2,w
	
	call	_usb_putch
	line	355
	
l2160:
;usb_cdc.c: 355: s++;
	incf	((c:usb_putstr@s)),c
	line	353
	
l2162:
	movf	((c:usb_putstr@s)),c,w
	movwf	fsr2l
	clrf	fsr2h
	movf	indf2,w
	btfss	status,2
	goto	u1481
	goto	u1480
u1481:
	goto	l2158
u1480:
	line	357
	
l394:
	return
	opt stack 0
GLOBAL	__end_of_usb_putstr
	__end_of_usb_putstr:
	signat	_usb_putstr,4216
	global	_usb_putch

;; *************** function _usb_putch *****************
;; Defined at:
;;		line 331 in file "D:\Projects\USBtin\firmware\source\usb_cdc.c"
;; Parameters:    Size  Location     Type
;;  ch              1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  ch              1   18[COMRAM] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr1l, fsr1h, fsr2l, fsr2h, status,2, status,0, tblptrl, tblptrh, tblptru, cstack
;; Tracked objects:
;;		On entry : 0/1
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         1       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         1       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    4
;; This function calls:
;;		_usb_process
;; This function is called by:
;;		_parseLine
;;		_main
;;		_usb_putstr
;; This function uses a non-reentrant model
;;
psect	text12,class=CODE,space=0,reloc=2
	line	331
global __ptext12
__ptext12:
psect	text12
	file	"D:\Projects\USBtin\firmware\source\usb_cdc.c"
	line	331
	global	__size_of_usb_putch
	__size_of_usb_putch	equ	__end_of_usb_putch-_usb_putch
	
_usb_putch:
;incstack = 0
	opt	stack 26
;usb_putch@ch stored from wreg
	movwf	((c:usb_putch@ch)),c
	line	333
	
l2138:
;usb_cdc.c: 333: if (txbuffer_bytesleft == 128) {
	movf	((c:_txbuffer_bytesleft)),c,w
	xorlw	128

	btfss	status,2
	goto	u1451
	goto	u1450
u1451:
	goto	l2142
u1450:
	goto	l387
	line	338
	
l2142:
;usb_cdc.c: 336: }
;usb_cdc.c: 338: txbuffer[txbuffer_writepos] = ch;
	movlb	1	; () banked
	movlw	low(_txbuffer)
	addwf	((c:_txbuffer_writepos)),c,w
	movwf	c:fsr2l
	clrf	1+c:fsr2l
	movlw	high(_txbuffer)
	addwfc	1+c:fsr2l
	movff	(c:usb_putch@ch),indf2

	line	339
	
l2144:; BSR set to: 1

;usb_cdc.c: 339: txbuffer_writepos++;
	incf	((c:_txbuffer_writepos)),c
	line	340
	
l2146:; BSR set to: 1

;usb_cdc.c: 340: if (txbuffer_writepos == 128) txbuffer_writepos = 0;
	movf	((c:_txbuffer_writepos)),c,w
	xorlw	128

	btfss	status,2
	goto	u1461
	goto	u1460
u1461:
	goto	l2150
u1460:
	
l2148:; BSR set to: 1

	clrf	((c:_txbuffer_writepos)),c
	line	341
	
l2150:; BSR set to: 1

;usb_cdc.c: 341: txbuffer_bytesleft++;
	incf	((c:_txbuffer_bytesleft)),c
	line	344
	
l2152:; BSR set to: 1

;usb_cdc.c: 344: usb_process();
	call	_usb_process	;wreg free
	line	345
	
l387:
	return
	opt stack 0
GLOBAL	__end_of_usb_putch
	__end_of_usb_putch:
	signat	_usb_putch,4216
	global	_usb_process

;; *************** function _usb_process *****************
;; Defined at:
;;		line 458 in file "D:\Projects\USBtin\firmware\source\usb_cdc.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  i               1   16[COMRAM] unsigned char 
;;  i               1   15[COMRAM] unsigned char 
;;  i               1   17[COMRAM] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr1l, fsr1h, fsr2l, fsr2h, status,2, status,0, tblptrl, tblptrh, tblptru, cstack
;; Tracked objects:
;;		On entry : 0/1
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         3       0       0       0       0       0       0       0       0       0       0
;;      Temps:          1       0       0       0       0       0       0       0       0       0       0
;;      Totals:         4       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_usb_handleDescriptorRequest
;;		_usb_sendProcess
;;		_usb_txprocess
;; This function is called by:
;;		_main
;;		_usb_putch
;; This function uses a non-reentrant model
;;
psect	text13,class=CODE,space=0,reloc=2
	line	458
global __ptext13
__ptext13:
psect	text13
	file	"D:\Projects\USBtin\firmware\source\usb_cdc.c"
	line	458
	global	__size_of_usb_process
	__size_of_usb_process	equ	__end_of_usb_process-_usb_process
	
_usb_process:
;incstack = 0
	opt	stack 26
	line	460
	
l2030:
;usb_cdc.c: 460: usb_txprocess();
	call	_usb_txprocess	;wreg free
	line	462
	
l2032:
;usb_cdc.c: 462: if (UIRbits.TRNIF) {
	btfss	((c:3944)),c,3	;volatile
	goto	u1331
	goto	u1330
u1331:
	goto	l462
u1330:
	line	465
	
l2034:
;usb_cdc.c: 465: if (USTAT == 0x00) {
	tstfsz	((c:3948)),c	;volatile
	goto	u1341
	goto	u1340
u1341:
	goto	l2126
u1340:
	line	469
	
l2036:
;usb_cdc.c: 469: if (((ep[0].out.stat >> 2) & 0x0F) == 0xD) {
	movff	(512),??_usb_process+0+0	;volatile
	bcf	status,0
	rrcf	(??_usb_process+0+0),c
	bcf	status,0
	rrcf	(??_usb_process+0+0),c

	movlw	0Fh
	andwf	(??_usb_process+0+0),c
	movf	(??_usb_process+0+0),c,w
	xorlw	0Dh

	btfss	status,2
	goto	u1351
	goto	u1350
u1351:
	goto	l2110
u1350:
	line	472
	
l2038:
;usb_cdc.c: 472: ep[0].in.stat = 0;
	movlb	2	; () banked
	clrf	(0+(512+04h))&0ffh	;volatile
	line	473
;usb_cdc.c: 473: ep[0].in.stat = 0;
	clrf	(0+(512+04h))&0ffh	;volatile
	line	475
	
l2040:; BSR set to: 2

;usb_cdc.c: 475: if ((ep0out_buffer[0] & 0x60) == (0<<5)) {
	movff	(640),??_usb_process+0+0	;volatile
	movlw	060h
	andwf	(??_usb_process+0+0),c
	btfss	status,2
	goto	u1361
	goto	u1360
u1361:
	goto	l2070
u1360:
	goto	l2068
	line	479
	
l2044:; BSR set to: 2

;usb_cdc.c: 479: if (!usb_handleDescriptorRequest(ep0out_buffer[3], ep0out_buffer[2] , (ep0out_buffer[7] << 8) | ep0out_buffer[6])) {
	movff	0+(640+02h),(c:usb_handleDescriptorRequest@index)	;volatile
	movf	(0+(640+07h))&0ffh,w	;volatile
	movwf	((c:usb_handleDescriptorRequest@length+1)),c
	movf	(0+(640+06h))&0ffh,w	;volatile
	movwf	((c:usb_handleDescriptorRequest@length)),c
	movf	(0+(640+03h))&0ffh,w	;volatile
	
	call	_usb_handleDescriptorRequest
	iorlw	0
	btfss	status,2
	goto	u1371
	goto	u1370
u1371:
	goto	l441
u1370:
	line	480
	
l2046:
;usb_cdc.c: 480: ep[0].in.cnt = 0;
	movlb	2	; () banked
	clrf	(0+(512+05h))&0ffh	;volatile
	line	481
	
l2048:; BSR set to: 2

;usb_cdc.c: 481: ep[0].in.stat = 0xCC;
	movlw	low(0CCh)
	movwf	(0+(512+04h))&0ffh	;volatile
	goto	l2124
	line	484
;usb_cdc.c: 484: case 0x05:
	
l431:; BSR set to: 2

	line	486
;usb_cdc.c: 486: usb_setaddress = ep0out_buffer[2];
	movff	0+(640+02h),(c:_usb_setaddress)	;volatile
	line	488
;usb_cdc.c: 488: ep[0].in.cnt = 0;
	clrf	(0+(512+05h))&0ffh	;volatile
	line	489
	
l2050:; BSR set to: 2

;usb_cdc.c: 489: ep[0].in.stat = 0xC8;
	movlw	low(0C8h)
	movwf	(0+(512+04h))&0ffh	;volatile
	line	491
;usb_cdc.c: 491: break;
	goto	l2124
	line	492
;usb_cdc.c: 492: case 0x09:
	
l432:; BSR set to: 2

	line	494
;usb_cdc.c: 494: usb_config = ep0out_buffer[2];
	movff	0+(640+02h),(c:_usb_config)	;volatile
	line	495
	
l2052:; BSR set to: 2

;usb_cdc.c: 495: configured = 1;
	movlw	high(01h)
	movwf	((c:_configured+1)),c
	movlw	low(01h)
	movwf	((c:_configured)),c
	line	496
	
l2054:; BSR set to: 2

;usb_cdc.c: 496: ep[0].in.cnt = 0;
	clrf	(0+(512+05h))&0ffh	;volatile
	line	497
;usb_cdc.c: 497: ep[0].in.stat = 0xC8;
	movlw	low(0C8h)
	movwf	(0+(512+04h))&0ffh	;volatile
	line	498
;usb_cdc.c: 498: break;
	goto	l2124
	line	500
;usb_cdc.c: 500: case 0x08:
	
l433:; BSR set to: 2

	line	502
;usb_cdc.c: 502: ep0in_buffer[0] = usb_config;
	movff	(c:_usb_config),(648)	;volatile
	line	503
	
l2056:; BSR set to: 2

;usb_cdc.c: 503: ep[0].in.cnt = 1;
	movlw	low(01h)
	movwf	(0+(512+05h))&0ffh	;volatile
	line	504
;usb_cdc.c: 504: ep[0].in.stat = 0xC8;
	movlw	low(0C8h)
	movwf	(0+(512+04h))&0ffh	;volatile
	line	505
;usb_cdc.c: 505: break;
	goto	l2124
	line	507
;usb_cdc.c: 507: case 0x0A:
	
l434:; BSR set to: 2

	line	509
;usb_cdc.c: 509: ep0in_buffer[0] = 0;
	clrf	((648))&0ffh	;volatile
	line	510
	
l2058:; BSR set to: 2

;usb_cdc.c: 510: ep[0].in.cnt = 1;
	movlw	low(01h)
	movwf	(0+(512+05h))&0ffh	;volatile
	line	511
;usb_cdc.c: 511: ep[0].in.stat = 0xC8;
	movlw	low(0C8h)
	movwf	(0+(512+04h))&0ffh	;volatile
	line	512
;usb_cdc.c: 512: break;
	goto	l2124
	line	515
	
l436:; BSR set to: 2

	line	516
;usb_cdc.c: 515: case 0x00:
;usb_cdc.c: 516: ep0in_buffer[0] = 0;
	clrf	((648))&0ffh	;volatile
	line	517
;usb_cdc.c: 517: ep0in_buffer[1] = 0;
	clrf	(0+(648+01h))&0ffh	;volatile
	line	518
	
l2060:; BSR set to: 2

;usb_cdc.c: 518: ep[0].in.cnt = 2;
	movlw	low(02h)
	movwf	(0+(512+05h))&0ffh	;volatile
	line	519
;usb_cdc.c: 519: ep[0].in.stat = 0xC8;
	movlw	low(0C8h)
	movwf	(0+(512+04h))&0ffh	;volatile
	line	520
;usb_cdc.c: 520: break;
	goto	l2124
	line	524
	
l439:; BSR set to: 2

	line	525
;usb_cdc.c: 523: case 0x01:
;usb_cdc.c: 524: case 0x03:
;usb_cdc.c: 525: ep[0].in.cnt = 0;
	clrf	(0+(512+05h))&0ffh	;volatile
	line	526
	
l2062:; BSR set to: 2

;usb_cdc.c: 526: ep[0].in.stat = 0xC8;
	movlw	low(0C8h)
	movwf	(0+(512+04h))&0ffh	;volatile
	line	527
;usb_cdc.c: 527: break;
	goto	l2124
	line	528
;usb_cdc.c: 528: default:
	
l440:; BSR set to: 2

	line	529
;usb_cdc.c: 529: ep[0].in.cnt = 0;
	clrf	(0+(512+05h))&0ffh	;volatile
	line	530
	
l2064:; BSR set to: 2

;usb_cdc.c: 530: ep[0].in.stat = 0xCC;
	movlw	low(0CCh)
	movwf	(0+(512+04h))&0ffh	;volatile
	line	531
;usb_cdc.c: 531: break;
	goto	l2124
	line	477
	
l2068:; BSR set to: 2

	movf	(0+(640+01h))&0ffh,w	;volatile
	; Switch size 1, requested type "space"
; Number of cases is 10, Range of values is 0 to 18
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           31    16 (average)
;	Chosen strategy is simple_byte

	xorlw	0^0	; case 0
	skipnz
	goto	l436
	xorlw	1^0	; case 1
	skipnz
	goto	l439
	xorlw	3^1	; case 3
	skipnz
	goto	l439
	xorlw	5^3	; case 5
	skipnz
	goto	l431
	xorlw	6^5	; case 6
	skipnz
	goto	l2044
	xorlw	8^6	; case 8
	skipnz
	goto	l433
	xorlw	9^8	; case 9
	skipnz
	goto	l432
	xorlw	10^9	; case 10
	skipnz
	goto	l434
	xorlw	17^10	; case 17
	skipnz
	goto	l439
	xorlw	18^17	; case 18
	skipnz
	goto	l436
	goto	l440

	line	534
	
l2070:; BSR set to: 2

	movff	(640),??_usb_process+0+0	;volatile
	movlw	060h
	andwf	(??_usb_process+0+0),c
	movf	(??_usb_process+0+0),c,w
	xorlw	020h

	btfss	status,2
	goto	u1381
	goto	u1380
u1381:
	goto	l2124
u1380:
	goto	l2108
	line	537
;usb_cdc.c: 537: case 0x01:
	
l444:; BSR set to: 2

	line	538
;usb_cdc.c: 538: {unsigned char i; for (i=0; i<8; i++) {ep0in_buffer[i] = 0;}};
	clrf	((c:usb_process@i)),c
	
l2078:; BSR set to: 2

	movlw	low(648)	;volatile
	addwf	((c:usb_process@i)),c,w
	movwf	c:fsr2l
	clrf	1+c:fsr2l
	movlw	high(648)	;volatile
	addwfc	1+c:fsr2l
	movlw	low(0)
	movwf	indf2
	
l2080:; BSR set to: 2

	incf	((c:usb_process@i)),c
	
l2082:; BSR set to: 2

	movlw	(08h-1)
	cpfsgt	((c:usb_process@i)),c
	goto	u1391
	goto	u1390
u1391:
	goto	l2078
u1390:
	line	539
	
l2084:; BSR set to: 2

;usb_cdc.c: 539: ep[0].in.cnt = 8;
	movlw	low(08h)
	movwf	(0+(512+05h))&0ffh	;volatile
	line	540
;usb_cdc.c: 540: ep[0].in.stat = 0xC8;
	movlw	low(0C8h)
	movwf	(0+(512+04h))&0ffh	;volatile
	line	541
;usb_cdc.c: 541: break;
	goto	l2124
	line	544
	
l2086:; BSR set to: 2

;usb_cdc.c: 544: dolinecoding = 1;
	movlw	low(01h)
	movwf	((c:_dolinecoding)),c
	line	545
	
l2088:; BSR set to: 2

;usb_cdc.c: 545: ep[0].in.cnt = 0;
	clrf	(0+(512+05h))&0ffh	;volatile
	line	546
;usb_cdc.c: 546: ep[0].in.stat = 0xC8;
	movlw	low(0C8h)
	movwf	(0+(512+04h))&0ffh	;volatile
	line	547
;usb_cdc.c: 547: break;
	goto	l2124
	line	549
;usb_cdc.c: 549: case 0x21:
	
l449:; BSR set to: 2

	line	552
;usb_cdc.c: 550: {
;usb_cdc.c: 551: unsigned char i;
;usb_cdc.c: 552: for (i = 0; i < sizeof(linecoding); i++) {
	clrf	((c:usb_process@i_1029)),c
	line	553
	
l2094:; BSR set to: 2

;usb_cdc.c: 553: ep0in_buffer[i] = linecoding[i];
	movf	((c:usb_process@i_1029)),c,w
	addlw	low((c:_linecoding))
	movwf	fsr2l
	clrf	fsr2h
	movlw	low(648)	;volatile
	addwf	((c:usb_process@i_1029)),c,w
	movwf	c:fsr1l
	clrf	1+c:fsr1l
	movlw	high(648)	;volatile
	addwfc	1+c:fsr1l
	movff	indf2,indf1
	line	552
	
l2096:; BSR set to: 2

	incf	((c:usb_process@i_1029)),c
	
l2098:; BSR set to: 2

	movlw	(07h-1)
	cpfsgt	((c:usb_process@i_1029)),c
	goto	u1401
	goto	u1400
u1401:
	goto	l2094
u1400:
	line	556
	
l2100:; BSR set to: 2

;usb_cdc.c: 554: }
;usb_cdc.c: 555: }
;usb_cdc.c: 556: ep[0].in.cnt = 7;
	movlw	low(07h)
	movwf	(0+(512+05h))&0ffh	;volatile
	line	557
;usb_cdc.c: 557: ep[0].in.stat = 0xC8;
	movlw	low(0C8h)
	movwf	(0+(512+04h))&0ffh	;volatile
	line	558
;usb_cdc.c: 558: break;
	goto	l2124
	line	561
	
l453:; BSR set to: 2

	line	562
;usb_cdc.c: 561: case 0x00:
;usb_cdc.c: 562: ep[0].in.cnt = 0;
	clrf	(0+(512+05h))&0ffh	;volatile
	line	563
	
l2102:; BSR set to: 2

;usb_cdc.c: 563: ep[0].in.stat = 0xC8;
	movlw	low(0C8h)
	movwf	(0+(512+04h))&0ffh	;volatile
	line	564
;usb_cdc.c: 564: break;
	goto	l2124
	line	565
;usb_cdc.c: 565: default:
	
l454:; BSR set to: 2

	line	566
;usb_cdc.c: 566: ep[0].in.cnt = 0;
	clrf	(0+(512+05h))&0ffh	;volatile
	line	567
	
l2104:; BSR set to: 2

;usb_cdc.c: 567: ep[0].in.stat = 0xCC;
	movlw	low(0CCh)
	movwf	(0+(512+04h))&0ffh	;volatile
	line	568
;usb_cdc.c: 568: break;
	goto	l2124
	line	535
	
l2108:; BSR set to: 2

	movf	(0+(640+01h))&0ffh,w	;volatile
	; Switch size 1, requested type "space"
; Number of cases is 5, Range of values is 0 to 34
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           16     9 (average)
;	Chosen strategy is simple_byte

	xorlw	0^0	; case 0
	skipnz
	goto	l453
	xorlw	1^0	; case 1
	skipnz
	goto	l444
	xorlw	32^1	; case 32
	skipnz
	goto	l2086
	xorlw	33^32	; case 33
	skipnz
	goto	l449
	xorlw	34^33	; case 34
	skipnz
	goto	l453
	goto	l454

	line	573
	
l441:
;usb_cdc.c: 570: }
;usb_cdc.c: 573: } else {
	goto	l2124
	line	576
	
l2110:
;usb_cdc.c: 576: if (dolinecoding) {
	movf	((c:_dolinecoding)),c,w
	btfsc	status,2
	goto	u1411
	goto	u1410
u1411:
	goto	l2124
u1410:
	line	578
	
l2112:
;usb_cdc.c: 577: unsigned char i;
;usb_cdc.c: 578: for (i = 0; i < sizeof(linecoding); i++) {
	clrf	((c:usb_process@i_1030)),c
	line	579
	
l2118:
;usb_cdc.c: 579: linecoding[i] = ep0out_buffer[i];
	movlb	2	; () banked
	movlw	low(640)	;volatile
	addwf	((c:usb_process@i_1030)),c,w
	movwf	c:fsr2l
	clrf	1+c:fsr2l
	movlw	high(640)	;volatile
	addwfc	1+c:fsr2l
	movf	((c:usb_process@i_1030)),c,w
	addlw	low((c:_linecoding))
	movwf	fsr1l
	clrf	fsr1h
	movff	indf2,indf1
	line	578
	
l2120:; BSR set to: 2

	incf	((c:usb_process@i_1030)),c
	
l2122:; BSR set to: 2

	movlw	(07h-1)
	cpfsgt	((c:usb_process@i_1030)),c
	goto	u1421
	goto	u1420
u1421:
	goto	l2118
u1420:
	
l458:; BSR set to: 2

	line	581
;usb_cdc.c: 580: }
;usb_cdc.c: 581: dolinecoding = 0;
	clrf	((c:_dolinecoding)),c
	line	585
	
l2124:
;usb_cdc.c: 582: }
;usb_cdc.c: 583: }
;usb_cdc.c: 585: ep[0].out.cnt = 0x08;
	movlw	low(08h)
	movlb	2	; () banked
	movwf	(0+(512+01h))&0ffh	;volatile
	line	586
;usb_cdc.c: 586: ep[0].out.stat = 0x80;
	movlw	low(080h)
	movwf	((512))&0ffh	;volatile
	line	588
;usb_cdc.c: 588: } else if (USTAT == 0x04) {
	goto	l2134
	
l2126:
	movf	((c:3948)),c,w	;volatile
	xorlw	4

	btfss	status,2
	goto	u1431
	goto	u1430
u1431:
	goto	l2134
u1430:
	line	591
	
l2128:
;usb_cdc.c: 591: if (usb_setaddress > 0) {
	movf	((c:_usb_setaddress)),c,w
	btfsc	status,2
	goto	u1441
	goto	u1440
u1441:
	goto	l2132
u1440:
	line	592
	
l2130:
;usb_cdc.c: 592: UADDR = usb_setaddress;
	movff	(c:_usb_setaddress),(c:3950)	;volatile
	line	593
;usb_cdc.c: 593: usb_setaddress = 0;
	clrf	((c:_usb_setaddress)),c
	line	596
	
l2132:
;usb_cdc.c: 594: }
;usb_cdc.c: 596: usb_sendProcess();
	call	_usb_sendProcess	;wreg free
	line	600
	
l2134:
;usb_cdc.c: 598: }
;usb_cdc.c: 600: UCONbits.PKTDIS = 0;
	bcf	((c:3949)),c,4	;volatile
	line	601
	
l2136:
;usb_cdc.c: 601: UIRbits.TRNIF = 0;
	bcf	((c:3944)),c,3	;volatile
	line	603
	
l462:
	return
	opt stack 0
GLOBAL	__end_of_usb_process
	__end_of_usb_process:
	signat	_usb_process,88
	global	_usb_txprocess

;; *************** function _usb_txprocess *****************
;; Defined at:
;;		line 362 in file "D:\Projects\USBtin\firmware\source\usb_cdc.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  count           1    2[COMRAM] unsigned char 
;;  readpos         1    1[COMRAM] unsigned char 
;;  i               1    0[COMRAM] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr1l, fsr1h, fsr2l, fsr2h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/1
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         3       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         3       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_usb_process
;; This function uses a non-reentrant model
;;
psect	text14,class=CODE,space=0,reloc=2
	line	362
global __ptext14
__ptext14:
psect	text14
	file	"D:\Projects\USBtin\firmware\source\usb_cdc.c"
	line	362
	global	__size_of_usb_txprocess
	__size_of_usb_txprocess	equ	__end_of_usb_txprocess-_usb_txprocess
	
_usb_txprocess:
;incstack = 0
	opt	stack 28
	line	363
	
l1992:
;usb_cdc.c: 363: if (txbuffer_bytesleft == 0) return;
	tstfsz	((c:_txbuffer_bytesleft)),c
	goto	u1261
	goto	u1260
u1261:
	goto	l1996
u1260:
	goto	l398
	line	364
	
l1996:
;usb_cdc.c: 364: if (!configured) return;
	movf	((c:_configured+1)),c,w
	iorwf ((c:_configured)),c,w

	btfss	status,2
	goto	u1271
	goto	u1270
u1271:
	goto	l399
u1270:
	goto	l398
	
l399:
	line	365
;usb_cdc.c: 365: if (ep[1].in.stat & 0x80) return;
	movlb	2	; () banked
	
	btfss	(0+(512+0Ch))&0ffh,(7)&7	;volatile
	goto	u1281
	goto	u1280
u1281:
	goto	l400
u1280:
	goto	l398
	
l400:; BSR set to: 2

	line	367
;usb_cdc.c: 367: unsigned char count = txbuffer_bytesleft;
	movff	(c:_txbuffer_bytesleft),(c:usb_txprocess@count)
	line	368
	
l2002:; BSR set to: 2

;usb_cdc.c: 368: if (count > 0x40 - 1) count = 0x40 - 1;
	movlw	(040h-1)
	cpfsgt	((c:usb_txprocess@count)),c
	goto	u1291
	goto	u1290
u1291:
	goto	l2006
u1290:
	
l2004:; BSR set to: 2

	movlw	low(03Fh)
	movwf	((c:usb_txprocess@count)),c
	line	370
	
l2006:; BSR set to: 2

;usb_cdc.c: 370: unsigned char readpos = (128 + txbuffer_writepos - txbuffer_bytesleft) % 128;
	movf	((c:_txbuffer_bytesleft)),c,w
	subwf	((c:_txbuffer_writepos)),c,w
	addlw	low(080h)
	andlw	low(07Fh)
	movwf	((c:usb_txprocess@readpos)),c
	line	373
	
l2008:; BSR set to: 2

;usb_cdc.c: 372: unsigned char i;
;usb_cdc.c: 373: for (i = 0; i < count; i++) {
	clrf	((c:usb_txprocess@i)),c
	goto	l2020
	line	374
	
l2010:; BSR set to: 2

;usb_cdc.c: 374: ep1in_buffer[i] = txbuffer[readpos];
	movlb	1	; () banked
	movlw	low(_txbuffer)
	addwf	((c:usb_txprocess@readpos)),c,w
	movwf	c:fsr2l
	clrf	1+c:fsr2l
	movlw	high(_txbuffer)
	addwfc	1+c:fsr2l
	movlb	2	; () banked
	movlw	low(680)	;volatile
	addwf	((c:usb_txprocess@i)),c,w
	movwf	c:fsr1l
	clrf	1+c:fsr1l
	movlw	high(680)	;volatile
	addwfc	1+c:fsr1l
	movff	indf2,indf1
	line	375
	
l2012:; BSR set to: 2

;usb_cdc.c: 375: readpos ++;
	incf	((c:usb_txprocess@readpos)),c
	line	376
	
l2014:; BSR set to: 2

;usb_cdc.c: 376: if (readpos == 128) readpos = 0;
	movf	((c:usb_txprocess@readpos)),c,w
	xorlw	128

	btfss	status,2
	goto	u1301
	goto	u1300
u1301:
	goto	l2018
u1300:
	
l2016:; BSR set to: 2

	clrf	((c:usb_txprocess@readpos)),c
	line	373
	
l2018:; BSR set to: 2

	incf	((c:usb_txprocess@i)),c
	
l2020:; BSR set to: 2

	movf	((c:usb_txprocess@count)),c,w
	subwf	((c:usb_txprocess@i)),c,w
	btfss	status,0
	goto	u1311
	goto	u1310
u1311:
	goto	l2010
u1310:
	
l405:; BSR set to: 2

	line	379
;usb_cdc.c: 377: }
;usb_cdc.c: 379: ep[1].in.cnt = count;
	movff	(c:usb_txprocess@count),0+(512+0Dh)	;volatile
	line	380
	
l2022:; BSR set to: 2

;usb_cdc.c: 380: txbuffer_bytesleft -= count;
	movf	((c:usb_txprocess@count)),c,w
	subwf	((c:_txbuffer_bytesleft)),c
	line	382
	
l2024:; BSR set to: 2

;usb_cdc.c: 382: if (ep[1].in.stat & 0x40)
	
	btfss	(0+(512+0Ch))&0ffh,(6)&7	;volatile
	goto	u1321
	goto	u1320
u1321:
	goto	l2028
u1320:
	line	383
	
l2026:; BSR set to: 2

;usb_cdc.c: 383: ep[1].in.stat = 0x88;
	movlw	low(088h)
	movwf	(0+(512+0Ch))&0ffh	;volatile
	goto	l398
	line	385
	
l2028:; BSR set to: 2

;usb_cdc.c: 384: else
;usb_cdc.c: 385: ep[1].in.stat = 0xC8;
	movlw	low(0C8h)
	movwf	(0+(512+0Ch))&0ffh	;volatile
	line	386
	
l398:
	return
	opt stack 0
GLOBAL	__end_of_usb_txprocess
	__end_of_usb_txprocess:
	signat	_usb_txprocess,88
	global	_usb_handleDescriptorRequest

;; *************** function _usb_handleDescriptorRequest *****************
;; Defined at:
;;		line 308 in file "D:\Projects\USBtin\firmware\source\usb_cdc.c"
;; Parameters:    Size  Location     Type
;;  type            1    wreg     unsigned char 
;;  index           1   10[COMRAM] unsigned char 
;;  length          2   11[COMRAM] unsigned short 
;; Auto vars:     Size  Location     Type
;;  type            1   13[COMRAM] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, fsr2l, fsr2h, status,2, status,0, tblptrl, tblptrh, tblptru, cstack
;; Tracked objects:
;;		On entry : F/2
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         3       0       0       0       0       0       0       0       0       0       0
;;      Locals:         1       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         4       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_usb_loadDescriptor
;; This function is called by:
;;		_usb_process
;; This function uses a non-reentrant model
;;
psect	text15,class=CODE,space=0,reloc=2
	line	308
global __ptext15
__ptext15:
psect	text15
	file	"D:\Projects\USBtin\firmware\source\usb_cdc.c"
	line	308
	global	__size_of_usb_handleDescriptorRequest
	__size_of_usb_handleDescriptorRequest	equ	__end_of_usb_handleDescriptorRequest-_usb_handleDescriptorRequest
	
_usb_handleDescriptorRequest:
;incstack = 0
	opt	stack 26
;usb_handleDescriptorRequest@type stored from wreg
	movwf	((c:usb_handleDescriptorRequest@type)),c
	line	310
	
l1960:; BSR set to: 2

;usb_cdc.c: 310: switch (type) {
	goto	l1986
	line	312
	
l1962:; BSR set to: 2

;usb_cdc.c: 312: return usb_loadDescriptor(usb_dev_desc, sizeof(usb_dev_desc), length);
	movlw	high(012h)
	movwf	((c:usb_loadDescriptor@size+1)),c
	movlw	low(012h)
	movwf	((c:usb_loadDescriptor@size)),c
	movff	(c:usb_handleDescriptorRequest@length),(c:usb_loadDescriptor@length)
	movff	(c:usb_handleDescriptorRequest@length+1),(c:usb_loadDescriptor@length+1)
	movlw	(_usb_dev_desc)&0ffh
	
	call	_usb_loadDescriptor
	goto	l375
	line	314
	
l1966:; BSR set to: 2

;usb_cdc.c: 314: return usb_loadDescriptor(usb_config_desc, sizeof(usb_config_desc), length);
	movlw	high(043h)
	movwf	((c:usb_loadDescriptor@size+1)),c
	movlw	low(043h)
	movwf	((c:usb_loadDescriptor@size)),c
	movff	(c:usb_handleDescriptorRequest@length),(c:usb_loadDescriptor@length)
	movff	(c:usb_handleDescriptorRequest@length+1),(c:usb_loadDescriptor@length+1)
	movlw	(_usb_config_desc)&0ffh
	
	call	_usb_loadDescriptor
	goto	l375
	line	317
	
l1970:; BSR set to: 2

	movlw	high(04h)
	movwf	((c:usb_loadDescriptor@size+1)),c
	movlw	low(04h)
	movwf	((c:usb_loadDescriptor@size)),c
	movff	(c:usb_handleDescriptorRequest@length),(c:usb_loadDescriptor@length)
	movff	(c:usb_handleDescriptorRequest@length+1),(c:usb_loadDescriptor@length+1)
	movlw	(_usb_string_0)&0ffh
	
	call	_usb_loadDescriptor
	goto	l375
	line	318
	
l1974:; BSR set to: 2

	movlw	high(036h)
	movwf	((c:usb_loadDescriptor@size+1)),c
	movlw	low(036h)
	movwf	((c:usb_loadDescriptor@size)),c
	movff	(c:usb_handleDescriptorRequest@length),(c:usb_loadDescriptor@length)
	movff	(c:usb_handleDescriptorRequest@length+1),(c:usb_loadDescriptor@length+1)
	movlw	(_usb_string_manuf)&0ffh
	
	call	_usb_loadDescriptor
	goto	l375
	line	319
	
l1978:; BSR set to: 2

	movlw	high(0Eh)
	movwf	((c:usb_loadDescriptor@size+1)),c
	movlw	low(0Eh)
	movwf	((c:usb_loadDescriptor@size)),c
	movff	(c:usb_handleDescriptorRequest@length),(c:usb_loadDescriptor@length)
	movff	(c:usb_handleDescriptorRequest@length+1),(c:usb_loadDescriptor@length+1)
	movlw	(_usb_string_product)&0ffh
	
	call	_usb_loadDescriptor
	goto	l375
	line	316
	
l1984:; BSR set to: 2

	movf	((c:usb_handleDescriptorRequest@index)),c,w
	; Switch size 1, requested type "space"
; Number of cases is 3, Range of values is 0 to 2
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           10     6 (average)
;	Chosen strategy is simple_byte

	xorlw	0^0	; case 0
	skipnz
	goto	l1970
	xorlw	1^0	; case 1
	skipnz
	goto	l1974
	xorlw	2^1	; case 2
	skipnz
	goto	l1978
	goto	l1988

	line	310
	
l1986:; BSR set to: 2

	movf	((c:usb_handleDescriptorRequest@type)),c,w
	; Switch size 1, requested type "space"
; Number of cases is 3, Range of values is 1 to 3
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           10     6 (average)
;	Chosen strategy is simple_byte

	xorlw	1^0	; case 1
	skipnz
	goto	l1962
	xorlw	2^1	; case 2
	skipnz
	goto	l1966
	xorlw	3^2	; case 3
	skipnz
	goto	l1984
	goto	l1988

	line	323
	
l1988:; BSR set to: 2

;usb_cdc.c: 323: return 0;
	movlw	(0)&0ffh
	line	324
	
l375:
	return
	opt stack 0
GLOBAL	__end_of_usb_handleDescriptorRequest
	__end_of_usb_handleDescriptorRequest:
	signat	_usb_handleDescriptorRequest,12409
	global	_usb_loadDescriptor

;; *************** function _usb_loadDescriptor *****************
;; Defined at:
;;		line 288 in file "D:\Projects\USBtin\firmware\source\usb_cdc.c"
;; Parameters:    Size  Location     Type
;;  descbuffer      1    wreg     PTR const unsigned char 
;;		 -> usb_string_product(14), usb_string_manuf(54), usb_string_0(4), usb_config_desc(67), 
;;		 -> usb_dev_desc(18), 
;;  size            2    5[COMRAM] unsigned short 
;;  length          2    7[COMRAM] unsigned short 
;; Auto vars:     Size  Location     Type
;;  descbuffer      1    9[COMRAM] PTR const unsigned char 
;;		 -> usb_string_product(14), usb_string_manuf(54), usb_string_0(4), usb_config_desc(67), 
;;		 -> usb_dev_desc(18), 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, fsr2l, fsr2h, status,2, status,0, tblptrl, tblptrh, tblptru, cstack
;; Tracked objects:
;;		On entry : F/2
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         4       0       0       0       0       0       0       0       0       0       0
;;      Locals:         1       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         5       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_usb_sendProcess
;; This function is called by:
;;		_usb_handleDescriptorRequest
;; This function uses a non-reentrant model
;;
psect	text16,class=CODE,space=0,reloc=2
	line	288
global __ptext16
__ptext16:
psect	text16
	file	"D:\Projects\USBtin\firmware\source\usb_cdc.c"
	line	288
	global	__size_of_usb_loadDescriptor
	__size_of_usb_loadDescriptor	equ	__end_of_usb_loadDescriptor-_usb_loadDescriptor
	
_usb_loadDescriptor:
;incstack = 0
	opt	stack 26
;usb_loadDescriptor@descbuffer stored from wreg
	movwf	((c:usb_loadDescriptor@descbuffer)),c
	line	290
	
l1948:; BSR set to: 2

;usb_cdc.c: 290: if (length > size)
	movf	((c:usb_loadDescriptor@length)),c,w
	subwf	((c:usb_loadDescriptor@size)),c,w
	movf	((c:usb_loadDescriptor@length+1)),c,w
	subwfb	((c:usb_loadDescriptor@size+1)),c,w
	btfsc	status,0
	goto	u1251
	goto	u1250
u1251:
	goto	l369
u1250:
	line	291
	
l1950:; BSR set to: 2

;usb_cdc.c: 291: length = size;
	movff	(c:usb_loadDescriptor@size),(c:usb_loadDescriptor@length)
	movff	(c:usb_loadDescriptor@size+1),(c:usb_loadDescriptor@length+1)
	
l369:; BSR set to: 2

	line	293
;usb_cdc.c: 293: usb_sendleft = length;
	movff	(c:usb_loadDescriptor@length),(c:_usb_sendleft)
	movff	(c:usb_loadDescriptor@length+1),(c:_usb_sendleft+1)
	line	294
	
l1952:; BSR set to: 2

;usb_cdc.c: 294: usb_sendbuffer = descbuffer;
		movlw	low highword(__smallconst)
	movwf	((c:_usb_sendbuffer+2)),c
	movlw	high(__smallconst)
	movwf	((c:_usb_sendbuffer+1)),c
	movff	(c:usb_loadDescriptor@descbuffer),(c:_usb_sendbuffer)

	line	296
	
l1954:; BSR set to: 2

;usb_cdc.c: 296: usb_sendProcess();
	call	_usb_sendProcess	;wreg free
	line	297
	
l1956:
;usb_cdc.c: 297: return length;
	movf	((c:usb_loadDescriptor@length)),c,w
	line	298
	
l370:
	return
	opt stack 0
GLOBAL	__end_of_usb_loadDescriptor
	__end_of_usb_loadDescriptor:
	signat	_usb_loadDescriptor,12409
	global	_usb_sendProcess

;; *************** function _usb_sendProcess *****************
;; Defined at:
;;		line 259 in file "D:\Projects\USBtin\firmware\source\usb_cdc.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  length          2    3[COMRAM] unsigned short 
;;  i               1    2[COMRAM] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr2l, fsr2h, status,2, status,0, tblptrl, tblptrh, tblptru
;; Tracked objects:
;;		On entry : 0/1
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         3       0       0       0       0       0       0       0       0       0       0
;;      Temps:          2       0       0       0       0       0       0       0       0       0       0
;;      Totals:         5       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_usb_loadDescriptor
;;		_usb_process
;; This function uses a non-reentrant model
;;
psect	text17,class=CODE,space=0,reloc=2
	line	259
global __ptext17
__ptext17:
psect	text17
	file	"D:\Projects\USBtin\firmware\source\usb_cdc.c"
	line	259
	global	__size_of_usb_sendProcess
	__size_of_usb_sendProcess	equ	__end_of_usb_sendProcess-_usb_sendProcess
	
_usb_sendProcess:
;incstack = 0
	opt	stack 28
	line	260
	
l1924:
;usb_cdc.c: 260: if (usb_sendleft == 0) return;
	movf	((c:_usb_sendleft+1)),c,w
	iorwf ((c:_usb_sendleft)),c,w

	btfss	status,2
	goto	u1211
	goto	u1210
u1211:
	goto	l359
u1210:
	goto	l360
	
l359:
	line	262
;usb_cdc.c: 262: unsigned short length = usb_sendleft;
	movff	(c:_usb_sendleft),(c:usb_sendProcess@length)
	movff	(c:_usb_sendleft+1),(c:usb_sendProcess@length+1)
	line	263
	
l1928:
;usb_cdc.c: 263: if (length > 0x08)
	movlw	09h
	subwf	((c:usb_sendProcess@length)),c,w
	movlw	0
	subwfb	((c:usb_sendProcess@length+1)),c,w
	btfss	status,0
	goto	u1221
	goto	u1220
u1221:
	goto	l1932
u1220:
	line	264
	
l1930:
;usb_cdc.c: 264: length = 0x08;
	movlw	high(08h)
	movwf	((c:usb_sendProcess@length+1)),c
	movlw	low(08h)
	movwf	((c:usb_sendProcess@length)),c
	line	267
	
l1932:
;usb_cdc.c: 266: unsigned char i;
;usb_cdc.c: 267: for (i = 0; i < length; i++) {
	clrf	((c:usb_sendProcess@i)),c
	goto	l1942
	line	268
	
l1934:
;usb_cdc.c: 268: ep0in_buffer[i] = *usb_sendbuffer;
	movff	(c:_usb_sendbuffer),tblptrl
	movff	(c:_usb_sendbuffer+1),tblptrh
	movff	(c:_usb_sendbuffer+2),tblptru
	movlb	2	; () banked
	movlw	low(648)	;volatile
	addwf	((c:usb_sendProcess@i)),c,w
	movwf	c:fsr2l
	clrf	1+c:fsr2l
	movlw	high(648)	;volatile
	addwfc	1+c:fsr2l
	tblrd	*
	
	movff	tablat,indf2
	line	269
	
l1936:; BSR set to: 2

;usb_cdc.c: 269: usb_sendbuffer ++;
	movlw	low(01h)
	addwf	((c:_usb_sendbuffer)),c
	movlw	high(01h)
	addwfc	((c:_usb_sendbuffer+1)),c
	movlw	low highword(01h)
	addwfc	((c:_usb_sendbuffer+2)),c
	line	270
	
l1938:; BSR set to: 2

;usb_cdc.c: 270: usb_sendleft--;
	decf	((c:_usb_sendleft)),c
	btfss	status,0
	decf	((c:_usb_sendleft+1)),c
	line	267
	
l1940:; BSR set to: 2

	incf	((c:usb_sendProcess@i)),c
	
l1942:
	movf	((c:usb_sendProcess@i)),c,w
	movwf	(??_usb_sendProcess+0+0)&0ffh,c
	clrf	(??_usb_sendProcess+0+0+1)&0ffh,c

	movf	((c:usb_sendProcess@length)),c,w
	subwf	(??_usb_sendProcess+0+0),c,w
	movf	((c:usb_sendProcess@length+1)),c,w
	subwfb	(??_usb_sendProcess+0+1),c,w
	btfss	status,0
	goto	u1231
	goto	u1230
u1231:
	goto	l1934
u1230:
	
l364:
	line	273
;usb_cdc.c: 271: }
;usb_cdc.c: 273: ep[0].in.cnt = length;
	movff	(c:usb_sendProcess@length),0+(512+05h)	;volatile
	line	274
;usb_cdc.c: 274: if (ep[0].in.stat & 0x40)
	movlb	2	; () banked
	
	btfss	(0+(512+04h))&0ffh,(6)&7	;volatile
	goto	u1241
	goto	u1240
u1241:
	goto	l1946
u1240:
	line	275
	
l1944:; BSR set to: 2

;usb_cdc.c: 275: ep[0].in.stat = 0x88;
	movlw	low(088h)
	movwf	(0+(512+04h))&0ffh	;volatile
	goto	l360
	line	277
	
l1946:; BSR set to: 2

;usb_cdc.c: 276: else
;usb_cdc.c: 277: ep[0].in.stat = 0xC8;
	movlw	low(0C8h)
	movwf	(0+(512+04h))&0ffh	;volatile
	line	278
	
l360:
	return
	opt stack 0
GLOBAL	__end_of_usb_sendProcess
	__end_of_usb_sendProcess:
	signat	_usb_sendProcess,88
	global	_mcp2515_set_bittiming

;; *************** function _mcp2515_set_bittiming *****************
;; Defined at:
;;		line 226 in file "D:\Projects\USBtin\firmware\source\mcp2515.c"
;; Parameters:    Size  Location     Type
;;  cnf1            1    wreg     unsigned char 
;;  cnf2            1    3[COMRAM] unsigned char 
;;  cnf3            1    4[COMRAM] unsigned char 
;; Auto vars:     Size  Location     Type
;;  cnf1            1    5[COMRAM] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, cstack
;; Tracked objects:
;;		On entry : E/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         2       0       0       0       0       0       0       0       0       0       0
;;      Locals:         1       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         3       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_mcp2515_write_register
;; This function is called by:
;;		_parseLine
;; This function uses a non-reentrant model
;;
psect	text18,class=CODE,space=0,reloc=2
	file	"D:\Projects\USBtin\firmware\source\mcp2515.c"
	line	226
global __ptext18
__ptext18:
psect	text18
	file	"D:\Projects\USBtin\firmware\source\mcp2515.c"
	line	226
	global	__size_of_mcp2515_set_bittiming
	__size_of_mcp2515_set_bittiming	equ	__end_of_mcp2515_set_bittiming-_mcp2515_set_bittiming
	
_mcp2515_set_bittiming:
;incstack = 0
	opt	stack 27
;mcp2515_set_bittiming@cnf1 stored from wreg
	movwf	((c:mcp2515_set_bittiming@cnf1)),c
	line	228
	
l2346:
;mcp2515.c: 228: mcp2515_write_register(0x2A, cnf1);
	movff	(c:mcp2515_set_bittiming@cnf1),(c:mcp2515_write_register@data)
	movlw	(02Ah)&0ffh
	
	call	_mcp2515_write_register
	line	229
;mcp2515.c: 229: mcp2515_write_register(0x29, cnf2);
	movff	(c:mcp2515_set_bittiming@cnf2),(c:mcp2515_write_register@data)
	movlw	(029h)&0ffh
	
	call	_mcp2515_write_register
	line	230
;mcp2515.c: 230: mcp2515_write_register(0x28, cnf3);
	movff	(c:mcp2515_set_bittiming@cnf3),(c:mcp2515_write_register@data)
	movlw	(028h)&0ffh
	
	call	_mcp2515_write_register
	line	232
	
l251:
	return
	opt stack 0
GLOBAL	__end_of_mcp2515_set_bittiming
	__end_of_mcp2515_set_bittiming:
	signat	_mcp2515_set_bittiming,12408
	global	_mcp2515_set_SJA1000_filter_mask

;; *************** function _mcp2515_set_SJA1000_filter_mask *****************
;; Defined at:
;;		line 165 in file "D:\Projects\USBtin\firmware\source\mcp2515.c"
;; Parameters:    Size  Location     Type
;;  amr0            1    wreg     unsigned char 
;;  amr1            1    3[COMRAM] unsigned char 
;;  amr2            1    4[COMRAM] unsigned char 
;;  amr3            1    5[COMRAM] unsigned char 
;; Auto vars:     Size  Location     Type
;;  amr0            1    6[COMRAM] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, cstack
;; Tracked objects:
;;		On entry : F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         3       0       0       0       0       0       0       0       0       0       0
;;      Locals:         1       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         4       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_mcp2515_write_register
;; This function is called by:
;;		_parseLine
;; This function uses a non-reentrant model
;;
psect	text19,class=CODE,space=0,reloc=2
	line	165
global __ptext19
__ptext19:
psect	text19
	file	"D:\Projects\USBtin\firmware\source\mcp2515.c"
	line	165
	global	__size_of_mcp2515_set_SJA1000_filter_mask
	__size_of_mcp2515_set_SJA1000_filter_mask	equ	__end_of_mcp2515_set_SJA1000_filter_mask-_mcp2515_set_SJA1000_filter_mask
	
_mcp2515_set_SJA1000_filter_mask:
;incstack = 0
	opt	stack 27
;mcp2515_set_SJA1000_filter_mask@amr0 stored from wreg
	movwf	((c:mcp2515_set_SJA1000_filter_mask@amr0)),c
	line	172
	
l2364:; BSR set to: 0

;mcp2515.c: 172: mcp2515_write_register(0x20, ~amr0);
	movf	((c:mcp2515_set_SJA1000_filter_mask@amr0)),c,w
	xorlw	0ffh
	movwf	((c:mcp2515_write_register@data)),c
	movlw	(020h)&0ffh
	
	call	_mcp2515_write_register
	line	173
;mcp2515.c: 173: mcp2515_write_register(0x21, (~amr1) & 0xE0);
	movf	((c:mcp2515_set_SJA1000_filter_mask@amr1)),c,w
	xorlw	0ffh
	andlw	low(0E0h)
	movwf	((c:mcp2515_write_register@data)),c
	movlw	(021h)&0ffh
	
	call	_mcp2515_write_register
	line	174
	
l2366:
;mcp2515.c: 174: mcp2515_write_register(0x22, 0x00);
	movlw	low(0)
	movwf	((c:mcp2515_write_register@data)),c
	movlw	(022h)&0ffh
	
	call	_mcp2515_write_register
	line	175
	
l2368:
;mcp2515.c: 175: mcp2515_write_register(0x23, 0x00);
	movlw	low(0)
	movwf	((c:mcp2515_write_register@data)),c
	movlw	(023h)&0ffh
	
	call	_mcp2515_write_register
	line	178
;mcp2515.c: 178: mcp2515_write_register(0x24, ~amr2);
	movf	((c:mcp2515_set_SJA1000_filter_mask@amr2)),c,w
	xorlw	0ffh
	movwf	((c:mcp2515_write_register@data)),c
	movlw	(024h)&0ffh
	
	call	_mcp2515_write_register
	line	179
;mcp2515.c: 179: mcp2515_write_register(0x25, (~amr3) & 0xE0);
	movf	((c:mcp2515_set_SJA1000_filter_mask@amr3)),c,w
	xorlw	0ffh
	andlw	low(0E0h)
	movwf	((c:mcp2515_write_register@data)),c
	movlw	(025h)&0ffh
	
	call	_mcp2515_write_register
	line	180
	
l2370:
;mcp2515.c: 180: mcp2515_write_register(0x26, 0x00);
	movlw	low(0)
	movwf	((c:mcp2515_write_register@data)),c
	movlw	(026h)&0ffh
	
	call	_mcp2515_write_register
	line	181
	
l2372:
;mcp2515.c: 181: mcp2515_write_register(0x27, 0x00);
	movlw	low(0)
	movwf	((c:mcp2515_write_register@data)),c
	movlw	(027h)&0ffh
	
	call	_mcp2515_write_register
	line	183
	
l245:
	return
	opt stack 0
GLOBAL	__end_of_mcp2515_set_SJA1000_filter_mask
	__end_of_mcp2515_set_SJA1000_filter_mask:
	signat	_mcp2515_set_SJA1000_filter_mask,16504
	global	_mcp2515_set_SJA1000_filter_code

;; *************** function _mcp2515_set_SJA1000_filter_code *****************
;; Defined at:
;;		line 195 in file "D:\Projects\USBtin\firmware\source\mcp2515.c"
;; Parameters:    Size  Location     Type
;;  acr0            1    wreg     unsigned char 
;;  acr1            1    3[COMRAM] unsigned char 
;;  acr2            1    4[COMRAM] unsigned char 
;;  acr3            1    5[COMRAM] unsigned char 
;; Auto vars:     Size  Location     Type
;;  acr0            1    6[COMRAM] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, cstack
;; Tracked objects:
;;		On entry : F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         3       0       0       0       0       0       0       0       0       0       0
;;      Locals:         1       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         4       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_mcp2515_write_register
;; This function is called by:
;;		_parseLine
;; This function uses a non-reentrant model
;;
psect	text20,class=CODE,space=0,reloc=2
	line	195
global __ptext20
__ptext20:
psect	text20
	file	"D:\Projects\USBtin\firmware\source\mcp2515.c"
	line	195
	global	__size_of_mcp2515_set_SJA1000_filter_code
	__size_of_mcp2515_set_SJA1000_filter_code	equ	__end_of_mcp2515_set_SJA1000_filter_code-_mcp2515_set_SJA1000_filter_code
	
_mcp2515_set_SJA1000_filter_code:
;incstack = 0
	opt	stack 27
;mcp2515_set_SJA1000_filter_code@acr0 stored from wreg
	movwf	((c:mcp2515_set_SJA1000_filter_code@acr0)),c
	line	198
	
l2374:; BSR set to: 0

;mcp2515.c: 198: mcp2515_write_register(0x00, acr0);
	movff	(c:mcp2515_set_SJA1000_filter_code@acr0),(c:mcp2515_write_register@data)
	movlw	(0)&0ffh
	
	call	_mcp2515_write_register
	line	199
	
l2376:
;mcp2515.c: 199: mcp2515_write_register(0x01, (acr1) & 0xE0);
	movf	((c:mcp2515_set_SJA1000_filter_code@acr1)),c,w
	andlw	low(0E0h)
	movwf	((c:mcp2515_write_register@data)),c
	movlw	(01h)&0ffh
	
	call	_mcp2515_write_register
	line	200
	
l2378:
;mcp2515.c: 200: mcp2515_write_register(0x04, acr0);
	movff	(c:mcp2515_set_SJA1000_filter_code@acr0),(c:mcp2515_write_register@data)
	movlw	(04h)&0ffh
	
	call	_mcp2515_write_register
	line	201
;mcp2515.c: 201: mcp2515_write_register(0x05, ((acr1) & 0xE0) | 0x08);
	movf	((c:mcp2515_set_SJA1000_filter_code@acr1)),c,w
	andlw	low(0E0h)
	iorlw	low(08h)
	movwf	((c:mcp2515_write_register@data)),c
	movlw	(05h)&0ffh
	
	call	_mcp2515_write_register
	line	204
	
l2380:
;mcp2515.c: 204: mcp2515_write_register(0x08, acr2);
	movff	(c:mcp2515_set_SJA1000_filter_code@acr2),(c:mcp2515_write_register@data)
	movlw	(08h)&0ffh
	
	call	_mcp2515_write_register
	line	205
	
l2382:
;mcp2515.c: 205: mcp2515_write_register(0x09, (acr3) & 0xE0);
	movf	((c:mcp2515_set_SJA1000_filter_code@acr3)),c,w
	andlw	low(0E0h)
	movwf	((c:mcp2515_write_register@data)),c
	movlw	(09h)&0ffh
	
	call	_mcp2515_write_register
	line	206
;mcp2515.c: 206: mcp2515_write_register(0x10, acr2);
	movff	(c:mcp2515_set_SJA1000_filter_code@acr2),(c:mcp2515_write_register@data)
	movlw	(010h)&0ffh
	
	call	_mcp2515_write_register
	line	207
	
l2384:
;mcp2515.c: 207: mcp2515_write_register(0x11, ((acr3) & 0xE0) | 0x08);
	movf	((c:mcp2515_set_SJA1000_filter_code@acr3)),c,w
	andlw	low(0E0h)
	iorlw	low(08h)
	movwf	((c:mcp2515_write_register@data)),c
	movlw	(011h)&0ffh
	
	call	_mcp2515_write_register
	line	210
	
l2386:
;mcp2515.c: 210: mcp2515_write_register(0x14, 0x00);
	movlw	low(0)
	movwf	((c:mcp2515_write_register@data)),c
	movlw	(014h)&0ffh
	
	call	_mcp2515_write_register
	line	211
	
l2388:
;mcp2515.c: 211: mcp2515_write_register(0x15, 0x00);
	movlw	low(0)
	movwf	((c:mcp2515_write_register@data)),c
	movlw	(015h)&0ffh
	
	call	_mcp2515_write_register
	line	212
	
l2390:
;mcp2515.c: 212: mcp2515_write_register(0x18, 0x00);
	movlw	low(0)
	movwf	((c:mcp2515_write_register@data)),c
	movlw	(018h)&0ffh
	
	call	_mcp2515_write_register
	line	213
	
l2392:
;mcp2515.c: 213: mcp2515_write_register(0x19, 0x00);
	movlw	low(0)
	movwf	((c:mcp2515_write_register@data)),c
	movlw	(019h)&0ffh
	
	call	_mcp2515_write_register
	line	214
	
l248:
	return
	opt stack 0
GLOBAL	__end_of_mcp2515_set_SJA1000_filter_code
	__end_of_mcp2515_set_SJA1000_filter_code:
	signat	_mcp2515_set_SJA1000_filter_code,16504
	global	_mcp2515_read_register

;; *************** function _mcp2515_read_register *****************
;; Defined at:
;;		line 63 in file "D:\Projects\USBtin\firmware\source\mcp2515.c"
;; Parameters:    Size  Location     Type
;;  address         1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  address         1    1[COMRAM] unsigned char 
;;  data            1    2[COMRAM] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, cstack
;; Tracked objects:
;;		On entry : E/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         2       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         2       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_spi_transmit
;; This function is called by:
;;		_parseLine
;; This function uses a non-reentrant model
;;
psect	text21,class=CODE,space=0,reloc=2
	line	63
global __ptext21
__ptext21:
psect	text21
	file	"D:\Projects\USBtin\firmware\source\mcp2515.c"
	line	63
	global	__size_of_mcp2515_read_register
	__size_of_mcp2515_read_register	equ	__end_of_mcp2515_read_register-_mcp2515_read_register
	
_mcp2515_read_register:
;incstack = 0
	opt	stack 28
;mcp2515_read_register@address stored from wreg
	movwf	((c:mcp2515_read_register@address)),c
	line	68
	
l2348:
;mcp2515.c: 65: unsigned char data;
;mcp2515.c: 68: LATCbits.LATC6 = 0;
	bcf	((c:3979)),c,6	;volatile
	line	70
	
l2350:
;mcp2515.c: 70: spi_transmit(0x03);
	movlw	(03h)&0ffh
	
	call	_spi_transmit
	line	71
;mcp2515.c: 71: spi_transmit(address);
	movf	((c:mcp2515_read_register@address)),c,w
	
	call	_spi_transmit
	line	72
;mcp2515.c: 72: data = spi_transmit(0xff);
	movlw	(0FFh)&0ffh
	
	call	_spi_transmit
	movwf	((c:mcp2515_read_register@data)),c
	line	75
	
l2352:
;mcp2515.c: 75: LATCbits.LATC6 = 1;
	bsf	((c:3979)),c,6	;volatile
	line	77
	
l2354:
;mcp2515.c: 77: return data;
	movf	((c:mcp2515_read_register@data)),c,w
	line	78
	
l230:
	return
	opt stack 0
GLOBAL	__end_of_mcp2515_read_register
	__end_of_mcp2515_read_register:
	signat	_mcp2515_read_register,4217
	global	_mcp2515_receive_message

;; *************** function _mcp2515_receive_message *****************
;; Defined at:
;;		line 349 in file "D:\Projects\USBtin\firmware\source\mcp2515.c"
;; Parameters:    Size  Location     Type
;;  p_canmsg        2    4[COMRAM] PTR struct .
;;		 -> main@canmsg(14), 
;; Auto vars:     Size  Location     Type
;;  i               1   15[COMRAM] unsigned char 
;;  temp            4   11[COMRAM] unsigned long 
;;  status          1   17[COMRAM] unsigned char 
;;  address         1   16[COMRAM] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, fsr2l, fsr2h, status,2, status,0, cstack
;; Tracked objects:
;;		On entry : F/1
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         2       0       0       0       0       0       0       0       0       0       0
;;      Locals:         7       0       0       0       0       0       0       0       0       0       0
;;      Temps:          5       0       0       0       0       0       0       0       0       0       0
;;      Totals:        14       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:       14 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_mcp2515_bit_modify
;;		_mcp2515_rx_status
;;		_spi_transmit
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text22,class=CODE,space=0,reloc=2
	line	349
global __ptext22
__ptext22:
psect	text22
	file	"D:\Projects\USBtin\firmware\source\mcp2515.c"
	line	349
	global	__size_of_mcp2515_receive_message
	__size_of_mcp2515_receive_message	equ	__end_of_mcp2515_receive_message-_mcp2515_receive_message
	
_mcp2515_receive_message:
;incstack = 0
	opt	stack 28
	line	351
	
l2646:; BSR set to: 1

;mcp2515.c: 351: unsigned char status = mcp2515_rx_status();
	call	_mcp2515_rx_status	;wreg free
	movwf	((c:mcp2515_receive_message@status)),c
	line	354
	
l2648:
;mcp2515.c: 352: unsigned char address;
;mcp2515.c: 354: if (status & 0x40) {
	
	btfss	((c:mcp2515_receive_message@status)),c,(6)&7
	goto	u2181
	goto	u2180
u2181:
	goto	l278
u2180:
	line	355
	
l2650:
;mcp2515.c: 355: address = 0x00;
	clrf	((c:mcp2515_receive_message@address)),c
	line	356
;mcp2515.c: 356: } else if (status & 0x80) {
	goto	l2658
	
l278:
	
	btfss	((c:mcp2515_receive_message@status)),c,(7)&7
	goto	u2191
	goto	u2190
u2191:
	goto	l2654
u2190:
	line	357
	
l2652:
;mcp2515.c: 357: address = 0x04;
	movlw	low(04h)
	movwf	((c:mcp2515_receive_message@address)),c
	line	358
;mcp2515.c: 358: } else {
	goto	l2658
	line	360
	
l2654:
;mcp2515.c: 360: return 0;
	movlw	(0)&0ffh
	goto	l282
	line	363
	
l2658:
;mcp2515.c: 361: }
;mcp2515.c: 363: p_canmsg->flags.rtr = (status >> 3) & 0x01;
	movff	(c:mcp2515_receive_message@status),??_mcp2515_receive_message+0+0
	bcf	status,0
	rrcf	(??_mcp2515_receive_message+0+0),c
	bcf	status,0
	rrcf	(??_mcp2515_receive_message+0+0),c
	bcf	status,0
	rrcf	(??_mcp2515_receive_message+0+0),c

	movlw	01h
	andwf	(??_mcp2515_receive_message+0+0),c
	lfsr	2,04h
	movf	((c:mcp2515_receive_message@p_canmsg)),c,w
	addwf	fsr2l
	movf	((c:mcp2515_receive_message@p_canmsg+1)),c,w
	addwfc	fsr2h
	movf	(indf2),c,w
	xorwf	(??_mcp2515_receive_message+0+0),c,w
	andlw	not ((1<<1)-1)
	xorwf	(??_mcp2515_receive_message+0+0),c,w
	movwf	(indf2),c
	line	364
;mcp2515.c: 364: p_canmsg->flags.extended = (status >> 4) & 0x01;
	movff	(c:mcp2515_receive_message@status),??_mcp2515_receive_message+0+0
	swapf	(??_mcp2515_receive_message+0+0),c
	movlw	(0ffh shr 4) & 0ffh
	andwf	(??_mcp2515_receive_message+0+0),c
	movlw	01h
	andwf	(??_mcp2515_receive_message+0+0),c
	lfsr	2,04h
	movf	((c:mcp2515_receive_message@p_canmsg)),c,w
	addwf	fsr2l
	movf	((c:mcp2515_receive_message@p_canmsg+1)),c,w
	addwfc	fsr2h
	rlncf	(??_mcp2515_receive_message+0+0),c
	movf	(indf2),c,w
	xorwf	(??_mcp2515_receive_message+0+0),c,w
	andlw	not (((1<<1)-1)<<1)
	xorwf	(??_mcp2515_receive_message+0+0),c,w
	movwf	(indf2),c
	line	367
	
l2660:
;mcp2515.c: 367: LATCbits.LATC6 = 0;
	bcf	((c:3979)),c,6	;volatile
	line	369
	
l2662:
;mcp2515.c: 369: spi_transmit(0x90 | address);
	movf	((c:mcp2515_receive_message@address)),c,w
	iorlw	low(090h)
	
	call	_spi_transmit
	line	371
	
l2664:
;mcp2515.c: 371: if (p_canmsg->flags.extended) {
	lfsr	2,04h
	movf	((c:mcp2515_receive_message@p_canmsg)),c,w
	addwf	fsr2l
	movf	((c:mcp2515_receive_message@p_canmsg+1)),c,w
	addwfc	fsr2h
	btfss	indf2,1
	goto	u2201
	goto	u2200
u2201:
	goto	l2678
u2200:
	line	372
	
l2666:
;mcp2515.c: 372: p_canmsg->id = (unsigned long) spi_transmit(0xff) << 21;
	movlw	(0FFh)&0ffh
	
	call	_spi_transmit
	movwf	(??_mcp2515_receive_message+0+0)&0ffh,c
	movff	(??_mcp2515_receive_message+0+0),??_mcp2515_receive_message+1+0
	clrf	(??_mcp2515_receive_message+1+0+1)&0ffh,c
	clrf	(??_mcp2515_receive_message+1+0+2)&0ffh,c
	clrf	(??_mcp2515_receive_message+1+0+3)&0ffh,c
	movlw	015h
u2215:
	bcf	status,0
	rlcf	(??_mcp2515_receive_message+1+0),c
	rlcf	(??_mcp2515_receive_message+1+1),c
	rlcf	(??_mcp2515_receive_message+1+2),c
	rlcf	(??_mcp2515_receive_message+1+3),c
	decfsz	wreg
	goto	u2215
	movff	(c:mcp2515_receive_message@p_canmsg),fsr2l
	movff	(c:mcp2515_receive_message@p_canmsg+1),fsr2h
	movff	??_mcp2515_receive_message+1+0,postinc2
	movff	??_mcp2515_receive_message+1+1,postinc2
	movff	??_mcp2515_receive_message+1+2,postinc2
	movff	??_mcp2515_receive_message+1+3,postdec2
	decf	fsr2
	decf	fsr2
	line	373
	
l2668:
;mcp2515.c: 373: unsigned long temp = spi_transmit(0xff);
	movlw	(0FFh)&0ffh
	
	call	_spi_transmit
	movwf	((c:mcp2515_receive_message@temp)),c
	clrf	((c:mcp2515_receive_message@temp+1)),c
	clrf	((c:mcp2515_receive_message@temp+2)),c
	clrf	((c:mcp2515_receive_message@temp+3)),c

	line	374
	
l2670:
;mcp2515.c: 374: p_canmsg->id |= (temp & 0xe0) << 13;
	movlw	0E0h
	andwf	((c:mcp2515_receive_message@temp)),c,w
	movwf	(??_mcp2515_receive_message+0+0)&0ffh,c
	movlw	0
	andwf	((c:mcp2515_receive_message@temp+1)),c,w
	movwf	1+(??_mcp2515_receive_message+0+0)&0ffh,c
	movlw	0
	andwf	((c:mcp2515_receive_message@temp+2)),c,w
	movwf	2+(??_mcp2515_receive_message+0+0)&0ffh,c
	movlw	0
	andwf	((c:mcp2515_receive_message@temp+3)),c,w
	movwf	3+(??_mcp2515_receive_message+0+0)&0ffh,c
	movlw	0Dh
u2225:
	bcf	status,0
	rlcf	(??_mcp2515_receive_message+0+0),c
	rlcf	(??_mcp2515_receive_message+0+1),c
	rlcf	(??_mcp2515_receive_message+0+2),c
	rlcf	(??_mcp2515_receive_message+0+3),c
	decfsz	wreg
	goto	u2225
	movff	(c:mcp2515_receive_message@p_canmsg),fsr2l
	movff	(c:mcp2515_receive_message@p_canmsg+1),fsr2h
	movf	(??_mcp2515_receive_message+0+0),c,w
	iorwf	postinc2
	movf	(??_mcp2515_receive_message+0+1),c,w
	iorwf	postinc2
	movf	(??_mcp2515_receive_message+0+2),c,w
	iorwf	postinc2
	movf	(??_mcp2515_receive_message+0+3),c,w
	iorwf	postdec2
	movf	postdec2
	movf	postdec2
	line	375
	
l2672:
;mcp2515.c: 375: p_canmsg->id |= (temp & 0x03) << 16;
	movlw	03h
	andwf	((c:mcp2515_receive_message@temp)),c,w
	movwf	(??_mcp2515_receive_message+0+0)&0ffh,c
	movlw	0
	andwf	((c:mcp2515_receive_message@temp+1)),c,w
	movwf	1+(??_mcp2515_receive_message+0+0)&0ffh,c
	movlw	0
	andwf	((c:mcp2515_receive_message@temp+2)),c,w
	movwf	2+(??_mcp2515_receive_message+0+0)&0ffh,c
	movlw	0
	andwf	((c:mcp2515_receive_message@temp+3)),c,w
	movwf	3+(??_mcp2515_receive_message+0+0)&0ffh,c
	movff	??_mcp2515_receive_message+0+1,??_mcp2515_receive_message+0+3
	movff	??_mcp2515_receive_message+0+0,??_mcp2515_receive_message+0+2
	clrf	(??_mcp2515_receive_message+0+1),c
	clrf	(??_mcp2515_receive_message+0+0),c
	movff	(c:mcp2515_receive_message@p_canmsg),fsr2l
	movff	(c:mcp2515_receive_message@p_canmsg+1),fsr2h
	movf	(??_mcp2515_receive_message+0+0),c,w
	iorwf	postinc2
	movf	(??_mcp2515_receive_message+0+1),c,w
	iorwf	postinc2
	movf	(??_mcp2515_receive_message+0+2),c,w
	iorwf	postinc2
	movf	(??_mcp2515_receive_message+0+3),c,w
	iorwf	postdec2
	movf	postdec2
	movf	postdec2
	line	376
	
l2674:
;mcp2515.c: 376: p_canmsg->id |= (unsigned long) spi_transmit(0xff) << 8;
	movlw	(0FFh)&0ffh
	
	call	_spi_transmit
	movwf	(??_mcp2515_receive_message+0+0)&0ffh,c
	movff	(??_mcp2515_receive_message+0+0),??_mcp2515_receive_message+1+0
	clrf	(??_mcp2515_receive_message+1+0+1)&0ffh,c
	clrf	(??_mcp2515_receive_message+1+0+2)&0ffh,c
	clrf	(??_mcp2515_receive_message+1+0+3)&0ffh,c
	movff	??_mcp2515_receive_message+1+2,??_mcp2515_receive_message+1+3
	movff	??_mcp2515_receive_message+1+1,??_mcp2515_receive_message+1+2
	movff	??_mcp2515_receive_message+1+0,??_mcp2515_receive_message+1+1
	clrf	(??_mcp2515_receive_message+1+0),c
	movff	(c:mcp2515_receive_message@p_canmsg),fsr2l
	movff	(c:mcp2515_receive_message@p_canmsg+1),fsr2h
	movf	(??_mcp2515_receive_message+1+0),c,w
	iorwf	postinc2
	movf	(??_mcp2515_receive_message+1+1),c,w
	iorwf	postinc2
	movf	(??_mcp2515_receive_message+1+2),c,w
	iorwf	postinc2
	movf	(??_mcp2515_receive_message+1+3),c,w
	iorwf	postdec2
	movf	postdec2
	movf	postdec2
	line	377
	
l2676:
;mcp2515.c: 377: p_canmsg->id |= (unsigned long) spi_transmit(0xff);
	movlw	(0FFh)&0ffh
	
	call	_spi_transmit
	movff	(c:mcp2515_receive_message@p_canmsg),fsr2l
	movff	(c:mcp2515_receive_message@p_canmsg+1),fsr2h
	iorwf	postinc2
	movlw	0
	iorwf	postinc2
	iorwf	postinc2
	iorwf	postdec2
	movf	postdec2
	movf	postdec2
	line	378
;mcp2515.c: 378: } else {
	goto	l284
	line	379
	
l2678:
;mcp2515.c: 379: p_canmsg->id = (unsigned long) spi_transmit(0xff) << 3;
	movlw	(0FFh)&0ffh
	
	call	_spi_transmit
	movwf	(??_mcp2515_receive_message+0+0)&0ffh,c
	movff	(??_mcp2515_receive_message+0+0),??_mcp2515_receive_message+1+0
	clrf	(??_mcp2515_receive_message+1+0+1)&0ffh,c
	clrf	(??_mcp2515_receive_message+1+0+2)&0ffh,c
	clrf	(??_mcp2515_receive_message+1+0+3)&0ffh,c
	movlw	03h
u2235:
	bcf	status,0
	rlcf	(??_mcp2515_receive_message+1+0),c
	rlcf	(??_mcp2515_receive_message+1+1),c
	rlcf	(??_mcp2515_receive_message+1+2),c
	rlcf	(??_mcp2515_receive_message+1+3),c
	decfsz	wreg
	goto	u2235
	movff	(c:mcp2515_receive_message@p_canmsg),fsr2l
	movff	(c:mcp2515_receive_message@p_canmsg+1),fsr2h
	movff	??_mcp2515_receive_message+1+0,postinc2
	movff	??_mcp2515_receive_message+1+1,postinc2
	movff	??_mcp2515_receive_message+1+2,postinc2
	movff	??_mcp2515_receive_message+1+3,postdec2
	decf	fsr2
	decf	fsr2
	line	380
;mcp2515.c: 380: p_canmsg->id |= (unsigned long) spi_transmit(0xff) >> 5;
	movlw	(0FFh)&0ffh
	
	call	_spi_transmit
	movwf	(??_mcp2515_receive_message+0+0)&0ffh,c
	movlw	05h
	movwf	(??_mcp2515_receive_message+1+0)&0ffh,c
u2245:
	bcf	status,0
	rrcf	((??_mcp2515_receive_message+0+0)),c
	decfsz	(??_mcp2515_receive_message+1+0)&0ffh,c
	goto	u2245
	movf	((??_mcp2515_receive_message+0+0)),c,w
	movff	(c:mcp2515_receive_message@p_canmsg),fsr2l
	movff	(c:mcp2515_receive_message@p_canmsg+1),fsr2h
	iorwf	postinc2
	movlw	0
	iorwf	postinc2
	iorwf	postinc2
	iorwf	postdec2
	movf	postdec2
	movf	postdec2
	line	381
	
l2680:
;mcp2515.c: 381: spi_transmit(0xff);
	movlw	(0FFh)&0ffh
	
	call	_spi_transmit
	line	382
	
l2682:
;mcp2515.c: 382: spi_transmit(0xff);
	movlw	(0FFh)&0ffh
	
	call	_spi_transmit
	line	383
	
l284:
	line	386
;mcp2515.c: 383: }
;mcp2515.c: 386: p_canmsg->length = spi_transmit(0xff) & 0x0f;
	lfsr	2,05h
	movf	((c:mcp2515_receive_message@p_canmsg)),c,w
	addwf	fsr2l
	movf	((c:mcp2515_receive_message@p_canmsg+1)),c,w
	addwfc	fsr2h
	movlw	(0FFh)&0ffh
	
	call	_spi_transmit
	andlw	low(0Fh)
	movwf	indf2,c

	line	387
	
l2684:
;mcp2515.c: 387: if (!p_canmsg->flags.rtr) {
	lfsr	2,04h
	movf	((c:mcp2515_receive_message@p_canmsg)),c,w
	addwf	fsr2l
	movf	((c:mcp2515_receive_message@p_canmsg+1)),c,w
	addwfc	fsr2h
	btfsc	indf2,0
	goto	u2251
	goto	u2250
u2251:
	goto	l285
u2250:
	line	389
	
l2686:
;mcp2515.c: 388: unsigned char i;
;mcp2515.c: 389: for (i = 0; i < p_canmsg->length; i++) {
	clrf	((c:mcp2515_receive_message@i)),c
	goto	l2692
	line	390
	
l2688:
;mcp2515.c: 390: p_canmsg->data[i] = spi_transmit(0xff);
	movf	((c:mcp2515_receive_message@i)),c,w
	addwf	((c:mcp2515_receive_message@p_canmsg)),c,w
	movwf	(??_mcp2515_receive_message+0+0)&0ffh,c
	movlw	0
	addwfc	((c:mcp2515_receive_message@p_canmsg+1)),c,w
	movwf	(??_mcp2515_receive_message+0+0+1)&0ffh,c
	movlw	low(06h)
	addwf	(??_mcp2515_receive_message+0+0),c,w
	movwf	c:fsr2l
	movlw	high(06h)
	addwfc	(??_mcp2515_receive_message+0+1),c,w
	movwf	1+c:fsr2l
	movlw	(0FFh)&0ffh
	
	call	_spi_transmit
	movwf	indf2,c

	line	389
	
l2690:
	incf	((c:mcp2515_receive_message@i)),c
	
l2692:
	lfsr	2,05h
	movf	((c:mcp2515_receive_message@p_canmsg)),c,w
	addwf	fsr2l
	movf	((c:mcp2515_receive_message@p_canmsg+1)),c,w
	addwfc	fsr2h
	movf	indf2,w
	subwf	((c:mcp2515_receive_message@i)),c,w
	btfss	status,0
	goto	u2261
	goto	u2260
u2261:
	goto	l2688
u2260:
	line	392
	
l285:
	line	395
;mcp2515.c: 391: }
;mcp2515.c: 392: }
;mcp2515.c: 395: LATCbits.LATC6 = 1;
	bsf	((c:3979)),c,6	;volatile
	line	397
;mcp2515.c: 397: if (address == 0) address = 1;
	tstfsz	((c:mcp2515_receive_message@address)),c
	goto	u2271
	goto	u2270
u2271:
	goto	l2696
u2270:
	
l2694:
	movlw	low(01h)
	movwf	((c:mcp2515_receive_message@address)),c
	goto	l2698
	line	398
	
l2696:
;mcp2515.c: 398: else address = 2;
	movlw	low(02h)
	movwf	((c:mcp2515_receive_message@address)),c
	line	399
	
l2698:
;mcp2515.c: 399: mcp2515_bit_modify(0x2C, address, 0);
	movff	(c:mcp2515_receive_message@address),(c:mcp2515_bit_modify@mask)
	movlw	low(0)
	movwf	((c:mcp2515_bit_modify@data)),c
	movlw	(02Ch)&0ffh
	
	call	_mcp2515_bit_modify
	line	401
	
l2700:
;mcp2515.c: 401: return 1;
	movlw	(01h)&0ffh
	line	402
	
l282:
	return
	opt stack 0
GLOBAL	__end_of_mcp2515_receive_message
	__end_of_mcp2515_receive_message:
	signat	_mcp2515_receive_message,4217
	global	_mcp2515_rx_status

;; *************** function _mcp2515_rx_status *****************
;; Defined at:
;;		line 259 in file "D:\Projects\USBtin\firmware\source\mcp2515.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  status          1    1[COMRAM] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, cstack
;; Tracked objects:
;;		On entry : F/1
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         1       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         1       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_spi_transmit
;; This function is called by:
;;		_mcp2515_receive_message
;; This function uses a non-reentrant model
;;
psect	text23,class=CODE,space=0,reloc=2
	line	259
global __ptext23
__ptext23:
psect	text23
	file	"D:\Projects\USBtin\firmware\source\mcp2515.c"
	line	259
	global	__size_of_mcp2515_rx_status
	__size_of_mcp2515_rx_status	equ	__end_of_mcp2515_rx_status-_mcp2515_rx_status
	
_mcp2515_rx_status:
;incstack = 0
	opt	stack 28
	line	262
	
l2394:; BSR set to: 1

;mcp2515.c: 262: LATCbits.LATC6 = 0;
	bcf	((c:3979)),c,6	;volatile
	line	264
	
l2396:; BSR set to: 1

;mcp2515.c: 264: spi_transmit(0xB0);
	movlw	(0B0h)&0ffh
	
	call	_spi_transmit
	line	265
;mcp2515.c: 265: unsigned char status = spi_transmit(0xff);
	movlw	(0FFh)&0ffh
	
	call	_spi_transmit
	movwf	((c:mcp2515_rx_status@status)),c
	line	268
	
l2398:
;mcp2515.c: 268: LATCbits.LATC6 = 1;
	bsf	((c:3979)),c,6	;volatile
	line	270
	
l2400:
;mcp2515.c: 270: return status;
	movf	((c:mcp2515_rx_status@status)),c,w
	line	271
	
l257:
	return
	opt stack 0
GLOBAL	__end_of_mcp2515_rx_status
	__end_of_mcp2515_rx_status:
	signat	_mcp2515_rx_status,89
	global	_mcp2515_bit_modify

;; *************** function _mcp2515_bit_modify *****************
;; Defined at:
;;		line 90 in file "D:\Projects\USBtin\firmware\source\mcp2515.c"
;; Parameters:    Size  Location     Type
;;  address         1    wreg     unsigned char 
;;  mask            1    1[COMRAM] unsigned char 
;;  data            1    2[COMRAM] unsigned char 
;; Auto vars:     Size  Location     Type
;;  address         1    3[COMRAM] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         2       0       0       0       0       0       0       0       0       0       0
;;      Locals:         1       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         3       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_spi_transmit
;; This function is called by:
;;		_parseLine
;;		_mcp2515_receive_message
;; This function uses a non-reentrant model
;;
psect	text24,class=CODE,space=0,reloc=2
	line	90
global __ptext24
__ptext24:
psect	text24
	file	"D:\Projects\USBtin\firmware\source\mcp2515.c"
	line	90
	global	__size_of_mcp2515_bit_modify
	__size_of_mcp2515_bit_modify	equ	__end_of_mcp2515_bit_modify-_mcp2515_bit_modify
	
_mcp2515_bit_modify:
;incstack = 0
	opt	stack 28
;mcp2515_bit_modify@address stored from wreg
	movwf	((c:mcp2515_bit_modify@address)),c
	line	93
	
l2358:
;mcp2515.c: 93: LATCbits.LATC6 = 0;
	bcf	((c:3979)),c,6	;volatile
	line	95
	
l2360:
;mcp2515.c: 95: spi_transmit(0x05);
	movlw	(05h)&0ffh
	
	call	_spi_transmit
	line	96
;mcp2515.c: 96: spi_transmit(address);
	movf	((c:mcp2515_bit_modify@address)),c,w
	
	call	_spi_transmit
	line	97
;mcp2515.c: 97: spi_transmit(mask);
	movf	((c:mcp2515_bit_modify@mask)),c,w
	
	call	_spi_transmit
	line	98
;mcp2515.c: 98: spi_transmit(data);
	movf	((c:mcp2515_bit_modify@data)),c,w
	
	call	_spi_transmit
	line	101
	
l2362:
;mcp2515.c: 101: LATCbits.LATC6 = 1;
	bsf	((c:3979)),c,6	;volatile
	line	102
	
l233:
	return
	opt stack 0
GLOBAL	__end_of_mcp2515_bit_modify
	__end_of_mcp2515_bit_modify:
	signat	_mcp2515_bit_modify,12408
	global	_mcp2515_init

;; *************** function _mcp2515_init *****************
;; Defined at:
;;		line 108 in file "D:\Projects\USBtin\firmware\source\mcp2515.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  dummy           1    3[COMRAM] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         1       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         1       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_mcp2515_write_register
;;		_spi_transmit
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text25,class=CODE,space=0,reloc=2
	line	108
global __ptext25
__ptext25:
psect	text25
	file	"D:\Projects\USBtin\firmware\source\mcp2515.c"
	line	108
	global	__size_of_mcp2515_init
	__size_of_mcp2515_init	equ	__end_of_mcp2515_init-_mcp2515_init
	
_mcp2515_init:
;incstack = 0
	opt	stack 28
	line	113
	
l2600:
;mcp2515.c: 110: unsigned char dummy;
;mcp2515.c: 113: SSPSTAT = 0x40;
	movlw	low(040h)
	movwf	((c:4039)),c	;volatile
	line	114
;mcp2515.c: 114: SSPCON1 = 0x21;
	movlw	low(021h)
	movwf	((c:4038)),c	;volatile
	line	115
	
l2602:
	movf	((c:4041)),c,w	;volatile
	line	116
	
l2604:
;mcp2515.c: 116: dummy = 0;
	clrf	((c:mcp2515_init@dummy)),c
	line	118
	
l2606:
;mcp2515.c: 118: TRISBbits.TRISB4 = 1;
	bsf	((c:3987)),c,4	;volatile
	line	119
	
l2608:
;mcp2515.c: 119: TRISCbits.TRISC6 = 0;
	bcf	((c:3988)),c,6	;volatile
	line	120
	
l2610:
;mcp2515.c: 120: TRISCbits.TRISC7 = 0;
	bcf	((c:3988)),c,7	;volatile
	line	121
	
l2612:
;mcp2515.c: 121: TRISBbits.TRISB6 = 0;
	bcf	((c:3987)),c,6	;volatile
	line	122
	
l2614:
;mcp2515.c: 122: LATCbits.LATC6 = 1;
	bsf	((c:3979)),c,6	;volatile
	line	124
;mcp2515.c: 124: while (++dummy) {};
	
l236:
	incfsz	((c:mcp2515_init@dummy)),c
	
	goto	l236
	
l238:
	line	127
;mcp2515.c: 127: LATCbits.LATC6 = 0;
	bcf	((c:3979)),c,6	;volatile
	line	128
	
l2616:
;mcp2515.c: 128: spi_transmit(0xC0);
	movlw	(0C0h)&0ffh
	
	call	_spi_transmit
	line	129
	
l2618:
;mcp2515.c: 129: LATCbits.LATC6 = 1;
	bsf	((c:3979)),c,6	;volatile
	line	131
;mcp2515.c: 131: while (++dummy) {};
	
l239:
	incfsz	((c:mcp2515_init@dummy)),c
	
	goto	l239
	line	133
	
l2620:
;mcp2515.c: 133: mcp2515_write_register(0x0F, 0x85);
	movlw	low(085h)
	movwf	((c:mcp2515_write_register@data)),c
	movlw	(0Fh)&0ffh
	
	call	_mcp2515_write_register
	line	136
;mcp2515.c: 136: mcp2515_write_register(0x60, 0x00);
	movlw	low(0)
	movwf	((c:mcp2515_write_register@data)),c
	movlw	(060h)&0ffh
	
	call	_mcp2515_write_register
	line	137
;mcp2515.c: 137: mcp2515_write_register(0x70, 0x00);
	movlw	low(0)
	movwf	((c:mcp2515_write_register@data)),c
	movlw	(070h)&0ffh
	
	call	_mcp2515_write_register
	line	140
;mcp2515.c: 140: mcp2515_write_register(0x20, 0x00);
	movlw	low(0)
	movwf	((c:mcp2515_write_register@data)),c
	movlw	(020h)&0ffh
	
	call	_mcp2515_write_register
	line	141
;mcp2515.c: 141: mcp2515_write_register(0x21, 0x00);
	movlw	low(0)
	movwf	((c:mcp2515_write_register@data)),c
	movlw	(021h)&0ffh
	
	call	_mcp2515_write_register
	line	142
;mcp2515.c: 142: mcp2515_write_register(0x22, 0x00);
	movlw	low(0)
	movwf	((c:mcp2515_write_register@data)),c
	movlw	(022h)&0ffh
	
	call	_mcp2515_write_register
	line	143
;mcp2515.c: 143: mcp2515_write_register(0x23, 0x00);
	movlw	low(0)
	movwf	((c:mcp2515_write_register@data)),c
	movlw	(023h)&0ffh
	
	call	_mcp2515_write_register
	line	144
;mcp2515.c: 144: mcp2515_write_register(0x24, 0x00);
	movlw	low(0)
	movwf	((c:mcp2515_write_register@data)),c
	movlw	(024h)&0ffh
	
	call	_mcp2515_write_register
	line	145
;mcp2515.c: 145: mcp2515_write_register(0x25, 0x00);
	movlw	low(0)
	movwf	((c:mcp2515_write_register@data)),c
	movlw	(025h)&0ffh
	
	call	_mcp2515_write_register
	line	146
;mcp2515.c: 146: mcp2515_write_register(0x26, 0x00);
	movlw	low(0)
	movwf	((c:mcp2515_write_register@data)),c
	movlw	(026h)&0ffh
	
	call	_mcp2515_write_register
	line	147
;mcp2515.c: 147: mcp2515_write_register(0x27, 0x00);
	movlw	low(0)
	movwf	((c:mcp2515_write_register@data)),c
	movlw	(027h)&0ffh
	
	call	_mcp2515_write_register
	line	149
;mcp2515.c: 149: mcp2515_write_register(0x2B, 0x03);
	movlw	low(03h)
	movwf	((c:mcp2515_write_register@data)),c
	movlw	(02Bh)&0ffh
	
	call	_mcp2515_write_register
	line	151
	
l242:
	return
	opt stack 0
GLOBAL	__end_of_mcp2515_init
	__end_of_mcp2515_init:
	signat	_mcp2515_init,88
	global	_mcp2515_write_register

;; *************** function _mcp2515_write_register *****************
;; Defined at:
;;		line 43 in file "D:\Projects\USBtin\firmware\source\mcp2515.c"
;; Parameters:    Size  Location     Type
;;  address         1    wreg     unsigned char 
;;  data            1    1[COMRAM] unsigned char 
;; Auto vars:     Size  Location     Type
;;  address         1    2[COMRAM] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         1       0       0       0       0       0       0       0       0       0       0
;;      Locals:         1       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         2       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_spi_transmit
;; This function is called by:
;;		_parseLine
;;		_mcp2515_init
;;		_mcp2515_set_SJA1000_filter_mask
;;		_mcp2515_set_SJA1000_filter_code
;;		_mcp2515_set_bittiming
;; This function uses a non-reentrant model
;;
psect	text26,class=CODE,space=0,reloc=2
	line	43
global __ptext26
__ptext26:
psect	text26
	file	"D:\Projects\USBtin\firmware\source\mcp2515.c"
	line	43
	global	__size_of_mcp2515_write_register
	__size_of_mcp2515_write_register	equ	__end_of_mcp2515_write_register-_mcp2515_write_register
	
_mcp2515_write_register:
;incstack = 0
	opt	stack 28
;mcp2515_write_register@address stored from wreg
	movwf	((c:mcp2515_write_register@address)),c
	line	46
	
l2290:
;mcp2515.c: 46: LATCbits.LATC6 = 0;
	bcf	((c:3979)),c,6	;volatile
	line	48
	
l2292:
;mcp2515.c: 48: spi_transmit(0x02);
	movlw	(02h)&0ffh
	
	call	_spi_transmit
	line	49
;mcp2515.c: 49: spi_transmit(address);
	movf	((c:mcp2515_write_register@address)),c,w
	
	call	_spi_transmit
	line	50
;mcp2515.c: 50: spi_transmit(data);
	movf	((c:mcp2515_write_register@data)),c,w
	
	call	_spi_transmit
	line	53
	
l2294:
;mcp2515.c: 53: LATCbits.LATC6 = 1;
	bsf	((c:3979)),c,6	;volatile
	line	54
	
l227:
	return
	opt stack 0
GLOBAL	__end_of_mcp2515_write_register
	__end_of_mcp2515_write_register:
	signat	_mcp2515_write_register,8312
	global	_spi_transmit

;; *************** function _spi_transmit *****************
;; Defined at:
;;		line 30 in file "D:\Projects\USBtin\firmware\source\mcp2515.c"
;; Parameters:    Size  Location     Type
;;  c               1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  c               1    0[COMRAM] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         1       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         1       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_mcp2515_write_register
;;		_mcp2515_read_register
;;		_mcp2515_bit_modify
;;		_mcp2515_init
;;		_mcp2515_read_status
;;		_mcp2515_rx_status
;;		_mcp2515_send_message
;;		_mcp2515_receive_message
;; This function uses a non-reentrant model
;;
psect	text27,class=CODE,space=0,reloc=2
	line	30
global __ptext27
__ptext27:
psect	text27
	file	"D:\Projects\USBtin\firmware\source\mcp2515.c"
	line	30
	global	__size_of_spi_transmit
	__size_of_spi_transmit	equ	__end_of_spi_transmit-_spi_transmit
	
_spi_transmit:
;incstack = 0
	opt	stack 27
;spi_transmit@c stored from wreg
	movwf	((c:spi_transmit@c)),c
	line	31
	
l2154:
;mcp2515.c: 31: SSPBUF = c;
	movff	(c:spi_transmit@c),(c:4041)	;volatile
	line	32
;mcp2515.c: 32: while (!SSPSTATbits.BF) {};
	
l221:
	btfss	((c:4039)),c,0	;volatile
	goto	u1471
	goto	u1470
u1471:
	goto	l221
u1470:
	
l223:
	line	33
;mcp2515.c: 33: return SSPBUF;
	movf	((c:4041)),c,w	;volatile
	line	34
	
l224:
	return
	opt stack 0
GLOBAL	__end_of_spi_transmit
	__end_of_spi_transmit:
	signat	_spi_transmit,4217
	global	_clock_process

;; *************** function _clock_process *****************
;; Defined at:
;;		line 41 in file "D:\Projects\USBtin\firmware\source\clock.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/1
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          2       0       0       0       0       0       0       0       0       0       0
;;      Totals:         2       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text28,class=CODE,space=0,reloc=2
	file	"D:\Projects\USBtin\firmware\source\clock.c"
	line	41
global __ptext28
__ptext28:
psect	text28
	file	"D:\Projects\USBtin\firmware\source\clock.c"
	line	41
	global	__size_of_clock_process
	__size_of_clock_process	equ	__end_of_clock_process-_clock_process
	
_clock_process:
;incstack = 0
	opt	stack 30
	line	42
	
l2408:
;clock.c: 42: if ((unsigned short) (TMR0 - clock_lastclock) > 375) {
	movff	(c:4054),??_clock_process+0+0	;volatile
	movff	(c:4054+1),??_clock_process+0+0+1	;volatile
	movf	((c:_clock_lastclock)),c,w
	subwf	(??_clock_process+0+0),c
	movf	((c:_clock_lastclock+1)),c,w
	subwfb	(??_clock_process+0+1),c
	movlw	078h
	subwf	(??_clock_process+0+0),c,w
	movlw	01h
	subwfb	(??_clock_process+0+1),c,w
	btfss	status,0
	goto	u1831
	goto	u1830
u1831:
	goto	l18
u1830:
	line	43
	
l2410:
;clock.c: 43: clock_lastclock += 375;
	movlw	low(0177h)
	addwf	((c:_clock_lastclock)),c
	movlw	high(0177h)
	addwfc	((c:_clock_lastclock+1)),c
	line	44
	
l2412:
;clock.c: 44: clock_msticker++;
	infsnz	((c:_clock_msticker)),c
	incf	((c:_clock_msticker+1)),c
	line	45
	
l2414:
;clock.c: 45: if (clock_msticker > 60000) clock_msticker = 0;
	movlw	061h
	subwf	((c:_clock_msticker)),c,w
	movlw	0EAh
	subwfb	((c:_clock_msticker+1)),c,w
	btfss	status,0
	goto	u1841
	goto	u1840
u1841:
	goto	l18
u1840:
	
l2416:
	clrf	((c:_clock_msticker)),c
	clrf	((c:_clock_msticker+1)),c
	line	47
	
l18:
	return
	opt stack 0
GLOBAL	__end_of_clock_process
	__end_of_clock_process:
	signat	_clock_process,88
	global	_clock_init

;; *************** function _clock_init *****************
;; Defined at:
;;		line 30 in file "D:\Projects\USBtin\firmware\source\clock.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_clock_reset
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text29,class=CODE,space=0,reloc=2
	line	30
global __ptext29
__ptext29:
psect	text29
	file	"D:\Projects\USBtin\firmware\source\clock.c"
	line	30
	global	__size_of_clock_init
	__size_of_clock_init	equ	__end_of_clock_init-_clock_init
	
_clock_init:
;incstack = 0
	opt	stack 29
	line	32
	
l2404:
;clock.c: 32: clock_reset();
	call	_clock_reset	;wreg free
	line	35
	
l2406:
;clock.c: 35: T0CON = 0x84;
	movlw	low(084h)
	movwf	((c:4053)),c	;volatile
	line	36
	
l13:
	return
	opt stack 0
GLOBAL	__end_of_clock_init
	__end_of_clock_init:
	signat	_clock_init,88
	global	_clock_reset

;; *************** function _clock_reset *****************
;; Defined at:
;;		line 61 in file "D:\Projects\USBtin\firmware\source\clock.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		None
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_clock_init
;;		_parseLine
;; This function uses a non-reentrant model
;;
psect	text30,class=CODE,space=0,reloc=2
	line	61
global __ptext30
__ptext30:
psect	text30
	file	"D:\Projects\USBtin\firmware\source\clock.c"
	line	61
	global	__size_of_clock_reset
	__size_of_clock_reset	equ	__end_of_clock_reset-_clock_reset
	
_clock_reset:
;incstack = 0
	opt	stack 29
	line	62
	
l2296:
;clock.c: 62: TMR0 = 0;
	clrf	((c:4054)),c	;volatile
	clrf	((c:4054+1)),c	;volatile
	line	63
;clock.c: 63: clock_lastclock = 0;
	clrf	((c:_clock_lastclock)),c
	clrf	((c:_clock_lastclock+1)),c
	line	64
;clock.c: 64: clock_msticker = 0;
	clrf	((c:_clock_msticker)),c
	clrf	((c:_clock_msticker+1)),c
	line	65
	
l24:
	return
	opt stack 0
GLOBAL	__end_of_clock_reset
	__end_of_clock_reset:
	signat	_clock_reset,88
	global	_clock_getMS

;; *************** function _clock_getMS *****************
;; Defined at:
;;		line 54 in file "D:\Projects\USBtin\firmware\source\clock.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  2    0[COMRAM] unsigned short 
;; Registers used:
;;		None
;; Tracked objects:
;;		On entry : 0/2
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMRAM   BANK0   BANK1 BANK2hh BANK2hl  BANK2l   BANK3   BANK4   BANK5   BANK6   BANK7
;;      Params:         2       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         2       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text31,class=CODE,space=0,reloc=2
	line	54
global __ptext31
__ptext31:
psect	text31
	file	"D:\Projects\USBtin\firmware\source\clock.c"
	line	54
	global	__size_of_clock_getMS
	__size_of_clock_getMS	equ	__end_of_clock_getMS-_clock_getMS
	
_clock_getMS:
;incstack = 0
	opt	stack 30
	line	55
	
l2418:
;clock.c: 55: return clock_msticker;
	movff	(c:_clock_msticker),(c:?_clock_getMS)
	movff	(c:_clock_msticker+1),(c:?_clock_getMS+1)
	line	56
	
l21:
	return
	opt stack 0
GLOBAL	__end_of_clock_getMS
	__end_of_clock_getMS:
	signat	_clock_getMS,90
psect	smallconst
	db 0	; dummy byte at the end
	global	__smallconst
	global	__mediumconst
	GLOBAL	__activetblptr
__activetblptr	EQU	2
	psect	intsave_regs,class=BIGRAM,space=1,noexec
	PSECT	rparam,class=COMRAM,space=1,noexec
	GLOBAL	__Lrparam
	FNCONF	rparam,??,?
GLOBAL	__Lparam, __Hparam
GLOBAL	__Lrparam, __Hrparam
__Lparam	EQU	__Lrparam
__Hparam	EQU	__Hrparam
	end
