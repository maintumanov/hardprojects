/************************** 
* Driver for Maxim DS18S20 
* Version 2.0 
* Date: 30/05/2007 
* Using CCS C Compiler 
* 

***************************/ 

// Define the data pins from microcontroler
#ifndef DS18B20_IO
#define DS18B20_IO PIN_c1
#endif


int8 i;   
int8 scratchpad_data[9]; 
signed int16 temp=0x0000; 
int8 busy =0;

int1 ds18b20_initialization(void){ 
   int1 ready; 
    
   output_low(DS18B20_IO); 
   delay_us(490);          //min 480us 
   output_float(DS18B20_IO); 
   delay_us(75);           //15 - 60us for device to respond 
   ready=!input(DS18B20_IO); 
   delay_us(415); 
   return ready; 
} 

void ds18b20_write_bit(int8 b){ 
   output_low(DS18B20_IO); 
   if(b == 1){ 
      delay_us(5);         //min 1us 
      output_float(DS18B20_IO); 
   } 
   delay_us(65); 
   output_float(DS18B20_IO); 
   delay_us(2);            //min 1us between write slots 
} 

void ds18b20_write_byte(int8 B){ 
   int8 i, aux; 
    
   for(i=0; i<8; i++){ 
      aux = B >> i;     //aux equals B shifted i times to the right 
      aux &= 0x01;      //least significant bit survives 
      ds18b20_write_bit(aux); 
   } 
   //delay_us(120); 
} 

int1 ds18b20_read_bit(void){ 
   output_low(DS18B20_IO); 
   delay_us(3);         //min 1us 
   output_float(DS18B20_IO); 
   delay_us(5); 
   return(input(DS18B20_IO)); //next a delay of 60 to 120us must be done! 
} 

int8 ds18b20_read_byte(void){ 
   int8 i, result=0x00; 
    
   for(i=0; i<8; i++){ 
      if(ds18b20_read_bit()) 
         result |= (0x01 << i); 
      delay_us(65); 
   } 
   return result; 
}

void ds18b20_start_convert()
{
      ds18b20_write_byte(0xCC);  //skip ROM command 
      ds18b20_write_byte(0x44);  //convert temperature command 
      output_float(DS18B20_IO); 
}


int8 ds18b20_wait()
{
int8 result;
result=ds18b20_read_byte(); 
return result; 
}

float ds18b20_read_temp()
{
float result;
ds18b20_initialization(); 
ds18b20_write_byte(0xCC);  //skip ROM command 
ds18b20_write_byte(0xBE);  //read scratch pad command 
 for(i=0; i<2; i++){ scratchpad_data[i]=ds18b20_read_byte();} 
      temp=make16(scratchpad_data[1], scratchpad_data[0]); 
      //result = result/16; 
      //calculate the whole number part 
      result = (temp >> 4) & 0x00FF; 
      //calculate the fractional part 
     
      if(temp & 0x0001) result = result + 0.06250; 
      if(temp & 0x0002) result = result + 0.12500; 
      if(temp & 0x0004) result = result + 0.25000; 
      if(temp & 0x0008) result = result + 0.50000; 
       if (result>128)result=result-256;
return result; 
}

