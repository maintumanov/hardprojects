#include <12F629.h>

#FUSES NOWDT                 	//No Watch Dog Timer
#FUSES HS                    	//High speed Osc (> 4mhz for PCM/PCH) (>10mhz for PCD)
#FUSES NOCPD                 	//No EE protection
#FUSES NOPROTECT             	//Code not protected from reading
#FUSES MCLR                  	//Master Clear pin enabled
#FUSES NOPUT                 	//No Power Up Timer
#FUSES NOBROWNOUT            	//No brownout reset
#FUSES BANDGAP_HIGH          
#use delay(clock=20000000)
#use rs232(baud=9600,parity=N,xmit=PIN_A3,rcv=PIN_A2,bits=8,stream=rs,enable=PIN_A0)

