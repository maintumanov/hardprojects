/************************** 
* Driver for Maxim DS18S20 
* Version 2.0 
* Date: 30/05/2007 
* Using CCS C Compiler 
* 

***************************/ 

// Define the data pins from microcontroler 

//#define ds18s20_dq_sensor1   PIN_c1 



// ROM Commands 
#define DS18S20_READ_ROM   0x33 
#define DS18S20_MATCH_ROM   0x55 
#define DS18S20_SKIP_ROM   0xCC 


// Function Commands 
#define DS18S20_CONVERT_T   0x44 
#define DS18S20_READ_SCRATCHPAD   0xBE 


// For memorize the CRC value 
int ds18s20_crc; 


// Actualize ds18s20_crc after insert a bit 
void ds18s20_actualizeCRCbit(int1 valueBit) 
{ 
   int1 xor_crcBeforeBit0_Input; 
   byte crcBefore; 

   crcBefore = ds18s20_crc; 

   // Calculation xor_crcBeforeBit0_Input 
   if ((crcBefore & 0x01) == valueBit) xor_crcBeforeBit0_Input = 0; 
   else   xor_crcBeforeBit0_Input = 1; 
    

   // Calculation crcBit0 
   if ((crcBefore & 0x02) > 0) ds18s20_crc |= 0x01; 
   else   ds18s20_crc &= 0xFE; 

   // Calculation crcBit1 
   if ((crcBefore & 0x04) > 0) ds18s20_crc |= 0x02; 
   else   ds18s20_crc &= 0xFD; 

   // Calculation crcBit2 
   if ((crcBefore & 0x08) > 0) 
   { 
      if (xor_crcBeforeBit0_Input == 0) ds18s20_crc |= 0x04; 
      else   ds18s20_crc &= 0xFB; 
   } 
   else 
   { 
      if (xor_crcBeforeBit0_Input == 1) ds18s20_crc |= 0x04; 
      else   ds18s20_crc &= 0xFB; 
   } 

   // Calculation crcBit3 
   if ((crcBefore & 0x10) > 0) 
   { 
      if (xor_crcBeforeBit0_Input == 0) ds18s20_crc |= 0x08; 
      else   ds18s20_crc &= 0xF7; 
   } 
   else 
   { 
      if (xor_crcBeforeBit0_Input == 1) ds18s20_crc |= 0x08; 
      else   ds18s20_crc &= 0xF7; 
   } 
    
   // Calculation crcBit4 
   if ((crcBefore & 0x20) > 0) ds18s20_crc |= 0x10; 
   else   ds18s20_crc &= 0xEF; 

   // Calculation crcBit5 
   if ((crcBefore & 0x40) > 0) ds18s20_crc |= 0x20; 
   else   ds18s20_crc &= 0xDF; 

   // Calculation crcBit6 
   if ((crcBefore & 0x80) > 0) ds18s20_crc |= 0x40; 
   else   ds18s20_crc &= 0xBF; 

   // Calculation crcBit7 
   if (xor_crcBeforeBit0_Input == 1) ds18s20_crc |= 0x80; 
   else   ds18s20_crc &= 0x7F; 
} 


// Inicialize the line 
// Returns 0 if exists one or more sensors in the line 
int1 ds18s20_inicializeLine() 
{ 
   int1 ack; 
    
   ack = 0; 

   output_float(ds18s20_dq_sensor1); 
   delay_ms(1); 

         output_bit(ds18s20_dq_sensor1, 0); 
         delay_us(600); 

         output_float(ds18s20_dq_sensor1); 
         delay_us(100); 
         ack = input(ds18s20_dq_sensor1); 
         delay_us(500); 


   return (ack); 
} 



// Write bit in the line 
void ds18s20_writeBit(int1 valueBit) 
{ 
    
   if (valueBit == 0) 
   { 
    
            output_bit(ds18s20_dq_sensor1, 0); 
            delay_us(110); 
            output_float(ds18s20_dq_sensor1); 
            delay_us(100); 

    
   } 
   else 
   { 


            output_bit(ds18s20_dq_sensor1, 0); 
            delay_us(10); 
            output_float(ds18s20_dq_sensor1); 
            delay_us(200); 

   } 
    
} 



// Read bit in the line 
int1 ds18s20_readBit() 
{ 
   int1 readBit; 


         output_bit(ds18s20_dq_sensor1, 0); 
         delay_us(5); 
         output_float(ds18s20_dq_sensor1); 
         delay_us(15); 
         readBit = input(ds18s20_dq_sensor1); 
         delay_us(100); 


   ds18s20_actualizeCRCbit(readBit);   // Actualize CRC 
   return(readBit); 
} 




// Write byte in the line 
void ds18s20_writeByte(int8 valueByte) 
{ 
   int i,mask; 

   mask = 0x01; 
   for (i=0; i<8; i++) 
   { 
      if((valueByte & mask) > 0)  ds18s20_writeBit(1); 
      else ds18s20_writeBit(0); 
      mask = mask << 1; 
   } 
} 


// Read byte in the line 
int8 ds18s20_readByte() 
{ 
   int8 i,valueByte = 0; 
    
   const int8 mask0 = 0x00; 
   const int8 mask1 = 0x80; 

   for (i=0; i<8; i++) 
   { 
      valueByte = valueByte >> 1; 
      if (ds18s20_readBit() == 1) valueByte |= mask1; 
       else valueByte |= mask0; 
   } 
   return (valueByte); 
}    



// Sends the command Read Scratchpad 
int1 ds18s20_sendsCommandReadScratchpad() 
{ 
   int1 ack; 

   ack = ds18s20_inicializeLine(); 
   if (ack == 1) 
   { 
      return (1); 
   } 

   ds18s20_writeByte(DS18S20_SKIP_ROM); 

   ds18s20_writeByte(DS18S20_READ_SCRATCHPAD); 

   return (0); 
} 


// Reads the value of temperature (resolution 1sC) 
int8 ds18s20_readTemperatureLowResolution() 
{ 
   int8 valueTemperature; 


   if (ds18s20_sendsCommandReadScratchpad() == 1) 
   { 
      return (100); 
   } 

   ds18s20_crc = 0; 
   valueTemperature = ds18s20_readByte()/2; 
   ds18s20_readByte(); 
   ds18s20_readByte(); 
   ds18s20_readByte(); 
   ds18s20_readByte(); 
   ds18s20_readByte(); 
   ds18s20_readByte(); 
   ds18s20_readByte(); 
   ds18s20_readByte(); 

   // Error on value read 
   if (ds18s20_crc != 0) 
   { 
      return (100); 
   } 

   return (valueTemperature); 
} 


// Reads the value of temperature (resolution 0.1sC) 
int16 ds18s20_readTemperatureHighResolution() 
{ 
   float temperature,aux; 
   int8 temp_read, count_per_c, count_remain; 


   if (ds18s20_sendsCommandReadScratchpad() == 1) 
   { 
      return (0); 
   } 

   ds18s20_crc = 0; 
   temp_read = ds18s20_readByte(); 
   ds18s20_readByte(); 
   ds18s20_readByte(); 
   ds18s20_readByte(); 
   ds18s20_readByte(); 
   ds18s20_readByte(); 
   count_remain = ds18s20_readByte(); 
   count_per_c = ds18s20_readByte(); 
   ds18s20_readByte(); 

   if (ds18s20_crc != 0) 
   { 
      return (0); 
   } 

   temp_read = temp_read >> 1; 
   temp_read &= 0x7F; 
   temperature = temp_read - 0.25; 
   aux = count_per_c - count_remain; 
   aux /= count_per_c; 
   temperature += aux; 
   temperature *= 10; 
    
   return (temperature); 
} 


// Send a command ConvertT for one sensor 
// Need to wait for about one second before read the value 
int1 ds18s20_startConvert() 
{ 
   int1 ack; 

   ack = ds18s20_inicializeLine(); 
   if (ack == 1) 
   { 
      return (1); 
   } 

   ds18s20_writeByte(DS18S20_SKIP_ROM); 
   ds18s20_writeByte(DS18S20_CONVERT_T); 
    
   return (0); 
}