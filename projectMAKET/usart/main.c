#include "D:\проекты\проекты контроллеры\projectmaket\usart\main.h"
//#include <stdlib.h>
#include <Flex_lcd_16x1.c>
#include <ds18B20.c>
int8 data;
int1 c;

#int_ext
void  ext_isr(void) 
{
data=fgetc(rs_2);
if (data==32) c=1; 
}

//#int_RDA
void  RDA_isr(void) 
{

}

void main()
{   
   float t;
   port_b_pullups(TRUE);
   setup_adc_ports( RA0_RA1_RA3_ANALOG );
   setup_adc(ADC_OFF);
   setup_psp(PSP_DISABLED);
   setup_timer_0(RTCC_INTERNAL|RTCC_DIV_1);
   setup_timer_1(T1_INTERNAL|T1_DIV_BY_1);
   setup_timer_2(T2_DIV_BY_16,255,1);
   setup_ccp1(CCP_OFF);
   setup_ccp2(CCP_PWM);
   set_pwm2_duty(0);
   setup_comparator(NC_NC_NC_NC);
   setup_vref(FALSE);
 // enable_interrupts(INT_RB);
 enable_interrupts(INT_ext);
 EXT_INT_EDGE(H_TO_L);
  // enable_interrupts(INT_RDA);
  enable_interrupts(GLOBAL);
 //  enable_interrupts(INT_AD);
  set_tris_d(0); 
  set_tris_c(0); 
  set_tris_e(0);
  set_tris_b(255);
  set_tris_a(255);
  c=0;
   // TODO: USER CODE!!
   lcd_init();
   while(1){ 
      if(ds18b20_initialization()){ 
      ds18b20_start_convert();
      while (ds18b20_wait()){} 

      printf(lcd_putc,"\fTemper:%3.1f   ",ds18b20_read_temp());
      if (c==1)
        {
        fprintf(rs_2, "temp:%7.1f C\r\n", ds18b20_read_temp());
        c=0;
        }
      } 
      delay_ms(200); 
   }

}
