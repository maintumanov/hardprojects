#include <main.h>
#include <Flex_lcd_16x1.c>

#include <2404.C>


#INT_EXT

void EXT_isr(void) {

}

//#define LCD_DB4   PIN_D1
//#define LCD_DB5   PIN_D2
//#define LCD_DB6   PIN_D3
//#define LCD_DB7   PIN_D4

//#define LCD_RS    PIN_E0
//#define LCD_RW    PIN_E1
//#define LCD_E     PIN_E2

void main() {
    char k;
    
    port_B_pullups(0xFF);
    setup_adc_ports(AN0_AN1_AN3);
    setup_adc(ADC_CLOCK_DIV_2);
    setup_psp(PSP_DISABLED);
    setup_timer_0(RTCC_INTERNAL | RTCC_DIV_1);
    setup_timer_1(T1_INTERNAL | T1_DIV_BY_1);
    setup_timer_2(T2_DIV_BY_16, 255, 1);
    setup_ccp1(CCP_OFF);
    setup_ccp2(CCP_PWM);
    set_pwm2_duty(0);
    setup_comparator(NC_NC_NC_NC);
    setup_vref(FALSE);
    enable_interrupts(INT_RB);
    enable_interrupts(INT_RDA);
    enable_interrupts(GLOBAL);

    setup_psp(PSP_DISABLED);
    set_tris_d(0);
    set_tris_e(0);


    //  lcd_init();
    //  lcd_putc("\f hello 1");


    while (TRUE) {

        //TODO: User Code
    }

}
