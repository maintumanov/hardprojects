#include <16F877A.h>
#device ADC=8

#FUSES NOWDT                    //No Watch Dog Timer
#FUSES NOBROWNOUT               //No brownout reset
#FUSES NOLVP                    //No low voltage prgming, B3(PIC16) or B5(PIC18) used for I/O

#use delay(crystal=20000000)
#use STANDARD_IO( C )
#use FIXED_IO( B_outputs=PIN_B3,PIN_B2,PIN_B1 )
#use FIXED_IO( C_outputs=PIN_C6,PIN_C5,PIN_C3 )

#define analog1   PIN_A0
#define analog2   PIN_A1
#define analog_input   PIN_A3
#define TSOP   PIN_A4
#define INTE   PIN_B0
#define KEYR1   PIN_B1
#define KEYR2   PIN_B2
#define KEYR3   PIN_B3
#define KEYR4   PIN_B4
#define KEYC1   PIN_B5
#define KEYC2   PIN_B6
#define KEYC3   PIN_B7
#define OneWare   PIN_C1
#define PSCL   PIN_C3
#define PSDA   PIN_C4
#define PSDO   PIN_C5
#define TX   PIN_C6
#define RX   PIN_C7
#define DB0   PIN_D0
#define DB1   PIN_D1
#define DB2   PIN_D2
#define DB3   PIN_D3
#define DB4   PIN_D4
#define DB5   PIN_D5
#define DB6   PIN_D6
#define DB7   PIN_D7
#define DRS   PIN_E0
#define DRW   PIN_E1
#define DE   PIN_E2


