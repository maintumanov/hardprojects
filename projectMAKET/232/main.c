  #include "D:\�������\������� �����������\projectmaket\232\main.h"
  #include <stdlib.h>
  #include <Flex_lcd_16x1.c>
  #include <ds1307.C> 



//RS485--------------------------------
int8 DeviceType=20;
int8 Address=0;            //������ ����������
int8 TargetAddress=0;      //������ ���������� ����������
int1 BuffFull=0;           //���� ������������ ������


int8 buff_sn[10];          //����� ��������
int8 buff_rs[61];          //����� ������
int8 ln_buff_rs;           //������� � ������ ������
int1 en_rs;                //���� ������� ��������
int8 data_rs;              //���������� ������

byte get_byte_of_buff(byte adr)  //������� ���������� ����� �� ������
{
byte rs,a,b;
a=buff_rs[adr];
b=buff_rs[adr+1];
rs=b>>4;
rs=rs+a;
return rs;
}

byte geth_byte(byte b)           //������� ���������� ������� ����� �����
{
return b & 0xF0;
}

byte getl_byte(byte b)           //������� ���������� ������� ����� �����
{
return b<<4;
}

void send_mes(byte ln)           //������� �������� ���������
{
byte cr,n;
cr=0;
buff_sn[0]=TargetAddress;
buff_sn[1]=Address;

fputc(':',rs485);
for(n=0;n<ln;++n)
 {
  cr=cr^buff_sn[n];
  fputc(geth_byte(buff_sn[n]),rs485);
  fputc(getl_byte(buff_sn[n]),rs485);
 }
  fputc(geth_byte(cr),rs485);
  fputc(getl_byte(cr),rs485);
  fputc(0x0D,rs485);
  
}



void send_error_crc()            //������� �������� ������ ����������� �����
{
buff_sn[2]=5;
send_mes(3);
}

void send_error_cmd()             //������� �������� ������ �������
{
buff_sn[2]=6;
send_mes(3);
}

void set_adr()                   //������� ��������� ������ �������
{

Address=get_byte_of_buff(6);
write_eeprom (1, Address);
buff_sn[2]=8;
send_mes(3);
}

void send_type()                 //������� �������� ������ ���� ����������
{
buff_sn[2]=16;
buff_sn[3]=DeviceType;
send_mes(4);
}

void cmd()                       // ��������� ��������� �������
{
byte n,adn,cm,cr,crm;
//output_bit (pin_A0, 1);
disable_interrupts(GLOBAL);

adn=get_byte_of_buff(0);
TargetAddress=get_byte_of_buff(2);
if (ln_buff_rs<61)
  if (adn==Address){
    cm=get_byte_of_buff(4);
    crm=0;
    for(n=0;n<ln_buff_rs-2;n=n+2) crm=crm^get_byte_of_buff(n);
    cr=get_byte_of_buff(ln_buff_rs-2);
    if (cr==crm) 
     switch (cm)  {
      case  1: send_type(); break;
      case  2: set_adr(); break;
     // case 50: sit_disp(); break;
     // default: send_error_cmd(); break;
     };
  };
ln_buff_rs=254;
en_rs=0;
//output_bit (pin_A0, 0);
enable_interrupts(GLOBAL);

}
//--------------------------------------------



int8 buff_com[30];
int8 buff_count;
int8 data;
int8 en_pc;

int8 sec,min,hrs; 
int8 bssec,bsmin,bshrs; 
int8 besec,bemin,behrs; 

int1 inper;
int1 beepen;
int8 beepstate;

int1 dmov;
int8 tmp;
int16 LcdDelay;


//#int_RB
void  RB_isr(void) 
{
 
}

#int_RDA
void  RDA_isr(void) 
{
data_rs=fgetc(rs485);
if (data_rs==':') ln_buff_rs=0;
 else 
   if (data_rs==0x0D ) en_rs=1;
   else  
   if (ln_buff_rs<61)
    { buff_rs[ln_buff_rs]=data_rs;
      ++ln_buff_rs; };
}

#int_EXT
void  EXT_isr(void) 
{
data=fgetc(PC);
if (data==254) buff_count=0;
 else 
  if (data==255 ) en_pc=1;
  else  
   if (buff_count<30)
    { buff_com[buff_count]=data;
      ++buff_count; }; 
}

void cmd_pc()
{
disable_interrupts(GLOBAL);
switch (buff_com[0]) 
 {
 case  1:
    {ds1307_set_date_time(buff_com[1],buff_com[2],buff_com[3],buff_com[4],buff_com[5],buff_com[6],buff_com[7]); //day,mth,year,dow,hour,min,sec
    }

 case  2:
    {
    bssec=buff_com[1]; 
    bsmin=buff_com[2]; 
    bshrs=buff_com[3];
    besec=buff_com[4]; 
    bemin=buff_com[5]; 
    behrs=buff_com[6];
    write_eeprom (10, bssec);
    write_eeprom (11, bsmin);
    write_eeprom (12, bshrs);
    write_eeprom (13, besec);
    write_eeprom (14, bemin);
    write_eeprom (15, behrs);
    }
    
 case  3:
    {
    fputc(254,pc);
    fputc(3,pc);
    fputc(bshrs,pc);
    fputc(bsmin,pc);
    fputc(behrs,pc);
    fputc(bemin,pc);
    fputc(255,pc);
    }
 }

en_pc=0;
buff_count=0;
enable_interrupts(GLOBAL);
}


void main()
{   
    bssec=read_eeprom (10); 
    bsmin=read_eeprom (11);
    bshrs=read_eeprom (12);
    besec=read_eeprom (13);
    bemin=read_eeprom (14); 
    behrs=read_eeprom (15);

     //rs485------
   address = read_eeprom (1);
   en_rs=0;
   ln_buff_rs=254;
   //-----------
   port_b_pullups(TRUE);
   setup_adc_ports(AN0_AN1_AN2_AN4_VSS_VREF);
   setup_adc(ADC_CLOCK_DIV_2);
   setup_psp(PSP_DISABLED);
   setup_timer_0(RTCC_INTERNAL|RTCC_DIV_1);
   setup_timer_1(T1_INTERNAL|T1_DIV_BY_1);
   setup_timer_2(T2_DIV_BY_16,255,1);
   setup_ccp1(CCP_OFF);
   setup_ccp2(CCP_PWM);
   set_pwm2_duty(0);
   setup_comparator(NC_NC_NC_NC);
   setup_vref(FALSE);
   EXT_INT_EDGE(H_TO_L);
 //  enable_interrupts(INT_RB);
   
   set_tris_b(0b00011111);
   set_tris_d(0);
   output_b(0b11000000);
   output_d(0);
   
   enable_interrupts(INT_RDA);
   enable_interrupts(INT_EXT);
   enable_interrupts(GLOBAL);  
   
   en_pc=0;
   buff_count=0;
   
   inper=0;
   beepen=0;
   dmov=0;
   LcdDelay=0;
   // TODO: USER CODE!!
   lcd_init();
   ds1307_init();
 while (1) {
 
if (en_pc==1) cmd_pc();
if (en_rs==1) cmd();   //�������� � ������� ����� �������

//====timer====
dmov=input(pin_b3);
tmp=0;
if (bshrs<=hrs) tmp++;
if (bsmin<=min) tmp++;
if (behrs>=hrs) tmp++;
if (bemin>=min) tmp++;
if (tmp==4) inper=1;
 else inper=0;

 if ((behrs==hrs)&& (bemin==min)) beepen=1;
 if (inper==0) beepen=0; 
 if (dmov&inper) beepen=1;   
 
 if ((beepstate!=sec)&& beepen==1) {beepstate=sec;output_bit(pin_b7,!input_state(pin_b7));}; 
 if (beepen==0) output_bit(pin_b7,0);

  if (LcdDelay==10000) {
  ds1307_get_time(hrs,min,sec);
  printf(lcd_putc,"\f\%02d:\%02d \%02d:\%02d %d%d%d\r\n", hrs,min,behrs,bemin,dmov,inper,beepen);
  LcdDelay=0;
  }
  LcdDelay++;

 }
}
