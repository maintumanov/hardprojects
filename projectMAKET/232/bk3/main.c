  #include "D:\�������\������� �����������\projectmaket\232\main.h"
  #include <stdlib.h>
  #include <Flex_lcd_16x1.c>
  #include <ds1307.C> 



//RS485--------------------------------
int8 DeviceType=11;
int8 Address=0;            //������ ����������
int8 TargetAddress=0;      //������ ���������� ����������
int1 BuffFull=0;           //���� ������������ ������


int8 buff_sn[10];          //����� ��������
int8 buff_rs[61];          //����� ������
int8 ln_buff_rs;           //������� � ������ ������
int1 en_rs;                //���� ������� ��������
int8 data_rs;              //���������� ������

byte get_byte_of_buff(byte adr)  //������� ���������� ����� �� ������
{
byte rs,a,b;
a=buff_rs[adr];
b=buff_rs[adr+1];
rs=b>>4;
rs=rs+a;
return rs;
}

byte geth_byte(byte b)           //������� ���������� ������� ����� �����
{
return b & 0xF0;
}

byte getl_byte(byte b)           //������� ���������� ������� ����� �����
{
return b<<4;
}

void send_mes(byte ln)           //������� �������� ���������
{
byte cr,n;
cr=0;
buff_sn[0]=TargetAddress;
buff_sn[1]=Address;

fputc(':',rs485);
for(n=0;n<ln;++n)
 {
  cr=cr^buff_sn[n];
  fputc(geth_byte(buff_sn[n]),rs485);
  fputc(getl_byte(buff_sn[n]),rs485);
 }
  fputc(geth_byte(cr),rs485);
  fputc(getl_byte(cr),rs485);
  fputc(0x0D,rs485);
  
}



void send_error_crc()            //������� �������� ������ ����������� �����
{
buff_sn[2]=5;
send_mes(3);
}

void send_error_cmd()             //������� �������� ������ �������
{
buff_sn[2]=6;
send_mes(3);
}

void set_adr()                   //������� ��������� ������ �������
{

Address=get_byte_of_buff(6);
write_eeprom (1, Address);
buff_sn[2]=8;
send_mes(3);
}

void send_type()                 //������� �������� ������ ���� ����������
{
buff_sn[2]=16;
buff_sn[3]=DeviceType;
send_mes(4);
}





int8 buff_com[30];
int8 buff_count;
int8 data;
int8 en_pc;

int8 sec; 
int8 min; 
int8 hrs;

int8 bssec; 
int8 bsmin; 
int8 bshrs;

int8 besec; 
int8 bemin; 
int8 behrs;

int1 inper;
int1 beepen;
int8 beepstate;

int1 dmov;
int8 tmp;
int16 LcdDelay;


#int_RB
void  RB_isr(void) 
{
data_rs=fgetc(rs485);
if (data_rs==':') ln_buff_rs=0;
 else 
   if (data_rs==0x0D ) en_rs=1;
   else  
   if (ln_buff_rs<61)
    { buff_rs[ln_buff_rs]=data_rs;
      ++ln_buff_rs; }; 
}

//#int_RDA
void  RDA_isr(void) 
{

}

#int_EXT
void  EXT_isr(void) 
{
data=fgetc(PC);
if (data==254) buff_count=0;
 else 
  if (data==255 ) en_pc=1;
  else  
   if (buff_count<30)
    { buff_com[buff_count]=data;
      ++buff_count; }; 
}

void cmd()
{
switch (buff_com[0]) 
 {
 case  1:
    {ds1307_set_date_time(buff_com[1],buff_com[2],buff_com[3],buff_com[4],buff_com[5],buff_com[6],buff_com[7]); //day,mth,year,dow,hour,min,sec
    }

 case  2:
    {
    bssec=buff_com[1]; 
    bsmin=buff_com[2]; 
    bshrs=buff_com[3];
    besec=buff_com[4]; 
    bemin=buff_com[5]; 
    behrs=buff_com[6];
    }
    
 case  3:
    {
    fputc(254,pc);
    fputc(3,pc);
    fputc(bshrs,pc);
    fputc(bsmin,pc);
    fputc(behrs,pc);
    fputc(bemin,pc);
    fputc(255,pc);
    }
 }

en_pc=0;
buff_count=0;
}


void main()
{   
   
   port_b_pullups(TRUE);
   setup_adc_ports(AN0_AN1_AN2_AN4_VSS_VREF);
   setup_adc(ADC_CLOCK_DIV_2);
   setup_psp(PSP_DISABLED);
   setup_timer_0(RTCC_INTERNAL|RTCC_DIV_1);
   setup_timer_1(T1_INTERNAL|T1_DIV_BY_1);
   setup_timer_2(T2_DIV_BY_16,255,1);
   setup_ccp1(CCP_OFF);
   setup_ccp2(CCP_PWM);
   set_pwm2_duty(0);
   setup_comparator(NC_NC_NC_NC);
   setup_vref(FALSE);
   EXT_INT_EDGE(H_TO_L);
 //  enable_interrupts(INT_RB);
 //  enable_interrupts(INT_RDA);

   
   set_tris_b(0b00011111);
   set_tris_d(0);
   output_b(0b11000000);
   output_d(0);
   
   enable_interrupts(INT_EXT);
   enable_interrupts(GLOBAL);  
   
   en_pc=0;
   buff_count=0;
   
   inper=0;
   beepen=0;
   dmov=0;
   LcdDelay=0;
   // TODO: USER CODE!!
   lcd_init();
   ds1307_init();
 while (1) {
 
if (en_pc==1) cmd();

//====timer====
dmov=input(pin_b3);
tmp=0;
if (bshrs<=hrs) tmp++;
if (bsmin<=min) tmp++;
if (behrs>=hrs) tmp++;
if (bemin>=min) tmp++;
if (tmp==4) inper=1;
 else inper=0;

 if ((behrs==hrs)&& (bemin==min)) beepen=1;
 if (inper==0) beepen=0; 
 if (dmov&inper) beepen=1;   
 
 if ((beepstate!=sec)&& beepen==1) {beepstate=sec;output_bit(pin_b7,!input_state(pin_b7));}; 
 if (beepen==0) output_bit(pin_b7,0);

  if (LcdDelay=1000) {
  ds1307_get_time(hrs,min,sec);
  printf(lcd_putc,"\f\%02d:\%02d:\%02d %d%d%d %d\r\n", hrs,min,sec,dmov,inper,beepen,bsmin);
  LcdDelay=0;
  }
  LcdDelay++;

// printf("Hello\n");
 }
}
