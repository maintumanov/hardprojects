
   #include <18F2550.h>
   #fuses HSPLL,NOWDT,NOPROTECT,NOLVP,NODEBUG,USBDIV,PLL5,CPUDIV1,VREGEN
   #use delay(clock=20000000)

   #DEFINE LED1 PIN_A0  //green
   #define LED2 PIN_A1 //red

   #define LEDS_OFF()   LED_OFF(LED1); LED_OFF(LED2); 


#ifndef LED_ON
#define LED_ON(x) output_high(x)
#endif

#ifndef LED_OFF
#define LED_OFF(x) output_low(x)
#endif

