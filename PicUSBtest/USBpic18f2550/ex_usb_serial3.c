



// in order for handle_incoming_usb() to be able to transmit the entire
// USB message in one pass, we need to increase the CDC buffer size from
// the normal size and use the USB_CDC_DELAYED_FLUSH option.
// failure to do this would cause some loss of data.
//#define USB_CDC_DELAYED_FLUSH
//#define USB_CDC_DATA_LOCAL_SIZE  128

////// end configuration /////////////////////////////////////////////////

//#include "ex_usb_common.h"
#include "ex_usb_serial3.h"


// if USB_CDC_ISR is defined, then this function will be called
// by the USB ISR when there incoming CDC (virtual com port) data.
// this is useful if you want to port old RS232 code that was use
// #int_rda to CDC.
#define USB_CDC_ISR() handle_incoming_usb()
static void handle_incoming_usb(void);

// Includes all USB code and interrupts, as well as the CDC API
#include "usb_cdc.h"

static void handle_incoming_usb(void)
{

   char c;

   while(usb_cdc_kbhit())
   {
      // since we cannot call usb_cdc_getc() from an ISR unless there
      // is already data in the buffer, we must state machine this routine
      // to continue from previous location.
      c = usb_cdc_getc();
      if (c == 'A') LED_ON(LED2); else LED_OFF(LED2);

   }


}

void main(void)
{
   LED_ON(LED1);
   LED_OFF(LED2);

   usb_init_cs();

   for(;;)
   {
      // service low level USB operations.
      usb_task();


   }
}
