#include <main.h>

/* TODO: Use usb_put_packet() to transmit data to USB HID,
using USB_HID_ENDPONT for the endpoint and the payload size
needs to match USB_CONFIG_HID_TX_SIZE. Use usb_get_packet()
to read incomming data, using USB_HID_ENDPOINT for the
endpoint. usb_enumerated() can be used to see if connected to
a host and ready to communicate. */

#define USB_CONFIG_VID 0x2400
#define USB_CONFIG_PID 0x000B
#define USB_CONFIG_BUS_POWER 100
#define USB_STRINGS_OVERWRITTEN

char USB_STRING_DESC_OFFSET[]={0,4,24};

char const USB_STRING_DESC[]={
   //string 0 - language
      4,  //length of string index
      0x03,  //descriptor type (STRING)
      0x09,0x04,  //Microsoft Defined for US-English
   //string 1 - manufacturer
      20,  //length of string index
      0x03,  //descriptor type (STRING)
      's',0,
      'i',0,
      'g',0,
      'n',0,
      'a',0,
      'l',0,
      'n',0,
      'e',0,
      't',0,
   //string 2 - product
      22,  //length of string index
      0x03,  //descriptor type (STRING)
      'h',0,
      'i',0,
      'd',0,
      ' ',0,
      'd',0,
      'e',0,
      'v',0,
      'i',0,
      'c',0,
      'e',0
};

#define USB_CONFIG_HID_TX_SIZE 63
#define USB_CONFIG_HID_RX_SIZE 63
#include <pic18_usb.h>
#include <usb_desc_hid.h>
#include <usb.c>



void main()
{
   usb_init_cs();

   while(TRUE)
   {
      usb_task();

   
   }

}
