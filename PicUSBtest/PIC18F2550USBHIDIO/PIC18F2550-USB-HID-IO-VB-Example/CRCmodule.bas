Attribute VB_Name = "CRCmodule"
Option Explicit
Function booleanAND(x As Long, y As Long) As Long
    Dim output As Long
    Dim i As Integer
    Dim temp, temp1 As Integer
    output = 0
    
    For i = 23 To 0 Step -1 '24 bit numbers accepted
        temp = x \ (2 ^ i) 'gets the bit of text1.text
        x = x - temp * (2 ^ i) 'subtracts it from the number
        temp1 = y \ (2 ^ i) 'gets the bit of text2.text
        y = y - temp1 * (2 ^ i) 'subtracts it from the number
        If temp = 1 And temp1 = 1 Then 'If both are equal to 1 then return a 1
            output = output + (2 ^ i) 'This returns a decimal number
        End If
    Next i
    
    booleanAND = output
End Function

Function booleanXOR(x As Long, y As Long) As Long
    Dim output As Long
    Dim i As Integer
    Dim temp, temp1 As Integer
    output = 0
    
    For i = 23 To 0 Step -1 '24 bit numbers accepted
        temp = x \ (2 ^ i) 'gets the bit of text1.text
        x = x - temp * (2 ^ i) 'subtracts it from the number
        temp1 = y \ (2 ^ i) 'gets the bit of text2.text
        y = y - temp1 * (2 ^ i) 'subtracts it from the number
        If temp = 1 Xor temp1 = 1 Then 'If one or the other but not both=1 then return a 1
            output = output + (2 ^ i) 'This returns a decimal number
        End If
    Next i
    
    booleanXOR = output
End Function

Function RShift(ByVal plValue As Long, piTimes As Integer) As Long
    'Note a unsigned int needs to be stored
    '     in a long
    plValue = (plValue \ (2 ^ piTimes))
    RShift = plValue
End Function

Function LShift(ByVal plValue As Long, piTimes As Integer) As Long
    Dim k As Integer
    'Note a unsigned int needs to be stored
    '     in a long
    plValue = plValue And CLng(RShift(CLng("&HFFFF"), piTimes))
    plValue = (plValue * (2 ^ piTimes))
    LShift = plValue
End Function

Public Function calc_CRC(oldCRC As Byte, newByte As Byte) As Integer
    Dim shift_reg, data_bit, sr_lsb, fb_bit, j As Integer
    shift_reg = oldCRC
    For j = 0 To 7
      data_bit = booleanAND(RShift(newByte, j), 1)
      sr_lsb = booleanAND(Val(shift_reg), 1)
      fb_bit = booleanAND(booleanXOR(Val(data_bit), Val(sr_lsb)), 1)
      shift_reg = RShift(shift_reg, 1)
      If (fb_bit = 1) Then shift_reg = booleanXOR(Val(shift_reg), 140)
    Next
    calc_CRC = shift_reg
End Function
