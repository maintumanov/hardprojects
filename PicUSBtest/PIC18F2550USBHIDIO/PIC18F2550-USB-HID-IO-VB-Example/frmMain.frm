VERSION 5.00
Begin VB.Form frmMain 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "PIC18F2550 USB HID IO - www.semifluid.com"
   ClientHeight    =   8055
   ClientLeft      =   240
   ClientTop       =   315
   ClientWidth     =   9825
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8055
   ScaleWidth      =   9825
   Begin VB.CommandButton cmdDemoWritePortB 
      Caption         =   "Demo Write Port B"
      Height          =   375
      Left            =   8160
      TabIndex        =   60
      Top             =   6600
      Width           =   1575
   End
   Begin VB.TextBox txtReadADCBytePin 
      Height          =   375
      Left            =   2520
      TabIndex        =   59
      Text            =   "0"
      Top             =   2280
      Width           =   495
   End
   Begin VB.TextBox txtReadADCnTimesPin 
      Height          =   375
      Left            =   3480
      TabIndex        =   57
      Text            =   "0"
      Top             =   2760
      Width           =   495
   End
   Begin VB.TextBox txtReadADCnTimesMSPin 
      Height          =   375
      Left            =   6000
      TabIndex        =   56
      Text            =   "0"
      Top             =   3720
      Width           =   495
   End
   Begin VB.TextBox txtReadADCnTimesUSPin 
      Height          =   375
      Left            =   6000
      TabIndex        =   55
      Text            =   "0"
      Top             =   3240
      Width           =   495
   End
   Begin VB.CommandButton cmdReadRAMData 
      Caption         =   "Read RAM Data"
      Height          =   375
      Left            =   8160
      TabIndex        =   54
      Top             =   2280
      Width           =   1575
   End
   Begin VB.TextBox txtReadRAMByte 
      Height          =   375
      Left            =   1800
      TabIndex        =   53
      Text            =   "0"
      Top             =   1320
      Width           =   495
   End
   Begin VB.TextBox txtInput 
      Height          =   1335
      Left            =   6960
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   52
      Top             =   2760
      Width           =   2775
   End
   Begin VB.TextBox txtReadPinValue 
      Enabled         =   0   'False
      Height          =   375
      Left            =   6960
      TabIndex        =   51
      Text            =   "0"
      Top             =   7080
      Width           =   495
   End
   Begin VB.TextBox txtWriteRAMByte 
      Height          =   375
      Left            =   2640
      TabIndex        =   50
      Text            =   "0"
      Top             =   1800
      Width           =   495
   End
   Begin VB.TextBox txtWriteRAMByteValue 
      Height          =   375
      Left            =   1800
      TabIndex        =   48
      Text            =   "0"
      Top             =   1800
      Width           =   495
   End
   Begin VB.TextBox txtReadRAMByteValue 
      Enabled         =   0   'False
      Height          =   375
      Left            =   6960
      TabIndex        =   47
      Text            =   "0"
      Top             =   1320
      Width           =   495
   End
   Begin VB.TextBox txtReadADCByteValue 
      Enabled         =   0   'False
      Height          =   375
      Left            =   6960
      TabIndex        =   46
      Text            =   "0"
      Top             =   2280
      Width           =   495
   End
   Begin VB.TextBox txtReadPort 
      Enabled         =   0   'False
      Height          =   375
      Left            =   6960
      TabIndex        =   45
      Text            =   "0"
      Top             =   6120
      Width           =   495
   End
   Begin VB.TextBox txtWritePort 
      Height          =   375
      Left            =   1800
      TabIndex        =   44
      Text            =   "0"
      Top             =   6600
      Width           =   495
   End
   Begin VB.TextBox txtWritePinValue 
      Height          =   375
      Left            =   3480
      TabIndex        =   43
      Text            =   "0"
      Top             =   7560
      Width           =   495
   End
   Begin VB.TextBox txtWritePin 
      Height          =   375
      Left            =   1800
      TabIndex        =   41
      Text            =   "0"
      Top             =   7560
      Width           =   495
   End
   Begin VB.CommandButton cmdWritePin 
      Caption         =   "Write Pin"
      Height          =   375
      Left            =   120
      TabIndex        =   40
      Top             =   7560
      Width           =   1575
   End
   Begin VB.TextBox txtReadPin 
      Height          =   375
      Left            =   1800
      TabIndex        =   38
      Text            =   "0"
      Top             =   7080
      Width           =   495
   End
   Begin VB.CommandButton cmdReadPin 
      Caption         =   "Read Pin"
      Height          =   375
      Left            =   120
      TabIndex        =   37
      Top             =   7080
      Width           =   1575
   End
   Begin VB.CommandButton cmdWritePort 
      Caption         =   "Write Port B"
      Height          =   375
      Left            =   120
      TabIndex        =   36
      Top             =   6600
      Width           =   1575
   End
   Begin VB.CommandButton cmdReadPort 
      Caption         =   "Read Port B"
      Height          =   375
      Left            =   120
      TabIndex        =   35
      Top             =   6120
      Width           =   1575
   End
   Begin VB.CommandButton cmdClearUseCRC 
      Caption         =   "Clear Use CRC"
      Height          =   375
      Left            =   120
      TabIndex        =   34
      Top             =   5640
      Width           =   1575
   End
   Begin VB.CommandButton cmdSetUseCRC 
      Caption         =   "Set Use CRC"
      Height          =   375
      Left            =   120
      TabIndex        =   33
      Top             =   5160
      Width           =   1575
   End
   Begin VB.TextBox txtSetRAMByte 
      Height          =   375
      Left            =   1800
      TabIndex        =   32
      Text            =   "255"
      Top             =   4680
      Width           =   495
   End
   Begin VB.CommandButton cmdSetRAMByte 
      Caption         =   "Write Byte to RAM"
      Height          =   375
      Left            =   120
      TabIndex        =   31
      Top             =   4680
      Width           =   1575
   End
   Begin VB.CommandButton cmdClearRAM 
      Caption         =   "Clear RAM"
      Height          =   375
      Left            =   120
      TabIndex        =   30
      Top             =   4200
      Width           =   1575
   End
   Begin VB.TextBox txtReadADCnTimesMSValue 
      Height          =   375
      Left            =   3240
      TabIndex        =   28
      Text            =   "1"
      Top             =   3720
      Width           =   495
   End
   Begin VB.TextBox txtReadADCnTimesMS 
      Height          =   375
      Left            =   1800
      TabIndex        =   26
      Text            =   "511"
      Top             =   3720
      Width           =   495
   End
   Begin VB.CommandButton cmdReadADCnTimesMS 
      Caption         =   "Read ADC"
      Height          =   375
      Left            =   120
      TabIndex        =   25
      Top             =   3720
      Width           =   1575
   End
   Begin VB.TextBox txtReadADCnTimesUSValue 
      Height          =   375
      Left            =   3240
      TabIndex        =   23
      Text            =   "1"
      Top             =   3240
      Width           =   495
   End
   Begin VB.TextBox txtReadADCnTimesUS 
      Height          =   375
      Left            =   1800
      TabIndex        =   21
      Text            =   "511"
      Top             =   3240
      Width           =   495
   End
   Begin VB.CommandButton cmdReadADCnTimesUS 
      Caption         =   "Read ADC"
      Height          =   375
      Left            =   120
      TabIndex        =   20
      Top             =   3240
      Width           =   1575
   End
   Begin VB.TextBox txtReadADCnTimes 
      Height          =   375
      Left            =   1800
      TabIndex        =   18
      Text            =   "511"
      Top             =   2760
      Width           =   495
   End
   Begin VB.CommandButton cmdReadADCnTimes 
      Caption         =   "Read ADC"
      Height          =   375
      Left            =   120
      TabIndex        =   17
      Top             =   2760
      Width           =   1575
   End
   Begin VB.CommandButton cmdReadADCByte 
      Caption         =   "Read ADC Byte"
      Height          =   375
      Left            =   120
      TabIndex        =   16
      Top             =   2280
      Width           =   1575
   End
   Begin VB.CommandButton cmdWriteRAMByte 
      Caption         =   "Write RAM Byte"
      Height          =   375
      Left            =   120
      TabIndex        =   15
      Top             =   1800
      Width           =   1575
   End
   Begin VB.CommandButton cmdReadRAMByte 
      Caption         =   "Read RAM Byte"
      Height          =   375
      Left            =   120
      TabIndex        =   14
      Top             =   1320
      Width           =   1575
   End
   Begin VB.TextBox txtByte 
      Height          =   285
      Index           =   7
      Left            =   6240
      TabIndex        =   10
      Text            =   "255"
      Top             =   480
      Width           =   495
   End
   Begin VB.TextBox txtByte 
      Height          =   285
      Index           =   6
      Left            =   5520
      TabIndex        =   9
      Text            =   "255"
      Top             =   480
      Width           =   495
   End
   Begin VB.TextBox txtByte 
      Height          =   285
      Index           =   5
      Left            =   4800
      TabIndex        =   8
      Text            =   "255"
      Top             =   480
      Width           =   495
   End
   Begin VB.TextBox txtByte 
      Height          =   285
      Index           =   4
      Left            =   4080
      TabIndex        =   7
      Text            =   "255"
      Top             =   480
      Width           =   495
   End
   Begin VB.TextBox txtByte 
      Height          =   285
      Index           =   3
      Left            =   3360
      TabIndex        =   6
      Text            =   "255"
      Top             =   480
      Width           =   495
   End
   Begin VB.TextBox txtByte 
      Height          =   285
      Index           =   2
      Left            =   2640
      TabIndex        =   5
      Text            =   "255"
      Top             =   480
      Width           =   495
   End
   Begin VB.TextBox txtByte 
      Height          =   285
      Index           =   1
      Left            =   1920
      TabIndex        =   4
      Text            =   "255"
      Top             =   480
      Width           =   495
   End
   Begin VB.TextBox txtByte 
      Height          =   285
      Index           =   0
      Left            =   1200
      TabIndex        =   3
      Text            =   "255"
      Top             =   480
      Width           =   495
   End
   Begin VB.CommandButton cmdOnce 
      Caption         =   "Read/Write"
      Height          =   372
      Left            =   6840
      TabIndex        =   2
      Top             =   360
      Width           =   1575
   End
   Begin VB.Timer timerCheckConnection 
      Interval        =   2000
      Left            =   3720
      Top             =   0
   End
   Begin VB.Timer tmrDelay 
      Enabled         =   0   'False
      Left            =   3240
      Top             =   0
   End
   Begin VB.Label Label12 
      Caption         =   "on Pin A"
      Height          =   255
      Left            =   1800
      TabIndex        =   58
      Top             =   2400
      Width           =   615
   End
   Begin VB.Line Line7 
      X1              =   6840
      X2              =   6600
      Y1              =   3840
      Y2              =   3840
   End
   Begin VB.Line Line6 
      X1              =   6840
      X2              =   6600
      Y1              =   3360
      Y2              =   3360
   End
   Begin VB.Line Line5 
      X1              =   6840
      X2              =   4080
      Y1              =   2880
      Y2              =   2880
   End
   Begin VB.Line Line4 
      X1              =   3240
      X2              =   6840
      Y1              =   7200
      Y2              =   7200
   End
   Begin VB.Line Line3 
      X1              =   1800
      X2              =   6840
      Y1              =   6240
      Y2              =   6240
   End
   Begin VB.Line Line2 
      X1              =   3120
      X2              =   6840
      Y1              =   2400
      Y2              =   2400
   End
   Begin VB.Line Line1 
      X1              =   2400
      X2              =   6840
      Y1              =   1440
      Y2              =   1440
   End
   Begin VB.Label Label11 
      Caption         =   "to"
      Height          =   255
      Left            =   2400
      TabIndex        =   49
      Top             =   1920
      Width           =   255
   End
   Begin VB.Label Label10 
      Caption         =   "on Port B with"
      Height          =   255
      Left            =   2400
      TabIndex        =   42
      Top             =   7680
      Width           =   1095
   End
   Begin VB.Label Label9 
      Caption         =   "on Port B"
      Height          =   255
      Left            =   2400
      TabIndex        =   39
      Top             =   7200
      Width           =   735
   End
   Begin VB.Label Label8 
      Caption         =   "ms between readings on Pin A"
      Height          =   255
      Left            =   3840
      TabIndex        =   29
      Top             =   3840
      Width           =   2175
   End
   Begin VB.Label Label7 
      Caption         =   "times with"
      Height          =   255
      Left            =   2400
      TabIndex        =   27
      Top             =   3840
      Width           =   735
   End
   Begin VB.Label Label6 
      Caption         =   "us between readings on Pin A"
      Height          =   255
      Left            =   3840
      TabIndex        =   24
      Top             =   3360
      Width           =   2175
   End
   Begin VB.Label Label5 
      Caption         =   "times with"
      Height          =   255
      Left            =   2400
      TabIndex        =   22
      Top             =   3360
      Width           =   735
   End
   Begin VB.Label Label4 
      Caption         =   "times on Pin A"
      Height          =   255
      Left            =   2400
      TabIndex        =   19
      Top             =   2880
      Width           =   1095
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Caption         =   "Send Data:"
      Height          =   255
      Left            =   0
      TabIndex        =   13
      Top             =   480
      Width           =   1095
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Caption         =   "Read Data:"
      Height          =   255
      Left            =   0
      TabIndex        =   12
      Top             =   840
      Width           =   1095
   End
   Begin VB.Label lblReadData 
      Height          =   255
      Left            =   1200
      TabIndex        =   11
      Top             =   840
      Width           =   4575
   End
   Begin VB.Label lblConnect 
      Height          =   255
      Left            =   1200
      TabIndex        =   1
      Top             =   120
      Width           =   1695
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "USB Device:"
      Height          =   255
      Left            =   0
      TabIndex        =   0
      Top             =   120
      Width           =   1095
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Declare Sub Sleep Lib "kernel32.dll" (ByVal dwMilliseconds As Long)

Dim outputArray() As Long
Dim dataArray As Variant

Dim crcOK As Boolean
Dim useCRC As Boolean
Dim sampleSize As Integer

Option Explicit

Private Sub cmdClearRAM_Click()
    resetFields
    
    txtByte(0).Text = "8"
    cmdOnce_Click
End Sub

Private Sub cmdClearUseCRC_Click()
    resetFields
    
    txtByte(0).Text = "11"
    useCRC = False
    cmdOnce_Click
End Sub

Private Sub cmdDemoWritePortB_Click()
    txtWritePort.Text = "1"
    cmdWritePort_Click
    Sleep (100)
    txtWritePort.Text = "2"
    cmdWritePort_Click
    Sleep (100)
    txtWritePort.Text = "4"
    cmdWritePort_Click
    Sleep (100)
    txtWritePort.Text = "8"
    cmdWritePort_Click
    Sleep (100)
    txtWritePort.Text = "16"
    cmdWritePort_Click
    Sleep (100)
    txtWritePort.Text = "32"
    cmdWritePort_Click
    Sleep (100)
    txtWritePort.Text = "64"
    cmdWritePort_Click
    Sleep (100)
    txtWritePort.Text = "128"
    cmdWritePort_Click
    Sleep (100)
    txtWritePort.Text = "64"
    cmdWritePort_Click
    Sleep (100)
    txtWritePort.Text = "32"
    cmdWritePort_Click
    Sleep (100)
    txtWritePort.Text = "16"
    cmdWritePort_Click
    Sleep (100)
    txtWritePort.Text = "8"
    cmdWritePort_Click
    Sleep (100)
    txtWritePort.Text = "4"
    cmdWritePort_Click
    Sleep (100)
    txtWritePort.Text = "2"
    cmdWritePort_Click
    Sleep (100)
    txtWritePort.Text = "1"
    cmdWritePort_Click
    Sleep (100)
    txtWritePort.Text = "3"
    cmdWritePort_Click
    Sleep (100)
    txtWritePort.Text = "7"
    cmdWritePort_Click
    Sleep (100)
    txtWritePort.Text = "15"
    cmdWritePort_Click
    Sleep (100)
    txtWritePort.Text = "31"
    cmdWritePort_Click
    Sleep (100)
    txtWritePort.Text = "63"
    cmdWritePort_Click
    Sleep (100)
    txtWritePort.Text = "127"
    cmdWritePort_Click
    Sleep (100)
    txtWritePort.Text = "255"
    cmdWritePort_Click
    Sleep (100)
    txtWritePort.Text = "254"
    cmdWritePort_Click
    Sleep (100)
    txtWritePort.Text = "252"
    cmdWritePort_Click
    Sleep (100)
    txtWritePort.Text = "248"
    cmdWritePort_Click
    Sleep (100)
    txtWritePort.Text = "240"
    cmdWritePort_Click
    Sleep (100)
    txtWritePort.Text = "224"
    cmdWritePort_Click
    Sleep (100)
    txtWritePort.Text = "192"
    cmdWritePort_Click
    Sleep (100)
    txtWritePort.Text = "128"
    cmdWritePort_Click
    Sleep (100)
    txtWritePort.Text = "0"
    cmdWritePort_Click
    Sleep (100)
End Sub

Private Sub cmdReadADCByte_Click()
    txtByte(0).Text = "4"
    txtByte(1).Text = txtReadADCBytePin.Text
    cmdOnce_Click
    txtReadADCByteValue.Text = Val(ReadBuffer(3))
End Sub

Private Sub cmdReadADCnTimes_Click()
    resetFields

    txtByte(0).Text = "5"
    txtByte(1).Text = Str(Int(Int(txtReadADCnTimes.Text) / 256))
    txtByte(2).Text = Str(Int(txtReadADCnTimes.Text) - Int(Int(txtReadADCnTimes.Text) / 256) * 256)
    txtByte(3).Text = txtReadADCnTimesPin.Text
    cmdOnce_Click
End Sub

Private Sub cmdReadADCnTimesMS_Click()
    Dim theText As String
    Dim i As Integer
    
    resetFields

    txtByte(0).Text = "7"
    txtByte(1).Text = Str(Int(Int(txtReadADCnTimesMS.Text) / 256))
    txtByte(2).Text = Str(Int(txtReadADCnTimesMS.Text) - Int(Int(txtReadADCnTimesMS.Text) / 256) * 256)
    txtByte(3).Text = Str(Int(Int(txtReadADCnTimesMSValue.Text) / 256))
    txtByte(4).Text = Str(Int(txtReadADCnTimesMSValue.Text) - Int(Int(txtReadADCnTimesMSValue.Text) / 256) * 256)
    txtByte(5).Text = txtReadADCnTimesMSPin.Text
    cmdOnce_Click
End Sub

Private Sub cmdReadADCnTimesUS_Click()
    Dim theText As String
    Dim i As Integer
    
    resetFields

    txtByte(0).Text = "12"
    txtByte(1).Text = Str(Int(Int(txtReadADCnTimesUS.Text) / 256))
    txtByte(2).Text = Str(Int(txtReadADCnTimesUS.Text) - Int(Int(txtReadADCnTimesUS.Text) / 256) * 256)
    txtByte(3).Text = Str(Int(Int(txtReadADCnTimesUSValue.Text) / 256))
    txtByte(4).Text = Str(Int(txtReadADCnTimesUSValue.Text) - Int(Int(txtReadADCnTimesUSValue.Text) / 256) * 256)
    txtByte(5).Text = txtReadADCnTimesUSPin.Text
    cmdOnce_Click
End Sub

Private Sub cmdReadPin_Click()
    resetFields
    
    txtByte(0).Text = "15"
    txtByte(1).Text = txtReadPin.Text
    cmdOnce_Click
    txtReadPinValue.Text = ReadBuffer(4)
End Sub

Private Sub cmdReadPort_Click()
    resetFields
    
    txtByte(0).Text = "13"
    cmdOnce_Click
    txtReadPort.Text = ReadBuffer(3)
End Sub

Private Sub cmdReadRAMByte_Click()
    txtByte(0).Text = "2"
    txtByte(1).Text = Str(Int(Int(txtReadRAMByte.Text) / 256))
    txtByte(2).Text = Str(Int(txtReadRAMByte.Text) - Int(Int(txtReadRAMByte.Text) / 256) * 256)
    cmdOnce_Click
    txtReadRAMByteValue.Text = Val(ReadBuffer(5))
End Sub

Private Sub cmdReadRAMData_Click()
    Dim theText As String
    Dim i As Integer
    
    resetFields

    For i = 0 To sampleSize
        DoEvents
        txtByte(0).Text = "2"
        txtByte(1).Text = Str(Int(i / 256))
        txtByte(2).Text = Str(i - Int(i / 256) * 256)
        cmdOnce_Click
        dataArray(i) = Val(ReadBuffer(5))
    Next

    theText = "("
    For i = 1 To UBound(dataArray)
        theText = theText & Str(dataArray(i - 1)) & ","
    Next
    txtInput.Text = Mid(theText, 1, Len(theText) - 1) & ")"
End Sub

Private Sub cmdSetRAMByte_Click()
    resetFields
    
    txtByte(0).Text = "9"
    txtByte(1).Text = txtSetRAMByte
    cmdOnce_Click
End Sub

Private Sub cmdSetUseCRC_Click()
    resetFields
    
    txtByte(0).Text = "10"
    useCRC = True
    cmdOnce_Click
End Sub

Private Sub cmdWritePin_Click()
    resetFields
    
    txtByte(0).Text = "16"
    txtByte(1).Text = txtWritePin.Text
    txtByte(2).Text = txtWritePinValue.Text
    cmdOnce_Click
End Sub

Private Sub cmdWritePort_Click()
    resetFields
    
    txtByte(0).Text = "14"
    txtByte(1).Text = txtWritePort.Text
    cmdOnce_Click
End Sub

Private Sub cmdWriteRAMByte_Click()
    txtByte(0).Text = "3"
    txtByte(1).Text = Str(Int(Int(txtWriteRAMByte.Text) / 256))
    txtByte(2).Text = Str(Int(txtWriteRAMByte.Text) - Int(Int(txtWriteRAMByte.Text) / 256) * 256)
    txtByte(3).Text = txtWriteRAMByteValue.Text
    cmdOnce_Click
End Sub

Private Sub Form_Load()
    frmMain.Show
    tmrDelay.Enabled = False
    
    sampleSize = 511
    useCRC = True
    
    ReDim dataArray(sampleSize) As Integer
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim Result As Long
    
    Result = CloseHandle(HIDHandle)
    Result = CloseHandle(ReadHandle)
End Sub


Private Sub cmdOnce_Click()
    Dim x As Integer
    Dim theCRC As Byte
    
    If Not MyDeviceDetected Then
        MyDeviceDetected = FindTheHid
    End If
    If MyDeviceDetected Then
        OutputReportData(0) = Val(txtByte(0).Text)
        OutputReportData(1) = Val(txtByte(1).Text)
        OutputReportData(2) = Val(txtByte(2).Text)
        OutputReportData(3) = Val(txtByte(3).Text)
        OutputReportData(4) = Val(txtByte(4).Text)
        OutputReportData(5) = Val(txtByte(5).Text)
        OutputReportData(6) = Val(txtByte(6).Text)
        OutputReportData(7) = Val(txtByte(7).Text)
    
        Call ReadAndWriteToDevice
        lblReadData = Str$(ReadBuffer(1)) & "," & Str$(ReadBuffer(2)) & "," & Str$(ReadBuffer(3)) & "," & Str$(ReadBuffer(4)) & "," & Str$(ReadBuffer(5)) & "," & Str$(ReadBuffer(6)) & "," & Str$(ReadBuffer(7)) & "," & Str$(ReadBuffer(8))
        If useCRC Then
            theCRC = calc_CRC(0, ReadBuffer(1))
            theCRC = calc_CRC(theCRC, ReadBuffer(2))
            theCRC = calc_CRC(theCRC, ReadBuffer(3))
            theCRC = calc_CRC(theCRC, ReadBuffer(4))
            theCRC = calc_CRC(theCRC, ReadBuffer(5))
            theCRC = calc_CRC(theCRC, ReadBuffer(6))
            theCRC = calc_CRC(theCRC, ReadBuffer(7))
            If theCRC = ReadBuffer(8) Then
                lblReadData = lblReadData & " CRC OK"
                crcOK = True
            Else
                lblReadData = lblReadData & " CRC BAD"
                crcOK = False
            End If
        End If
    End If
End Sub

Public Sub resetFields()
    Dim i As Integer
    For i = 0 To 7
        txtByte(i).Text = "255"
    Next
End Sub

Private Sub timerCheckConnection_Timer()
    If FindTheHid Then
        lblConnect.Caption = "Connected"
        lblConnect.ForeColor = RGB(0, 150, 0)
    Else
        lblConnect.Caption = "Disconnected"
        lblConnect.ForeColor = RGB(150, 0, 0)
    End If
End Sub

Private Sub tmrDelay_Timer()
    Timeout = True
    tmrDelay.Enabled = False
End Sub

Private Sub txtReadADCBytePin_Change()
    If Val(txtReadADCBytePin.Text) < 0 Then
        txtReadADCBytePin.Text = 0
    ElseIf Val(txtReadADCBytePin.Text) > 4 Then
        txtReadADCBytePin.Text = 4
    End If
End Sub

Private Sub txtReadADCnTimes_Change()
    If Val(txtReadADCnTimes.Text) < 0 Then
        txtReadADCnTimes.Text = 0
    ElseIf Val(txtReadADCnTimes.Text) > sampleSize Then
        txtReadADCnTimes.Text = sampleSize
    End If
End Sub

Private Sub txtReadADCnTimesMS_Change()
    If Val(txtReadADCnTimesMS.Text) < 0 Then
        txtReadADCnTimesMS.Text = 0
    ElseIf Val(txtReadADCnTimesMS.Text) > sampleSize Then
        txtReadADCnTimesMS.Text = sampleSize
    End If
End Sub

Private Sub txtReadADCnTimesMSPin_Change()
    If Val(txtReadADCnTimesMSPin.Text) < 0 Then
        txtReadADCnTimesMSPin.Text = 0
    ElseIf Val(txtReadADCnTimesMSPin.Text) > 4 Then
        txtReadADCnTimesMSPin.Text = 4
    End If
End Sub

Private Sub txtReadADCnTimesPin_Change()
    If Val(txtReadADCnTimesPin.Text) < 0 Then
        txtReadADCnTimesPin.Text = 0
    ElseIf Val(txtReadADCnTimesPin.Text) > 4 Then
        txtReadADCnTimesPin.Text = 4
    End If
End Sub

Private Sub txtReadADCnTimesUS_Change()
    If Val(txtReadADCnTimesUS.Text) < 0 Then
        txtReadADCnTimesUS.Text = 0
    ElseIf Val(txtReadADCnTimesUS.Text) > sampleSize Then
        txtReadADCnTimesUS.Text = sampleSize
    End If
End Sub

Private Sub txtReadADCnTimesUSPin_Change()
    If Val(txtReadADCnTimesUSPin.Text) < 0 Then
        txtReadADCnTimesUSPin.Text = 0
    ElseIf Val(txtReadADCnTimesUSPin.Text) > 4 Then
        txtReadADCnTimesUSPin.Text = 4
    End If
End Sub

Private Sub txtReadPin_Change()
    If Val(txtReadPin.Text) < 0 Then
        txtReadPin.Text = 0
    ElseIf Val(txtReadPin.Text) > 7 Then
        txtReadPin.Text = 7
    End If
End Sub

Private Sub txtReadRAMByte_Change()
    If Val(txtReadRAMByte.Text) < 0 Then
        txtReadRAMByte.Text = 0
    ElseIf Val(txtReadRAMByte.Text) > sampleSize Then
        txtReadRAMByte.Text = sampleSize
    End If
End Sub

Private Sub txtSetRAMByte_Change()
    If Val(txtSetRAMByte.Text) < 0 Then
        txtSetRAMByte.Text = 0
    ElseIf Val(txtSetRAMByte.Text) > 255 Then
        txtSetRAMByte.Text = 255
    End If
End Sub

Private Sub txtWritePin_Change()
    If Val(txtWritePin.Text) < 0 Then
        txtWritePin.Text = 0
    ElseIf Val(txtWritePin.Text) > 7 Then
        txtWritePin.Text = 7
    End If
End Sub

Private Sub txtWritePinValue_Change()
    If Val(txtWritePinValue.Text) < 0 Then
        txtWritePinValue.Text = 0
    ElseIf Val(txtWritePinValue.Text) > 1 Then
        txtWritePinValue.Text = 1
    End If
End Sub

Private Sub txtWritePort_Change()
    If Val(txtWritePort.Text) < 0 Then
        txtWritePort.Text = 0
    ElseIf Val(txtWritePort.Text) > 255 Then
        txtWritePort.Text = 255
    End If
End Sub

Private Sub txtWriteRAMByte_Change()
    If Val(txtWriteRAMByte.Text) < 0 Then
        txtWriteRAMByte.Text = 0
    ElseIf Val(txtWriteRAMByte.Text) > sampleSize Then
        txtWriteRAMByte.Text = sampleSize
    End If
End Sub

Private Sub txtWriteRAMByteValue_Change()
    If Val(txtWriteRAMByteValue.Text) < 0 Then
        txtWriteRAMByteValue.Text = 0
    ElseIf Val(txtWriteRAMByteValue.Text) > 255 Then
        txtWriteRAMByteValue.Text = 255
    End If
End Sub
