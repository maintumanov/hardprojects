////////////////////////////////////////////////////////////////////////////////
//                 PIC18F2550 USB HID IO
//
// Filename     : 18F2550 USB HID CRC IO.c
// Programmer   : Steven Cholewiak, www.semifluid.com
// Version      : Version 1.0 - 03/28/2006
// Remarks      : More information on the circuit can be found at:
//                http://www.semifluid.com/PIC18F2550_usb_hid_io.html
////////////////////////////////////////////////////////////////////////////////

#define __USB_PIC_PERIF__ 1

#include <18F2550.h>
#device ADC=8
#fuses HSPLL,NOWDT,NOPROTECT,NOLVP,NODEBUG,USBDIV,PLL5,CPUDIV1,VREGEN
#use delay(clock=48000000)

#use rs232(stream=PC, baud=115200, xmit=PIN_C6, rcv=PIN_C7, ERRORS)

// CCS Library dynamic defines
#DEFINE USB_HID_DEVICE  TRUE //Tells the CCS PIC USB firmware to include HID handling code.
#define USB_EP1_TX_ENABLE  USB_ENABLE_INTERRUPT   //turn on EP1 for IN bulk/interrupt transfers
#define USB_EP1_TX_SIZE    64  //allocate 64 bytes in the hardware for transmission
#define USB_EP1_RX_ENABLE  USB_ENABLE_INTERRUPT   //turn on EP1 for OUT bulk/interrupt transfers
#define USB_EP1_RX_SIZE    64  //allocate 64 bytes in the hardware for reception

// CCS USB Libraries
#include <pic18_usb.h>   //Microchip 18Fxx5x hardware layer for usb.c
#include <usb_desc_hid 8-byte.h>	//USB Configuration and Device descriptors for this UBS device
#include <usb.c>        //handles usb setup tokens and get descriptor reports

void usb_debug_task(void) {
   static int8 last_connected;
   static int8 last_enumerated;
   int8 new_connected;
   int8 new_enumerated;

   new_connected=usb_attached();
   new_enumerated=usb_enumerated();

   if (new_connected && !last_connected) {
      printf("\r\n\nUSB connected, waiting for enumaration...");}
   if (!new_connected && last_connected) {
      printf("\r\n\nUSB disconnected, waiting for connection...");}
   if (new_enumerated && !last_enumerated) {
      printf("\r\n\nUSB enumerated by PC/HOST");}
   if (!new_enumerated && last_enumerated) {
      printf("\r\n\nUSB unenumerated by PC/HOST, waiting for enumeration...");}

   last_connected=new_connected;
   last_enumerated=new_enumerated;
}

#INT_RDA
void serial_isr()                         // Serial Interrupt
{
   int8 uReceive;

   disable_interrupts(GLOBAL);            // Disable Global Interrupts

   uReceive = fgetc(PC);

   switch (uReceive) {
      case 0x12: {
            if (fgetc(PC) == 0x34 & fgetc(PC) == 0x56 & fgetc(PC) == 0x78 & fgetc(PC) == 0x90) #asm reset #endasm
         }
         break;
   }

   enable_interrupts(GLOBAL);                // Enable Global Interrupts
}

int calc_crc(int oldcrc, int newbyte) {
   // Please see: http://pdfserv.maxim-ic.com/arpdf/AppNotes/app27.pdf

   int shift_reg, data_bit, sr_lsb, fb_bit, j;
   shift_reg=oldcrc;
   for(j=0; j<8; j++) {   // for each bit
      data_bit = (newbyte >> j) & 0x01;
      sr_lsb = shift_reg & 0x01;
      fb_bit = (data_bit ^ sr_lsb) & 0x01;
      shift_reg = shift_reg >> 1;
      if (fb_bit)
         shift_reg = shift_reg ^ 0x8c;
      }
   return(shift_reg);
}

#define theSampleSize            512

#define usbConfirmAction         0
#define usbCheckStatus           1
#define usbReadRam               2
#define usbWriteRam              3
#define usbReadADC               4
#define usbReadADCnTimes         5
#define usbReadADCPeriod         6
#define usbReadADCnTimesMS       7
#define usbClearRam              8
#define usbSetRamByte            9
#define usbSetUseCRC             10
#define usbClearUseCRC           11
#define usbReadADCnTimesUS       12
#define usbReadPort              13
#define usbWritePort             14
#define usbReadPin               15
#define usbWritePin              16
#define usbError                 66

void main() {
   int1 useCRC;
   int8 in_data[8];
   int8 out_data[8];
   int8 adcData[theSampleSize];
   int8 theCRC, tempADC, currentADCpin;
   int16 n, approxUS, approxMS, period;

   SETUP_ADC_PORTS(AN0_TO_AN4);
   SETUP_ADC(ADC_CLOCK_DIV_64);
   SETUP_TIMER_0(RTCC_INTERNAL|RTCC_DIV_1);
   SETUP_TIMER_1(T1_DISABLED);
   SETUP_TIMER_2(T2_DISABLED, 127, 1);
   SETUP_TIMER_3(T3_INTERNAL | T3_DIV_BY_8);
   SETUP_CCP1(CCP_OFF);
   SETUP_CCP2(CCP_OFF);
   enable_interrupts(INT_RDA);
   enable_interrupts(GLOBAL);

   usb_init();
   useCRC = true;
   currentADCpin = 0;
   set_adc_channel(0);
   delay_ms(1);

   while (TRUE) {
      usb_task();
      usb_debug_task();
      if (usb_enumerated()) {
         if (usb_kbhit(1)) {
            usb_get_packet(1, in_data, 8);

            if (useCRC) {
               theCRC = 0;
               theCRC = calc_crc(theCRC,in_data[0]);
               theCRC = calc_crc(theCRC,in_data[1]);
               theCRC = calc_crc(theCRC,in_data[2]);
               theCRC = calc_crc(theCRC,in_data[3]);
               theCRC = calc_crc(theCRC,in_data[4]);
               theCRC = calc_crc(theCRC,in_data[5]);
               theCRC = calc_crc(theCRC,in_data[6]);
            }
            else {
               theCRC = in_data[7];
            }

            if (theCRC = in_data[7]) {
               out_data[0] = 255;
               out_data[1] = 255;
               out_data[2] = 255;
               out_data[3] = 255;
               out_data[4] = 255;
               out_data[5] = 255;
               out_data[6] = 255;

               switch (in_data[0]) {
                  case usbReadRam: {
                        if (make16(in_data[1],in_data[2]) <= theSampleSize) {
                           out_data[0] = usbConfirmAction;
                           out_data[1] = usbReadRam;
                           out_data[2] = in_data[1];
                           out_data[3] = in_data[2];
                           out_data[4] = adcData[make16(in_data[1],in_data[2])];
                        }
                        else {
                           out_data[0] = usbError;
                           out_data[1] = usbReadRam;
                           out_data[2] = in_data[1];
                           out_data[3] = in_data[2];
                        }
                     }
                     break;
                  case usbWriteRam: {
                        if (make16(in_data[1],in_data[2]) <= theSampleSize) {
                           adcData[make16(in_data[1],in_data[2])] = in_data[3];
                           out_data[0] = usbConfirmAction;
                           out_data[1] = usbWriteRam;
                           out_data[2] = in_data[1];
                           out_data[3] = in_data[2];
                           out_data[4] = in_data[3];
                        }
                        else {
                           out_data[0] = usbError;
                           out_data[1] = usbWriteRam;
                           out_data[2] = in_data[1];
                           out_data[3] = in_data[2];
                           out_data[4] = in_data[3];
                        }
                     }
                     break;
                  case usbReadADC: {
                        if (in_data[1] != 255 & in_data[1] != currentADCpin) {
                           currentADCpin = in_data[1];
                           set_adc_channel(currentADCpin);
                           delay_ms(1);
                        }
                        tempADC = READ_ADC();
                        out_data[0] = usbConfirmAction;
                        out_data[1] = usbReadADC;
                        out_data[2] = tempADC;
                        out_data[3] = in_data[1];
                     }
                     break;
                  case usbReadADCnTimes: {
                        if (make16(in_data[1],in_data[2]) <= theSampleSize) {
                           if (in_data[3] != 255 & in_data[3] != currentADCpin) {
                              currentADCpin = in_data[3];
                              set_adc_channel(currentADCpin);
                              delay_ms(1);
                           }
                           set_timer3(0);
                           for (n=0;n<make16(in_data[1],in_data[2]);n++)
                           {
                              adcData[n] = READ_ADC();
                           }
                           period = get_timer3();   // 1000/((clock/4)/8) for ms
                           out_data[0] = usbConfirmAction;
                           out_data[1] = usbReadADCnTimes;
                           out_data[2] = in_data[1];
                           out_data[3] = in_data[2];
                           out_data[4] = in_data[3];
                        }
                        else {
                           out_data[0] = usbError;
                           out_data[1] = usbReadADCnTimes;
                           out_data[2] = in_data[1];
                           out_data[3] = in_data[2];
                           out_data[4] = in_data[3];
                        }
                     }
                     break;
                  case usbReadADCPeriod: {
                        out_data[0] = usbConfirmAction;
                        out_data[1] = usbReadADCPeriod;
                        out_data[2] = make8(period,1);
                        out_data[3] = make8(period,0);
                     }
                     break;
                  case usbReadADCnTimesUS: {
                        if (make16(in_data[1],in_data[2]) <= theSampleSize) {
                           if (in_data[5] != 255 & in_data[5] != currentADCpin) {
                              currentADCpin = in_data[5];
                              set_adc_channel(currentADCpin);
                              delay_ms(1);
                           }
                           approxUS = make16(in_data[3],in_data[4]);
                           for (n=0;n<make16(in_data[1],in_data[2]);n++)
                           {
                              set_timer3(0);
                              adcData[n] = READ_ADC();
                              while (get_timer3() * 2/3 < approxUS);   // 1000000/((clock/4)/8)
                           }
                           out_data[0] = usbConfirmAction;
                           out_data[1] = usbReadADCnTimesUS;
                           out_data[2] = in_data[1];
                           out_data[3] = in_data[2];
                           out_data[4] = in_data[3];
                           out_data[5] = in_data[4];
                           out_data[6] = in_data[5];
                        }
                        else {
                           out_data[0] = usbError;
                           out_data[1] = usbReadADCnTimesUS;
                           out_data[2] = in_data[1];
                           out_data[3] = in_data[2];
                           out_data[4] = in_data[3];
                           out_data[5] = in_data[4];
                           out_data[6] = in_data[5];
                        }
                     }
                     break;
                  case usbReadADCnTimesMS: {
                        if (make16(in_data[1],in_data[2]) <= theSampleSize) {
                           if (in_data[5] != 255 & in_data[5] != currentADCpin) {
                              currentADCpin = in_data[5];
                              set_adc_channel(currentADCpin);
                              delay_ms(1);
                           }
                           approxMS = make16(in_data[3],in_data[4]);
                           for (n=0;n<make16(in_data[1],in_data[2]);n++)
                           {
                              set_timer3(0);
                              adcData[n] = READ_ADC();
                              while (get_timer3() * 1/1500 < approxMS);   // 1000/((clock/4)/8)
                           }
                           out_data[0] = usbConfirmAction;
                           out_data[1] = usbReadADCnTimesMS;
                           out_data[2] = in_data[1];
                           out_data[3] = in_data[2];
                           out_data[4] = in_data[3];
                           out_data[5] = in_data[4];
                           out_data[6] = in_data[5];
                        }
                        else {
                           out_data[0] = usbError;
                           out_data[1] = usbReadADCnTimesMS;
                           out_data[2] = in_data[1];
                           out_data[3] = in_data[2];
                           out_data[4] = in_data[3];
                           out_data[5] = in_data[4];
                           out_data[6] = in_data[5];
                        }
                     }
                     break;
                  case usbClearRam: {
                        for (n=0;n<512;n++)
                        {
                           adcData[n] = 0;
                        }
                        out_data[0] = usbConfirmAction;
                        out_data[1] = usbClearRam;
                     }
                     break;
                  case usbSetRamByte: {
                        for (n=0;n<512;n++)
                        {
                           adcData[n] = in_data[1];
                        }
                        out_data[0] = usbConfirmAction;
                        out_data[1] = usbSetRamByte;
                        out_data[2] = in_data[1];
                     }
                     break;
                  case usbSetUseCRC: {
                        useCRC = true;
                        out_data[0] = usbConfirmAction;
                        out_data[1] = usbSetUseCRC;
                     }
                     break;
                  case usbClearUseCRC: {
                        useCRC = false;
                        out_data[0] = usbConfirmAction;
                        out_data[1] = usbClearUseCRC;
                     }
                     break;
                  case usbReadPort: {
                        out_data[0] = usbConfirmAction;
                        out_data[1] = usbReadPort;
                        out_data[2] = input_b();
                     }
                     break;
                  case usbWritePort: {
                        output_b(in_data[1]);
                        out_data[0] = usbConfirmAction;
                        out_data[1] = usbWritePort;
                        out_data[2] = in_data[1];
                     }
                     break;
                  case usbReadPin: {
                        out_data[0] = usbConfirmAction;
                        out_data[1] = usbReadPin;
                        out_data[2] = in_data[1];
                        switch (in_data[1]) {
                           case 0: out_data[3] = input(PIN_B0); break;
                           case 1: out_data[3] = input(PIN_B1); break;
                           case 2: out_data[3] = input(PIN_B2); break;
                           case 3: out_data[3] = input(PIN_B3); break;
                           case 4: out_data[3] = input(PIN_B4); break;
                           case 5: out_data[3] = input(PIN_B5); break;
                           case 6: out_data[3] = input(PIN_B6); break;
                           case 7: out_data[3] = input(PIN_B7); break;
                        }
                     }
                     break;
                  case usbWritePin: {
                        switch (in_data[1]) {
                           case 0: output_bit(PIN_B0, in_data[2] & 1); break;
                           case 1: output_bit(PIN_B1, in_data[2] & 1); break;
                           case 2: output_bit(PIN_B2, in_data[2] & 1); break;
                           case 3: output_bit(PIN_B3, in_data[2] & 1); break;
                           case 4: output_bit(PIN_B4, in_data[2] & 1); break;
                           case 5: output_bit(PIN_B5, in_data[2] & 1); break;
                           case 6: output_bit(PIN_B6, in_data[2] & 1); break;
                           case 7: output_bit(PIN_B7, in_data[2] & 1); break;
                        }
                        out_data[0] = usbConfirmAction;
                        out_data[1] = usbWritePin;
                        out_data[2] = in_data[1];
                        out_data[3] = in_data[2];
                     }
                     break;
               }
               if (useCRC) {
                  theCRC = 0;
                  theCRC = calc_crc(theCRC,out_data[0]);
                  theCRC = calc_crc(theCRC,out_data[1]);
                  theCRC = calc_crc(theCRC,out_data[2]);
                  theCRC = calc_crc(theCRC,out_data[3]);
                  theCRC = calc_crc(theCRC,out_data[4]);
                  theCRC = calc_crc(theCRC,out_data[5]);
                  theCRC = calc_crc(theCRC,out_data[6]);
                  out_data[7] = theCRC;
               }

               usb_put_packet(1, out_data, 8, USB_DTS_TOGGLE);
            }

            delay_ms(1);
         }
      }
   }
}
