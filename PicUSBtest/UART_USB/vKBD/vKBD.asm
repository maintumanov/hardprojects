
_interrupt:

;vKBD.c,6 :: 		void interrupt(void)
;vKBD.c,8 :: 		USB_Interrupt_Proc( );
	CALL        _USB_Interrupt_Proc+0, 0
;vKBD.c,9 :: 		}
L_end_interrupt:
L__interrupt10:
	RETFIE      1
; end of _interrupt

_clrUSB:

;vKBD.c,10 :: 		void clrUSB(void)
;vKBD.c,12 :: 		memset(USBCommand, 0, sizeof USBCommand);
	MOVLW       _USBCommand+0
	MOVWF       FARG_memset_p1+0 
	MOVLW       hi_addr(_USBCommand+0)
	MOVWF       FARG_memset_p1+1 
	CLRF        FARG_memset_character+0 
	MOVLW       8
	MOVWF       FARG_memset_n+0 
	MOVLW       0
	MOVWF       FARG_memset_n+1 
	CALL        _memset+0, 0
;vKBD.c,13 :: 		while ( !HID_Write(&USBCommand, sizeof USBCommand));
L_clrUSB0:
	MOVLW       _USBCommand+0
	MOVWF       FARG_HID_Write_writebuff+0 
	MOVLW       hi_addr(_USBCommand+0)
	MOVWF       FARG_HID_Write_writebuff+1 
	MOVLW       8
	MOVWF       FARG_HID_Write_len+0 
	CALL        _HID_Write+0, 0
	MOVF        R0, 1 
	BTFSS       STATUS+0, 2 
	GOTO        L_clrUSB1
	GOTO        L_clrUSB0
L_clrUSB1:
;vKBD.c,14 :: 		}
L_end_clrUSB:
	RETURN      0
; end of _clrUSB

_main:

;vKBD.c,16 :: 		main(void)
;vKBD.c,18 :: 		ADCON1 |= 0x0F;
	MOVLW       15
	IORWF       ADCON1+0, 1 
;vKBD.c,19 :: 		CMCON |= 7;
	MOVLW       7
	IORWF       CMCON+0, 1 
;vKBD.c,20 :: 		HID_Enable(&USBResponse, &USBCommand);
	MOVLW       _USBResponse+0
	MOVWF       FARG_HID_Enable_readbuff+0 
	MOVLW       hi_addr(_USBResponse+0)
	MOVWF       FARG_HID_Enable_readbuff+1 
	MOVLW       _USBCommand+0
	MOVWF       FARG_HID_Enable_writebuff+0 
	MOVLW       hi_addr(_USBCommand+0)
	MOVWF       FARG_HID_Enable_writebuff+1 
	CALL        _HID_Enable+0, 0
;vKBD.c,21 :: 		delay_ms(2000);
	MOVLW       244
	MOVWF       R11, 0
	MOVLW       130
	MOVWF       R12, 0
	MOVLW       5
	MOVWF       R13, 0
L_main2:
	DECFSZ      R13, 1, 1
	BRA         L_main2
	DECFSZ      R12, 1, 1
	BRA         L_main2
	DECFSZ      R11, 1, 1
	BRA         L_main2
;vKBD.c,22 :: 		for (i=0; i < strlen(text); i++)
	CLRF        _i+0 
	CLRF        _i+1 
L_main3:
	MOVF        _text+0, 0 
	MOVWF       FARG_strlen_s+0 
	MOVF        _text+1, 0 
	MOVWF       FARG_strlen_s+1 
	CALL        _strlen+0, 0
	MOVLW       128
	XORWF       _i+1, 0 
	MOVWF       R2 
	MOVLW       128
	XORWF       R1, 0 
	SUBWF       R2, 0 
	BTFSS       STATUS+0, 2 
	GOTO        L__main13
	MOVF        R0, 0 
	SUBWF       _i+0, 0 
L__main13:
	BTFSC       STATUS+0, 0 
	GOTO        L_main4
;vKBD.c,24 :: 		USBCommand[2] = text[i]-93;
	MOVF        _i+0, 0 
	ADDWF       _text+0, 0 
	MOVWF       FSR0 
	MOVF        _i+1, 0 
	ADDWFC      _text+1, 0 
	MOVWF       FSR0H 
	MOVLW       93
	SUBWF       POSTINC0+0, 0 
	MOVWF       1290 
;vKBD.c,25 :: 		while ( !HID_Write(&USBCommand, sizeof USBCommand));
L_main6:
	MOVLW       _USBCommand+0
	MOVWF       FARG_HID_Write_writebuff+0 
	MOVLW       hi_addr(_USBCommand+0)
	MOVWF       FARG_HID_Write_writebuff+1 
	MOVLW       8
	MOVWF       FARG_HID_Write_len+0 
	CALL        _HID_Write+0, 0
	MOVF        R0, 1 
	BTFSS       STATUS+0, 2 
	GOTO        L_main7
	GOTO        L_main6
L_main7:
;vKBD.c,26 :: 		clrUSB();
	CALL        _clrUSB+0, 0
;vKBD.c,27 :: 		delay_ms(200);
	MOVLW       25
	MOVWF       R11, 0
	MOVLW       90
	MOVWF       R12, 0
	MOVLW       177
	MOVWF       R13, 0
L_main8:
	DECFSZ      R13, 1, 1
	BRA         L_main8
	DECFSZ      R12, 1, 1
	BRA         L_main8
	DECFSZ      R11, 1, 1
	BRA         L_main8
	NOP
	NOP
;vKBD.c,22 :: 		for (i=0; i < strlen(text); i++)
	INFSNZ      _i+0, 1 
	INCF        _i+1, 1 
;vKBD.c,28 :: 		}
	GOTO        L_main3
L_main4:
;vKBD.c,29 :: 		}
L_end_main:
	GOTO        $+0
; end of _main
