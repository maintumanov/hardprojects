
_main:

;UART.c,2 :: 		void main()
;UART.c,4 :: 		UART1_Init(9600);
	BSF         BAUDCON+0, 3, 0
	MOVLW       9
	MOVWF       SPBRGH+0 
	MOVLW       195
	MOVWF       SPBRG+0 
	BSF         TXSTA+0, 2, 0
	CALL        _UART1_Init+0, 0
;UART.c,5 :: 		UART1_Write_Text("Hello Habrahabr");
	MOVLW       ?ICS?lstr1_UART+0
	MOVWF       TBLPTRL 
	MOVLW       hi_addr(?ICS?lstr1_UART+0)
	MOVWF       TBLPTRH 
	MOVLW       higher_addr(?ICS?lstr1_UART+0)
	MOVWF       TBLPTRU 
	MOVLW       ?lstr1_UART+0
	MOVWF       FSR1 
	MOVLW       hi_addr(?lstr1_UART+0)
	MOVWF       FSR1H 
	MOVLW       16
	MOVWF       R0 
	MOVLW       1
	MOVWF       R1 
	CALL        ___CC2DW+0, 0
	MOVLW       ?lstr1_UART+0
	MOVWF       FARG_UART1_Write_Text_uart_text+0 
	MOVLW       hi_addr(?lstr1_UART+0)
	MOVWF       FARG_UART1_Write_Text_uart_text+1 
	CALL        _UART1_Write_Text+0, 0
;UART.c,6 :: 		UART1_Write(10);
	MOVLW       10
	MOVWF       FARG_UART1_Write_data_+0 
	CALL        _UART1_Write+0, 0
;UART.c,7 :: 		UART1_Write(13);
	MOVLW       13
	MOVWF       FARG_UART1_Write_data_+0 
	CALL        _UART1_Write+0, 0
;UART.c,8 :: 		for(;;)
L_main0:
;UART.c,10 :: 		if(UART1_Data_Ready())
	CALL        _UART1_Data_Ready+0, 0
	MOVF        R0, 1 
	BTFSC       STATUS+0, 2 
	GOTO        L_main3
;UART.c,12 :: 		uart_rd = UART1_Read( );
	CALL        _UART1_Read+0, 0
	MOVF        R0, 0 
	MOVWF       _uart_rd+0 
;UART.c,13 :: 		switch(uart_rd)
	GOTO        L_main4
;UART.c,15 :: 		case 0xD:
L_main6:
;UART.c,16 :: 		UART1_Write(10);
	MOVLW       10
	MOVWF       FARG_UART1_Write_data_+0 
	CALL        _UART1_Write+0, 0
;UART.c,17 :: 		UART1_Write(13);
	MOVLW       13
	MOVWF       FARG_UART1_Write_data_+0 
	CALL        _UART1_Write+0, 0
;UART.c,18 :: 		break;
	GOTO        L_main5
;UART.c,19 :: 		default:
L_main7:
;UART.c,20 :: 		UART1_Write(uart_rd);
	MOVF        _uart_rd+0, 0 
	MOVWF       FARG_UART1_Write_data_+0 
	CALL        _UART1_Write+0, 0
;UART.c,21 :: 		break;
	GOTO        L_main5
;UART.c,22 :: 		}
L_main4:
	MOVF        _uart_rd+0, 0 
	XORLW       13
	BTFSC       STATUS+0, 2 
	GOTO        L_main6
	GOTO        L_main7
L_main5:
;UART.c,23 :: 		}
L_main3:
;UART.c,24 :: 		}
	GOTO        L_main0
;UART.c,25 :: 		}
L_end_main:
	GOTO        $+0
; end of _main
