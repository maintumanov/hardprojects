#include "isrindicator.h"

ISRIndicator::ISRIndicator(QWidget *parent) :
    QWidget(parent)
{

    Led = false;
    raw = 13000;
    avg = 15000;
    trip = 2000;
    MaxTrip = 3000;
    Pushed = false;
    OnLed = false;
    gestiresis = 64;
    optTrip = 1500;
    Caption = "Chanel 0";
}

ISRIndicator::~ISRIndicator()
{

}

void ISRIndicator::SetTrip(quint16 vol)
{
    trip = vol;
    repaint();
}

void ISRIndicator::SetAvg(quint16 vol)
{
    avg = vol;
    repaint();
}

void ISRIndicator::SetRaw(quint16 vol)
{
    raw = vol;
    repaint();
}

void ISRIndicator::paintEvent(QPaintEvent *pe)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    qreal indWidth = this->width();
    if (indWidth > 64) indWidth = 64;
    qreal mWidth = indWidth - 14;

    qreal spacing = 4;
    qreal fontHeight = 10;

    qreal ledTop = fontHeight + spacing * 2;
    qreal ledHeight = 20;
    qreal ledWidth = indWidth - spacing * 2;

    qreal progressWidth = (mWidth - spacing * 2) / 2;
    qreal progressLeft = (mWidth - progressWidth) / 2;
    qreal progressBottom = this->height() - spacing - fontHeight * 4;

    qreal progressTop = spacing + ledHeight + ledTop;
    qreal graphHeight = (progressBottom - progressTop);
    qreal scale = graphHeight / 65535;
    qreal progressHeight =  raw * scale;

    if  (Pushed)
    {
        trip = (progressBottom - cur + 3) / (graphHeight / MaxTrip);
        if (trip > MaxTrip) trip = MaxTrip;
        if (cur - 3 > progressBottom) trip = 0;
    }

    qreal progTripHeight =  trip * (graphHeight / MaxTrip);

    qreal ticWidth = progressLeft - spacing - 2;
    qreal ticLeft = spacing;
    qreal avgTop = progressBottom - avg * scale;

    qreal tic2Width = progressLeft - spacing - 2;
    qreal tic2Rigth = mWidth - spacing;
    qreal mtripTop = progressBottom - (avg - trip) * scale;

    qreal tripProgrWidth = 10;
    qreal tripProgrContrHeight = 6;


  //gray Frame and background
    painter.setBrush(QBrush(QColor(210, 210, 210), Qt::SolidPattern));
    painter.setPen(QPen(QColor(100, 100, 100), 2, Qt::SolidLine));
    painter.drawRect(QRect(1, 1, indWidth - 2, this->height()-2));

 //progress
    painter.setBrush(QBrush(QColor(100, 100, 220), Qt::SolidPattern));
    painter.setPen(QPen(QColor(30, 30, 100), 1, Qt::SolidLine));
    painter.drawRect(QRectF(progressLeft, progressBottom-progressHeight , progressWidth, progressHeight));
    painter.drawText(spacing, this->height() - spacing - fontHeight, QString("raw:%1").arg(raw));

 //avg tic
    painter.setBrush(QBrush(QColor(100, 220, 220), Qt::SolidPattern));
    painter.setPen(QPen(QColor(30, 100, 100), 1, Qt::SolidLine));
    const QPointF points1[3] = {
         QPointF(ticLeft, avgTop - 3),
         QPointF(ticLeft+ticWidth, avgTop),
         QPointF(ticLeft, avgTop + 3),
     };
     painter.drawConvexPolygon(points1, 3);
     painter.drawLine(QPointF(ticLeft+ticWidth, avgTop), QPointF(tic2Rigth - tic2Width, avgTop));
     painter.drawText(spacing, this->height() - spacing - fontHeight * 2, QString("avg:%1").arg(avg));

 //trip tic
     painter.setBrush(QBrush(QColor(220, 220, 100), Qt::SolidPattern));
     painter.setPen(QPen(QColor(100, 100, 30), 1, Qt::SolidLine));
     const QPointF points2[3] = {
          QPointF(tic2Rigth, mtripTop - 3),
          QPointF(tic2Rigth - tic2Width, mtripTop),
          QPointF(tic2Rigth, mtripTop + 3),
     };
     painter.drawConvexPolygon(points2, 3);
//     painter.drawRect(QRectF(tic2Rigth - tic2Width, mtripTop - gestiresis , ticLeft + ticWidth, mtripTop));
     painter.drawLine(QPointF(tic2Rigth - tic2Width, mtripTop), QPointF(ticLeft + ticWidth, mtripTop));
     painter.drawText(spacing, this->height() - spacing - fontHeight * 3, QString("trip:%1").arg(trip));

 // Led
     if (raw < (avg - trip)) OnLed = true;
     if (raw > (avg - trip + gestiresis)) OnLed = false;

     if (OnLed)
     {
        painter.setBrush(QBrush(QColor(220, 20, 20), Qt::SolidPattern));
        painter.setPen(QPen(QColor(150, 70, 70), 1, Qt::SolidLine));
     } else
     {
         painter.setBrush(QBrush(QColor(20, 220, 20), Qt::SolidPattern));
         painter.setPen(QPen(QColor(70, 150, 70), 1, Qt::SolidLine));
     }
     painter.drawRect(QRectF(spacing, ledTop, ledWidth, ledHeight));

 //progressTrip
    painter.setBrush(QBrush(QColor(220, 220, 100), Qt::SolidPattern));
    painter.setPen(QPen(QColor(100, 100, 30), 1, Qt::SolidLine));
    painter.drawRect(QRectF(indWidth - spacing - tripProgrWidth, progressBottom - progTripHeight , tripProgrWidth, progTripHeight));
    painter.drawRect(QRectF(indWidth - spacing - tripProgrWidth, progressBottom - progTripHeight , tripProgrWidth, tripProgrContrHeight));
    rtTrip = QRect(indWidth - spacing - tripProgrWidth, progressBottom - progTripHeight , tripProgrWidth, tripProgrContrHeight);

  //Label
   // painter.setBrush(QBrush(QColor(210, 210, 210), Qt::SolidPattern));
    painter.setPen(QPen(QColor(100, 100, 100), 2, Qt::SolidLine));
    painter.drawText(spacing, spacing + fontHeight, Caption);
  //Label recomendet
     // painter.setBrush(QBrush(QColor(210, 210, 210), Qt::SolidPattern));
    painter.setPen(QPen(QColor(100, 100, 100), 2, Qt::SolidLine));
    painter.drawText(spacing, this->height() - spacing, QString("Otrip:%1").arg(optTrip));

}

void ISRIndicator::mousePressEvent(QMouseEvent *mouseEvent)
{
    if ( mouseEvent->button() == Qt::LeftButton )
        if (mouseEvent->x() > rtTrip.left() && mouseEvent->x() < rtTrip.right() &&
              mouseEvent->y() > rtTrip.top() && mouseEvent->y() < rtTrip.bottom())
        {
            Pushed = true;
            cur = mouseEvent->y();
        } else
        {
            Pushed = false;

        }
}

void ISRIndicator::mouseMoveEvent(QMouseEvent *mouseEvent)
{
    if (Pushed == false) return;
    cur = mouseEvent->y();
    SendTrip(trip);
    SentTripToChanel(ChannelTrip, trip);
    this->repaint();
}
