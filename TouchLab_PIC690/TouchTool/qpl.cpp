#include "qpl.h"


QPL::QPL(QObject *parent) :
    QRS232(parent)
{
    NewMsg.AnalysisState = 0;
    SNSpeeds << QLatin1String("1200") << QLatin1String("2400") << QLatin1String("9600") << QLatin1String("19200")
             << QLatin1String("57600") << QLatin1String("115200");
    SetSpeedList(&SNSpeeds);
    connect(this,SIGNAL(OnRecive(quint16)),this,SLOT(AnalisisCicle()));
}

void QPL::AnalisisCicle()
{
    while(!Queue.empty())
    {
        Analysis(Queue.dequeue());
    }
}

void QPL::Analysis(quint8 D)
{
    if (D == 254)
    {
        NewMsg.AnalysisState = 1;
        NewMsg.Msg.StartByte = D;
        NewMsg.acs = 0;
        return;
    }

    if (D == 255)  NewMsg.AnalysisState += 50;
    else
        if (NewMsg.AnalysisState >= 50)
        {
            NewMsg.AnalysisState -= 50;
            D+=128;
        }

    switch (NewMsg.AnalysisState)
    {
    case 1:
        NewMsg.Msg.DataSize = D;
        NewMsg.acs ^= D;
        NewMsg.Msg.Data.clear();
        NewMsg.AnalysisState = 2;
        break;

    case 2:
        NewMsg.Msg.Data.append(D);
        NewMsg.acs ^= D;
        if (NewMsg.Msg.Data.count() == NewMsg.Msg.DataSize) NewMsg.AnalysisState = 3;
        break;

    case 3:
        NewMsg.Msg.ACS = D;
        NewMsg.AnalysisState = 0;
        if (NewMsg.acs == NewMsg.Msg.ACS)  PacketAnalysis(NewMsg.Msg);
        break;
    }
}

void QPL::PacketAnalysis(QSNmessage Msg)
{
    QVector<quint8> Data = Msg.Data;
    quint8 Channel = Data[0];
    Data.remove(0);
    ReciveData(Channel, Data);
}

void QPL::ENetActionReadMemory(quint16 Sender, QVector<quint8> *Data)
{
//    if (Data->count() < 5) return;
//    quint16 StartAddr = MsgDataToInt(Data,1);
//    quint16 Count = MsgDataToInt(Data,3);
//    QSNmessage M;
//    M.Address=Sender;
//    M.Sender=DeviceAddress;
//    M.Data.append(10);
//    MsgIntToData(&M.Data,1,StartAddr);
//    for (quint16 i = 0; i < Count; i ++ )
//        M.Data.append(Memory[i + StartAddr]);
//    ERecivePack(M);
}

void QPL::ENetActionWriteMemory(quint16 Sender, QVector<quint8> *Data)
{
//    if (Data->count() < 6) return;
//    quint16 StartAddr = MsgDataToInt(Data,1);
//    quint16 Count = MsgDataToInt(Data,3);
//    for (quint16 i = 0; i < Count; i ++ )
//       Memory[i + StartAddr] = Data->at(i + 5);
//    QSNmessage M;
//    M.Address=Sender;
//    M.Sender=DeviceAddress;
//    M.Data.append(8);
//    ERecivePack(M);
}

quint16 QPL::MsgDataToInt(QVector<quint8> *Data, quint16 i)
{
    quint16 ret;
    ret = 0;
    if (Data->count() <= i+1) return(ret);
    ret = Data->value(i) << 8;
    ret += Data->value(i+1);
    return(ret);
}

quint16 QPL::GetInt16FromData(QVector<quint8> *Data)
{
    quint16 ret;
    ret = 0;
    ret = Data->value(0) << 8;
    ret += Data->value(1);
    return(ret);
}

quint16 QPL::GetInt8FromData(QVector<quint8> *Data)
{
    return(Data->value(0));
}

void QPL::SaveSettings(QSettings *settings)
{
    settings->setValue(QLatin1String("UARTport"), GetPort());
    settings->setValue(QLatin1String("UARTspeed"), GetSpeed());
}

void QPL::LoadSettings(QSettings *settings)
{
    SetPort(settings->value(QLatin1String("UARTport"), GetPort()).toString());
    SetSpeed(settings->value(QLatin1String("UARTspeed"), GetSpeed()).toString());
}


void QPL::USendStart()
{
    SendByte(254);
}

void QPL::USendByte(quint8 D, quint8 *ACS)
{
    if (D > 253)
    {
        SendByte(255);
        SendByte(D - 128);
    }
    else
        SendByte(D);
    *ACS ^= D;
}

void QPL::USendData(QVector<quint8> *Data, quint8 *ACS)
{
    if (Data->count() == 0) return;
    USendByte(Data->count(), ACS);
    for(quint16 i = 0; i < Data->count(); i++)
        USendByte(Data->value(i), ACS);
    USendByte(*ACS, ACS);
}

void QPL::USendInt16(quint16 D, quint8 *ACS)
{
    USendByte(D >> 8, ACS);
    USendByte(D & 255, ACS);
}

void QPL::SendMSG(QSNmessage Msg)
{
    if (!IsConnected()) return;
    quint8 acs;
    acs = 0;
    USendStart();
    USendData(&Msg.Data, &acs);
}

void QPL::SendData(quint8 Channel, QVector<quint8> Data)
{
    if (!IsConnected()) return;
    quint8 acs = 0;
    QVector<quint8> tData = Data;
    tData.prepend(Channel);
    USendStart();
    USendData(&tData, &acs);
}

void QPL::SendInt8(quint8 Channel, quint8 vol)
{
   QVector<quint8> Data;
   Data.append(vol);
   SendData(Channel,Data);
}

void QPL::SendInt16(quint8 Channel, quint16 vol)
{
    QVector<quint8> Data;

    Data.append(vol >> 8);
    Data.append(vol & 255);
    SendData(Channel,Data);
}

void QPL::MsgIntToData(QVector<quint8> *Data, quint16 i, quint16 D)
{
    if (Data->count() <= i+1) Data->resize(i+2);
    Data->replace(i,D >> 8);
    Data->replace(i + 1,D & 255);
}

void QPL::ciSetState(bool connected)
{
    if (connected) OpenPort();
    else ClosePort();
}


