#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    PL = new QPL(this);
    PL->SetEnabled(true);
    ui->combo_port->addItems(*PL->GetPortList());
    ui->combo_speed->addItems(*PL->GetSpeedList());

    connect(PL,SIGNAL(StateChange(bool)),this,SLOT(StateConnectChange(bool)));
    connect(ui->combo_port,SIGNAL(currentIndexChanged(int)),PL,SLOT(SetPort(int)));
    connect(ui->combo_speed,SIGNAL(currentIndexChanged(int)),PL,SLOT(SetSpeed(int)));
    connect(PL,SIGNAL(ReciveData(quint8,QVector<quint8>)),this,SLOT(ReciveData(quint8,QVector<quint8>)));
//    connect(ui->spinAver,SIGNAL(valueChanged(int)),ui->AverSlider,SLOT(setValue(int)));
//    connect(ui->AverSlider,SIGNAL(valueChanged(int)),ui->spinAver,SLOT(setValue(int)));

    IIC0 = new ISRIndicator(this);
    IIC1 = new ISRIndicator(this);
    IIC2 = new ISRIndicator(this);
    IIC3 = new ISRIndicator(this);

    ui->horizontalLayout_8->addWidget(IIC0);
    ui->horizontalLayout_8->addWidget(IIC1);
    ui->horizontalLayout_8->addWidget(IIC2);
    ui->horizontalLayout_8->addWidget(IIC3);

    IIC0->ChannelTrip = 0;
    IIC1->ChannelTrip = 1;
    IIC2->ChannelTrip = 2;
    IIC3->ChannelTrip = 3;

    connect(IIC0,SIGNAL(SentTripToChanel(quint8,quint16)),PL,SLOT(SendInt16(quint8,quint16)));
    connect(IIC1,SIGNAL(SentTripToChanel(quint8,quint16)),PL,SLOT(SendInt16(quint8,quint16)));
    connect(IIC2,SIGNAL(SentTripToChanel(quint8,quint16)),PL,SLOT(SendInt16(quint8,quint16)));
    connect(IIC3,SIGNAL(SentTripToChanel(quint8,quint16)),PL,SLOT(SendInt16(quint8,quint16)));

    PL->SetPort(0);
    PL->SetSpeed(4);
    ui->combo_speed->setCurrentIndex(4);
    ui->combo_port->setCurrentIndex(0);
    ui->stackedWidget->setCurrentWidget(ui->page);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    PL->OpenPort();
}

void MainWindow::StateConnectChange(bool state)
{

    if (state && ui->stackedWidget->currentWidget() == ui->page) ui->stackedWidget->setCurrentWidget(ui->page_2);
    if (!state) ui->stackedWidget->setCurrentWidget(ui->page);
}

void MainWindow::ReciveData(quint8 Channel, QVector<quint8> Data)
{
    switch (Channel)
    {
        case 0:
            IIC0->SetRaw(PL->GetInt16FromData(&Data));
            break;
        case 1:
            IIC1->SetRaw(PL->GetInt16FromData(&Data));
            break;
        case 2:
            IIC2->SetRaw(PL->GetInt16FromData(&Data));
            break;
        case 3:
            IIC3->SetRaw(PL->GetInt16FromData(&Data));
            break;
        case 4:
            IIC0->SetAvg(PL->GetInt16FromData(&Data));
            break;
        case 5:
            IIC1->SetAvg(PL->GetInt16FromData(&Data));
            break;
        case 6:
            IIC2->SetAvg(PL->GetInt16FromData(&Data));
            break;
        case 7:
            IIC3->SetAvg(PL->GetInt16FromData(&Data));
            break;
        case 8:
            IIC0->SetTrip(PL->GetInt16FromData(&Data));
            break;
        case 9:
            IIC1->SetTrip(PL->GetInt16FromData(&Data));
            break;
        case 10:
            IIC2->SetTrip(PL->GetInt16FromData(&Data));
            break;
        case 11:
            IIC3->SetTrip(PL->GetInt16FromData(&Data));
            break;
    }
}


//void MainWindow::on_AverSlider_valueChanged(int value)
//{
//    PL->SendInt16(1, value);
//}

//void MainWindow::on_Timer1Slider_valueChanged(int value)
//{
//    PL->SendInt8(11, value);
//}

//void MainWindow::on_Timer0Slider_valueChanged(int value)
//{
//    PL->SendInt8(10, value);
//}

//void MainWindow::on_ChanelSlider_valueChanged(int value)
//{
//    PL->SendInt8(12, value);
//}

//void MainWindow::on_checkBortB6_clicked()
//{
//    if (ui->checkBortB6->isChecked()) PL->SendInt8(100, 1);
//    else PL->SendInt8(100, 0);
//}

