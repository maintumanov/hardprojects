#ifndef QPL_H
#define QPL_H

#include <QObject>
#include <QtCore>
#include "qrs232.h"
#include <QDebug>


struct QSNmessage
{
    quint8 DataSize;
    quint8 ACS;
    quint8 StartByte;
    QVector<quint8> Data;
};

struct QRMsg
{
    quint16 AnalysisState;
    quint8 acs;
    QSNmessage Msg;
};

class QPL : public QRS232
{
    Q_OBJECT

private:
    QRMsg NewMsg;
    quint16  DeviceAddress;
    quint16  DeviceType;
    quint16  CountBroadSendMsg;
    quint16  CountTestSendMsg;
    QStringList SNSpeeds;
    QVector<quint8> Memory;
    void Analysis(quint8 D);
    void PacketAnalysis(QSNmessage Msg);
    void USendStart();
    void USendByte(quint8 D, quint8 *ACS);
    void USendInt16(quint16 D, quint8 *ACS);
    void USendData(QVector<quint8> *Data, quint8 *ACS);
    void SendMSG(QSNmessage Msg);
    void ERecivePack(QSNmessage Msg);
    void EReciveSignal(QSNmessage Msg);
    bool EPacketHandling(quint16 Sender, QVector<quint8> *Data);
    void ENetActionReadMemory(quint16 Sender, QVector<quint8> *Data);
    void ENetActionWriteMemory(quint16 Sender, QVector<quint8> *Data);

public:
    explicit QPL(QObject *parent = 0);
    void MsgIntToData(QVector<quint8> *Data, quint16 i, quint16 D);
    quint16 MsgDataToInt(QVector<quint8> *Data, quint16 i);

    quint16 GetInt16FromData(QVector<quint8> *Data);
    quint16 GetInt8FromData(QVector<quint8> *Data);
    void SaveSettings(QSettings *settings);
    void LoadSettings(QSettings *settings);
    quint16 GetAddress();
    void SetAddress(quint16 address);
    quint16 GetType();
    void SetType(quint16 type);
    quint16 GetMemorySize();
    void SetMemorySize(quint16 size);


signals:
    //---- BM
    void ReciveData(quint8 Channel, QVector<quint8> Data);
//    void iReciveSYScmd(quint8 NetCmd, quint16 arg1, quint16 arg2);
    //-------
private slots:
    void AnalisisCicle();

public slots:
    //----BM
    void SendData(quint8 Channel, QVector<quint8> Data);
    void SendInt8(quint8 Channel, quint8 vol);
    void SendInt16(quint8 Channel, quint16 vol);
    void ciSetState(bool connected);

};

#endif // QSNUART_H
