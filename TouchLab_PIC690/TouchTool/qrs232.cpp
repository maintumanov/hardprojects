#include "qrs232.h"
#include "QDebug"
#include <unistd.h>

QRS232::QRS232(QObject *parent) :
    QObject(parent)
{
    QObject::connect(&PoolTimer, SIGNAL(timeout()), this, SLOT(PoolCheck()));
    Queue.clear();
    ConnectState = false;
    UpdatePortList();
    DefSpeedList();
    SetSpeed("");
    SetPort("");
}

void QRS232::OpenPort()
{
    if (!Enabled) return;
#ifdef __linux__   /* Linux */
    int error;
    // struct termios new_port_settings, old_port_settings[50];
    struct termios  config;
    C_port = open("/dev/"+Port.toAscii(), O_RDWR | O_NOCTTY | O_NDELAY);

    if(C_port==-1)
    {
        ErrorOpenPort(tr("Unable to open comport"));
        return;
    }

    error = tcgetattr(C_port, &config);

    if(error==-1)
    {
        close(C_port);
        ErrorOpenPort(tr("unable to read portsettings"));
        return;
    }

    config.c_iflag = IGNPAR;
    config.c_oflag = 0;
    config.c_lflag = 0;
    config.c_cflag = CS8 | CLOCAL | CREAD;
    config.c_cc[VMIN]  = 0;
    config.c_cc[VTIME] = 0;

    switch (Speed.toInt(0,10))
    {
    case 1200: if (cfsetispeed(&config, B1200) < 0 || cfsetospeed(&config, B1200) < 0) error =- 1; break;
    case 2400: if (cfsetispeed(&config, B2400) < 0 || cfsetospeed(&config, B2400) < 0) error =- 1; break;
    case 9600: if (cfsetispeed(&config, B9600) < 0 || cfsetospeed(&config, B9600) < 0) error =- 1; break;
    case 19200: if (cfsetispeed(&config, B19200) < 0 || cfsetospeed(&config, B19200) < 0) error =- 1; break;
    case 57600: if (cfsetispeed(&config, B57600) < 0 || cfsetospeed(&config, B57600) < 0) error =- 1; break;
    case 115200: if (cfsetispeed(&config, B115200) < 0 || cfsetospeed(&config, B115200) < 0) error =- 1; break;
    }

    error = tcsetattr(C_port, TCSAFLUSH, &config);
    if(error==-1)
    {
        close(C_port);
        ErrorOpenPort(tr("unable to adjust portsettings "));
        return;
    }

#else             /* windows */

    H_port = CreateFileA("\\\\.\\"+Port.toAscii(),GENERIC_READ|GENERIC_WRITE,0,NULL,OPEN_EXISTING,0,NULL);

    if(H_port==INVALID_HANDLE_VALUE)
    {
        ErrorOpenPort(tr("Unable to open comport"));
        return;
    }

    DCB port_settings;
    memset(&port_settings, 0, sizeof(port_settings));  /* clear the new struct  */
    port_settings.DCBlength = sizeof(port_settings);

    if(!BuildCommDCBA("baud="+Speed.toAscii()+" data=8 parity=N stop=1", &port_settings))
    {
        ErrorOpenPort(tr("Unable to set comport dcb settings"));
        CloseHandle(H_port);
        return;
    }

    if(!SetCommState(H_port, &port_settings))
    {
        ErrorOpenPort(tr("Unable to set comport cfg settings"));
        CloseHandle(H_port);
        return;
    }

    COMMTIMEOUTS Cptimeouts;

    Cptimeouts.ReadIntervalTimeout         = MAXDWORD;
    Cptimeouts.ReadTotalTimeoutMultiplier  = 0;
    Cptimeouts.ReadTotalTimeoutConstant    = 0;
    Cptimeouts.WriteTotalTimeoutMultiplier = 0;
    Cptimeouts.WriteTotalTimeoutConstant   = 0;

    if(!SetCommTimeouts(H_port, &Cptimeouts))
    {
        ErNotify(this, QString(tr("Unable to set comport time-out settings")));
        CloseHandle(H_port);
        return;
    }
#endif
    PoolTimer.start(10);
    ConnectState = true;
    StateChange(true);
    OnConnect();
}

void QRS232::ClosePort()
{
    PoolTimer.stop();
#ifdef __linux__   /* Linux */
    close(C_port);
    tcsetattr(C_port, TCSANOW, old_port_settings);
#else             /* windows */
    CloseHandle(H_port);
#endif
    ConnectState = false;
    StateChange(false);
    OnDisconnect();
}

quint16 QRS232::PollComport()
{
    quint16 n;
#ifdef __linux__/* Linux */
    n = read(C_port, buffer, 4096);
#else          /* windows */
    ReadFile(H_port, buffer, 4096, (LPDWORD)((void *)&n), NULL);
#endif
    return(n);
}

quint16 QRS232::SendByte(quint8 byte)
{
    if (!ConnectState) return(0);
    quint16 n;
#ifdef __linux__   /* Linux */
    n = write(C_port, &byte, 1);
#else             /* windows */
    WriteFile(H_port, &byte, 1, (LPDWORD)((void *)&n), NULL);
#endif
    if(n<0)  return(1);
    return(0);
}

quint16 QRS232::SendBuf(quint8 *buff, quint16 size)
{
    if (!ConnectState) return(-1);
#ifdef __linux__   /* Linux */
    return(write(C_port, buff, size));
#else             /* windows */
    int n;
    if(WriteFile(H_port, buff, size, (LPDWORD)((void *)&n), NULL))
    {
        return(n);
    }
    return(-1);
#endif

}

void QRS232::SendString(QString str)
{
    //    quint8 * buff;
    //    (char)*buff=str.toAscii().data();
    //    SendBuf(buff,str.size());
}

bool QRS232::IsCTSEnabled()
{
    int status;
    if (!ConnectState) return(0);
#ifdef __linux__   /* Linux */
    /*
    Constant  Description
    TIOCM_LE  DSR (data set ready/line enable)
    TIOCM_DTR DTR (data terminal ready)
    TIOCM_RTS RTS (request to send)
    TIOCM_ST  Secondary TXD (transmit)
    TIOCM_SR  Secondary RXD (receive)
    TIOCM_CTS CTS (clear to send)
    TIOCM_CAR DCD (data carrier detect)
    TIOCM_CD  Synonym for TIOCM_CAR
    TIOCM_RNG RNG (ring)
    TIOCM_RI  Synonym for TIOCM_RNG
    TIOCM_DSR DSR (data set ready)
    */
    status = ioctl(C_port, TIOCMGET, &status);
    if(status&TIOCM_CTS) return(true);
    else return(false);
#else             /* windows */
    GetCommModemStatus(H_port, (LPDWORD)((void *)&status));
    if(status&MS_CTS_ON) return(1);
    else return(0);
#endif
}

void QRS232::PoolCheck()
{
    quint16 count;
    count = PollComport();
    if (count == 0) return;
    for (quint16 i=0; i<count; i++) Queue.enqueue(buffer[i]);
    OnRecive(count);
}

QString QRS232::GetStrFromQueue()
{
    QString str = "";
    while (!Queue.empty())  str+=Queue.dequeue();
    return (str);
}

void QRS232::UpdatePortList()
{
    PortList.clear();
#ifdef __linux__   /* Linux */
    QDir dr("/dev");
    QString msk = "ttyS* ttyUSB* rfcomm* ttyACM";
    PortList = dr.entryList(msk.split(" "), QDir::System | QDir::Files);

    //res.append("ttyUSB0");
    //ttyUSB0
#else             /* windows */
    HKEY hKey;
    int i=0;
    TCHAR Name[4096];
    TCHAR Value[4096];
    DWORD cName=4096;
    DWORD cValue=4096;
    if( RegOpenKey(HKEY_LOCAL_MACHINE,L"HARDWARE\\DEVICEMAP\\SERIALCOMM",&hKey) == ERROR_SUCCESS)
    {
        while(RegEnumValue(hKey,i,Name,&(cName = 4096),NULL,NULL,(LPBYTE)&Value,&(cValue = 4096))==ERROR_SUCCESS)
        {
            PortList.append(QString::fromUtf16((ushort*)Value));
            i++;
        }
    }
#endif
    PortChange(Port);
}

void QRS232::DefSpeedList()
{
    SpeedList.clear();
    SpeedList<<"1200"<<"2400"<<"4800"<<"9600"<<"19200"<<"57600"<<"115200";
    SpeedChange(Speed);
}

void QRS232::SetPort(QString sPort)
{
    if (Port == sPort) return;
    Port = sPort;
    PortChange(Port);
}

void QRS232::SetSpeed(QString sSpeed)
{
    if (Speed == sSpeed) return;
    Speed = sSpeed;
    SpeedChange(Speed);
}

QString QRS232::GetPort()
{
    return Port;
}

QString QRS232::GetSpeed()
{
    return Speed;
}

int  QRS232::GetIndexPort(QString sPort)
{
    if (PortList.count() == 0) return(-1);
    int iPort = -1;
    for(int i=0; i<PortList.count(); i++)
        if(sPort == PortList[i]) iPort = i;
    return(iPort);
}

int  QRS232::GetIndexSpeed(QString sSpeed)
{
    if (SpeedList.count() == 0) return(-1);
    int iSpeed = -1;
    for(int i=0; i<SpeedList.count(); i++)
        if(sSpeed == SpeedList[i]) iSpeed = i;
    return(iSpeed);
}

void QRS232::SetPort(int index)
{
    QString sPort = "";
    if (PortList.count() > index && index > -1) sPort = PortList[index];
    SetPort(sPort);
}

void QRS232::SetSpeed(int index)
{
    QString sSpeed = "";
    if (SpeedList.count() > index && index > -1) sSpeed = SpeedList[index];
    SetSpeed(sSpeed);
}

QStringList * QRS232::GetPortList()
{
    return(&PortList);
}

QStringList * QRS232::GetSpeedList()
{
    return(&SpeedList);
}

bool QRS232::IsConnected()
{
    return(ConnectState);
}

void QRS232::SetSpeedList(QStringList * SpList)
{
    SpeedList = * SpList;
}

void QRS232::SetEnabled(bool enable)
{
    Enabled = enable;
}

bool QRS232::IsEnabled()
{
    return  Enabled;
}

//============================================================================================
