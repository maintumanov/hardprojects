#ifndef QRS232_H
#define QRS232_H

#ifdef __linux__

#include <termios.h>
#include <sys/ioctl.h>
//#include <unistd.h>
#include <fcntl.h>
//#include <sys/types.h>
//#include <sys/stat.h>
//#include <limits.h>

#else

#include <windows.h>

#endif

#include <QObject>
#include <QStringList>
#include <QTimer>
#include <QDebug>
#include <QQueue>
#include <QDir>


class QRS232 : public QObject
{
    Q_OBJECT

private:

#ifdef __linux__   /* Linux */
    int C_port;
    struct termios new_port_settings, old_port_settings[50];
#else             /* windows */
    HANDLE H_port;
#endif
    QTimer  PoolTimer;
    QString Port;
    QString Speed;
    QStringList PortList;
    QStringList SpeedList;
    bool ConnectState;
    bool Enabled;
    quint8  buffer[2048];
public:
    explicit QRS232(QObject *parent = 0);

    QString GetStrFromQueue();
    bool IsCTSEnabled();
    QString GetPort();
    QString GetSpeed();
    int  GetIndexPort(QString sPort);
    int  GetIndexSpeed(QString sSpeed);
    QStringList * GetPortList();
    QStringList * GetSpeedList();
    bool IsConnected();
    void SetSpeedList(QStringList * SpList);
    void SetEnabled(bool enable);
    bool IsEnabled();

protected:
    quint16 PollComport();
    QQueue<quint8> Queue;

Q_SIGNALS:
    void OnConnect();
    void OnDisconnect();
    void StateChange(bool Connected);
    void PortChange(QString sPort);
    void SpeedChange(QString sSpeed);
    void ErrorOpenPort(QString Note);
    void OnRecive(quint16 count);
    void ErNotify(QObject *sender, QString text);

public Q_SLOTS:
    void OpenPort();
    void ClosePort();
    virtual void PoolCheck();
    quint16  SendByte(quint8 byte);
    quint16  SendBuf(quint8 *buff, quint16 size);
    void SendString(QString str);
    void SetPort(QString sPort);
    void SetPort(int index);
    void SetSpeed(QString sSpeed);
    void SetSpeed(int index);
    void UpdatePortList();
    void DefSpeedList();
};

#endif // QRS232_H
