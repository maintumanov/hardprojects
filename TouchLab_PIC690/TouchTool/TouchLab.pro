#-------------------------------------------------
#
# Project created by QtCreator 2012-09-20T21:21:01
#
#-------------------------------------------------

QT       += core gui

TARGET = TouchLab
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qrs232.cpp \
    qpl.cpp \
    isrindicator.cpp

HEADERS  += mainwindow.h \
    qrs232.h \
    qpl.h \
    isrindicator.h

FORMS    += mainwindow.ui
