#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "qpl.h"
#include "isrindicator.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QPL *PL;
    
private slots:
    void on_pushButton_clicked();

    void StateConnectChange(bool state);
    void ReciveData(quint8 Channel, QVector<quint8> Data);

private:
    Ui::MainWindow *ui;
    ISRIndicator *IIC0;
    ISRIndicator *IIC1;
    ISRIndicator *IIC2;
    ISRIndicator *IIC3;
};

#endif // MAINWINDOW_H
