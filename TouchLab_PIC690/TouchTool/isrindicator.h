#ifndef ISRINDICATOR_H
#define ISRINDICATOR_H

#include <QWidget>
#include <QtCore>
#include <QtGui>
#include <QRectF>
#include <QMouseEvent>

class ISRIndicator : public QWidget
{
    Q_OBJECT
    
public:
    explicit ISRIndicator(QWidget *parent = 0);
    ~ISRIndicator();
    bool Led;
    quint16 raw;
    quint16 avg;
    quint16 trip;
    quint16 MaxTrip;
    quint16 gestiresis;
    QString Caption;
    quint8 ChannelTrip;

    void SetTrip(quint16 vol);
    void SetAvg(quint16 vol);
    void SetRaw(quint16 vol);

signals:
    void SendTrip(quint16 vol);
    void SentTripToChanel(quint8 channel, quint16 vol);

protected:
    virtual void paintEvent(QPaintEvent* pe);
    void mousePressEvent( QMouseEvent *mouseEvent );
    void mouseMoveEvent( QMouseEvent *mouseEvent );
private:
    int cur;
    QRect rtTrip;
    bool Pushed;
    bool OnLed;
    quint16 optTrip;
};

#endif // ISRINDICATOR_H
