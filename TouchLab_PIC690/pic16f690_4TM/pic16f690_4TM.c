#include <pic16f690_4TM.h>
#include "PL.c"


void SetNextSensor();
void RestartTimers();
void CapISR();
void Averaging();
static int16 raw = 0;

static unsigned int8 index = 0;
static unsigned int8 AvgIndex = 0;
unsigned int16 avarage[4];
unsigned int16 trip[4];


#int_RTCC
void  RTCC_isr(void) 
{
	raw = get_timer1();
	CapISR();
}


void CapInit() 
{
	trip[0] = 400;
	trip[1] = 1200;
	trip[2] = 1500;
	trip[3] = 500;
	
	avarage[0] = 3000;
	avarage[1] = 3000;
	avarage[2] = 3000;
	avarage[3] = 3000;		
}

void CapISR()
{
	if (raw < (avarage[index] - trip[index]) ) {
		switch (index) {
			case 0: output_high(PIN_C5); break;
			case 1: output_high(PIN_C6); break;
			case 2: output_high(PIN_C7); break;
			case 3: output_high(PIN_B6); break;	
			}
	//	avarage[index] = avarage[index] + ((signed int32)raw - (signed int32)avarage[index] + trip[index]) / 4;
	} else if (raw > (avarage[index] - trip[index] + 64) ) {
		switch (index) {
			case 0: output_low(PIN_C5); break;
			case 1: output_low(PIN_C6); break;
			case 2: output_low(PIN_C7); break;
			case 3: output_low(PIN_B6); break;	
			}
		if (AvgIndex < 6) AvgIndex++;
		else AvgIndex = 0;
		if (AvgIndex == 5)
			avarage[index] = avarage[index] + ((signed int32)raw - (signed int32)avarage[index]) / 4;
	}
	PLSendInt16(index, raw);
	PLSendInt16(index + 4, avarage[index]);
	SetNextSensor();
	RestartTimers();
}

void SetNextSensor()
{
	index = (++index) & 0x03;
	switch (index)
    	{
	   case 0:
	    setup_comparator(CP1_A1_VR | CP1_INVERT | CP2_A1_C0 | CP2_OUT_ON_C4 | COMP_C1_LATCHED | COMP_T1_GATE);
	    break;
	   case 1:
	    setup_comparator(CP1_A1_VR | CP1_INVERT | CP2_C1_C0 | CP2_OUT_ON_C4 | COMP_C1_LATCHED | COMP_T1_GATE);
	    break;
	   case 2:
	    setup_comparator(CP1_A1_VR | CP1_INVERT | CP2_C2_C0 | CP2_OUT_ON_C4 | COMP_C1_LATCHED | COMP_T1_GATE);
	    break;
	   case 3:
	    setup_comparator(CP1_A1_VR | CP1_INVERT | CP2_C3_C0 | CP2_OUT_ON_C4 | COMP_C1_LATCHED | COMP_T1_GATE);
	    break;
	}			
}

void RestartTimers()
{
	set_timer1(0);
	SET_RTCC(0);	
}

void OnReadData(byte channel)
{
  switch (channel)
  {
	  
    case 0:
    	trip[0] = PLGetInt16();
    	break;
    case 1:
    	trip[1] = PLGetInt16();
    	break;
    case 2:
    	trip[2] = PLGetInt16();
    	break;
    case 3:
    	trip[3] = PLGetInt16();
    	break;    	
 	  
  }
}

void main()
{
   setup_oscillator(OSC_8MHZ);
   port_a_pullups(FALSE);
   SET_TRIS_A( 0xFF );
   SET_TRIS_B( 0x2F );
   SET_TRIS_C( 0x0F );
   setup_timer_0(RTCC_INTERNAL|RTCC_DIV_128|RTCC_8_bit);
   setup_timer_1(T1_EXTERNAL|T1_DIV_BY_2);      //32.7 ms overflow
   setup_comparator(CP1_A1_VR | CP1_INVERT | CP2_C1_C0 | CP2_OUT_ON_C4 | COMP_C1_LATCHED | COMP_T1_GATE);
   setup_vref(VREF_COMP1 | 0x02);
   SRCON = 0xF0;
    setup_adc(ADC_OFF);
   enable_interrupts(INT_RTCC);
   enable_interrupts(GLOBAL);
     
 PLOnReadData = OnReadData;
   PLInit();
 //  delay_ms(1000);
//   for (int i = 8; i < 12; i++)
//   PLSendInt16(i, trip[i]);
   
  // SET_TIMER1(0xFF00);
   output_low(PIN_C5);
   output_low(PIN_C6);
   output_low(PIN_C7);
   output_low(PIN_B6);
	CapInit();
   while (true)
   {
      PLCheckMess();
      rs232_errors = 0;

   }

}
