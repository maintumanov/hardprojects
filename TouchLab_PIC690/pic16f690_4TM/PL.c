/********************************************************
 *   PL - The driver of a ring network on base UART   *
 *                                                       *
 *     author Tumanov Stanislav Aleksandrovich,          *
 *     Russia, Kirov city, maintumanov@mail.ru           *
 *     #define PLPort - ���� RS232
 *********************************************************/
#include "PL.h"


#ifndef PLReciveBufferSize
  #define PLReciveBufferSize 44
#endif 

#ifndef PLUARTspeed
  #define PLUARTspeed 57600
#endif 

#ifndef PLDataBufferSize
  #define PLDataBufferSize 16
#endif 

#ifndef PLDefault_Mode
  #define PLDefault_Mode 0
#endif 

#ifndef PLDefaultAddress
  #define PLDefaultAddress 2
#endif 

#ifndef PLPort
  #define PLPort PLstream
#endif 

typedef void(*PLPOnReadData)(byte);




int1 PLavailable(void);
void PLInit();
void PLSendByte(byte Data);
void PLFProcessed(void);
int1 PLCheckACS(void);
void PLAnalysis(byte data);
void PLCheckMess(void);
void PLSendStart(void);
void PLSetNetSpeed(byte Speed);
byte PLRead(void);
void PLMesAnalys();
void PLSetAddress(int16 Addr);
void PLChangeSpeed(byte Speed);
void PLSendInt16(Int16 Data);
int8 PLIntToACS(int16 data);
void PLSendData(byte DSize, byte *Data);
void PLDataSend(byte DSize, byte *Data);
void PLSetMemory();
void PLGetMemory();
void ie_write_eeprom(byte addr, byte data);
void PLSendInt8(byte channel, byte Data);
void PLSendInt16(byte channel, int16 Data);
byte PLGetInt8();
int16 PLGetInt16();


//-----------------------------------------------------------------//
//            ������ ����������                            
//-----------------------------------------------------------------//


PLPOnReadData PLOnReadData;

//���������� ������
byte PLReadBuffer[PLReciveBufferSize];
byte PLBuffTail = 0;
byte PLBuffHead = 0;
byte PLBuffCount = 0;



//���������� ����� �������
byte PLPDataBuff[PLDataBufferSize];
byte PLPAnalysisState = 0;
byte PLPDataPos = 0;
byte PLPDataSize;
byte PLPDataACS;
byte PLPIncACS = 0;

//-----------------------------------------------------------------//
//            ������ �������                                      //
//-----------------------------------------------------------------//

void PLInit()
{
  set_uart_speed(PLUARTspeed);
  enable_interrupts(INT_RDA);
  enable_interrupts(GLOBAL);
}

//-------------------------------------------------------------------------

#int_RDA                     
void PLRDA_isr(void)
{
  do
  {
    PLReadBuffer[PLBuffTail] = fgetc(PLPort);
    PLBuffCount++;
    if (PLBuffTail++ == PLReciveBufferSize - 1)
      PLBuffTail = 0;
  }
  while (kbhit());
}

//-------------------------------------------------------------------------

void PLCheckMess(void)
{
  while (PLBuffCount > 0)
    PLAnalysis(PLRead());
    
}

//-------------------------------------------------------------------------

byte PLRead(void)
{
  byte res;
  res = PLReadBuffer[PLBuffHead];
  PLBuffCount--;
  if (PLBuffHead++ == PLReciveBufferSize - 1)
    PLBuffHead = 0;
  return (res);
}

//-------------------------------------------------------------------------

void PLAnalysis(byte data)
{
  if (data == 254)
  {
    PLPAnalysisState = 1;
    PLPIncACS = 0;
    return ;
  }

  if (data == 255)
    PLPAnalysisState += 50;
  else
  if (PLPAnalysisState >= 50)
  {
    PLPAnalysisState -= 50;
    data += 128;
  };
  switch (PLPAnalysisState)
  {

    case 1:
      PLPDataSize = data;
      PLPIncACS = PLPIncACS ^ data;
      PLPDataPos = 0;
      PLPAnalysisState = 2;
      break;
    case 2:
      if (PLDataBufferSize > PLPDataPos)
        PLPDataBuff[PLPDataPos] = data;
      PLPDataPos++;
      PLPIncACS = PLPIncACS ^ data;
      if (PLPDataPos == PLPDataSize)
        PLPAnalysisState = 3;
      break;
    case 3:
      PLPDataACS = data;
      if (PLCheckACS())
        PLMesAnalys();
      PLPAnalysisState = 0;
      break;
  }
}

//-------------------------------------------------------------------------

int1 PLCheckACS(void)
{
  return (PLPIncACS == PLPDataACS);
}

//-------------------------------------------------------------------------

void PLMesAnalys()
{
   if (PLPDataBuff[0] <= 200) {
   (*PLOnReadData)(PLPDataBuff[0]);
   return;
   }
   
   if (PLPDataBuff[0] == 201) PLSetMemory();
   if (PLPDataBuff[0] == 202) PLGetMemory();

}

//-------------------------------------------------------------------------
/*
void PLFProcessed(void)
{
  int16 tmp;
  switch (PLPDataBuff[0])
  {
    case 0:
      PLGetSignal();
      PLTransmit();
      break;
    case 1:
      PLQueueCmdProc();
      break;
    case 2:
      PLSetAddress(make16(PLPDataBuff[1], PLPDataBuff[2]));
      break;
    case 3:
      int8 D[5] = 
      {
        4, PLDeviceType, PLDeviceSubType, 0, 0
      }
      ;
      D[3] = PLDeviceMemorySize >> 8;
      D[4] = PLDeviceMemorySize &0xFF;
      PLSendData(PLPDataSender, 5, D);
      PLTransmit();
      break;
    case 5:
      if (PLCommandMode == 1)
        break;
      tmp = make16(PLPDataBuff[1], PLPDataBuff[2]);
      tmp++;
      PLPDataBuff[1] = tmp >> 8;
      PLPDataBuff[2] = tmp &0xFF;
      PLSetAddress(tmp);
      PLTransmit();
      break;
    case 6:
      PLTransmit();
      Delay_ms(100);
      PLChangeSpeed(PLPDataBuff[1]);
      break;
    case 7:
      PLSetMemory();
      break;
    case 9:
      PLGetMemory();
      break;
    default:
      if (PLPDataAdresse == 0)
        PLTransmit();
  }
}
*/

//-------------------------------------------------------------------------

void PLSendStart(void)
{
  fputc(254, PLPort);
}

//-------------------------------------------------------------------------

void PLSendByte(byte Data)
{
  if (Data < 254)
    fputc(Data, PLPort);
  else
  {
    fputc(255, PLPort);
    fputc(Data - 128, PLPort);
  }
  PLPIncACS = PLPIncACS ^ Data;
}

//-------------------------------------------------------------------------

void PLSendInt16(Int16 Data)
{
  PLSendByte(Data >> 8);
  PLSendByte(Data &0xFF);
}

//-------------------------------------------------------------------------

void PLDataSend(byte DSize, byte *Data)
{
  byte i = 0;
  PLSendByte(DSize);
  for (i = 0; i < DSize; i++)
    PLSendByte(Data[i]);
  PLSendByte(PLPIncACS);
}


//-------------------------------------------------------------------------�������� ������

void PLSendData(byte DSize, byte *Data)
{
  PLPIncACS = 0;
  PLSendStart();
  PLDataSend(DSize, Data);
}

//-------------------------------------------------------------------------

void PLSetMemory()
{
  byte n = 5;
  int16 a = 0;
  int16 c = 0;
  int16 i = 0;
  a = make16(PLPDataBuff[1], PLPDataBuff[2]);
  c = make16(PLPDataBuff[3], PLPDataBuff[4]);
  for (i = 0; i < c; i++)
    ie_write_eeprom(a++, PLPDataBuff[n++]);
  PLPDataBuff[0] = 8;
  PLSendData(1, PLPDataBuff);
}

//-------------------------------------------------------------------------

void PLGetMemory()
{
  int16 a = 0;
  int16 c = 0;
  int16 i;
  a = make16(PLPDataBuff[1], PLPDataBuff[2]);
  c = make16(PLPDataBuff[3], PLPDataBuff[4]);
  PLPDataBuff[0] = 10;
  if (PLDataBufferSize < c + 3)
    return ;
  for (i = 1; i <= c; i++)
    PLPDataBuff[i + 2] = read_eeprom(a++);
  PLSendData(c + 3, PLPDataBuff);
}

//-------------------------------------------------------------------------

void ie_write_eeprom(byte addr, byte data)
{
  #byte EEADR = 0x9B;
  #byte EEDATA = 0x9A;
  #bit WREN = 0x9C.2;
  #bit WR = 0x9C.1;
  #byte EECON2 = 0x9D;
  disable_interrupts(GLOBAL);
  EEADR = addr;
  EEDATA = data;
  WREN = 1;
  EECON2 = 0x55;
  EECON2 = 0xAA;
  WR = 1;
  enable_interrupts(GLOBAL);
  while (WR == 1);
  WREN = 0;
}

//-------------------------------------------------------------------------

void PLSendInt8(byte channel, byte Data)
{
  byte PLPTempBuff[2];
  PLPTempBuff[0] = channel;
  PLPTempBuff[1] = Data;
  PLSendData(2, PLPTempBuff);
}

void PLSendInt16(byte channel, int16 Data)
{
  byte PLPTempBuff[3];
  PLPTempBuff[0] = channel;
  PLPTempBuff[1] = (Data >> 8);
  PLPTempBuff[2] = (Data &0xFF);
  PLSendData(3, PLPTempBuff);
}

byte PLGetInt8()
{
  return PLPDataBuff[1];
}

int16 PLGetInt16()
{
  return make16(PLPDataBuff[1], PLPDataBuff[2]);
}

