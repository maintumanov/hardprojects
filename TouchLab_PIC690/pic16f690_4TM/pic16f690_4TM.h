#include <16F690.h>
#device *=16
#device adc=16
#FUSES NOWDT                    //No Watch Dog Timer
#FUSES INTRC_IO                 //Internal RC Osc, no CLKOUT
#FUSES NOBROWNOUT               //No brownout reset
#byte SRCON = 0x019E
#byte WPUA = 0x0095
#use delay(int=8000000)
#use rs232(baud=57600,parity=N,xmit=PIN_B7,rcv=PIN_B5,bits=8,stream=PLstream,ERRORS)

#define CIN0-   PIN_A1
#define MCLR   PIN_A3
#define RX232   PIN_B5
#define TX232   PIN_B7
#define C2IN+   PIN_C0
#define CIN1-   PIN_C1
#define CIN2-   PIN_C2
#define CIN3-   PIN_C3
#define Led1   PIN_B6
#define Led2   PIN_C5
#define Led3   PIN_C6
#define Led4   PIN_C7

#define DELAY 700




