#include <16F690.h>
#device *=16
#device adc=16

#FUSES NOWDT                    //No Watch Dog Timer
#FUSES INTRC_IO                 //Internal RC Osc, no CLKOUT
#FUSES NOBROWNOUT               //No brownout reset
#byte SRCON = 0x019E
#byte WPUA = 0x0095
#use delay(int=8000000)

