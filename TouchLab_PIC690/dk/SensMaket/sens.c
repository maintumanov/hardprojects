#include <sens.h>
#int_RTCC
void  RTCC_isr(void) 
{

}

#int_TIMER1
void  TIMER1_isr(void) 
{
   output_toggle(PIN_C7);
    SET_TIMER1(0xFF00);
}



void main()
{
   port_a_pullups(FALSE);
   SET_TRIS_A( 0xFF );
   SET_TRIS_B( 0x0F );
   SET_TRIS_C( 0x0F );
   setup_timer_1(T1_EXTERNAL|T1_DIV_BY_1);      //32.7 ms overflow
   setup_comparator(CP1_A1_VR | CP1_INVERT | CP2_A1_C0 | CP1_OUT_ON_A2 | CP2_OUT_ON_C4 | COMP_C1_LATCHED | COMP_T1_GATE);
  setup_vref(VREF_COMP1 | 0x0C);
   
   SRCON = 0xF0;
   
    setup_adc(ADC_OFF);
 //  setup_adc_ports(sAN1 | sAN4 | sAN5 | sAN6| sAN7);
 //  enable_interrupts(INT_RTCC);
   enable_interrupts(INT_TIMER1);
   enable_interrupts(GLOBAL);
   setup_oscillator(OSC_8MHZ);
   SET_TIMER1(0xFF00);
   while (true)
   {

   }

}
