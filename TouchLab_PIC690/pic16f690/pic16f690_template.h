#include <16F690.h>
#device *=16
#device adc=16

#FUSES NOWDT                    //No Watch Dog Timer
#FUSES INTRC                    //Internal RC Osc
#FUSES PUT                      //Power Up Timer
#FUSES NOBROWNOUT               //No brownout reset

#use delay(int=8000000)
#use rs232(baud=57600,parity=N,xmit=PIN_B7,rcv=PIN_B5,bits=8,stream=PLstream,ERRORS)

