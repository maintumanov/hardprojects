#include <pic16f690_template.h>
#include "PL.c"

#int_TIMER1
void  TIMER1_isr(void) 
{
  PLPDataBuff[0] = 1;
  PLPDataBuff[1] = 1;
  PLPDataBuff[2] = 1;
  PLPDataSize = 3;
  PLSendData(PLPDataSize, PLPDataBuff);
}


//-------------------------------------------------------------------------

void OnReadData(byte channel)
{
  switch (channel)
  {
    case 0:

      break;
  }
}

void init()
{
  setup_timer_0(RTCC_INTERNAL | RTCC_DIV_1);
  setup_timer_1(T1_INTERNAL | T1_DIV_BY_8);
  setup_timer_2(T2_DISABLED, 0, 1);
  setup_comparator(NC_NC_NC_NC);
  setup_vref(FALSE);
  PLInit();
  PLOnReadData = OnReadData;
  set_tris_b(0b00010000);
  set_tris_a(0b11111111);
  set_tris_c(0b00000000);
  enable_interrupts(INT_TIMER1);
  enable_interrupts(GLOBAL);
}


void main()
{

   setup_timer_1(T1_INTERNAL|T1_DIV_BY_8);      //262 ms overflow
   setup_comparator(NC_NC_NC_NC);// This device COMP currently not supported by the PICWizard
   enable_interrupts(INT_TIMER1);
   enable_interrupts(GLOBAL);

  Init();
  while (true)
  {
    PLCheckMess();
    rs232_errors = 0;
  }

}
